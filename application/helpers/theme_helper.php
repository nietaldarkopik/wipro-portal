<?php

function plugin_url($plugin_name = "")
{
	return base_url().'page_plugin/plugin_controller/'.$plugin_name;
}

function current_theme_url()
{
	return base_url().'themes/user/'.CURRENT_THEME.'/';
}

function current_admin_theme_url()
{
	return base_url().'themes/admin/'.CURRENT_ADMIN_THEME.'/';
}

function current_user_url()
{
  return str_replace('/index.php','',current_url());
}

function current_admin_url()
{
  return str_replace('/index.php','',current_url());
}

function current_main_url()
{
  $CI =& get_instance();
  $segments = $CI->uri->segment_array();
  $uri = array();
  foreach($segments as $i => $s)
  {
    if(!is_numeric($s))
      $uri[] = $s;
  }
  $uri = implode("/",$uri);
  return str_replace('/index.php','',base_url($uri));
}

function scandir_r($dir = "")
{
   
   $result = array(); 

   $cdir = scandir($dir); 
   foreach ($cdir as $key => $value) 
   { 
      if (!in_array($value,array(".",".."))) 
      { 
         if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) 
         { 
            $result[$value] = scandir_r($dir . DIRECTORY_SEPARATOR . $value); 
         } 
         else 
         { 
            $result[] = $value; 
         } 
      } 
   } 
   
   return $result; 
  
}

function get_class_methods2_r($controller_files = array(),$base_path = '')
{
  $controllers = array();
  foreach ($controller_files as $key => $filename)
  {
    if(is_array($filename) and count($filename) > 0)
    {
      $controllers[$key] = get_class_methods_r($filename,$base_path.$key.'/');
    }else{
      ob_start();

      require_once($base_path.$filename);

      $classname = ucfirst(substr($filename, 0, strrpos($filename, '.')));
      if(!class_exists($classname))
        continue;
      $controller = new $classname();
      $methods = get_class_methods($controller);

      foreach ($methods as $index => $method)
      {
        if((strpos($method,'_') <= 0 and strpos($method,'_') === 0) or $method == 'get_instance')
        {
          unset($methods[$index]);
        }
      }

      $controller_info = array(
          'filename' => $filename,
          'class_name' => $classname,
          'methods'  => $methods
      );
      array_push($controllers,$controller_info);
      ob_end_clean();
    }
  }
  return $controllers;
}

function get_class_methods_r($controller_files = array(),$base_path = '')
{
  $controllers = array();
  foreach ($controller_files as $key => $filename)
  {
    if(is_array($filename) and count($filename) > 0)
    {
      $controllers[$key] = get_class_methods_r($filename,$base_path.$key.'/');
    }else{
      
      $classname = ucfirst(substr($filename, 0, strrpos($filename, '.')));
      $arr = file($base_path.$filename);
      $methods = array();
      foreach ($arr as $line)
      {
          if (preg_match('/function ([_A-Za-z0-9]+)/', $line, $regs))
          {
              $methods[] = $regs[1];
          }
      }
      foreach ($methods as $index => $method)
      {
        if((strpos($method,'_') <= 0 and strpos($method,'_') === 0) or $method == 'get_instance')
        {
          unset($methods[$index]);
        }
      }
      $controller_info = array(
          'filename' => $filename,
          'class_name' => $classname,
          'methods'  => $methods
      );
      #array_push($controllers,$controller_info);
      $controllers[] = $controller_info;
    }
  }
  return $controllers;
}


function get_class_methods_r_path($controller_files = array(),$base_path = '',$data_controllers = array(),$path = '/')
{
  $controllers = array();
  $o_controllers = array();
  if(is_array($controller_files) and count($controller_files) > 0)
    $controllers = get_class_methods_r($controller_files,$base_path);
    
  if(is_array($data_controllers) and count($data_controllers) > 0)
    $controllers = $data_controllers;
  
  if(is_array($controllers) and count($controllers) > 0)
  {
    foreach($controllers as $i => $c)
    {
      if(isset($c['filename']) and !empty($c['filename']))
      {
        $o_controllers[] =  array(
                                    'path' => $path,
                                    'filename' => $c['filename'],
                                    'class_name' => $c['class_name'],
                                    'methods'  => $c['methods']
                                );
      }else{
        $tmp_o_controllers = get_class_methods_r_path(array(),$base_path.$i.'/',$c,$path.$i);
        foreach($tmp_o_controllers as $k => $con)
        {
          $o_controllers[] = $con;
        }
        #$o_controllers = $o_controllers + $tmp_o_controllers;
      }
    }
  }
  return $o_controllers;
}
