<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if (!function_exists('simple_sanitize')){
	function simple_sanitize($string){
		$string = str_replace('http://', '_', $string);
		$string = str_replace('/', '_', $string);
		$string = str_replace('.', '_', $string);
		return $string;
	}
}
if (!function_exists('rp')){
	function rp($price){
		$CI =& get_instance();
		$CI->load->library('skayla_price');
		return $CI->skayla_price->price_to_human($price);
	}
}

if (!function_exists('base64_url_encode')){
	function base64_url_encode($input) {
	 return strtr(base64_encode($input), '+/=', '-_,');
	}
}

if (!function_exists('array_to_object')){
	function array_to_object(array $arr, stdClass $parent = null) {
		if ($parent === null) {
				$parent = 'this';
		}

		foreach ($arr as $key => $val) {
				if (is_array($val)) {
						$parent->$key = array_to_object($val, new stdClass);
				} else {
						$parent->$key = $val;
				}
		}

		return $parent;
	}
}

if (!function_exists('array_keys_exists')){
	function array_keys_exists($keys, $array) {
		foreach($keys as $k) {
			if(isset($array[$k])) {
				return true;
			}
		}
		return false;
	}
}

if (!function_exists('in_array_recursive')){
	function in_array_recursive($needle, $haystack) {
		$it = new RecursiveIteratorIterator(new RecursiveArrayIterator($haystack));

		foreach($it AS $element) {
			if($element == $needle) {
				return true;
			}
		}

		return false;
	}
}

if (!function_exists('base64_url_decode')){
	function base64_url_decode($input) {
	 return base64_decode(strtr($input, '-_,', '+/='));
	}
}

if (!function_exists('date_format_mask')){
	function date_format_mask($date, $format) {
		$date = date_create($date);
		return date_format($date, $format);
	}
}

if (!function_exists('classified_search_uri')) {
	function classified_search_uri($type='semua-tipe', $prov='semua-provinsi', $city='semua-kota', $cat='semua-kategori', $sub_cat='semua-sub-kategori'){
		$new_slug = site_url('classified/s/'.$type.'/'.$prov.'/'.$city.'/'.$cat.'/'.$sub_cat.'/');

		return $new_slug;
		
	}
}

if (!function_exists('classified_search_segment')) {
	function classified_search_segment($slug, $segment){
		switch($segment){
			case 1:
			$new_slug = site_url('classified/s/'.$slug.'/semua-provinsi/semua-kota/semua-kategori/semua-sub-kategori');
			break;
			
			case 2:
			$new_slug = site_url('classified/s/semua-tipe/'.$slug.'/semua-kota/semua-kategori/semua-sub-kategori');
			break;
			
			case 3:
			$new_slug = site_url('classified/s/semua-tipe/semua-provinsi/'.$slug.'/semua-kategori/semua-sub-kategori');
			break;
			
			case 4:
			$new_slug = site_url('classified/s/semua-tipe/semua-provinsi/semua-kota/'.$slug.'/semua-sub-kategori');
			break;
			
			case 5:
			$new_slug = site_url('classified/s/semua-tipe/semua-provinsi/semua-kota/semua-kategori/'.$slug.'');
			break;
			
			case 6:
			$new_slug = site_url('classified/s/semua-tipe/semua-provinsi/semua-kota/semua-kategori/semua-sub-kategori/'.$slug);
			break;
		}
		
		return $new_slug;
		
	}
}

if (!function_exists('dump')) {
    function dump ($var, $label = 'Dump', $echo = TRUE)
    {
        // Store dump in variable 
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        
        // Add formatting
        $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
        $output = '<pre style="background: #FFFEEF; color: #000; border: 1px dotted #000; padding: 10px; margin: 10px 0; text-align: left;">' . $label . ' => ' . $output . '</pre>';
        
        // Output
        if ($echo == TRUE) {
            echo $output;
        }
        else {
            return $output;
        }
    }
}

if (!function_exists('dump_exit')) {
    function dump_exit($var, $label = 'Dump', $echo = TRUE) {
        dump ($var, $label, $echo);
        exit;
    }
}

if (!function_exists('is_serialized')) {
	function is_serialized( $data ) {
			// if it isn't a string, it isn't serialized
			if ( !is_string( $data ) )
					return false;
			$data = trim( $data );
			if ( 'N;' == $data )
					return true;
			if ( !preg_match( '/^([adObis]):/', $data, $badions ) )
					return false;
			switch ( $badions[1] ) {
					case 'a' :
					case 'O' :
					case 's' :
							if ( preg_match( "/^{$badions[1]}:[0-9]+:.*[;}]\$/s", $data ) )
									return true;
							break;
					case 'b' :
					case 'i' :
					case 'd' :
							if ( preg_match( "/^{$badions[1]}:[0-9.E-]+;\$/", $data ) )
									return true;
							break;
			}
			return false;
	}
}

if ( ! function_exists('encode_url')){
	function encode_url($key = ''){
		$CI =& get_instance();
		return $CI->skayla_encode_url->encode($key);
	}
}

if ( ! function_exists('decode_url')){
	function decode_url($key = ''){
		$CI =& get_instance();
		return $CI->skayla_encode_url->decode($key);
	}
}

if (!function_exists('active_menu')) {
    function active_menu($menu_slug="", $cur_slug="")
    {
        if($cur_slug == $menu_slug){
            return 'active';
        }else{
            return '';
        }
    }
}

/* End of file general_helper.php */
/* Location: ./application/helpers/general_helper.php */
