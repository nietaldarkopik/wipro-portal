<?php
/**
 *
 * Safely encrypts data for POST transport
 * URL issues
 *  transforms + to spaces
 *  / are param value separators
 *  = are param value separators
 *
 *  we process the string on reverse
 * @param string $string
 */
function my_urlsafe_b64encode($string)
{
    $data = base64_encode($string);
    $data = str_replace(array('+','/','='),array('-','_','.'),$data);
    return $data;
}

/**
 *
 * The encoding string had to be specially encoded to be transported
 * over the HTTP
 *
 * The reverse function has to be executed on the client
 *
 * @param string $string
 */
function my_urlsafe_b64decode($string)
{
  $data = str_replace(array('-','_','.'),array('+','/','='),$string);
  $mod4 = strlen($data) % 4;
  if ($mod4) {
    $data .= substr('====', $mod4);
  }
  return base64_decode($data);
}

function is_date( $str ){
    $stamp = strtotime( $str );
    if (!is_numeric($stamp) || !preg_match("^\d{1,2}[.-/]\d{2}[.-/]\d{4}^", $str))
        return FALSE;
    $month = date( 'm', $stamp );
    $day   = date( 'd', $stamp );
    $year  = date( 'Y', $stamp );
    if (checkdate($month, $day, $year))
        return TRUE;
    return FALSE;
}


function format_currency($nominal = 0,$currency = "")
{
  $nominal = number_format($nominal,0,",",".");
  return $currency.$nominal;
}