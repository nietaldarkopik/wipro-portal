<?php

class Input_mce{
  var $CI;
  var $form_field = "";
  var $assets = "";
  function Input_mce()
  {
		$this->CI =& get_instance();
  }

  function config($field = array())
  {
    $the_config = array();
    $output = "";
    $type = "textarea";
    
    $star_required = (isset($field['star_required']))?$field['star_required']:"";
    $label = (isset($field['label']))?$field['label']:"";
    $name = (isset($field['name']))?$field['name']:"";
    $id = (isset($field['id']))?$field['id']:"";
    $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:"";
    $maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
    $size = (isset($field['size']))?$field['size']:"";
    $style = (isset($field['style']))?$field['style']:"";
    $class = (isset($field['class']))?$field['class']:"";
    $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
    $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
    $rules = (isset($field['rules']))?$field['rules']:"";
    $class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
    $class .= ' form-control addon_input_mce';
    
    $data = array(
      'name'        => $input_name,
      'id'          => $id,
      'value'       => $value,
      'maxlength'   => $maxlength,
      'size'        => $size,
      'style'       => $style,
      'type'       => $type,
      'class'		=> $class
    );
    $field_attributes = $field;
    $field_attributes = array_merge($field_attributes,$data);
    foreach($field_attributes as $i => $o)
    {
      if(is_array($o) and count($o) >  0)
      {
        unset($field_attributes[$i]);
      }
    }

    $error = form_error($input_name);
    $label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
    $output .= '  <div class="row">
                    <br/><div class="col-lg-3">
                    <div class="input-group">
                      <label for="'. $name .'">'. $label .'</label>';
    $form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_textarea($field_attributes));
    $output .= '    </div>
                  </div>
                  <div class="col-lg-9">'.$form.'
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->';      
    $assets_head['css'] = '<link href="'.current_admin_theme_url().'static/js/jquery/css/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" />';
    $assets_head['js'] =  "";
    $assets_head['css_inline'] =  "";
    $assets_head['js_inline'] =  "";
    
    $assets_body['css'] =  '';
    #$assets_body['js'] =  ' <script type="text/javascript" src="'.current_admin_theme_url().'static/js/tinymce/tinymce.min.js"></script>
    #                        <script type="text/javascript" src="'.current_admin_theme_url().'static/js/tinymce/jquery.tinymce.min.js"></script>';
    $assets_body['css_inline'] =  "";
    $assets_body['js_inline'] =  '
    <script>
        $(document).ready(function(){
          if(jQuery(".addon_input_mce").length > 0)
          {
            /*
            if (typeof(tinyMCE) != "undefined") {
              if (tinyMCE.activeEditor == null || tinyMCE.activeEditor.isHidden() != false) {
              tinyMCE.editors=[];
              }
            }
            jQuery("#'.$id.'").tinymce({
              plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
              ],
              menubar: false,
              toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
            });*/
            
            tinyMCE.editors=[];
            tinyMCE.execCommand("mceRemoveEditor", false, "'.$id.'");
            tinyMCE.execCommand("mceAddEditor", false, "'.$id.'");
            tinyMCE.triggerSave();
          }
      });
    </script>';
    
    #$this->assets['head'] = $assets_head;
    #$this->assets['body'] = $assets_body;
    
    $output .= implode("",$assets_body);
    $this->config_field = array();
    $this->output_field = $output;
    
  }
  
  function output()
  {
    return $this->output_field;
  }
  function get_assets()
  {
    return $this->assets;
  }
}

