<?php

class Input_multiple{
  var $CI;
  var $config_field;
  var $output_field;
  
  function Input_multiple()
  {
		$this->CI =& get_instance();
  }
  
  function config($field = array())
  {
    $the_config = array();
    $output = "";
    $type = "text";
    
    $star_required = (isset($field['star_required']))?$field['star_required']:"";
    $label = (isset($field['label']))?$field['label']:"";
    $name = (isset($field['name']))?$field['name']:"";
    $id = (isset($field['id']))?$field['id']:"";
    $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:"";
    $maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
    $size = (isset($field['size']))?$field['size']:"";
    $style = (isset($field['style']))?$field['style']:"";
    $class = (isset($field['class']))?$field['class']:"";
    $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
    $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
    $rules = (isset($field['rules']))?$field['rules']:"";
	$multiple_cols = (isset($field['multiple_cols']))?$field['multiple_cols']:"";
    $class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
    $class .= ' form-control';
    
    $data = array(
      'name'        => $input_name,
      'id'          => $id,
      'value'       => $value,
      'maxlength'   => $maxlength,
      'size'        => $size,
      'style'       => $style,
      'type'       => $type,
      'class'		=> $class
    );
    $field_attributes = $field;
    $field_attributes = array_merge($field_attributes,$data);
    foreach($field_attributes as $i => $o)
    {
      if(is_array($o) and count($o) >  0)
      {
        unset($field_attributes[$i]);
      }
    }

	$multiple_cols_output = "";
	
	if(is_array($multiple_cols) and count($multiple_cols) > 0)
	{
		$value2 = json_decode($value,true);
		$value = array();
		if(is_array($value2) and count($value2) > 0)
		{
			foreach($value2 as $n => $v)
			{
				$no = 0;
				$val = array();
				foreach($v as $n2 => $v2)
				{
					$value[$no][$n] = $v2;
					$no++;
				}
			}
		}
		
		$multiple_cols_output = "<table border='1' class='input_multiple' id='tbl_multiple_".$name."'><tr>";
		$multiple_cols_output .= '<td>No</td>';
		foreach($multiple_cols as $index => $col)
		{
			$multiple_cols_output .= '<td>'.$col['title'].'</td>';
		}
		$multiple_cols_output .= '<td>Action</td>';
		$multiple_cols_output .= '</tr>';
		$multiple_cols_output .= '<tr class="master" style="display:none;"><td>1</td>';
		foreach($multiple_cols as $index => $col)
		{
			$input_multiple = "";
			$attr = (isset($col['attr']))?$col['attr']:"";
			$name = (isset($col['name']))?$col['name']:"";
			$val = (isset($value[$name][$index]))?$value[$name][$index]:"";
			if($col['type'] == 'text')
			{
				$input_multiple = '<input type="text" '.$attr.' name="'.$input_name.'['.$name.'][]" value="'.$val.'"/>';
			}else{
				$input_multiple = '<textarea '.$attr.' name="'.$input_name.'['.$name.'][]">'.$val.'</textarea>';
			}
			$multiple_cols_output .= '<td>'.$input_multiple.'</td>';
		}						
		$multiple_cols_output .= '<td><a href="javascript:void(0);" class="remove_row_input_multiple">delete</a></td>';
		$multiple_cols_output .= '</tr>';
		
		if(is_array($value) and count($value) > 0)
		{
			$no = 0;
			foreach($value as $i => $vals)
			{
				if(implode("",$vals) == "")
					continue;
				
				$no++;
				$multiple_cols_output .= '<tr class="master2"><td>' . ($no) .'</td>';
				foreach($multiple_cols as $index => $col)
				{
					$input_multiple = "";
					$attr = (isset($col['attr']))?$col['attr']:"";
					$sname = (isset($col['name']))?$col['name']:"";
					$val = (isset($vals[$sname]))?$vals[$sname]:"";
					if($col['type'] == 'text')
					{
						$input_multiple = '<input type="text" '.$attr.' name="'.$input_name.'['.$sname.'][]" value="'.$val.'"/>';
					}else{
						$input_multiple = '<textarea '.$attr.' name="'.$input_name.'['.$sname.'][]">'.$val.'</textarea>';
					}
					$multiple_cols_output .= '<td>'.$input_multiple.'</td>';
				}
				$multiple_cols_output .= '<td><a href="javascript:void(0);" class="remove_row_input_multiple">delete</a></td>';
				$multiple_cols_output .= '</tr>';
			}
		}
		
		$multiple_cols_output .= "<tr>
									<td colspan='". (count($multiple_cols)+2) ."'>
										<a href='javascript:void(0);' class='add_row_input_multiple'>add new</a>
									</td>
								  </tr>
								</table>";
	}
				
	$desc = "";
	$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
	$multiple_cols_output  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$multiple_cols_output);

    $error = form_error($input_name);
    $label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
    $output .= '  <br/><div class="row">
                    <div class="col-lg-3">
                    <div class="input-group">
                      <label for="'. $name .'">'. $label .'</label>';
    $form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_input($field_attributes));
                  $form = $multiple_cols_output.$error;
    $this->form_field = $form;
    $output .= '    </div>
                  </div>
                  <div class="col-lg-9">'.$form.'
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->';    
	
    $this->config_field = array();
    $this->output_field = $output;
  }
  
  function output()
  {
    return $this->output_field;
  }
}

