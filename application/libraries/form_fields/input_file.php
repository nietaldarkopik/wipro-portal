<?php

class Input_file{
  var $CI;
  var $form_field = "";
  function Input_file()
  {
		$this->CI =& get_instance();
  }

  function config($field = array())
  {
    $the_config = array();
    $output = "";
    $type = "file";
    
    $desc = (isset($field['desc']))?$field['desc']:"";
    $star_required = (isset($field['star_required']))?$field['star_required']:"";
    $label = (isset($field['label']))?$field['label']:"";
    $name = (isset($field['name']))?$field['name']:"";
    $id = (isset($field['id']))?$field['id']:"";
    $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:"";
    $maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
    $size = (isset($field['size']))?$field['size']:"";
    $style = (isset($field['style']))?$field['style']:"";
    $class = (isset($field['class']))?$field['class']:"";
    $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
    $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
    $rules = (isset($field['rules']))?$field['rules']:"";
    $class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
    $class .= ' form-control';
    
    $data = array(
      'name'        => $input_name,
      'id'          => $id,
      'value'       => $value,
      'maxlength'   => $maxlength,
      'size'        => $size,
      'style'       => $style,
      'type'       => $type,
      'class'		=> $class
    );
    $field_attributes = $field;
    $field_attributes = array_merge($field_attributes,$data);
    foreach($field_attributes as $i => $o)
    {
      if(is_array($o) and count($o) >  0)
      {
        unset($field_attributes[$i]);
      }
    }

    $error = "";
    $this->CI->load->library('upload');
    if(isset($field['config_upload']) and isset($field['config_upload']['allowed_types']) and !empty($field['config_upload']['allowed_types']))
    {
      $format_file = str_replace("|",",",$field['config_upload']['allowed_types']);
      $desc .= "<span class='desc_field'><i>Format file : (".$format_file.")</i></span>";
    }
    
    $error .= form_error($input_name);
    $error .= $this->CI->upload->display_error_single($name,'<p class="alert alert-danger error">','</p>');
    $label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
    $output .= '  
                  <br/>
                  <div class="row">
                    <div class="col-lg-3">
                    <div class="input-group">
                      <label for="'. $name .'">'. $label .'</label>';
    $form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_input($field_attributes));
                  $form = 
                  '<div class="input-group">
                    '.$form.'
                    '.
                    (($star_required == 1)?'
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                    </span>':'')
                    .
                    ((!empty($stardesc_required))?'
                    <span class="input-group-addon">
                      <span class="glyphicon glyphicon-warning-sign"></span>
                      '.$stardesc_required.'
                    </span>':'')
                    .'
                    <span class="input-group-addon">
                      <span class="glyphicon">&nbsp;</span>
                    </span>
                  </div>'.
                  $error;
    $this->form_field = $form;
    $output .= '    </div>
                  </div>
                  <div class="col-lg-9">'.$form . 
                    ((!empty($desc))?'
                    <span class="help-block">'.$desc.'</span>':'').'
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->';      
    $this->config_field = array();
    $this->output_field = $output;
  }
  
  function output()
  {
    return $this->output_field;
  }
}

