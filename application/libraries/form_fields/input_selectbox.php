<?php

class Input_selectbox{
  var $CI;
  var $form_field = "";
  function Input_selectbox()
  {
		$this->CI =& get_instance();
  }
  
  function config($field = array())
  {
      $output = "";
      $value = "";
      $type = "";
      $star_required = "";
      $name = "";
      $auto_complete = (isset($field['auto_complete']))?$field['auto_complete']:"";
      $star_required = (isset($field['star_required']))?$field['star_required']:"";
      $label = (isset($field['label']))?$field['label']:$field;
      $name = (isset($field['name']))?$field['name']:$field;
      $id = (isset($field['id']))?$field['id']:$name;
      $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
      $style = (isset($field['style']))?$field['style']:"";
      $class = (isset($field['class']))?$field['class']:"";
      $class .= ($auto_complete)?" selectcombobox ":"";
      $options = (isset($field['options']))?$field['options']:"";
      $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
      $rules = (isset($field['rules']))?$field['rules']:"";
      $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
      
      $t_attribute = "";
      if(!empty($id))
        $t_attribute .= ' id="'.$id.'" ';
      if(!empty($style))
        $t_attribute .= ' style="'.$style.'" ';
      if(!empty($class))
        $t_attribute .= ' class="'.$class.' '. ((is_array($rules))?implode(' ',$rules):$rules) .'" ';
        
      $attribute = (isset($field['attribute']))?$field['attribute'] . $t_attribute:$t_attribute;
      $query = (isset($field['query']))?$field['query']:"";
      
      if(!empty($query))
      {
        $q = $this->CI->db->query($query);
        if($q->num_rows() > 0)
        {
          $t_options = $q->result_array();
          foreach($t_options as $index => $row)
          {
            if(isset($row['value']))
            {
              $t_label = (isset($row['label']))?$row['label']:$row['value'];
              $options[$row['value']] = $t_label;
            }
          }
        }
      }elseif(isset($field['table']) and !empty($field['table']))
      {
        $this->CI->db->order_by("value","ASC");
        $this->CI->db->select($field['select']);
        $q = $this->CI->db->get($field['table']);
        
        if($q->num_rows() > 0)
        {
          $t_options = $q->result_array();
          foreach($t_options as $index => $row)
          {
            if(isset($row['value']))
            {
              $t_label = (isset($row['label']))?$row['label']:$row['value'];
              $options[$row['value']] = $t_label;
            }
          }
        }
      
      }

      $label = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
      $value = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$value);
      $form = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_dropdown($input_name, $options, $value ,$attribute));
      
      $this->form_field = $form;
      //$output .= '<span class="value_view"> : '.  '</span>'.$star_required.'<br class="fclear"/>';
      if(isset($field['js_connect_to']) and !empty($field['js_connect_to']))
      {
        $table 	= $field['js_connect_to']['table'];
        $where 	= $field['js_connect_to']['where'];
        $select	= $field['js_connect_to']['select'];
        $value 	= $this->CI->data->primary_key_value;
        $foreign_key 	= $field['js_connect_to']['foreign_key'];
        
        $param = "foreign_key=$foreign_key&table=$table&where=$where&select=$select&value=$value";
        $field['base64'] = (isset($field['base64']))?$field['base64']:true;
        
        if($field['base64'] !== false)
        {
          $the_param = my_urlsafe_b64encode($param);
          $param = "params=".$the_param;
        }else{
          $param = urldecode($param);
        }
        $output .= '<script type="text/javascript">
                jQuery(document).ready(function(){
                  jQuery('.$field['js_connect_to']['id_field_parent'].').on("change",function(){
                    var foreign_key = jQuery(this).val();
                    jQuery.ajax({
                      url: "'.base_url().'services/get_options",
                      type: "post",
                      data: "'.$param.'&fk="+foreign_key,
                      success: function(msg){
                        var first_row = jQuery("#'.$id.'").find("option").eq(0).clone();
                        jQuery("#'.$id.'").html("");
                        jQuery("#'.$id.'").append(jQuery(first_row));
                        jQuery("#'.$id.'").append(msg);
                      }
                    });
                  });
                });
              </script>';
      }
      
      if($auto_complete)
      {
        $output .= '<script src="'.current_admin_theme_url().'static/js/chosen/chosen.jquery.js"></script>';
        $output .= '<link rel="stylesheet" href="'.current_admin_theme_url().'static/js/chosen/chosen.css"/>';
        $output .= '<script>
                      $(document).ready(function(){
                        $("#'.$id.'").chosen({width:"100%",no_results_text: "Data '.$label.' tidak tersedia",allow_single_deselect: false});
                      });
                    </script>';
      }
      
      $output .= '
          <br/>
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="'. $name .'">'. $label .'</label>
              </div>
            </div>
            <div class="col-lg-9">
              <div class="input-group">
                <span class="form-control padding0">
                  '.$form.' 
                </span>
                '.
                (($star_required == 1)?'
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                </span>':'')
                .
                ((!empty($stardesc_required))?'
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-warning-sign"></span>
                  '.$stardesc_required.'
                </span>':'')
                .'
                <span class="input-group-addon">
                  <span class="glyphicon">&nbsp;</span>
                </span>
              </div>
              '.form_error($input_name) . '
            </div>
          </div>';
    $this->config_field = array();
    $this->output_field = $output;
  }
  
  function output()
  {
    return $this->output_field;
  }
}

