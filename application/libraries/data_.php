<?php

class Data{
	var $query;
	var $path_name;
	var $object_name;
	var $method_name;
	var $table;
	var $fields;
	var $action_buttons;
	var $files;
	var $filters;
	var $CI;
	var $config = array();
	var $is_pagination_init = false;
	var $primary_key = "";
	var $primary_key_value = "";
	var $paging_config = "";
	var $the_config = "";
	var $controllers = array();
	var $methods = array();
	var $controller_methods = array();
	var $controller_paths = array();
	var $data_rows = array();
	var $data_values = array();
	var $input_type_assets = array();
	var $errors = "";
	
	function Data(){
		$this->CI =& get_instance();
	}
	
	function init($config = array())
	{
		$method_name = $this->CI->uri->segment(3);
		$object_name = $this->CI->uri->segment(2);
		$path_name = $this->CI->uri->segment(1);
		$this->path_name = (isset($config['path_name']))?$config['path_name']:$path_name;
		$this->object_name = (isset($config['object_name']))?$config['object_name']:$object_name;
		$this->method_name = (isset($config['method_name']))?$config['method_name']:$method_name;
		$this->primary_key = (isset($config['primary_key']))?$config['primary_key']:"";
		$this->the_config = $config;
		
		$this->table = (isset($config['table']))?$config['table']:"";
		$fields = array();
		$query = "";
		
		if($this->table != "")
		{
			$q = $this->CI->db->get($this->table);
			$query = $this->CI->db->last_query();
		}
		
		$this->query = (isset($config['query']))?$config['query']:$query;
		$this->query = (!empty($this->query))?$this->query . " where 1 = 1":"";
		if($this->query != "")
		{		
			$q = $this->CI->db->query($this->query);
			$fields = $q->list_fields();
		}
		$this->fields = (isset($config['fields']))?$config['fields']:$fields;
		$this->action_buttons = (isset($config['action_buttons']))?$config['action_buttons']:"";
		
		$filters = (isset($config['filters']))?$config['filters']:$this->get_filter();
		$t_filter = "";
		if(!empty($filters) and is_array($filters) and count($filters) > 0)
		{
			$t_filter = " 1 = 1 ";
			foreach($filters as $index => $filter)
			{
				if(!is_array($filter))
				$t_filter .= $filter;
			}
		}else{
			$t_filter = $filters;
		}
		$this->filters = $t_filter;
		
	}
	
	function init_pagination($config = array())
	{
		$function = $this->CI->uri->segment(2);
		if($function == 'listing' or $function == 'index')
		{
			$function = "";
		}else{
			$function = "/".$function;
		}
		$this->CI->load->library("pagination");
		$this->paging_config = array();
		$this->paging_config['base_url'] = base_url().$this->CI->uri->segment(1).$function.'/listing/';
		$this->paging_config['total_rows'] = $this->get_total_data();
		$this->paging_config['per_page'] = 50; 
		//$this->paging_config['uri_segment'] = 3;
    $this->paging_config['uri_segment'] = 4;
    $this->paging_config['num_links'] = 2;
		$this->paging_config['use_page_numbers'] = FALSE;
		$this->paging_config['page_query_string'] = FALSE;
		$this->paging_config['full_tag_open'] = '<ul id="pager" class="pagination margin0">';
		$this->paging_config['full_tag_close'] = '</ul>';
		$this->paging_config['first_link'] = '&laquo;';
		$this->paging_config['first_tag_open'] = '<li>';
		$this->paging_config['first_tag_close'] = '</li>';
		$this->paging_config['last_link'] = '&raquo;';
		$this->paging_config['last_tag_open'] = '<li>';
		$this->paging_config['last_tag_close'] = '</li>';
		$this->paging_config['next_link'] = '&rsaquo;';
		$this->paging_config['next_tag_open'] = '<li>';
		$this->paging_config['next_tag_close'] = '</li>';
		$this->paging_config['prev_link'] = '&lsaquo;';
		$this->paging_config['prev_tag_open'] = '<li>';
		$this->paging_config['prev_tag_close'] = '</li>';
		$this->paging_config['cur_tag_open'] = '<li class="active""><a href="#">';
		$this->paging_config['cur_tag_close'] = '</a></li>';
		$this->paging_config['num_tag_open'] = '<li>';
		$this->paging_config['num_tag_close'] = '</li>';
		
		$config = array_merge($this->paging_config,$config);
		$this->paging_config = $config;
		$this->is_pagination_init = true;
	}
	
	function create_pagination($config = array())
	{
		if($this->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->init_pagination($config);
			
		$this->CI->pagination->initialize($this->paging_config); 
		return $this->CI->pagination->create_links();
	}
	
	function create_view($config = array())
	{
		$method_name = (!empty($this->method_name))?$this->method_name:'listing';
		$t_nonce = $this->CI->user_access->get_nonce();
		#$action = (isset($config['action']))?$config['action']:base_url().$this->object_name.'/'.$method_name;
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		
		$form_title = $this->CI->hook->do_action('hook_create_view_form_title','View Data');
		$output = '
			<form id="form" action="#" method="post" class="form">
				<fieldset id="edit_form_fieldset">
				<legend>'.$form_title.'</legend>';
						
				if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
				{
					foreach($this->fields as $index => $field)
					{
							$field['parent_name'] = 'data';
							$label = (isset($config['primary_key']))?$config['primary_key']:$this->primary_key;
							
							if(empty($label))
							{
								$label = 'label';
							}
							
							$custom_query = (isset($config['query']))?$config['query']:'';
							$where = array($label => "'".$this->primary_key_value."'");
							$value = $this->get_value($this->table,$field['name'] . " label",$where,"",$custom_query);
							$field['value'] = $value;
							$field_name = explode(".",$field['name']);
							$field['name'] = (isset($field_name[1]) and !empty($field_name[1]))?$field_name[1]:$field_name[0];
							$this->data_values[$field['name']] = $value;
							$output .= $this->create_form_view($field);
					}
				}
		
		$output .= '	<br class="fclear"/>
					</fieldset>
				</form>';
		
		return $output;
	}
	
	
	function create_form($config = array())
	{
		$method_name = (!empty($this->method_name))?$this->method_name:'listing';
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:current_url();
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$form_title = $this->CI->hook->do_action('hook_create_form_form_title','Form Data');
    $is_ajax = $this->CI->hook->do_action('hook_create_form_is_ajax','1');
    $the_fields = (isset($config['fields']) && !empty($config['fields'])) ? $config['fields'] : $this->fields;
    $the_fields = $this->CI->hook->do_action('hook_create_form_fields',$the_fields);
    $ajax = ($is_ajax == 1)?'ajax':'';
		$output = '';
    
		if(!empty($the_fields) and is_array($the_fields) and count($the_fields) > 0)
		{
			foreach($the_fields as $index => $field)
			{
					$field['parent_name'] = 'data';
					$label = (isset($config['primary_key']))?$config['primary_key']:"";
					
					if(empty($label))
					{
						$label = 'label';
					}
				
					$custom_query = (isset($config['query']))?$config['query']:'';
					$where = array($label => "'".$this->primary_key_value."'");
					$value = $this->get_value($this->table,$field['name'] . " label",$where,"",$custom_query);
					$field['value'] = $value;
					$field_name = explode(".",$field['name']);
					$field['name'] = (isset($field_name[1]) and !empty($field_name[1]))?$field_name[1]:$field_name[0];
					$output .= $this->create_form_field($field,$value);
			}
		}

		$conf_button_action = array(
			'name' => 'input_submit',
			'label' => 'Save',
			'id' => 'input_submit',
			'value' => '',
			'type' => 'input_submit',
			'use_search' => true,
			'use_listing' => true,
			'rules' => 'required'
		  );
		if(!empty($this->action_buttons) and is_array($this->action_buttons) and count($this->action_buttons) > 0)
		{
			foreach($this->action_buttons as $index => $button)
			{
					$output .= $this->create_form_field($button);
			}
		}else{
		  $button_action = $this->create_form_field($conf_button_action);
		}

		if(isset($this->input_type_assets) and is_array($this->input_type_assets) and count($this->input_type_assets) >  0)
    {
      $the_input_assets = array();
      foreach($this->input_type_assets as $i => $a)
      {
        if(is_array($a) and count($a) > 0)
        {
          foreach($a as $i2 => $a2)
          {
            $the_input_assets[$i2] = $a2;
          }
        }
      }
      
      foreach($the_input_assets as $i => $a)
      {
        $this->CI->assets->add_js($a['js'],$i);
        $this->CI->assets->add_css($a['css'],$i);
        $this->CI->assets->add_js_inline($a['js_inline'],$i);
        $this->CI->assets->add_css_inline($a['css_inline'],$i);
      }
    }
    
		$ajax_target = $this->CI->hook->do_action('hook_create_form_ajax_target','.ajax_container');
		$button_action = $this->CI->hook->do_action('hook_create_form_button_action',$button_action);
		$output .= '
							<input type="hidden" name="nonce" value="'.$nonce.'"/> 
							<input type="hidden" name="ajax_target" value="'.$ajax_target.'"/> 
							<input type="hidden" name="is_ajax" value="'.$is_ajax.'"/> ';
    $output .= $button_action;
    
		$form_title = $this->CI->hook->do_action('hook_create_form_title','Add New Data Form');
    $output = '<form method="post" class="form '.$ajax.'" action="'.$action.'" id="form" data-target-ajax="'.$ajax_target.'" enctype="multipart/form-data"><h3>'.$form_title.'</h3>'.$output.'</form>';

		return $output;
	}
	
	function create_form_export($config = array())
	{
		$method_name = (!empty($this->method_name))?$this->method_name:'listing';
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:current_url();
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$form_title = $this->CI->hook->do_action('hook_create_form_export_form_title','Form Data');
    $is_ajax = $this->CI->hook->do_action('hook_create_form_export_is_ajax','1');
    $ajax = ($is_ajax == 1)?'ajax':'';
    
		$conf_button_action = array(
			'name' => 'input_submit',
			'label' => 'Save',
			'id' => 'input_submit',
			'value' => '',
			'type' => 'input_submit',
			'use_search' => true,
			'use_listing' => true,
			'rules' => 'required'
		  );
      
		if(!empty($this->action_buttons) and is_array($this->action_buttons) and count($this->action_buttons) > 0)
		{
			foreach($this->action_buttons as $index => $button)
			{
					$output .= $this->create_form_field($button);
			}
		}else{
		  $button_action = $this->create_form_field($conf_button_action);
		}

		
		$conf_input_csv = array(
			'name' => 'input_csv',
			'label' => 'CSV Data',
			'id' => 'input_csv',
			'value' => '',
			'type' => 'input_csv',
			'use_search' => true,
			'use_listing' => true,
			'rules' => 'required'
		  );
    $input_csv = $this->create_form_field($conf_input_csv);
		$ajax_target = $this->CI->hook->do_action('hook_create_form_export_ajax_target','.ajax_container');
		$button_action = $this->CI->hook->do_action('hook_create_form_export_button_action',$button_action);
    $output = $input_csv;
		$output .= '
							<input type="hidden" name="nonce" value="'.$nonce.'"/> 
							<input type="hidden" name="ajax_target" value="'.$ajax_target.'"/> 
							<input type="hidden" name="is_ajax" value="'.$is_ajax.'"/> ';
    $output .= $button_action;
    
    $output = '<form method="post" class="form '.$ajax.'" action="'.$action.'" id="form" data-target-ajax="'.$ajax_target.'">'.$output.'</form>';
		return $output;
	}
	
	function create_form_import($config = array())
	{
		$method_name = (!empty($this->method_name))?$this->method_name:'listing';
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:str_replace("/listing","",current_url());
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
    $csv_fields = (isset($config['csv_fields']))?$config['csv_fields']:'';
		$form_title = $this->CI->hook->do_action('hook_create_form_import_form_title','Form Data');
		$is_ajax = $this->CI->hook->do_action('hook_create_form_import_is_ajax','1');
    $ajax = ($is_ajax == 1)?'ajax':'';

    $csv_fields_data = explode(",",$csv_fields);
    $csv_fields_used = array();
    $inline_js = "";
    if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
    {
      foreach($this->fields as $index => $field)
      {
          if((isset($field['name']) and in_array($field['name'],$csv_fields_data)) or (is_array($csv_fields_data) and count($csv_fields_data) == 1))
          {
            $csv_fields_used[] = $field;
            /*
            $field['parent_name'] = 'data';
            $label = (isset($config['primary_key']))?$config['primary_key']:"";
            
            if(empty($label))
            {
              $label = 'label';
            }
            $where = array($label => "'".$this->primary_key_value."'");
            $value = $this->get_value($this->table,$field['name'] . " label",$where,"");
            $field['value'] = $value;
            $output .= $this->create_form_field($field,$value);
            */
          }
      }
    }
      
    if($this->CI->input->post("input_csv") != "")
    {
      $this->fields = $csv_fields_used;
      $output = $this->create_listing_grid($config,$this->CI->input->post("input_csv"));
      $output .= '<script>
                    $(document).ready(function(){
                          var count_tr = jQuery("#import table tbody tr").length;
                          jQuery.each(jQuery("#import table tbody tr"),function(k,v){
                            do_check(v);
                          });
                    });
                  </script>';
    }else
    {
      $conf_button_action = array(
        'name' => 'input_submit_csv',
        'label' => 'Save',
        'id' => 'input_submit_csv',
        'value' => '',
        'type' => 'input_submit',
        'use_search' => false,
        'use_listing' => false,
        'rules' => ''
        );

      $conf_button_check = array(
        'name' => 'input_check_csv',
        'label' => 'Check Data',
        'id' => 'input_check_csv',
        'value' => 'Check Data',
        'type' => 'input_submit',
        'use_search' => false,
        'use_listing' => false,
        'rules' => ''
        );
        
      if(!empty($this->action_buttons) and is_array($this->action_buttons) and count($this->action_buttons) > 0)
      {
        foreach($this->action_buttons as $index => $button)
        {
            $output .= $this->create_form_field($button);
        }
      }else{
        $button_action = $this->create_form_field($conf_button_action);
      }
        
      if(!empty($this->action_check) and is_array($this->action_check) and count($this->action_check) > 0)
      {
        foreach($this->action_check as $index => $button)
        {
            $output .= $this->create_form_field($button);
        }
      }else{
        $button_check = $this->create_form_field($conf_button_check);
      }

      
      $conf_input_csv = array(
        'name' => 'input_csv',
        'label' => 'CSV Data',
        'id' => 'input_csv',
        'value' => '',
        'type' => 'input_csv',
        'use_search' => true,
        'use_listing' => true,
        'rules' => 'required'
        );
      $input_csv = $this->create_form_field($conf_input_csv);
      $ajax_target = $this->CI->hook->do_action('hook_create_form_import_ajax_target','.tab-content #import');
      $button_action = $this->CI->hook->do_action('hook_create_form_import_button_action',$button_action);
      $button_check = $this->CI->hook->do_action('hook_create_form_import_button_check',$button_check);
      $output = $input_csv;
      $output .= '
                <input type="hidden" name="nonce" value="'.$nonce.'"/> 
                <input type="hidden" name="ajax_target" value="'.$ajax_target.'"/> 
                <input type="hidden" name="is_ajax" value="'.$is_ajax.'"/> ';
      $output .= $button_check . $button_action;
      
      $output = '<form method="post" class="form '.$ajax.'" action="'.$action.'" id="form_import" data-target-ajax="'.$ajax_target.'">'.$output.'</form>';
    }
      $output .= $this->create_desc_csv_import($csv_fields_used);
      $inline_js .= '
                          var count_errors = 0;
                          function do_check(row)
                          {
                              var data_json = jQuery(row).find(":input").serializeArray();
                              var data = new Object();
                              jQuery.each(data_json,function(k,v){
                                if(typeof v["name"] != "undefined")
                                {
                                  var name = v["name"];
                                  var tmp_data = {};
                                  tmp_data[name] = v["value"];
                                  data[v["name"]] = v["value"];
                                }
                              });
                              
                              jQuery.ajax({
                                url: "'.$action.'/check_csv",
                                data: {data:data,nonce: "'.$t_nonce.'"},
                                type: "post",
                                async: true,
                                success:function(msg){
                                  if(msg == 1)
                                  {
                                    jQuery(row).find("td").first().html("<span class=\"label label-success\">0 error</span>");
                                  }else{
                                    var count_errors = jQuery("<div/>").append(msg).find("div.alert-danger").length;
                                    var btn_status = jQuery("<div/>").append("<button type=\"button\" class=\"popover_btn label label-success btn btn-warning grid-error-item\" data-container=\"body\" data-toggle=\"popover\" data-html=\"true\" data-placement=\"right\">"+count_errors+" error</button>");
                                    jQuery(btn_status).find("button").attr("data-content",msg);
                                    jQuery(row).find("td").first().html(jQuery(btn_status).html());
                                    jQuery(row).find(".popover_btn").popover();
                                    count_errors = count_errors + 1;
                                    jQuery("button#save_import").hide();
                                  }
                                }
                              });
                          }
                          
                          function do_save(row)
                          {
                              var data_json = jQuery(row).find(":input").serializeArray();
                              var data = [];
                              var data = new Object();
                              jQuery.each(data_json,function(k,v){
                                if(typeof v["name"] != "undefined")
                                {
                                  var name = v["name"];
                                  var tmp_data = {};
                                  tmp_data[name] = v["value"];
                                  data[v["name"]] = v["value"];
                                }
                              });
                                                            
                              jQuery.ajax({
                                url: "'.$action.'/add_csv",
                                data: {data:data,nonce: "'.$t_nonce.'"},
                                type: "post",
                                async: true,
                                success:function(msg){
                                  if(msg == 1)
                                  {
                                    jQuery(row).find("td").first().html("<span class=\"label label-success\">saved!</span>");
                                  }else{
                                    var count_errors = jQuery("<div/>").append(msg).find("div.alert-danger").length;
                                    var btn_status = jQuery("<div/>").append("<button type=\"button\" class=\"popover_btn label label-success btn btn-warning grid-error-item\" data-container=\"body\" data-toggle=\"popover\" data-html=\"true\" data-placement=\"right\">"+count_errors+" error</button>");
                                    jQuery(btn_status).find("button").attr("data-content",msg);
                                    jQuery(row).find("td").first().html(jQuery(btn_status).html());
                                    jQuery(row).find(".popover_btn").popover();
                                    count_errors = count_errors + 1;
                                    jQuery("button#save").hide();
                                  }
                                }
                              });
                          }
                          
                          jQuery(document).on("click","button#input_check_csv",function(){                            
                            var count_tr = jQuery("#import table tbody tr").length;
                            jQuery.each(jQuery("#import table tbody tr"),function(k,v){
                              do_check(v);
                            });  
                          });
                          jQuery(document).on("click","button#input_submit_csv",function(){                            
                            var count_tr = jQuery("#import table tbody tr").length;
                            jQuery.each(jQuery("#import table tbody tr"),function(k,v){
                              do_save(v);
                            });  
                          });
                          jQuery(document).on("click","#input_csv,#save_import",function(){                            
                            var count_tr = jQuery("#import table tbody tr").length;
                            jQuery.each(jQuery("#import table tbody tr"),function(k,v){
                              do_save(v);
                            });  
                          });
                        jQuery(document).ready(function(){
                        });';
      $this->CI->assets->add_js_inline($inline_js,'body');
		return $output;
	}
	
	function create_form_filter($config = array())
	{
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action_search']))?$config['action_search']:"";
		$action = (empty($action) and isset($config['search']))?$config['search']:$action;
		$action = (empty($action))?current_main_url():$action;
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;

		$filter = $this->get_filter();
		$ajax_target = $this->CI->hook->do_action('hook_create_form_filter_ajax_target','.ajax_container');
		$form_title = $this->CI->hook->do_action('hook_create_form_filter_form_title','Search Form');
		$is_ajax = $this->CI->hook->do_action('hook_create_form_filter_is_ajax','1');
    $ajax = ($is_ajax == 1)?'ajax':'';
    $this->fields = $this->CI->hook->do_action('hook_create_form_filter_fields',$this->fields);
		$output = '
			<form id="form_filter" action="'.$action.'" method="post" class="form '.$ajax.'" data-target-ajax="'.$ajax_target.'">
				<h3>'.$form_title.'</h3>';
				if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
				{
					foreach($this->fields as $index => $field)
					{
						if(isset($field['use_search']) and $field['use_search'] == true)
						{
							$value = (isset($filter[$field['name']]))?$filter[$field['name']]:"";
							$field['parent_name'] = 'data_filter';
							if(isset($field['id']))
								$field['id'] = 'data_filter_'.$field['id'];
							if(isset($field['type']) and $field['type'] == 'input_textarea')
								$field['type'] = 'input_text';
								
							$field['value'] = $value;
							$field_name = explode(".",$field['name']);
							$field['name'] = (isset($field_name[1]) and !empty($field_name[1]))?$field_name[1]:$field_name[0];
							$output .= $this->create_form_filter_field($field,$value,false);
						}
					}
				}
		
		$conf_button_action = array(
			'name' => 'input_submit',
			'label' => 'Search',
			'id' => 'input_submit',
			'value' => '',
			'type' => 'input_submit',
			'use_search' => true,
			'use_listing' => true,
			'rules' => 'required'
		  );
    $conf_button_action = $this->CI->hook->do_action('hook_create_form_filter_conf_button_action',$conf_button_action);
    $button_action = "";
		if(!empty($this->action_buttons) and is_array($this->action_buttons) and count($this->action_buttons) > 0)
		{
			foreach($this->action_buttons as $index => $button)
			{
					$output .= $this->create_form_field($button);
			}
		}else{
		  $button_action = $this->create_form_field($conf_button_action);
		}
		
		if(isset($this->input_type_assets) and is_array($this->input_type_assets) and count($this->input_type_assets) >  0)
    {
      $the_input_assets = array();
      foreach($this->input_type_assets as $i => $a)
      {
        if(is_array($a) and count($a) > 0)
        {
          foreach($a as $i2 => $a2)
          {
            $the_input_assets[$i2] = $a2;
          }
        }
      }
      
      foreach($the_input_assets as $i => $a)
      {
        $this->CI->assets->add_js($a['js'],$i);
        $this->CI->assets->add_css($a['css'],$i);
        $this->CI->assets->add_js_inline($a['js_inline'],$i);
        $this->CI->assets->add_css_inline($a['css_inline'],$i);
      }
    }
    $button_action = $this->CI->hook->do_action('hook_create_form_filter_button_action',$button_action);
    
    $output .= '
          <label>&nbsp;</label>
          <input type="hidden" name="nonce" value="'.$nonce.'"/> 
          <input type="hidden" name="ajax_target" value="'.$ajax_target.'"/> 
          <input type="hidden" name="is_ajax" value="'.$is_ajax.'"/> 
          <span class="value_view">
            &nbsp;&nbsp;
            '.$button_action.'
          </span>
      </fieldset>	
      <br class="fclear"/>
    </form>';

    $output = $this->CI->hook->do_action('hook_create_form_filter_output',$output);
    
		return $output;
	}
	
	function create_form_view($field = array(),$value = "",$show_star = true)
	{
		$output = "";
		$type = (isset($field['type']))?$field['type']:'text';
		$is_required = (isset($field['rules']))?$field['rules']:false;
		if(strpos($is_required,'required') !== false)
		{
			$is_required = true;
		}else{
			$is_required = false;
		}
		
		$field['star_required'] = ($is_required and $show_star)?1:0;
		$field['stardesc_required'] = ($is_required and $show_star)?'wajib diisi':'';
    $value = (isset($field['value']))?$field['value']:$value;
		$field['value'] = $value;
    $this->CI->load->library('form_views');
    $form_fields = $this->CI->form_views->get_fields();
    $get_field_names = $this->CI->form_views->get_field_names();
    $input_type = (in_array($type,$get_field_names))?$type:'input_default';
    $output = $this->CI->form_views->field($input_type,$field);
		
		return $output;
	}
	
	
	function create_form_field($field = array(),$value = "",$show_star = true,$only_input = false)
	{
		$output = "";
		$type = (isset($field['type']))?$field['type']:'text';
		$is_required = (isset($field['rules']))?$field['rules']:false;
		if(strpos($is_required,'required') !== false)
		{
			$is_required = true;
		}else{
			$is_required = false;
		}
		
		$field['star_required'] = ($is_required and $show_star)?1:0;
		$field['stardesc_required'] = ($is_required and $show_star)?'wajib diisi':'';
    $value = (isset($field['value']) and !empty($field['value']))?$field['value']:$value;
		$field['value'] = $value;
    $this->CI->load->library('form_fields');
    $form_fields = $this->CI->form_fields->get_fields();
    $get_field_names = $this->CI->form_fields->get_field_names();
    
    $input_type = (in_array($type,$get_field_names))?$type:'input_default';
    $output = $this->CI->form_fields->field($input_type,$field);
		if($only_input)
    {
      $output = $this->CI->form_fields->input_field($input_type,$field);
    }
    $this->input_type_assets[$input_type] = $this->CI->form_fields->assets;
		return $output;
	}
	
	function create_form_filter_field($field = array(),$value = "",$show_star = true,$only_input = false)
	{
		$output = "";
		$type = (isset($field['type']))?$field['type']:'text';
		$is_required = (isset($field['rules']))?$field['rules']:false;
		if(strpos($is_required,'required') !== false)
		{
			$is_required = true;
		}else{
			$is_required = false;
		}
		
		$field['star_required'] = ($is_required and $show_star)?1:0;
		$field['stardesc_required'] = ($is_required and $show_star)?'wajib diisi':'';
    $value = (isset($field['value']) and !empty($field['value']))?$field['value']:$value;
		$field['value'] = $value;
    $this->CI->load->library('form_filters');
    $form_filters = $this->CI->form_filters->get_fields();
    $get_field_names = $this->CI->form_filters->get_field_names();
    
    $input_type = (in_array('filter_'.strtolower($type),$get_field_names))?'filter_'.strtolower($type):'filter_input_default';
    $output = $this->CI->form_filters->field($input_type,$field);
		if($only_input)
    {
      $output = $this->CI->form_filters->input_field($input_type,$field);
    }
    $this->input_type_assets[$input_type] = $this->CI->form_filters->assets;
		return $output;
	}
	
	
	function create_form_field_bu($field = array(),$value = "",$show_star = true)
	{
		$output = "";
		$type = (isset($field['type']))?$field['type']:'text';
		$is_required = (isset($field['rules']))?$field['rules']:false;
		if(strpos($is_required,'required') !== false)
		{
			$is_required = true;
		}else{
			$is_required = false;
		}
		
		$star_required = ($is_required and $show_star)?'<span style="color:red;">*</span>':'';
		$stardesc_required = ($is_required and $show_star)?'<span style="color:red;">*</span> wajib diisi':'';
		switch($type)
		{			
			case 'selectbox':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"";
				$options = (isset($field['options']))?$field['options']:"";
				$parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
				$rules = (isset($field['rules']))?$field['rules']:"";
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				
				$t_attribute = "";
				if(!empty($id))
					$t_attribute .= ' id="'.$id.'" ';
				if(!empty($style))
					$t_attribute .= ' style="'.$style.'" ';
				if(!empty($class))
					$t_attribute .= ' class="'.$class.' '. ((is_array($rules))?implode(' ',$rules):$rules) .'" ';
					
				$attribute = (isset($field['attribute']))?$field['attribute'] . $t_attribute:$t_attribute;
				$query = (isset($field['query']))?$field['query']:"";
				
				if(!empty($query))
				{
					$q = $this->CI->db->query($query);
					if($q->num_rows() > 0)
					{
						$t_options = $q->result_array();
						foreach($t_options as $index => $row)
						{
							if(isset($row['value']))
							{
								$t_label = (isset($row['label']))?$row['label']:$row['value'];
								$options[$row['value']] = $t_label;
							}
						}
					}
				}elseif(isset($field['table']) and !empty($field['table']))
				{
					$this->CI->db->order_by("value","ASC");
					$this->CI->db->select($field['select']);
					$q = $this->CI->db->get($field['table']);
					
					if($q->num_rows() > 0)
					{
						$t_options = $q->result_array();
						foreach($t_options as $index => $row)
						{
							if(isset($row['value']))
							{
								$t_label = (isset($row['label']))?$row['label']:$row['value'];
								$options[$row['value']] = $t_label;
							}
						}
					}
				
				}
				
				$label = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$value = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$value);
				
				$output .= '<label for="'. $name .'">'. $label .'</label>';
				$output .= '<span class="value_view"> : '. form_dropdown($input_name, $options, $value ,$attribute) . form_error($input_name) . '</span>'.$star_required.'<br class="fclear"/>';
				if(isset($field['js_connect_to']) and !empty($field['js_connect_to']))
				{
				
					$table 	= $field['js_connect_to']['table'];
					$where 	= $field['js_connect_to']['where'];
					$select	= $field['js_connect_to']['select'];
					$value 	= $field['primary_key'];
					$foreign_key 	= $field['js_connect_to']['foreign_key'];
					
					$param = "foreign_key=$foreign_key&table=$table&where=$where&select=$select&value=$value";
					$param = urldecode($param);
					
					$output .= '<script type="text/javascript">
									jQuery(document).ready(function(){
										jQuery("#'.$field['js_connect_to']['id_field_parent'].'").on("change",function(){
											var foreign_key = jQuery(this).val();
											jQuery.ajax({
												url: "'.base_url().'services/get_options",
												type: "post",
												data: "'.$param.'&fk="+foreign_key,
												success: function(msg){
													var first_row = jQuery("#'.$id.'").find("option").eq(0).clone();
													jQuery("#'.$id.'").html("");
													jQuery("#'.$id.'").append(jQuery(first_row));
													jQuery("#'.$id.'").append(msg);
												}
											});
										});
									});
								</script>';
				}
			break;			
			
			case 'multiple':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = 'input_multiple';
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$multiple_cols = (isset($field['multiple_cols']))?$field['multiple_cols']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$multiple_cols_output = "";
				
				if(is_array($multiple_cols) and count($multiple_cols) > 0)
				{
					$value2 = json_decode($value,true);
					$value = array();
					if(is_array($value2) and count($value2) > 0)
					{
						foreach($value2 as $n => $v)
						{
							$no = 0;
							$val = array();
							foreach($v as $n2 => $v2)
							{
								$value[$no][$n] = $v2;
								$no++;
							}
						}
					}
					
					$multiple_cols_output = "<table border='1' class='input_multiple' id='tbl_multiple_".$name."'><tr>";
					$multiple_cols_output .= '<td>No</td>';
					foreach($multiple_cols as $index => $col)
					{
						$multiple_cols_output .= '<td>'.$col['title'].'</td>';
					}
					$multiple_cols_output .= '<td>Action</td>';
					$multiple_cols_output .= '</tr>';
					$multiple_cols_output .= '<tr class="master" style="display:none;"><td>1</td>';
					foreach($multiple_cols as $index => $col)
					{
						$input_multiple = "";
						$attr = (isset($col['attr']))?$col['attr']:"";
						$name = (isset($col['name']))?$col['name']:"";
						$val = (isset($value[$name][$index]))?$value[$name][$index]:"";
						if($col['type'] == 'text')
						{
							$input_multiple = '<input type="text" '.$attr.' name="'.$input_name.'['.$name.'][]" value="'.$val.'"/>';
						}else{
							$input_multiple = '<textarea '.$attr.' name="'.$input_name.'['.$name.'][]">'.$val.'</textarea>';
						}
						$multiple_cols_output .= '<td>'.$input_multiple.'</td>';
					}						
					$multiple_cols_output .= '<td><a href="javascript:void(0);" class="remove_row_input_multiple">delete</a></td>';
					$multiple_cols_output .= '</tr>';
					
					if(is_array($value) and count($value) > 0)
					{
						$no = 0;
						foreach($value as $i => $vals)
						{
							if(implode("",$vals) == "")
								continue;
							
							$no++;
							$multiple_cols_output .= '<tr class="master2"><td>' . ($no) .'</td>';
							foreach($multiple_cols as $index => $col)
							{
								$input_multiple = "";
								$attr = (isset($col['attr']))?$col['attr']:"";
								$sname = (isset($col['name']))?$col['name']:"";
								$val = (isset($vals[$sname]))?$vals[$sname]:"";
								if($col['type'] == 'text')
								{
									$input_multiple = '<input type="text" '.$attr.' name="'.$input_name.'['.$sname.'][]" value="'.$val.'"/>';
								}else{
									$input_multiple = '<textarea '.$attr.' name="'.$input_name.'['.$sname.'][]">'.$val.'</textarea>';
								}
								$multiple_cols_output .= '<td>'.$input_multiple.'</td>';
							}
							$multiple_cols_output .= '<td><a href="javascript:void(0);" class="remove_row_input_multiple">delete</a></td>';
							$multiple_cols_output .= '</tr>';
						}
					}
					
					$multiple_cols_output .= "<tr>
												<td colspan='". (count($multiple_cols)+2) ."'>
													<a href='javascript:void(0);' class='add_row_input_multiple'>add new</a>
												</td>
											  </tr>
											</table>";
				}
							
				$desc = "";
				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$multiple_cols_output  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$multiple_cols_output);

				$output .= '<label for="'. $name .'" class="label_input_multiple">'. $label .'</label>';
				$output .= '<span class="value_view" class="span_input_multiple">'. $multiple_cols_output . '</span>'.$star_required.'' . $desc . '<br class="fclear"/>';
			break;
			
			case 'fixed_multiple_field':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = 'input_multiple';
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$columns = (isset($field['columns']))?$field['columns']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$columns_output = "";
				
				if(is_array($columns) and count($columns) > 0)
				{
					
					$columns_output = "<table class='input_fixed_multiple' id='tbl_fixed_multiple_".$name."'><tr>";
					
					$decoded_value = json_decode($value, true);		
					foreach($columns as $index => $col)
					{
						$columns_output .= '<tr>';
						$columns_output .= '<td>'.$col['label'].'</td>';
						
						$option_val = '';
						foreach($col['options'] as $opt){
							if($this->is_option_selected($decoded_value, $col['key_option'], $opt))
								$option_val .= "<option selected='selected'>{$opt}</option>\n";
							else
								$option_val .= "<option>{$opt}</option>\n";
						}
						
						$columns_output .= '<td>
																		<input type="hidden" name="fixed_multiple_data['.$name.']['.$index.'][label]" value="'.$col['key_option'].'" />
																		<select name="fixed_multiple_data['.$name.']['.$index.'][value]" >
																			'. $option_val. '
																		</select>
																</td>';
					}
					$columns_output .= "</tr>";
					

					$columns_output .= "</table>";
				}
							
				$desc = "";
				$output .= '<br /><br />';
				
				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$columns_output  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$columns_output);

				$output .= '<label for="'. $name .'" class="label_input_multiple">'. $label .'</label>';
				$output .= '<span class="value_view" class="span_input_multiple">'. $columns_output . '</span>'.$star_required.'' . $desc . '<br class="fclear"/>';
			break;
			case 'fixed_multiple_field_text':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = 'input_multiple';
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$columns = (isset($field['columns']))?$field['columns']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$columns_output = "";
				
				if((is_array($columns) and count($columns) > 0))
				{
					
					$columns_output = "<table class='input_fixed_multiple' id='tbl_fixed_multiple_".$name."'><tr>";
					
					$decoded_value = json_decode($value, true);			
					$values = array();
					if(is_array($decoded_value))
					{
						foreach($decoded_value as $i => $v)
						{
							$values[$v['label']] = $v['value'];
						}
					}
					foreach($columns as $index => $col)
					{
						if(!isset($values[$col['key_option']]))
							continue;
						$columns_output .= '<tr>';
						$columns_output .= '<td>'.$col['label'].'</td>';
						
						$option_val = (isset($values[$col['key_option']]))?$values[$col['key_option']]:0;
						$columns_output .= '<td>
													<input type="hidden" name="fixed_multiple_data['.$name.']['.$index.'][label]" value="'.$col['key_option'].'" />
													<input type="text" name="fixed_multiple_data['.$name.']['.$index.'][value]" value="'.$option_val.'">
											</td>';
					}
					$columns_output .= "</tr>";
					

					$columns_output .= "</table>";
				}
							
				$desc = "";
				$output .= '<br /><br />';
				
				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$columns_output  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$columns_output);

				$output .= '<label for="'. $name .'" class="label_input_multiple">'. $label .'</label>';
				$output .= '<span class="value_view" class="span_input_multiple">'. $columns_output . '</span>'.$star_required.'' . $desc . '<br class="fclear"/>';
			break;
			case 'range_date_selectbox':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				$options_to = (isset($field['options_to']))?$field['options_to']:array('y' => array(),'m' => array(),'d' => array());
				$options_from = (isset($field['options_from']))?$field['options_from']:array('y' => array(),'m' => array(),'d' => array());
				
				$data = array(
				  'name'        => $input_name,
				  'id'          => $id,
				  'value'       => $value,
				  'maxlength'   => $maxlength,
				  'size'        => $size,
				  'style'       => $style,
				  'type'       => $type,
				  'class'		=> $class
				);
				
				$desc = "";
				$error = form_error($input_name);
				
				$options_from_y = '';
				$options_from_m = '';
				$options_from_d = '';
				$options_to_y = '';
				$options_to_m = '';
				$options_to_d = '';
				
				if(is_array($options_from['y']) and count($options_from['y']) > 0)
				{
					foreach($options_from['y'] as $i => $x)
					{
						$options_from_y .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_from['m']) and count($options_from['m']) > 0)
				{
					foreach($options_from['m'] as $i => $x)
					{
						$options_from_m .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_from['d']) and count($options_from['d']) > 0)
				{
					foreach($options_from['d'] as $i => $x)
					{
						$options_from_d .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['y']) and count($options_to['y']) > 0)
				{
					foreach($options_to['y'] as $i => $x)
					{
						$options_to_y .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['m']) and count($options_to['m']) > 0)
				{
					foreach($options_to['m'] as $i => $x)
					{
						$options_to_m .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['d']) and count($options_to['d']) > 0)
				{
					foreach($options_to['d'] as $i => $x)
					{
						$options_to_d .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				$input_form = '	<select name="'.$input_name.'[from][y]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_from_y.'
												</select>
												<select name="'.$input_name.'[from][m]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_from_m.'
												</select>
												<select name="'.$input_name.'[from][d]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_from_d.'
												</select>
												-
												<select name="'.$input_name.'[to][y]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_to_y.'
												</select>
												<select name="'.$input_name.'[to][m]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_to_m.'
												</select>
												<select name="'.$input_name.'[to][d]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_to_d.'
												</select>
											';
				
				#$input_from = form_dropdown($data['name'].'[from]', $options_from['y'], $value_from ,$attribute);
				
				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$output .= '<label for="'. $name .'">'. $label .'</label>';
				if($type == "textarea")
				{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$input_form);
					$output .= '<span class="value_view"> : '. $form . $error . '</span>'.$star_required.'' . $desc . '<br class="fclear"/>';
				}else{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$input_form);
					$output .= '<span class="value_view"> : '. $form . $error . '</span>'.$star_required.'' . $desc . '<br class="fclear"/>';
				}
			break;
			case 'range_year_selectbox':
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				$options_to = (isset($field['options_to']))?$field['options_to']:array('y' => array(),'m' => array(),'d' => array());
				$options_from = (isset($field['options_from']))?$field['options_from']:array('y' => array(),'m' => array(),'d' => array());
				
				$data = array(
				  'name'        => $input_name,
				  'id'          => $id,
				  'value'       => $value,
				  'maxlength'   => $maxlength,
				  'size'        => $size,
				  'style'       => $style,
				  'type'       => $type,
				  'class'		=> $class
				);
				
				$desc = "";
				$error = form_error($input_name);
				
				$options_from_y = '';
				$options_from_m = '';
				$options_from_d = '';
				$options_to_y = '';
				$options_to_m = '';
				$options_to_d = '';
				
				if(is_array($options_from['y']) and count($options_from['y']) > 0)
				{
					foreach($options_from['y'] as $i => $x)
					{
						$options_from_y .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_from['m']) and count($options_from['m']) > 0)
				{
					foreach($options_from['m'] as $i => $x)
					{
						$options_from_m .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_from['d']) and count($options_from['d']) > 0)
				{
					foreach($options_from['d'] as $i => $x)
					{
						$options_from_d .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['y']) and count($options_to['y']) > 0)
				{
					foreach($options_to['y'] as $i => $x)
					{
						$options_to_y .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['m']) and count($options_to['m']) > 0)
				{
					foreach($options_to['m'] as $i => $x)
					{
						$options_to_m .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				if(is_array($options_to['d']) and count($options_to['d']) > 0)
				{
					foreach($options_to['d'] as $i => $x)
					{
						$options_to_d .= '<option value="'.$i.'">'.$x.'</option>';
					}
				}
				$input_form = '	<select name="'.$input_name.'[from][y]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_from_y.'
												</select>
												-
												<select name="'.$input_name.'[to][y]" class="'.$data['class'].'" style="'.$data['style'].'">
													'.$options_to_y.'
												</select>
											';
				
				#$input_from = form_dropdown($data['name'].'[from]', $options_from['y'], $value_from ,$attribute);
				
				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$output .= '<label for="'. $name .'">'. $label .'</label>';
				if($type == "textarea")
				{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$input_form);
					$output .= '<span class="value_view"> : '. $form . $error . '</span>' . $desc . '<br class="fclear"/>';
				}else{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$input_form);
					$output .= '<span class="value_view"> : '. $form . $error . '</span>' . $desc . '<br class="fclear"/>';
				}
			break;
			case 'text':
			case 'password':
			case 'hidden':
			case 'date':
			case 'file':
			case 'textarea':
			case 'tinymce':
			case 'tinymce_simple':
			default :
				$label = (isset($field['label']))?$field['label']:$field;
				$name = (isset($field['name']))?$field['name']:$field;
				$id = (isset($field['id']))?$field['id']:$name;
				$value = (isset($field['value']) and $field['value'] != "" )?$field['value']:$value;
				$maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
				$size = (isset($field['size']))?$field['size']:"";
				$style = (isset($field['style']))?$field['style']:"";
				$class = (isset($field['class']))?$field['class']:"input_".$type;
				$parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
				$input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
				$rules = (isset($field['rules']))?$field['rules']:"";
				$class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
				
				$data = array(
				  'name'        => $input_name,
				  'id'          => $id,
				  'value'       => ($type == "password")?"":$value,
				  'maxlength'   => $maxlength,
				  'size'        => $size,
				  'style'       => $style,
				  'type'       => $type,
				  'class'		=> $class
				);
				
				$desc = "";
				$error = form_error($input_name);
				if($type == "file")
				{
					$this->CI->load->library('upload');
					if(isset($field['config_upload']) and isset($field['config_upload']['allowed_types']) and !empty($field['config_upload']['allowed_types']))
					{
						$format_file = str_replace("|",",",$field['config_upload']['allowed_types']);
						$desc .= "<span class='desc_field'><i>Format file : (".$format_file.")</i></span>";
					}
					$error = $this->CI->upload->display_errors('<p class="alert alert-danger error">','</p>');
				}

				$label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
				$output .= '<label for="'. $name .'">'. $label .'</label>';
				if($type == "textarea")
				{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_textarea($data));
					$output .= '<span class="value_view"> : '. $form . $error . '</span>'.$star_required.'' . $desc . '<br class="fclear"/>';
				}else{
					$form  = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_input($data));
					$output .= '<span class="value_view"> : '. $form . $error . '</span>'.$star_required.'' . $desc . '<br class="fclear"/>';
				}
			break;
		}
		
		$output  = $this->CI->hook->do_action('hook_create_form_field_output_'.$name,$output);
		return $output;
	}
	
	function get_value($table = "",$field = "", $where = "",$default = "",$custom_query = "")
	{
		if(empty($table) or empty($field) or empty($where))
			return $default;
		  
		$query = "";
		
		if(!empty($custom_query))
		{
		  $the_field = "";
		  if(is_array($field) and count($field) > 0)
		  {
			foreach($field as $i => $f)
			{
			  $the_field .= " $i = '".$f."',";
			}
		  }else{
			$the_field = $field . ", ";
		  }
		  $the_where = "";
		  if(is_array($where) and count($where) > 0)
		  {
			foreach($where as $i => $f)
			{
			  $the_where .= " AND $i = ".$f."";
			}
		  }else{
			$the_where = $where;
		  }
		  $custom_query = str_replace("SELECT ","SELECT ".$the_field,$custom_query);
		  $query = $this->CI->db->query($custom_query . $the_where);
		}else{
		  $this->CI->db->where($where,"",FALSE);
		  $this->CI->db->select($field,FALSE);
		  $query = $this->CI->db->get($table);
		  
		}
    
		$data = $query->row_array();
		
		return (isset($data['label']))?$data['label']:$default;
	}
	
	
	function create_listing_grid($config = array(),$data = "")
	{
		if($this->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->init_pagination($config);
		
    $data = (!empty($data))?$this->csv_explode($data):"";
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:'search';
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$panel_function = (isset($config['panel_function']))?$config['panel_function']:array("edit","view","delete");
		
		$action = $this->show_panel_allowed("","","",$panel_function);
		
		$col = "";				
		if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
		{
			if(!empty($action))
			{
				$col .= '
						<th align="center" valign="middle" class="no-print bulk_data_all_th noscrolled"><input type="checkbox" name="bulk_data_all" value="1" class="bulk_data_all"/></th>';
			}
			$col .= '<th class="noscrolled">No</th>';
			foreach($this->fields as $index => $field)
			{
				if(isset($field['use_listing']) and $field['use_listing'] == true or (is_array($data) and count($data) > 0))
				{
					$label = (isset($field['label']))?$field['label']:$field;
					$name = (isset($field['name']))?$field['name']:$field;
					#$col .= '<th><a href="#">ID<img src="static/img/icons/arrow_down_mini.gif" width="16" height="16" align="absmiddle" /></a></th>';
					$col .= '
							<th>' . $label . '</th>';
				}
			}
			if(!empty($action))
				$col .= '<th class="action_menu_col">Action</th>';
		}
		
		$col  = $this->CI->hook->do_action('hook_create_listing_grid_cols',$col);
		
		$list = "";	
		$is_listing = $this->CI->uri->segment($this->paging_config['uri_segment']-1,'');
		
		$start_page = 0;
		if($is_listing == 'listing')
		$start_page = $this->CI->uri->segment($this->paging_config['uri_segment'],0);
		$this->data_rows = array();
    
    if(empty($data))
    {
      $start_page  = $this->CI->hook->do_action('hook_create_listing_grid_start_page',$start_page);
      $to_page  = $this->CI->hook->do_action('hook_create_listing_grid_to_page',$this->paging_config['per_page']);
      
      $sql = $this->get_query() . ' LIMIT ' . $start_page . ',' . $to_page;
      $query = $this->CI->db->query($sql);
      $data_rows = $query->result_array();
      if($query->num_rows() > 0)
      {
        $this->data_rows = $data_rows;
      }
    }else{
        $this->data_rows = $data;
    }
    
    if(is_array($this->data_rows) and count($this->data_rows) > 0)
    {
      foreach($this->data_rows as $index => $data_row)
      {
        $strcheckrow = implode('',$data_row);
        if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0 and is_array($data_row) and count($data_row) > 0 and trim($strcheckrow) != '')
        {
          $tmp_list = "";
          $tmp_list.= '
                <tr>';
          if(!empty($action))
          {
            $id_bulk_data = (isset($data_row[$this->primary_key]))?$data_row[$this->primary_key]:"";
            $tmp_list.= '
                  <td align="center" valign="middle" class="no-print noscrolled"><input type="checkbox" name="bulk_data[]" value="'.$id_bulk_data.'" class="bulk_data"/></td>';
          }
            $tmp_list.= '
                  <td align="center" class="noscrolled">' . ++$start_page .'</td>';
          $no_field = 0;
          foreach($this->fields as $index_field => $field)
          {
            if(isset($field['use_listing']) and $field['use_listing'] == true or (is_array($data) and count($data) > 0))
            {
              $name = (isset($field['name']))?$field['name']:$field;
              $value = (isset($data_row[$name]))?$data_row[$name]:"";
              if(!empty($data))
              {
                $value = (isset($data_row[$no_field]))?$data_row[$no_field]:"";
              }
              if(isset($field['table']) and !empty($field['table']))
              {
                $label = (isset($field['primary_key']))?$field['primary_key']:"";
                if(empty($label))
                {
                  $label = 'label';
                }
                $where = "";
                if(isset($field['value']) and $label)
                  $where = array($label => "'".$value."'");
                  
                $value = $this->get_value($field['table'],$field['select'],$where,$value);
                
                if(!empty($data))
                {
                  $value = (isset($data_row[$no_field]))?$data_row[$no_field]:"";
                }
                
                if($field['type'] == 'file')
                {
                  $path = (	isset($field['config_upload']) and 
                            isset($field['config_upload']['upload_path']) and 
                            !empty($field['config_upload']['upload_path']))
                          ?$field['config_upload']['upload_path']
                          :"";
                  $path_value = $path . $value;
                  $is_image = $this->is_image($path_value);
                  if($is_image)
                  {
                    $path = str_replace(getcwd().'/',base_url(),$path_value);
                    $value = $this->show_image($path,'height="50"');
                  }
                }
                
                
                if(!empty($data))
                {
                  $value = (isset($data_row[$no_field]))?$data_row[$no_field]:"";
                }
                
                if($field['type'] == 'date' or $field['type'] == 'datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
                {
                  $value = $this->human_date($value,false,false);
                }
                  
                if(!empty($data))
                {
                  $value = (isset($data_row[$no_field]))?$data_row[$no_field]:"";
                }
                $list_style = (isset($field['list_style']))?$field['list_style']:"";
                $value  = $this->CI->hook->do_action('hook_create_listing_grid_value_'.$name,$value);
                $tmp_list.= '
                      <td '.$list_style.'>' . $this->create_form_field($field,$value,false,true) . '</td>';
              }else{
                
                if($field['type'] == 'file')
                {
                  $path = (	isset($field['config_upload']) and 
                            isset($field['config_upload']['upload_path']) and 
                            !empty($field['config_upload']['upload_path']))
                          ?$field['config_upload']['upload_path']
                          :"";
                  $path_value = $path . $value;
                  $is_image = $this->is_image($path_value);
                  if($is_image)
                  {
                    $path = str_replace(getcwd().'/',base_url(),$path_value);
                    $value = $this->show_image($path,'height="50"');
                  }
                }
                if($field['type'] == 'date' or $field['type'] == 'datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
                {
                  $value = $this->human_date($value,false,false);
                }
                
                if($field['type'] == 'fixed_multiple_field' || $field['type'] == 'fixed_multiple_field_text'){
                  $decoded_value = json_decode($value, true);
                  
                  if(is_array($decoded_value) and count($decoded_value) > 0){
                    $value = "<ul style='padding:10px;'>";
                    foreach($decoded_value as $idx => $dv){
                      $label = $this->get_label_value($dv['label'], $field['columns']);
                      $value .= "<li><span>{$label} :</span> <span class='value_data'>{$dv['value']}</span> </li>";
                    }
                    $value .= "</ul>";
                  }
                }
                
                $list_style = (isset($field['list_style']))?$field['list_style']:"";
                $value  = $this->CI->hook->do_action('hook_create_listing_grid_value_'.$name,$value);
                $tmp_list.= '
                      <td '.$list_style.'>' . $this->create_form_field($field,$value,false,true) . '</td>';
              }
              $no_field++;
            }
          }
          /*
          if(!empty($action))
          {
            $action = $this->show_panel_allowed("","","",$panel_function,$data_row[$this->primary_key]);
            $list_style = (isset($this->the_config['list_style']))?$this->the_config['list_style']:"class='rows_action'";
            $action  = $this->CI->hook->do_action('hook_create_listing_grid_action',$action);
            $tmp_list.= '<td '.$list_style.'>'. $action .'</td>';
          }
          */
          $tmp_list.= '<td '.$list_style.'>&nbsp;</td>';
          $tmp_list.= '
                </tr>';
          $list  .= $this->CI->hook->do_action('hook_create_listing_grid_rows',$tmp_list);
        }
      }
    }
    
    $button_action = '<div class="row grid_action">
          <div class="col-lg-3"><div class="input-group">&nbsp;</div></div>
				  <div class="col-lg-6">
						<button name="save" id="save_import" value="Save Data" maxlength="" size="" style="" class=" required btn btn-primary btn-block">Save Data</button>
						<a href="'.current_url().'#import" onclick="window.location.reload(true);" id="back" value="Back" class="btn btn-primary btn-block">Back</a>
				  </div>
          <div class="col-lg-3"><div class="input-group">&nbsp;</div></div>
				</div>';
    $output = '
        <div class="table-responsive listing_grid"><table class="table table-striped  table-hover">
						<thead>
							<tr>
                  ' . $col . '
              </tr>
						</thead>';
		$output .= 	'				
						<tbody>
							'. $list .'
						</tbody>
					</table></div>
          '.$button_action.'';		
		
		$output  = $this->CI->hook->do_action('hook_create_listing_grid_output',$output);
		return $output;
	}
	
	function create_listing($config = array())
	{
		$config = (is_array($config) and count($config) > 0)?$config:$this->config;
		if($this->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->init_pagination($config);
		
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:'search';
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$panel_function = (isset($config['panel_function']))?$config['panel_function']:array("edit","view","delete");
		
		$path = (isset($config['path']))?$config['path']:'';
		$controller = (isset($config['controller']))?$config['controller']:'';
		$function = (isset($config['function']))?$config['function']:'';
		$action = $this->show_panel_allowed("",$path,$controller,$panel_function);
    
		$col = "";				
		if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
		{
			if(!empty($action))
			{
				$col .= '
						<th align="center" valign="middle" class="no-print bulk_data_all_th" width="5"><input type="checkbox" name="bulk_data_all" value="1" class="bulk_data_all"/></th>';
			}
			$col .= '<th>No</th>';
			foreach($this->fields as $index => $field)
			{
				if(isset($field['use_listing']) and $field['use_listing'] == true)
				{
					$label = (isset($field['label']))?$field['label']:$field;
					$name = (isset($field['name']))?$field['name']:$field;
					#$col .= '<th><a href="#">ID<img src="static/img/icons/arrow_down_mini.gif" width="16" height="16" align="absmiddle" /></a></th>';
					$col .= '
							<th>' . $label . '</th>';
				}
			}
			if(!empty($action))
				$col .= '<th class="action_menu_col">Action</th>';
		}
		
		$col  = $this->CI->hook->do_action('hook_create_listing_cols',$col);
		
		$list = "";	
		$is_listing = $this->CI->uri->segment($this->paging_config['uri_segment']-1,'');
		
		$start_page = 0;
		if($is_listing == 'listing')
		$start_page = $this->CI->uri->segment($this->paging_config['uri_segment'],0);
		
		$start_page  = $this->CI->hook->do_action('hook_create_listing_start_page',$start_page);
		$to_page  = $this->CI->hook->do_action('hook_create_listing_to_page',$this->paging_config['per_page']);
		
		$sql = $this->get_query() . ' LIMIT ' . $start_page . ',' . $to_page;
		$query = $this->CI->db->query($sql);

		if($query->num_rows() > 0)
		{
			$data_rows = $query->result_array();
			$this->data_rows = $data_rows;
			foreach($data_rows as $index => $data_row)
			{
				if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
				{
					$tmp_list = "";
					$tmp_list.= '
								<tr>';
					if(!empty($action))
					{
						$id_bulk_data = (isset($data_row[$this->primary_key]))?$data_row[$this->primary_key]:"";
						$tmp_list.= '
									<td align="center" valign="middle" class="no-print"><input type="checkbox" name="bulk_data[]" value="'.$id_bulk_data.'" class="bulk_data"/></td>';
					}
						$tmp_list.= '
									<td align="center">' . ++$start_page .'</td>';
					foreach($this->fields as $index_field => $field)
					{
						if(isset($field['use_listing']) and $field['use_listing'] == true)
						{
							$name = (isset($field['name']))?$field['name']:"";
              $arr_name = explode(".",$name);
              $name = (isset($arr_name[1]))?$arr_name[1]:$name;
							$value = (isset($data_row[$name]))?$data_row[$name]:"";

              /*
							if(isset($field['table']) and !empty($field['table']))
							{
								$label = (isset($field['primary_key']))?$field['primary_key']:"";
								if(empty($label))
								{
									$label = 'label';
								}
								$where = "";
								if(isset($field['value']) and $label)
									$where = array($label => "'".$value."'");
									
								$value = $this->get_value($field['table'],$field['select'],$where,$value);
              }
              */
              if($field['type'] == 'input_file')
              {
                $path_upload = (	isset($field['config_upload']) and 
                          isset($field['config_upload']['upload_path']) and 
                          !empty($field['config_upload']['upload_path']))
                        ?$field['config_upload']['upload_path']
                        :"";
                $path_value = $path_upload . $value;
                $is_image = $this->is_image($path_value);
                if($is_image)
                {
                  $path_upload = str_replace(getcwd().'/',base_url(),$path_value);
                  $value = $this->show_image($path_upload,'height="50"');
                }
              }
              
              if($field['type'] == 'input_date' or $field['type'] == 'input_datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
              {
                $value = $this->human_date($value,false,false);
              }
              
              $list_style = (isset($field['list_style']))?$field['list_style']:"";
              $value  = $this->CI->hook->do_action('hook_create_listing_value_'.$name,$value);
              $tmp_list.= '
                    <td '.$list_style.'>' . $value . '</td>';
						}
					}
					if(!empty($action))
					{
						$the_action = $this->show_panel_allowed("",$path,$controller,$panel_function,$data_row[$this->primary_key]);
						$list_style = (isset($this->the_config['list_style']))?$this->the_config['list_style']:"class='rows_action'";
						$the_action  = $this->CI->hook->do_action('hook_create_listing_action',$the_action);
						$tmp_list.= '<td '.$list_style.'>'. $the_action .'</td>';
					}
					$tmp_list.= '
								</tr>';
					$list  .= $this->CI->hook->do_action('hook_create_listing_rows',$tmp_list);
				}
			}
		}
				
        $output = '<table width="100%" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
                ' . $col . '
              </tr>
						</thead>';
		$output .= 	'				
						<tbody>
							'. $list .'
						</tbody>
					</table>';		
		
		$output  = $this->CI->hook->do_action('hook_create_listing_output',$output);
		return $output;
	}
	
	function create_listing_report($config = array())
	{
		if($this->is_pagination_init == false or (is_array($config) and count($config) > 0))
			$this->init_pagination($config);
		
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:'search';
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		$panel_function = (isset($config['panel_function']))?$config['panel_function']:array("edit","view","delete");
		
		$action = $this->show_panel_allowed("","",$panel_function);
		
		$col = "";				
		if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
		{
			$col .= '<th width="5%">No</th>';
			foreach($this->fields as $index => $field)
			{
				if(isset($field['use_listing']) and $field['use_listing'] == true)
				{
					$label = (isset($field['label']))?$field['label']:$field;
					$name = (isset($field['name']))?$field['name']:$field;
					#$col .= '<th><a href="#">ID<img src="static/img/icons/arrow_down_mini.gif" width="16" height="16" align="absmiddle" /></a></th>';
					$col .= '<th>' . $label . '</th>';
				}
			}
		}
		
		$col  = $this->CI->hook->do_action('hook_create_listing_report_cols',$col);
		
		$list = "";	
		$is_listing = $this->CI->uri->segment($this->paging_config['uri_segment']-1,'');
		
		$start_page = 0;
		if($is_listing == 'listing')
		$start_page = $this->CI->uri->segment($this->paging_config['uri_segment'],0);
		
		$start_page  = $this->CI->hook->do_action('hook_create_listing_report_start_page',$start_page);
		$to_page  = $this->CI->hook->do_action('hook_create_listing_report_to_page',$this->paging_config['per_page']);
		
		$sql = $this->get_query() . ' LIMIT ' . $start_page . ',' . $to_page;
		$query = $this->CI->db->query($sql);

		if($query->num_rows() > 0)
		{
			$data_rows = $query->result_array();
			
			foreach($data_rows as $index => $data_row)
			{
				if(!empty($this->fields) and is_array($this->fields) and count($this->fields) > 0)
				{
					$tmp_list = "";
					$tmp_list.= '
								<tr>';
						$tmp_list.= '
									<td align="center" width="5%">' . ++$start_page .'</td>
								';
					foreach($this->fields as $index_field => $field)
					{
						if(isset($field['use_listing']) and $field['use_listing'] == true)
						{
							$name = (isset($field['name']))?$field['name']:$field;
							$value = (isset($data_row[$name]))?$data_row[$name]:"";
							$multiple_cols = (isset($field['multiple_cols']))?$field['multiple_cols']:"";
							
							if(isset($field['table']) and !empty($field['table']))
							{
								$label = (isset($field['primary_key']))?$field['primary_key']:"";
								if(empty($label))
								{
									$label = 'label';
								}
								$where = "";
								if(isset($field['value']) and $label)
									$where = array($label => "'".$value."'");
									
								$value = $this->get_value($field['table'],$field['select'],$where,$value);
								
								if($field['type'] == 'file')
								{
									$path = (	isset($field['config_upload']) and 
														isset($field['config_upload']['upload_path']) and 
														!empty($field['config_upload']['upload_path']))
													?$field['config_upload']['upload_path']
													:"";
									$path_value = $path . $value;
									$is_image = $this->is_image($path_value);
									if($is_image)
									{
										$path = str_replace(getcwd().'/',base_url(),$path_value);
										$value = $this->show_image($path,'height="50"');
									}
								}
								
								if($field['type'] == 'date' or $field['type'] == 'datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
								{
									$value = $this->human_date($value,false,false);
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->CI->hook->do_action('hook_create_listing_report_value_'.$name,$value);
								$tmp_list.= '
											<td '.$list_style.'>' . $value . '</td>';
							}else{
								
								if($field['type'] == 'file')
								{
									$path = (	isset($field['config_upload']) and 
														isset($field['config_upload']['upload_path']) and 
														!empty($field['config_upload']['upload_path']))
													?$field['config_upload']['upload_path']
													:"";
									$path_value = $path . $value;
									$is_image = $this->is_image($path_value);
									if($is_image)
									{
										$path = str_replace(getcwd().'/',base_url(),$path_value);
										$value = $this->show_image($path,'height="50"');
									}
								}
								if($field['type'] == 'date' or $field['type'] == 'datetime' or (isset($field['class']) and $field['class'] == 'input_date'))
								{
									$value = $this->human_date($value,false,false);
								}
								
								if($field['type'] == 'fixed_multiple_field' || $field['type'] == 'fixed_multiple_field_text'){
									$decoded_value = json_decode($value, true);
									
									if(is_array($decoded_value) and count($decoded_value) > 0){
										$value = "<ul style='padding:10px;'>";
										foreach($decoded_value as $idx => $dv){
											$label = $this->get_label_value($dv['label'], $field['columns']);
											$value .= "<li><span>{$label} :</span> {$dv['value']} </li>";
										}
										$value .= "</ul>";
									}
								}
								
								if($field['type'] == 'multiple'){
									$decoded_value = json_decode($value, true);
									if(is_array($multiple_cols) and count($multiple_cols) > 0)
									{
										$value2 = json_decode($value,true);
										$value = array();
										if(is_array($value2) and count($value2) > 0)
										{
											$no = 0;
											foreach($value2 as $n => $v)
											{
												$val = array();
												foreach($v as $n2 => $v2)
												{
													$value[$n2][$n] = $v2;
												}
											}
										}
										
										$multiple_cols_output = '';

										if(is_array($value) and count($value) > 0)
										{
											$no = 0;
											foreach($value as $i => $vals)
											{
												if(implode("",$vals) == "")
													continue;
												
												$no++;
												foreach($multiple_cols as $index => $col2)
												{
													$multiple_cols_output .= $no;
													$input_multiple = "";
													$attr = (isset($col2['attr']))?$col2['attr']:"";
													$sname = (isset($col2['name']))?$col2['name']:"";
													$val = (isset($vals[$sname]))?$vals[$sname]:"";
													$multiple_cols_output .=  ' | ' . $col2['title'] . ' : ' . $val;
												}
												$multiple_cols_output .= "<br>";
											}
										}
										$value = $multiple_cols_output;
									}
								}
								
								$list_style = (isset($field['list_style']))?$field['list_style']:"";
								$value  = $this->CI->hook->do_action('hook_create_listing_report_value_'.$name,$value);
								$tmp_list.= '
											<td '.$list_style.'>' . $value . '</td>';
							}
						}
					}
					$tmp_list.= '
								</tr>';
					$list  .= $this->CI->hook->do_action('hook_create_listing_report_rows',$tmp_list);
				}
			}
		}
				
        $output = '<table width="100%" cellpadding="0" cellspacing="0" border="1">
						<thead>
							<tr>
                            	' . $col . '
                            </tr>
						</thead>';
		$output .= 	'				
						<tbody>
							'. $list .'
						</tbody>
					</table>';		
		
		$output  = $this->CI->hook->do_action('hook_create_listing_report_output',$output);
		return $output;
	}
	
	function get_total_data($query = "")
	{
		$query = (isset($query) and !empty($query))?$query:$this->get_query();
		if(!empty($query))
		{
			$q = $this->CI->db->query($query);
			return $q->num_rows();
		}
		return 0;
	}
	
	function set_filter($sess_keyname = "")
	{
		if(empty($sess_keyname))
			$sess_keyname = $this->object_name.'_data_filter';
			
		$data_filter = $this->CI->input->post('data_filter');
		if(is_array($data_filter) and count($data_filter) > 0)
		{
			$this->CI->session->set_userdata($sess_keyname,"");
			$this->CI->session->set_userdata($sess_keyname,$data_filter);
		}		
	}
	
	function get_filter($sess_keyname = "",$to_string = false)
	{
		if(empty($sess_keyname))
			$sess_keyname = $this->object_name.'_data_filter';
		
		$filter = $this->CI->session->userdata($sess_keyname);
		
		if($to_string)
		{
			if(is_array($filter) and count($filter) > 0)
			{
				$tmp_filter = "";
				foreach($filter as $index => $fil)
				{
					$field = $this->get_config_field_by("name",$index);
					if($field['type'] == 'selectbox')
					{
						if($fil != "")
							$tmp_filter .= ' AND '.$index.' LIKE "'.$fil.'"';
					}elseif($field['type'] == 'range_date_selectbox')
					{
						if($fil != "")
						{
							$start_date = implode("-",$fil['from']);
							$end_date = implode("-",$fil['to']);
							if(empty($end_date))
								$end_date = $start_date;
								
							$tmp_filter .= ' AND ('.$index.' BETWEEN "'.$start_date.'" AND "'.$end_date.'")';
						}
					}elseif($field['type'] == 'range_year_selectbox')
					{
						if($fil != "")
						{
							$start_date = implode("-",$fil['from']);
							$end_date = implode("-",$fil['to']);
							if(empty($end_date))
								$end_date = $start_date;
								
							$tmp_filter .= ' AND ('.$index.' BETWEEN "'.$start_date.'-01-01" AND "'.$end_date.'-12-31")';
						}
					}else{
						if(!empty($fil))
						$tmp_filter .= ' AND '.$index.' LIKE "%'.$fil.'%"';
					}
				}
				$filter = $tmp_filter;
			}
		}
		
		return $filter;
	}
	
	function get_sort_order()
	{	
		$order_by = (isset($this->the_config['sort_order']))?$this->the_config['sort_order']:"";
		return $order_by;
	}
	
	function get_query($query = "",$sess_keyname = "")
	{
		$filter = $this->get_filter($sess_keyname,true);
		$query = (isset($query) and !empty($query))?$query:$this->query;		
		$append_group_by = (isset($this->the_config['group_by']))?$this->the_config['group_by']:"";
		$append_where = (isset($this->the_config['where']))?$this->the_config['where']:"";
		$append_where_operator = (isset($this->the_config['where_operator']))?$this->the_config['where_operator']:"AND";
    
		if(!empty($append_where))
		{
			if(is_array($append_where) and count($append_where) > 0)
			{
				$append_where_operator = ' ' . $append_where_operator . ' ';
				$tmp_append_where = "";
				foreach($append_where as $i => $w)
				{
					if(!empty($w) and is_array($w) and count($w) > 0)
					{
						$tmp_append_where .= implode($append_where_operator,$w);
					}
				}
				$append_where = $tmp_append_where;
			}else{
				$append_where = $append_where_operator . $append_where;
      }
      
			if(!empty($append_where))
			{
				$append_where = ' '.$append_where.' ';
			}
		}
		$query = $query . $append_where . $filter . $append_group_by ;
		$query .= ($this->get_sort_order() != "")?" ORDER BY " . $this->get_sort_order():"";
		return $query;
	}
	
  function example_data_valid($validation = "")
  {
    $output = "";

    if($validation == "required" OR
      $validation == "isset" OR
      $validation == "alpha")
      {
        $output = "Abcd";
      }
      
    if($validation == "valid_email" OR
      $validation == "valid_emails")
      {
        $output = "abcd@abc.com";
      }
    if($validation == "valid_url")
      {
        $output = "http://abcdefg.com";
      }
    if($validation == "valid_ip")
      {
        $output = "123.123.123.123";
      }
    if($validation == "alpha_numeric")
      {
        $output = "Abcd1234";
      }
    if($validation == "alpha_dash")
      {
        $output = "abcd-efgh";
      }
    if($validation == "numeric" OR 
      $validation == "is_numeric" OR 
      $validation == "integer" OR 
      $validation == "is_natural" OR 
      $validation == "is_natural_no_nol" OR 
      $validation == "decimal" OR 
      $validation == "less_than" OR 
      $validation == "greater_than" OR 
      $validation == "numeric")
      {
        $output = "0123456789";
      }
    if($validation == "valid_date")
      {
        $output = date("Y-m-d");
      }
    
    return $output;
  }
  
	function create_desc_csv_import($csv_fields_used = "")
	{
		if(empty($csv_fields_used))
      return false;
    $this->CI->load->helper('language');
    $this->CI->lang->load('form_validation', $this->CI->config->item('item language'));
    $format_csv = array();
    $example_csv = "";
    $description = "";
    
    if(is_array($csv_fields_used) and count($csv_fields_used) > 0)
    {
      foreach($csv_fields_used as $i => $v)
      {
        $example = "";
        $format_csv[] = $v['name'];
        $rules = explode("|",$v['rules']);
        
        foreach($rules as $i2 => $lang)
        {
          $rule = str_replace("callback_","",$lang);
          if(strpos($rule,"callback_") !== false)
          {
              $rule = str_replace("callback_","",$rule);
              $this->CI->validation->add_validate($rule);
          }
          $desc = lang($rule);
          if(!empty($desc))
          {
            $desc = str_replace("%s",$v['label'],$desc);
            $description .= '<strong>'.$desc.'</strong> ';
            $example = $this->example_data_valid($rule);
          }
        }
        $example = ($example == "")?"Abcd":$example;
        $description .= "<br/>";
        $example_csv[] = $example;
      }
    }
    $format_csv = implode(";",$format_csv);
    $example_csv = implode(";",$example_csv);
    
    $output = "<h3>Note</h3>";
    $output .= $description;
    $output .= "Format CSV : <strong>".$format_csv."</strong><br/>";
    $output .= "Contoh CSV : <strong>".$example_csv."</strong><br/>";
    return $output;
	}
	
	function clear_filter($sess_keyname = "")
	{
		if(empty($sess_keyname))
			$sess_keyname = $this->object_name.'_data_filter';
		$this->CI->session->set_userdata($sess_keyname,'');
		$this->CI->session->unset_userdata($sess_keyname);
	}
	
	function show_panel_allowed($user_id = "",$path = "",$controller = "",$panels = array(),$primary_key = "",$ul = true,$li = true)
	{
		$panels_allowed = array();
		$path = (!empty($path))?$path:$this->path_name;
    $path = trim($path,"/");
    $path = "/".$path."/";
    $path = str_replace("//","/",$path);
		$controller = (!empty($controller))?$controller:$this->object_name;
		if(is_array($panels) and count($panels) > 0)
		{
			foreach($panels as $index => $panel)
			{
				$is_allowed = $this->CI->user_access->is_process_allowed($user_id,$path,$controller,$panel);
				if($is_allowed)
				{
					$panels_allowed[] = $panel;
				}
			}
		}
    
		if((is_array($panels_allowed) and count($panels_allowed) == 0) or empty($panels_allowed))
			return "";
		
		$panels_output = "";
		foreach($panels_allowed as $index => $panel)
		{
      $panel_config = (is_array($panel))?$panel:array();
      $panel = (is_array($panel_config) and isset($panel_config['name']))?$panel_config['name']:$panel;
      $panel_class = (is_array($panel_config) and isset($panel_config['class']))?$panel_config['class']:"";
      $panel_title = (is_array($panel_config) and isset($panel_config['title']))?$panel_config['title']:$panel;
      
      $is_active = (trim($this->CI->user_access->get_url($path,$controller,$panel) . '/' . $primary_key,'/') == trim(str_replace('/index.php','',current_url()),'/'))?' active ':'';

			if($li)
			{
				
				$tmp_panels_output  = '<li role="presentation" class="'.$is_active.'"><a role="menuitem" tabindex="-1" href="'. $this->CI->user_access->get_url($path,$controller,$panel) . '/' . $primary_key .'" class="'.$panel_class.' icon glyphicon ajax" data-target-ajax=".ajax_container"> ' . str_replace("_"," ",$panel_title) . '</a></li>';
				$tmp_panels_output  = $this->CI->hook->do_action('hook_show_panel_allowed_panel_' . $path . '_' . $controller . '_' . $panel,$tmp_panels_output);
				$panels_output .= $tmp_panels_output;
			}
			else
			{
				
				$tmp_panels_output  = '<a href="'. $this->CI->user_access->get_url($path,$controller,$panel) . '/' . $primary_key .'" class="'.$panel_class.' '.$is_active.' icon glyphicon ajax" data-target-ajax=".ajax_container"> ' . str_replace("_"," ",$panel_title) . '</a>';
				$tmp_panels_output  = $this->CI->hook->do_action('hook_show_panel_allowed_panel_' . $path . '_' . $controller . '_' . $panel,$tmp_panels_output);
				$panels_output .= $tmp_panels_output;
			}
		}
    
		$output = '
    <div class="btn-group">
        <button type="button" class="btn btn-primary btn-sm">Choose Action</button>
        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
				<ul class="dropdown-menu action_menu dropdown-menu-right" role="menu">
					' . $panels_output . '
				</ul>
    </div>';
    
		if(!$ul)
			$output = $panels_output;
		
		$output  = $this->CI->hook->do_action('hook_show_panel_allowed_output',$output);
		return $output;
	}
	
	function show_bulk_allowed($user_id = "",$path = "",$controller = "",$panels = array(),$primary_key = "",$ul = true)
	{
		$panels_allowed = array();
		$path = (!empty($path))?$path:$this->path_name;
    $path = trim($path,"/");
    $path = "/".$path."/";
    $path = str_replace("//","/",$path);
		$controller = (!empty($controller))?$controller:$this->object_name;
		
		if(is_array($panels) and count($panels) > 0)
		{
			foreach($panels as $index => $panel)
			{
				$is_allowed = $this->CI->user_access->is_process_allowed($user_id,$path,$controller,$panel);
				if($is_allowed)
				{
					$panels_allowed[] = $panel;
				}
			}
		}
		
		if((is_array($panels_allowed) and count($panels_allowed) == 0) or empty($panels_allowed))
			return "";
		
		$panels_output = "";
		foreach($panels_allowed as $index => $panel)
		{
      $panel_config = (is_array($panel))?$panel:array();
      $panel = (is_array($panel_config) and isset($panel_config['name']))?$panel_config['name']:$panel;
      $panel_class = (is_array($panel_config) and isset($panel_config['class']))?$panel_config['class']:"";
      $panel_title = (is_array($panel_config) and isset($panel_config['title']))?$panel_config['title']:$panel;
      
			if($ul)
			{
				
				$tmp_panels_output  = '<option value="'. trim($path,'/') . '/' . $controller . '/' . $panel . '"> ' . str_replace("_"," ",$panel_title) . '</option>';
				$tmp_panels_output  = $this->CI->hook->do_action('hook_show_bulk_allowed_panel_' . $path . '_' .$controller . '_' . $panel,$tmp_panels_output);
				$panels_output .= $tmp_panels_output;
			}
			else
			{
				
				$tmp_panels_output  = '<option value="'. trim($path,'/') . '/' . $controller . '/' . $panel . '"> ' . str_replace("_"," ",$panel_title) . '</option>';
				$tmp_panels_output  = $this->CI->hook->do_action('hook_show_bulk_allowed_panel_' . $controller . '_' . $panel,$tmp_panels_output);
				$panels_output .= $tmp_panels_output;
			}
		}
		
		$t_nonce = $this->CI->user_access->get_nonce();
		$action = (isset($config['action']))?$config['action']:current_url();
		$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
		
		$is_ajax = $this->CI->hook->do_action('hook_show_bulk_allowed_is_ajax','1');
		$ajax_target = $this->CI->hook->do_action('hook_show_bulk_allowed_ajax_target','.ajax_container');
		$button_action = $this->CI->hook->do_action('hook_show_bulk_allowed_button_action','<div class="input-group-btn"><button type="submit" name="do_bulk_action" class="btn btn-default">GO</button></div>');
		$action = $this->CI->hook->do_action('hook_show_bulk_allowed_action_url',$action);
		$form_target = $this->CI->hook->do_action('hook_show_bulk_allowed_form_target','_self');
		$form_class = $this->CI->hook->do_action('hook_show_bulk_allowed_form_class','form_bulk no_ajax');

		$output = '
				<form method="post" action="'.$action.'" class="'.$form_class.' ajax form-inline" target="'.$form_target.'" data-target-ajax=".ajax_container">
          <div class="input-group">
            <span class="input-group-addon padding0 box bgfff bordernone">
              Bulk Action : &nbsp;
            </span>
            <select name="bulk_action" class="form-control">
              <option value="" align="center"> Choose Action </option>
              ' . $panels_output . '
            </select>
            <span class="input-group-btn">
            '.$button_action.'
            </span>
          </div>
					<input type="hidden" name="nonce" value="'.$nonce.'"/> 
					<input type="hidden" name="ajax_target" value="'.$ajax_target.'"/> 
					<input type="hidden" name="is_ajax" value="'.$is_ajax.'"/> 
					<input type="hidden" name="bulk_data" value="" class="bulk_data_tmp"/> 
					<br class="clearfix"/>
          <br/>
				</form>
				';
		if(!$ul)
			$output = $panels_output;
		
		$output  = $this->CI->hook->do_action('hook_show_bulk_allowed_output',$output);
		return $output;
	}

	function add($data = array(),$config = array())
	{
		$nonce = $this->CI->input->post("nonce");
		if(!empty($nonce))
		{
			$this->CI->load->library('upload');
			$this->CI->load->library('form_validation');
			$data = (is_array($data) and count($data) > 0)?$data:$this->CI->input->post('data');
			$input_multiple = $this->CI->input->post('input_multiple');
			$fixed_multiple_data = $this->CI->input->post('fixed_multiple_data');
			$the_config = $config;
			$config = array();
			
			$type_files = "";
			$rules_files = "";
			$label_files = "";
			$field_files = "";
			if(is_array($the_config) and count($the_config) > 0)
			{
				foreach($the_config as $index => $con)
				{
          $con_name = explode(".",$con['name']);
          $con['name'] = (isset($con_name[1]) and !empty($con_name[1]))?$con_name[1]:$con_name[0];
          
					$config[] = array(
										'field' => 'data['.$con['name'].']',
										'label' => $con['label'],
										'rules' => $con['rules']
									 );
					if($con['type'] == 'input_file')
					{
						$type_files[$con['name']] = array("config" => $con['config_upload']);
						$rules_files[$con['name']] = $con['rules'];
						$label_files[$con['name']] = $con['label'];
						$field_files[$con['name']] = 'data['.$con['name'].']';
					}
				}
			}
			
			$method_name = (!empty($this->method_name))?$this->method_name:'listing';
			$t_nonce = $this->CI->user_access->get_nonce();
			#$action = (isset($config['action']))?$config['action']:base_url().$this->object_name.'/'.$method_name;
			$action = (isset($config['action']))?$config['action']:current_url();
			$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
			
			
			$this->CI->form_validation->set_error_delimiters('<p class="alert alert-danger error">', '</p>');
			$this->CI->form_validation->set_rules($config);
			
			$this->CI->lang->load('upload', $this->CI->config->item('language'));
			$errors = "";
			if(is_array($type_files) and count($type_files) > 0 and empty($errors))
			{
				$error_empty_file = $this->CI->lang->line('upload_no_file_selected');
				$this->files = $this->reconf_file_upload();
				$upload = array();
				foreach($type_files as $input_name => $file)
				{
					$upload = $this->upload($input_name,$file['config']);	
					if(isset($upload['error']))
					{
            $the_file_rules = (isset($rules_files[$input_name]))?explode("|",$rules_files[$input_name]):array();
            
						if(in_array('required',$the_file_rules))
						{
							$errors .= '<strong>'.$label_files[$input_name] . '</strong> : ' . $upload['error'];
						}
					}else{
						$data[$input_name] = $upload['success']['file_name'];
            $_POST['data'][$input_name] = $upload['success']['file_name'];
					}
				}
				
			}
			
			$this->CI->form_validation->run();
      $errors .= validation_errors();
			if(!empty($errors))
      {
        $this->errors = $errors;
				return false;
      }
			
			if(is_array($input_multiple) and count($input_multiple) > 0)
			{
				foreach($input_multiple as $index => $mul)
				{
					if(isset($mul[0]))
						unset($mul[0]);
						
					if(is_array($mul) and count($mul) > 0)
					{
						$data[$index] = json_encode($mul);
					}
				}
			}
			
			if(is_array($fixed_multiple_data) and count($fixed_multiple_data))
			{
				foreach($fixed_multiple_data as $index => $mul)
				{
							
					if(is_array($mul) and count($mul) > 0)
					{
						$data[$index] = json_encode($mul);
					}
				}
				
			}
			
			if(count($data) > 0 and is_array($data))
			{
				$data  = $this->CI->hook->do_action('hook_do_add',$data);
				$query = $this->CI->db->insert($this->table,$data);
				$last_id = $this->CI->db->insert_id();
				$this->CI->hook->do_action('hook_do_after_add',$last_id);
				if($query)
				{
					return '<div class="alert alert-success">Data berhasil disimpan</div>';
				}else{
					return '<div class="alert alert-danger">Data gagal disimpan</div>';
				}
				
			}
		}
	}

	function check_csv($data = array(),$config = array())
	{
    $this->CI->load->library("validation");
		$nonce = $this->CI->input->post("nonce");
		$post_data = $this->CI->input->post("data");
    $data = (empty($data) or count($data) == 0)?$post_data:$data;
    $data = (empty($data) or count($data) == 0)?$_POST:$data;
		if(!empty($data))
		{
			$this->CI->load->library('upload');
			$this->CI->load->library('form_validation');
			$data = (is_array($data) and count($data) > 0)?$data:$this->CI->input->post('data');
			$input_multiple = $this->CI->input->post('input_multiple');
			$fixed_multiple_data = $this->CI->input->post('fixed_multiple_data');
			$the_config = $config;
			$config = array();
			
			$type_files = "";
			$rules_files = "";
			if(is_array($the_config) and count($the_config) > 0)
			{
				foreach($the_config as $index => $con)
				{
          $the_rules = $con['rules'];
          if(strpos($the_rules,"callback_") !== false)
          {
            $the_rules = explode("|",$the_rules);
            foreach($the_rules as $index_rule => $rule)
            {
              if(strpos($rule,"callback_") !== false)
              {
                  $rule = str_replace("callback_","",$rule);
                  $this->CI->validation->add_validate($rule);
              }
            }
          }
          
					$config[] = array(
										'field' => 'data['.$con['name'].']',
										'label' => $con['label'],
										'rules' => $con['rules']
									 );
					if($con['type'] == 'file')
					{
						$type_files[$con['name']] = array("config" => $con['config_upload']);
						$rules_files[$con['name']] = $con['rules'];
					}
				}
			}
			$_POST['data'] = $data;
			$method_name = (!empty($this->method_name))?$this->method_name:'listing';
			$t_nonce = $this->CI->user_access->get_nonce();
			#$action = (isset($config['action']))?$config['action']:base_url().$this->object_name.'/'.$method_name;
			$action = (isset($config['action']))?$config['action']:current_url();
			$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
			
			
			$this->CI->form_validation->set_error_delimiters('<div class="alert alert-danger margin0 padding0">', '</div>');
			$this->CI->form_validation->set_rules($config);
			$this->CI->form_validation->run();
			
			$this->CI->lang->load('upload', $this->CI->config->item('language'));
			
			$errors = validation_errors();
			if(!empty($errors))
      {
				return $errors;
      }
      
      return 1;
			
		}
	}

	function edit($data = array(),$config = array())
	{
		$nonce = $this->CI->input->post("nonce");
		if(!empty($nonce))
		{
			$this->CI->load->library('upload');
			$this->CI->load->library('form_validation');
			$data = (is_array($data) and count($data) > 0)?$data:$this->CI->input->post('data');
			$input_multiple = $this->CI->input->post('input_multiple');
			$fixed_multiple_data = $this->CI->input->post('fixed_multiple_data');
			$the_config = $config;
			$config = array();
			
			$type_files = "";
			$rules_files = "";
			$label_files = "";
			$field_files = "";
			if(is_array($the_config) and count($the_config) > 0)
			{
				foreach($the_config as $index => $con)
				{
          $con_name = explode(".",$con['name']);
          $con['name'] = (isset($con_name[1]) and !empty($con_name[1]))?$con_name[1]:$con_name[0];
          
					$config[] = array(
										'field' => 'data['.$con['name'].']',
										'label' => $con['label'],
										'rules' => $con['rules']
									 );
					if($con['type'] == 'input_file')
					{
						$type_files[$con['name']] = array("config" => $con['config_upload']);
						$rules_files[$con['name']] = $con['rules'];
						$label_files[$con['name']] = $con['label'];
						$field_files[$con['name']] = 'data['.$con['name'].']';
					}
				}
			}
			
			$method_name = (!empty($this->method_name))?$this->method_name:'listing';
			$t_nonce = $this->CI->user_access->get_nonce();
			#$action = (isset($config['action']))?$config['action']:base_url().$this->object_name.'/'.$method_name;
			$action = (isset($config['action']))?$config['action']:current_url();
			$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
			
			
			$this->CI->form_validation->set_error_delimiters('<p class="alert alert-danger error">', '</p>');
			$this->CI->form_validation->set_rules($config);
			
			$this->CI->lang->load('upload', $this->CI->config->item('language'));
			
			$errors = "";
			if(is_array($type_files) and count($type_files) > 0 and empty($errors))
			{
				$error_empty_file = $this->CI->lang->line('upload_no_file_selected');
				$this->reconf_file_upload();
				$upload = array();
				foreach($type_files as $input_name => $file)
				{
					$upload = $this->upload($input_name,$file['config']);
					if(isset($upload['error']))
					{
						if(in_array('required',$the_file_rules))
						{
							$errors .= '<strong>'.$label_files[$input_name] . '</strong> : ' . $upload['error'];
						}
					}else{
						if(isset($upload['success']['file_name']) and !empty($upload['success']['file_name']))
							$data[$input_name] = $upload['success']['file_name'];
            $_POST['data'][$input_name] = $upload['success']['file_name'];
					}
				}
			}
      
			$this->CI->form_validation->run();
      $errors .= validation_errors();
      
			if(!empty($errors))
				return false;
			
			if(is_array($input_multiple) and count($input_multiple) > 0)
			{
				foreach($input_multiple as $index => $mul)
				{
					if(isset($mul[0]))
						unset($mul[0]);
						
					if(is_array($mul) and count($mul) > 0)
					{
						$data[$index] = json_encode($mul);
					}
				}
			}
			
			
			if(is_array($fixed_multiple_data) and count($fixed_multiple_data))
			{
				foreach($fixed_multiple_data as $index => $mul)
				{
							
					if(is_array($mul) and count($mul) > 0)
					{
						$data[$index] = json_encode($mul);
					}
				}
				
			}
			
			if(count($data) > 0 and is_array($data))
			{
				$data  = $this->CI->hook->do_action('hook_do_edit',$data);
				$this->CI->db->where($this->primary_key,$this->primary_key_value);
				$query = $this->CI->db->update($this->table,$data);
				$this->CI->hook->do_action('hook_do_after_edit',$this->primary_key_value);
				if($query)
				{
					return '<div class="alert alert-success">Data berhasil dirubah</div>';
				}else{
					return '<div class="alert alert-danger">Data gagal dirubah</div>';
				}				
			}
		}
	}

	function delete($data = array(),$config = array())
	{
		$nonce = $this->CI->input->post("nonce");
		$bulk_data = $this->CI->input->post("bulk_data");
		if(!empty($bulk_data))
		{
			if(!is_array($bulk_data))
			{
				$bulk_data = explode(",",$bulk_data);
			}
		}
		#if(!empty($nonce))
		#{
				$t_nonce = $this->CI->user_access->get_nonce();
				$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
					
				$the_config = $config;
				$type_files = "";
				if(is_array($the_config) and count($the_config) > 0)
				{
					foreach($the_config as $index => $con)
					{
						$config[] = array(
											'field' => 'data['.$con['name'].']',
											'label' => $con['label'],
											'rules' => $con['rules']
										 );
						if($con['type'] == 'file')
						{
							$type_files[$con['name']] = array("config" => $con['config_upload']);
						}
					}
				}
				
				$query = false;
				
				if(!empty($bulk_data) and is_array($bulk_data) and count($bulk_data) > 0)
				{
					foreach($bulk_data as $index => $idx_value)
					{
						if(!empty($idx_value))
						{
							$this->primary_key_value = $idx_value;
							if(is_array($type_files) and count($type_files) > 0)
							{
								$this->CI->db->where($this->primary_key,$this->primary_key_value);
								$query = $this->CI->db->get($this->table);
								$the_data = $query->row_array();
								foreach($type_files as $index => $row)
								{
									$config_upload = $row['config'];
									if(isset($the_data[$index]) and !empty($the_data[$index]) and file_exists($config_upload['upload_path'] . $the_data[$index]))
									{
										@unlink($config_upload['upload_path'] . $the_data[$index]);
									}
								}
							}
							
							$data  = $this->CI->hook->do_action('hook_do_delete',$this);
							$this->CI->db->where($this->primary_key,$this->primary_key_value);
							$query = $this->CI->db->delete($this->table);
						}
					}
				
				}
				else
				{
					if(is_array($type_files) and count($type_files) > 0)
					{
						$this->CI->db->where($this->primary_key,$this->primary_key_value);
						$query = $this->CI->db->get($this->table);
						$the_data = $query->row_array();
						foreach($type_files as $index => $row)
						{
							$config_upload = $row['config'];
							if(isset($the_data[$index]) and !empty($the_data[$index]) and file_exists($config_upload['upload_path'] . $the_data[$index]))
							{
								@unlink($config_upload['upload_path'] . $the_data[$index]);
							}
						}
					}
					
					$data  = $this->CI->hook->do_action('hook_do_delete',$this);
					$this->CI->db->where($this->primary_key,$this->primary_key_value);
					$query = $this->CI->db->delete($this->table);
          $this->CI->hook->do_action('hook_do_after_delete',$this->primary_key_value);
				}
				
				if($query)
				{
					return '<div class="alert alert-success">Data berhasil dihapus</div>';
				}else{
					return '<div class="alert alert-danger">Data gagal dihapus</div>';
				}
		#}
	}
	
	function set_message_error()
	{
		
			$this->CI->form_validation->set_message('required', 'Your custom message here');
	}

	function upload($input_name = "",$config = array())
	{
		if(empty($input_name))
			return false;
			
		$tconfig = array();
		$tconfig['upload_path'] = './uploads/others/';
		
		$config = array_merge($tconfig,$config);
		
		$this->CI->upload->initialize($config);
		$data = "";
		if ( ! $this->CI->upload->do_upload($input_name))
		{
			$data = array('error' => $this->CI->upload->display_errors('<p class="alert alert-danger error">','</p>'));
		}
		else
		{
			$data = array('success' => $this->CI->upload->data());
		}
		return $data;
	}
	
	function reconf_file_upload()
	{
		$_FILES = (isset($_FILES) and count($_FILES) > 0)?$_FILES:array();
		$files = (isset($_FILES) and count($_FILES) > 0)?$_FILES:array();
		$the_files = array();
			
		if(is_array($files) and count($files) > 0)
		{
				foreach($files as $idx => $file)
				{
					foreach($file as $index => $f)
					{
						foreach($f as $i => $f2)
						{
							$the_files[$i][$index]= $f2;
						}  
					}
				}
			}
			
			if(is_array($the_files) and count($the_files) > 0)
			{
				foreach($the_files as $index => $data)
				{
					$_FILES[$index] = $data;
				}
			}
			
		return $_FILES; 
	}
	
	function show_image($url = "",$attribute = "")
	{
		$output = '<img src="'.$url.'" '.$attribute.' style="padding:5px;border:1px green solid;text-align:center;margin:5px auto;"/>';
		
		$output  = $this->CI->hook->do_action('hook_show_image_output',$output);
		return $output;
	}
	
	function is_image($path)
	{
			$result = false;

			if (is_file($path) === true)
			{
          if(function_exists('getimagesize'))
          {
             $result = (getimagesize($path))?true:false;
          }else 
          {
            if (function_exists('exif_imagetype') === true)
            {
                $result = image_type_to_mime_type(exif_imagetype($path));
            }else if (function_exists('mime_content_type') === true)
            {
                $result = preg_replace('~^(.+);.*$~', '$1', mime_content_type($path));
            }
            else if (function_exists('finfo_open') === true)
            {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                if (is_resource($finfo) === true)
                {
                    $result = finfo_file($finfo, $path);
                }
                finfo_close($finfo);
            }else {
              $type =array('jpg','gif','png');

                foreach($type as $val){
                  $ext = pathinfo($path, PATHINFO_EXTENSION);
                  $ext = strtolower($ext);
                  if(in_array($ext,$type))
                  {
                    $result = true;
                  }
                }
            }
          }
			}

			return $result;
	}
	
	function hari($hari = "")
	{
		$hari2 = array(	"1" => "Senin",
						"2" => "Selasa",
						"3" => "Rabu",
						"4" => "Kamis",
						"5" => "Jum'at",
						"6" => "Sabtu",
						"7" => "Minggu"
						);
		$str_hari = (isset($hari2[$hari]))?$hari2[$hari]:"";
		return $str_hari;
	}
	
	function bulan($int_bulan = "")
	{
		$bulan = array(	"1" => "Januari",
						"2" => "Februari",
						"3" => "Maret",
						"4" => "April",
						"5" => "Mei",
						"6" => "Juni",
						"7" => "Juli",
						"8" => "Agustus",
						"9" => "September",
						"10" => "Oktober",
						"11" => "November",
						"12" => "Desember"
						);
		$str_bulan = (isset($bulan[$int_bulan]))?$bulan[$int_bulan]:"";
		return $str_bulan;
	}
	
	function human_date($the_date = "",$the_time = false, $the_day = false)
	{
		$output = '';
		
		if(!empty($the_date))
		{
			$date = explode(" ",$the_date);
			$time = (isset($date[1]))?$date[1]:"";
			$date = explode("-",$date[0]);
			
			$day  = strtotime($the_date);
			$day  = date("N",$day);
			$day  = $this->hari($day);
			
			$month = (isset($date[1]))?$this->bulan((int)$date[1]):"";
			if($the_day)
				$output .= $day.", ";
			
			$output .= (isset($date[2]))?$date[2]." ":"";
			$output .= (!empty($month))?$month." ":"";
			$output .= (isset($date[0]))?$date[0]." ":"";
			
			if($the_time)
				$output .= $time;
		}
		
		return $output;
	}
	
	function get_config_field_by($by = "name",$value = "")
	{
		$fields = $this->fields;
		if(is_array($fields) and count($fields) > 0)
		{
			foreach($fields as $index => $field)
			{
				if($field[$by] == $value)
					return $field;
			}
		}
		
		return false;
	}
		
	function _get_controllers($type = "all")
	{
		$controllers         = array();
		$controller_paths         = array();
		$allmethods         = array();
		$all         = array();
    $dir                 = APPPATH.'controllers/';
    $controller_files    = scandir_r($dir);
    
    $controllers = get_class_methods_r_path($controller_files,$dir);
    if(is_array($controllers) and count($controllers) > 0)
    {
      foreach($controllers as $i => $c)
      {
        $all[strtolower($c['class_name'])] = strtolower($c['class_name']);
        $allmethods = $allmethods + $c['methods'];
        $controller_paths = $controller_paths + array($c['path']);
      }
    }
		$this->controllers =  $all;
		$this->methods =  $allmethods;
		$this->controller_methods =  $controllers;
		$this->controller_paths =  $controller_paths;
	}
	
	function excerpt($content = "",$length = 300)
	{
		$this->CI->load->helper('text');
		$content = strip_tags($content);
		$content = character_limiter($content,$length);
		$content  = $this->CI->hook->do_action('hook_excerpt_content',$content);
		return $content;
	}
	
	function is_option_selected($decoded_value, $keyoption, $opt){
		if(!is_array($decoded_value))
			return false;
		
		if(count($decoded_value) == 0)
			return false;
		
		foreach($decoded_value as $idx => $dv){
			if($dv['label'] == $keyoption AND $dv['value'] == $opt)
				return true;
		}
		
		return false;
	}
	
	function get_label_value($label_id, $columns){
		if(!is_array($columns)) return $label_id;
		
		foreach($columns as $idx => $column){
			if($column['key_option'] == $label_id)
				return $column['label'];
		}
		
		return $label_id;
	}
	
	function sum_data($data){
		//type data is json
		$data = json_decode($data, true);
		if(empty($data))
			return 0;
			
		$sum = 0;
		foreach($data as $idx => $dt){
			if(isset($dt['value']))
				$sum += $dt['value'];
		}
		
		return $sum;
	}
	
	function get_average_data($data, $total_data = ''){
		//type data is json
		$data = json_decode($data, true);
		if(empty($data))
			return 0;
		
		if(empty($total_data))
			$total_data = count($data);
			
		$sum = 0;
		foreach($data as $idx => $dt){
			if(isset($dt['value']))
				$sum += $dt['value'];
		}
		
		$average = $sum / $total_data;
		
		return $average;
	}
	
	function get_qualification($nilai = 0)
	{
		$output = "";
		if($nilai < 60)
			$output = "Tidak Lulus";
			
		if($nilai >= 60 and $nilai <= 69)
			$output = "Cukup";
			
		if($nilai >= 70 and $nilai <= 84)
			$output = "Baik";
			
		if($nilai >= 85)
			$output = "Baik Sekali";
		
		return $output;
	}
	
	function read_numeric_human($x = 0)
	{
	  $abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
	  if ($x < 12)
		return " " . $abil[$x];
	  elseif ($x < 20)
		return $this->read_numeric_human($x - 10) . "belas";
	  elseif ($x < 100)
		return $this->read_numeric_human($x / 10) . " puluh" . $this->read_numeric_human($x % 10);
	  elseif ($x < 200)
		return " seratus" . $this->read_numeric_human($x - 100);
	  elseif ($x < 1000)
		return $this->read_numeric_human($x / 100) . " ratus" . $this->read_numeric_human($x % 100);
	  elseif ($x < 2000)
		return " seribu" . $this->read_numeric_human($x - 1000);
	  elseif ($x < 1000000)
		return $this->read_numeric_human($x / 1000) . " ribu" . $this->read_numeric_human($x % 1000);
	  elseif ($x < 1000000000)
		return $this->read_numeric_human($x / 1000000) . " juta" . $this->read_numeric_human($x % 1000000);

	}
  
  function csv_explode($data = "",$separator = ";",$col_delimiter = "'",$escape = "\\")
  {
    if(empty($data))
      return "";
    
    $output = array();
    if(!function_exists('str_getcsv'))
    {
      $output = str_getcsv($data,$separator,$col_delimiter,$escape);
    }else{
        
      $data = explode("\n",$data);
      
      if(is_array($data) and count($data) > 0)
      {
        foreach($data as $i => $row)
        {
          $row = explode($separator,$row);
          foreach($row as $r => $d)
          {
            $d = trim($d,$col_delimiter);
            $row[$r] = $d;
          }
          $output[] = $row;
        }
      }
    }
    return $output;
  }
}
