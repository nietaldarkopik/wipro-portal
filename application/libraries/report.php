<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report{

	var $CI;
	
	function Report(){
			$CI =& get_instance();
	}
		
	function innerHTML($node) {
		$doc = $node->ownerDocument;
		$frag = $doc->createDocumentFragment();
		foreach ($node->childNodes as $child) {
			$frag->appendChild($child->cloneNode(TRUE));
		}
		return $doc->saveXML($frag);
	}

	function findSpanColor($node) {
		$pos = stripos($node, "color:");       // ie: looking for style='color: #FF0000;'
		if ($pos === false) {                  //                        12345678911111
			return '000000';                     //                                 01234
		}
		$node = substr($node, $pos);           // truncate to color: start
		$start = "#";                          // looking for html color string
		$end = ";";                            // should end with semicolon
		$node = " ".$node;                     // prefix node with blank
		$ini = stripos($node,$start);          // look for #
		if ($ini === false) return "000000";   // not found, return default color of black
		$ini += strlen($start);                // get 1 byte past start string
		$len = stripos($node,$end,$ini) - $ini; // grab substr between start and end positions
		return substr($node,$ini,$len);        // return the RGB color without # sign
	}

	function findStyleColor($style) {
		$pos = stripos($style, "color:");      // ie: looking for style='color: #FF0000;'
		if ($pos === false) {                  //                        12345678911111
			return '';                           //                                 01234
		}
		$style = substr($style, $pos);           // truncate to color: start
		$start = "#";                          // looking for html color string
		$end = ";";                            // should end with semicolon
		$style = " ".$style;                     // prefix node with blank
		$ini = stripos($style,$start);          // look for #
		if ($ini === false) return "";         // not found, return default color of black
		$ini += strlen($start);                // get 1 byte past start string
		$len = stripos($style,$end,$ini) - $ini; // grab substr between start and end positions
		return substr($style,$ini,$len);        // return the RGB color without # sign
	}

	function findBoldText($node) {
		$pos = stripos($node, "<b>");          // ie: looking for bolded text
		if ($pos === false) {                  //                        12345678911111
			return false;                        //                                 01234
		}
		return true;                           // found <b>
	}

	function findImg($node) {
		$pos = stripos($node, "<img");          // ie: looking for bolded text
		if ($pos === false) {                  //                        12345678911111
			return false;                        //                                 01234
		}
		return true;                           // found <b>
	}

	function download_excel($output, $filename) {
			#header('Content-type: application/vnd.ms-excel');
			#header('Content-Disposition: attachment; filename='.$filename.'.xls');
			#echo $output;
			
						
			#session_start();
			#ini_set("memory_limit", "-1");
			#ini_set("set_time_limit", "0");
			#set_time_limit(0);
			
			$username = 'nietaldarkopik@gmail.com';            // user's name
			$usermail = 'nietaldarkopik@gmail.com';         // user's emailid
			$usercompany = 'initialdesign';    // user's company
			if(!isset($_GET['limit'])) {
				$limit = 100000;
			}else{
				$limit = $_GET['limit'];
			}
			if(!isset($_GET['debug'])) {
				$debug = false;
			}else{
				$debug = true;
				$handle = fopen("Auditlog/exportlog.txt", "w");
				fwrite($handle, "\nDebugging On...");
			}
			if(!isset($output) OR $output == '') {
				echo "<br />Empty HTML Table, nothing to Export.";
				exit;
			}else{
				$htmltable = $output;
			}
			if(strlen($htmltable) == strlen(strip_tags($htmltable)) ) {
				echo "<br />Invalid HTML Table after Stripping Tags, nothing to Export.";
				exit;
			}
			$htmltable = strip_tags($htmltable, "<img><table><tr><th><thead><tbody><tfoot><td><br><br /><b><span>");
			$htmltable = str_replace("<br />", "\n", $htmltable);
			$htmltable = str_replace("<br/>", "\n", $htmltable);
			$htmltable = str_replace("<br>", "\n", $htmltable);
			$htmltable = str_replace("&nbsp;", " ", $htmltable);
			$htmltable = str_replace("\n\n", "\n", $htmltable);
			$htmltable = str_replace("&", "&amp;", $htmltable);
			//
			//  Extract HTML table contents to array
			//
			$dom = new domDocument;
			$dom->loadHTML($htmltable);
			if(!$dom) {
				echo "<br />Invalid HTML DOM, nothing to Export.";
				exit;
			}
			$dom->preserveWhiteSpace = false;             // remove redundant whitespace
			$tables = $dom->getElementsByTagName('table');
			if(!is_object($tables)) {
				echo "<br />Invalid HTML Table DOM, nothing to Export.";
				exit;
			}
			if($debug) {
				fwrite($handle, "\nTable Count: ".$tables->length);
			}
			$tbcnt = $tables->length - 1;                 // count minus 1 for 0 indexed loop over tables
			if($tbcnt > $limit) {
				$tbcnt = $limit;
			}
			//
			//
			// Create new PHPExcel object with default attributes
			//
			require_once (APPPATH . 'libraries/PHPExcel.php');
			$objPHPExcel = new PHPExcel();
			$objPHPExcel->getDefaultStyle()->getFont()->setName('Arial');
			$objPHPExcel->getDefaultStyle()->getFont()->setSize(9);
			$objPHPExcel->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
			$tm = date('YmdHis');
			$pos = strpos($usermail, "@");
			$user = substr($usermail, 0, $pos);
			$user = str_replace(".","",$user);
			$tfn = date("Y_m_d").'_'.$filename.".xlsx";
			//$fname = "AuditLog/".$tfn;
			$fname = $tfn;
			$objPHPExcel->getProperties()->setCreator($username)
										 ->setLastModifiedBy($username)
										 ->setTitle("Automated Export")
										 ->setSubject("Automated Report Generation")
										 ->setDescription("Automated report generation.")
										 ->setKeywords("Exported File")
										 ->setCompany($usercompany)
										 ->setCategory("Export");
			//
			// Loop over tables in DOM to create an array, each table becomes a worksheet
			//
			for($z=0;$z<=$tbcnt;$z++) {
				$maxcols = 0;
				$totrows = 0;
				$headrows = array();
				$bodyrows = array();
				$r = 0;
				$h = 0;
				$rows = $tables->item($z)->getElementsByTagName('tr');
				$totrows = $rows->length;
				if($debug) {
					fwrite($handle, "\nTotal Rows: ".$totrows);
				}
				foreach ($rows as $row) {
						$ths = $row->getElementsByTagName('th');
						if(is_object($ths)) {
							if($ths->length > 0) {
								$headrows[$h]['colcnt'] = $ths->length;
								if($ths->length > $maxcols) {
									$maxcols = $ths->length;
								}
								$nodes = $ths->length - 1;
								for($x=0;$x<=$nodes;$x++) {
									$thishdg = $ths->item($x)->nodeValue;
									$headrows[$h]['th'][] = $thishdg;
									$headrows[$h]['bold'][] = $this->findBoldText($this->innerHTML($ths->item($x)));
									
									$is_img = $this->findImg($this->innerHTML($ths->item($x)));
									if($is_img)
									{										
										if($ths->item($x)->hasAttribute('src')) {
											$src = $ths->item($x)->getAttribute('src');
											$headrows[$h]['img'][] = $src;
										}else{
											$headrows[$h]['img'][] = false;
										}
									}else{
										$headrows[$h]['img'][] = false;
									}
									
									$is_img = $this->findImg($this->innerHTML($ths->item($x)));
									$headrows[$r]['img'][] = false;
									if($is_img)
									{
										$img = $ths->item($x)->getELementsByTagName('img');
										if($img->item(0)->hasAttribute('src')) {
											$src = $img->item(0)->getAttribute('src');
											$headrows[$r]['img'][] = $src;
										}
									}

									if($ths->item($x)->hasAttribute('style')) {
										$style = $ths->item($x)->getAttribute('style');
										$stylecolor = findStyleColor($style);
										if($stylecolor == '') {
											$headrows[$h]['color'][] = $this->findSpanColor($this->innerHTML($ths->item($x)));
										}else{
											$headrows[$h]['color'][] = $stylecolor;
										}
									}else{
										$headrows[$h]['color'][] = $this->findSpanColor($this->innerHTML($ths->item($x)));
									}
									if($ths->item($x)->hasAttribute('colspan')) {
										$headrows[$h]['colspan'][] = $ths->item($x)->getAttribute('colspan');
									}else{
										$headrows[$h]['colspan'][] = 1;
									}
									if($ths->item($x)->hasAttribute('size')) {
										$headrows[$h]['size'][] = $ths->item($x)->getAttribute('size');
									}else{
										$headrows[$h]['size'][] = 12;
									}
									if($ths->item($x)->hasAttribute('rowspan')) {
										$headrows[$h]['rowspan'][] = $ths->item($x)->getAttribute('rowspan');
									}else{
										$headrows[$h]['rowspan'][] = 1;
									}
									if($ths->item($x)->hasAttribute('size')) {
										$headrows[$h]['size'][] = $ths->item($x)->getAttribute('size');
									}else{
										$headrows[$h]['size'][] = 12;
									}
									if($ths->item($x)->hasAttribute('align')) {
										$headrows[$h]['align'][] = $ths->item($x)->getAttribute('align');
									}else{
										$headrows[$h]['align'][] = 'left';
									}
									if($ths->item($x)->hasAttribute('valign')) {
										$headrows[$h]['valign'][] = $ths->item($x)->getAttribute('valign');
									}else{
										$headrows[$h]['valign'][] = 'top';
									}
									if($ths->item($x)->hasAttribute('bgcolor')) {
										$headrows[$h]['bgcolor'][] = str_replace("#", "", $ths->item($x)->getAttribute('bgcolor'));
									}else{
										$headrows[$h]['bgcolor'][] = 'FFFFFF';
									}
								}
								$h++;
							}
						}
				}
				foreach ($rows as $row) {
						$tds = $row->getElementsByTagName('td');
						if(is_object($tds)) {
							if($tds->length > 0) {
								$bodyrows[$r]['colcnt'] = $tds->length;
								if($tds->length > $maxcols) {
									$maxcols = $tds->length;
								}
								$nodes = $tds->length - 1;
								for($x=0;$x<=$nodes;$x++) {
									$thistxt = $tds->item($x)->nodeValue;
									$bodyrows[$r]['td'][] = $thistxt;
									$bodyrows[$r]['bold'][] = $this->findBoldText($this->innerHTML($tds->item($x)));
									
									$is_img = $this->findImg($this->innerHTML($tds->item($x)));
									if($is_img)
									{
										$img = $tds->item($x)->getELementsByTagName('img');
										if($img->item(0)->hasAttribute('src')) {
											$src = $img->item(0)->getAttribute('src');
											$bodyrows[$r]['img'][] = $src;
										}else{
											$bodyrows[$r]['img'][] = false;
										}
									}else{
										$bodyrows[$r]['img'][] = false;
									}
									
									if($tds->item($x)->hasAttribute('style')) {
										$style = $tds->item($x)->getAttribute('style');
										$stylecolor = $this->findStyleColor($style);
										if($stylecolor == '') {
											$bodyrows[$r]['color'][] = $this->findSpanColor($this->innerHTML($tds->item($x)));
										}else{
											$bodyrows[$r]['color'][] = $stylecolor;
										}
									}else{
										$bodyrows[$r]['color'][] = $this->findSpanColor($this->innerHTML($tds->item($x)));
									}
									if($tds->item($x)->hasAttribute('size')) {
										$bodyrows[$r]['size'][] = $tds->item($x)->getAttribute('size');
									}else{
										$bodyrows[$r]['size'][] = 12;
									}
									if($tds->item($x)->hasAttribute('colspan')) {
										$bodyrows[$r]['colspan'][] = $tds->item($x)->getAttribute('colspan');
									}else{
										$bodyrows[$r]['colspan'][] = 1;
									}
									if($tds->item($x)->hasAttribute('rowspan')) {
										$bodyrows[$r]['rowspan'][] = $tds->item($x)->getAttribute('rowspan');
									}else{
										$bodyrows[$r]['rowspan'][] = 1;
									}
									if($tds->item($x)->hasAttribute('align')) {
										$bodyrows[$r]['align'][] = $tds->item($x)->getAttribute('align');
									}else{
										$bodyrows[$r]['align'][] = 'left';
									}
									if($tds->item($x)->hasAttribute('valign')) {
										$bodyrows[$r]['valign'][] = $tds->item($x)->getAttribute('valign');
									}else{
										$bodyrows[$r]['valign'][] = 'top';
									}
									if($tds->item($x)->hasAttribute('bgcolor')) {
										$bodyrows[$r]['bgcolor'][] = str_replace("#", "", $tds->item($x)->getAttribute('bgcolor'));
									}else{
										$bodyrows[$r]['bgcolor'][] = 'FFFFFF';
									}
								}
								$r++;
							}
						}
				}
				
				if($z > 0) {
					$objPHPExcel->createSheet($z);
				}
				$suf = $z + 1;
				$tableid = $filename.$suf;
				$wksheetname = ucfirst($tableid);
				$objPHPExcel->setActiveSheetIndex($z);                      // each sheet corresponds to a table in html
				$objPHPExcel->getActiveSheet()->setTitle($wksheetname);     // tab name
				$worksheet = $objPHPExcel->getActiveSheet();                // set worksheet we're working on
				$style_overlay = array('font' =>
													array('color' =>
														array('rgb' => '000000'),'bold' => false,),
																'fill' 	=>
																		array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'CCCCFF')),
																'alignment' =>
																		array('wrap' => true, 'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
																							 'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP),
																'borders' => array('top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
																									 'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
																									 'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
																									 'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN)),
														 );
				$xcol = '';
				$xrow = 1;
				$usedhdrows = 0;
				$heightvars = array(1=>'42', 2=>'42', 3=>'48', 4=>'52', 5=>'58', 6=>'64', 7=>'68', 8=>'76', 9=>'82');
				for($h=0;$h<count($headrows);$h++) {
					$th = $headrows[$h]['th'];
					$colspans = $headrows[$h]['colspan'];
					$rowspans = $headrows[$h]['rowspan'];
					$aligns = $headrows[$h]['align'];
					$valigns = $headrows[$h]['valign'];
					$bgcolors = $headrows[$h]['bgcolor'];
					$colcnt = $headrows[$h]['colcnt'];
					$colors = $headrows[$h]['color'];
					$sizes = $headrows[$h]['size'];
					$bolds = $headrows[$h]['bold'];
					$imgs = $headrows[$h]['img'];
					$usedhdrows++;
					$mergedcells = false;
					for($t=0;$t<count($th);$t++) {
						if($xcol == '') {$xcol = 'A';}else{$xcol++;}
						$thishdg = $th[$t];
						$thisalign = $aligns[$t];
						$thisvalign = $valigns[$t];
						$thiscolspan = $colspans[$t];
						$thisrowspan = $rowspans[$t];
						$thiscolor = $colors[$t];
						$thissize = $sizes[$t];
						$thisbg = $bgcolors[$t];
						$thisbold = $bolds[$t];
						$thisimg = $imgs[$t];
						$strbold = ($thisbold==true) ? 'true' : 'false';
						$strimg = (!empty($thisimg)) ? $thisimg : false;
						if($thisbg == 'FFFFFF') {
							$style_overlay['fill']['type'] = PHPExcel_Style_Fill::FILL_NONE;
						}else{
							$style_overlay['fill']['type'] = PHPExcel_Style_Fill::FILL_SOLID;
						}
						$style_overlay['alignment']['vertical'] = $thisvalign;              // set styles for cell
						$style_overlay['alignment']['horizontal'] = $thisalign;
						$style_overlay['font']['color']['rgb'] = $thiscolor;
						$style_overlay['font']['bold'] = $thisbold;
						$style_overlay['font']['size'] = $thissize;
						$style_overlay['fill']['color']['rgb'] = $thisbg;
						
						if($strimg)
						{
							$objDrawing = new PHPExcel_Worksheet_Drawing();
							$objDrawing->setWorksheet($worksheet);
							#$objDrawing->setName("name");
							#$objDrawing->setDescription("Description");
							$objDrawing->setPath(str_replace(base_url(),getcwd().'/',$strimg));
							$objDrawing->setCoordinates($xcol.$xrow);
							$objDrawing->setOffsetX(1);
							$objDrawing->setOffsetY(5);
							$objDrawing->setWidth(100);
							$objDrawing->setHeight(150);
						}else{
							$worksheet->setCellValue($xcol.$xrow, $thishdg);
						}
						$worksheet->getStyle($xcol.$xrow)->applyFromArray($style_overlay);
						if($debug) {
							fwrite($handle, "\n".$xcol.":".$xrow." Rowspan:".$thisRowspan." ColSpan:".$thiscolspan." Color:".$thiscolor." Align:".$thisalign." VAlign:".$thisvalign." BGColor:".$thisbg." Bold:".$strbold." cellValue: ".$thishdg);
						}
						if($thiscolspan > 1) {                                                // spans more than 1 column
							$mergedcells = true;
							$lastxcol = $xcol;
							for($j=1;$j<$thiscolspan;$j++) {
								$lastxcol++;
								$worksheet->setCellValue($lastxcol.$xrow, '');
								$worksheet->getStyle($lastxcol.$xrow)->applyFromArray($style_overlay);
							}
							$cellRange = $xcol.$xrow.':'.$lastxcol.$xrow;
							if($debug) {
								fwrite($handle, "\nmergeCells: ".$xcol.":".$xrow." ".$lastxcol.":".$xrow);
							}
							$worksheet->mergeCells($cellRange);
							$worksheet->getStyle($cellRange)->applyFromArray($style_overlay);
							$num_newlines = substr_count($thishdg, "\n");                       // count number of newline chars
							if($num_newlines > 1) {
								$rowheight = $heightvars[1];                                      // default to 35
								if(array_key_exists($num_newlines, $heightvars)) {
									$rowheight = $heightvars[$num_newlines];
								}else{
									$rowheight = 75;
								}
								$worksheet->getRowDimension($xrow)->setRowHeight($rowheight);     // adjust heading row height
							}
							$xcol = $lastxcol;
						}
						if($thisrowspan > 1) {                                                // spans more than 1 column
							$mergedcells = true;
							$lastxcol = $xcol;
							for($j=1;$j<$thisrowspan;$j++) {
								$lastxcol++;
								$worksheet->setCellValue($lastxcol.$xrow, '');
								$worksheet->getStyle($lastxcol.$xrow)->applyFromArray($style_overlay);
							}
							$cellRange = $xcol.$xrow.':'.$xcol.$lastxcol;
							if($debug) {
								fwrite($handle, "\nmergeCells: ".$xcol.":".$xrow." ".$xcol.":".$lastxcol);
							}
							$worksheet->mergeCells($cellRange);
							$worksheet->getStyle($cellRange)->applyFromArray($style_overlay);
							$num_newlines = substr_count($thishdg, "\n");                       // count number of newline chars
							if($num_newlines > 1) {
								$rowheight = $heightvars[1];                                      // default to 35
								if(array_key_exists($num_newlines, $heightvars)) {
									$rowheight = $heightvars[$num_newlines];
								}else{
									$rowheight = 75;
								}
								$worksheet->getRowDimension($xrow)->setRowHeight($rowheight);     // adjust heading row height
							}
							$xcol = $lastxcol;
						}
					}
					$xrow++;
					$xcol = '';
				}
				//Put an auto filter on last row of heading only if last row was not merged
				if(!isset($mergedcells) OR !$mergedcells) {
					$worksheet->setAutoFilter("A$usedhdrows:" . $worksheet->getHighestColumn() . $worksheet->getHighestRow() );
				}
				if($debug) {
					fwrite($handle, "\nautoFilter: A".$usedhdrows.":".$worksheet->getHighestColumn().$worksheet->getHighestRow());
				}
				// Freeze heading lines starting after heading lines
				$usedhdrows++;
				$worksheet->freezePane("A$usedhdrows");
				if($debug) {
					fwrite($handle, "\nfreezePane: A".$usedhdrows);
				}
				//
				// Loop thru data rows and write them out
				//
				$xcol = '';
				$xrow = $usedhdrows;
				$skip_value_col_row = array();
				
				for($b=0;$b<count($bodyrows);$b++) {
					$td = $bodyrows[$b]['td'];
					$colcnt = $bodyrows[$b]['colcnt'];
					$colspans = $bodyrows[$b]['colspan'];
					$rowspans = $bodyrows[$b]['rowspan'];
					$aligns = $bodyrows[$b]['align'];
					$valigns = $bodyrows[$b]['valign'];
					$bgcolors = $bodyrows[$b]['bgcolor'];
					$colors = $bodyrows[$b]['color'];
					$bolds = $bodyrows[$b]['bold'];
					$imgs = $bodyrows[$b]['img'];
					
					for($t=0;$t<count($td);$t++) {
						if($xcol == '') {$xcol = 'A';}else{$xcol++;}
						if(isset($skip_value_col_row[$xcol]) and $skip_value_col_row[$xcol] >= $xrow)
						{
								$xcol++;
						}
						
						$thistext = $td[$t] . " ";
						$thisalign = $aligns[$t];
						$thisvalign = $valigns[$t];
						$thiscolspan = $colspans[$t];
						$thisrowspan = $rowspans[$t];
						$thiscolor = $colors[$t];
						$thisbg = $bgcolors[$t];
						$thisbold = $bolds[$t];
						$thisimg = $imgs[$t];
						
						$strbold = ($thisbold==true) ? 'true' : 'false';
						$strimg = (!empty($thisimg)) ? $thisimg : false;
						
						if($thisbg == 'FFFFFF') {
							$style_overlay['fill']['type'] = PHPExcel_Style_Fill::FILL_NONE;
						}else{
							$style_overlay['fill']['type'] = PHPExcel_Style_Fill::FILL_SOLID;
						}
						$style_overlay['alignment']['vertical'] = $thisvalign;              // set styles for cell
						$style_overlay['alignment']['horizontal'] = $thisalign;
						$style_overlay['font']['color']['rgb'] = $thiscolor;
						$style_overlay['font']['bold'] = $thisbold;
						$style_overlay['fill']['color']['rgb'] = $thisbg;
						if($thiscolspan == 1) {
							$worksheet->getColumnDimension($xcol)->setWidth(25);
						}
						
						if($strimg)
						{
							$objDrawing = new PHPExcel_Worksheet_Drawing();
							$objDrawing->setWorksheet($worksheet);
							#$objDrawing->setName("name");
							#$objDrawing->setDescription("Description");
							$objDrawing->setPath(str_replace(base_url(),getcwd().'/',$strimg));
							$objDrawing->setCoordinates($xcol.$xrow);
							$objDrawing->setOffsetX(1);
							$objDrawing->setOffsetY(1);
							$objDrawing->setWidth($heightvars[7]);
							$objDrawing->setHeight($heightvars[9]);
							$rowheight = $heightvars[9];
							$worksheet->getRowDimension($xrow)->setRowHeight($rowheight);
						}else{
							$worksheet->setCellValue($xcol.$xrow, $thistext);
						}
						
						if($debug) {
							fwrite($handle, "\n".$xcol.":".$xrow." rowspan:".$thisrowspan." ColSpan:".$thiscolspan." Color:".$thiscolor." Align:".$thisalign." VAlign:".$thisvalign." BGColor:".$thisbg." Bold:".$strbold." cellValue: ".$thistext);
						}
						$worksheet->getStyle($xcol.$xrow)->applyFromArray($style_overlay);
						if($thiscolspan > 1) {                                                // spans more than 1 column
							$lastxcol = $xcol;
							for($j=1;$j<$thiscolspan;$j++) {
								$lastxcol++;
							}
							$cellRange = $xcol.$xrow.':'.$lastxcol.$xrow;
							if($debug) {
								fwrite($handle, "\nmergeCells: ".$xcol.":".$xrow." ".$lastxcol.":".$xrow);
							}
							$worksheet->mergeCells($cellRange);
							$worksheet->getStyle($cellRange)->applyFromArray($style_overlay);
							$xcol = $lastxcol;
						}
						
						if($thisrowspan > 1) {                                                // spans more than 1 column
							$lastxrow = $xrow;
							for($j=1;$j<$thisrowspan;$j++) {
								$lastxrow++;
							}
							$skip_value_col_row[$xcol] = $lastxrow;
							$cellRange = $xcol.$xrow.':'.$xcol.$lastxrow;
							if($debug) {
								fwrite($handle, "\nmergeCells: ".$xcol.":".$xrow." ".$xcol.":".$lastxrow);
							}
							$worksheet->mergeCells($cellRange);
							#$worksheet->setCellValue($xcol.$xrow, $thistext);
							$worksheet->getStyle($cellRange)->applyFromArray($style_overlay);
						}
					}
					
					$xrow++;
					$xcol = '';
				}
				// autosize columns to fit data
				$azcol = 'A';
				for($x=1;$x==$maxcols;$x++) {
					$worksheet->getColumnDimension($azcol)->setAutoSize(true);
					$azcol++;
				}
				if($debug) {
					fwrite($handle, "\nHEADROWS: ".print_r($headrows, true));
					fwrite($handle, "\nBODYROWS: ".print_r($bodyrows, true));
				}
			} // end for over tables
			$objPHPExcel->setActiveSheetIndex(0);                      // set to first worksheet before close
			//
			// Write to Browser
			//
			if($debug) {
				fclose($handle);
			}
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header("Content-Disposition: attachment;filename=$fname");
			header('Cache-Control: max-age=0');
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			//$objWriter->save($fname);
			$objWriter->save('php://output');
			exit;
	}
	
	function download_pdf($output, $filename, $page_orientation = '', $pdf_unit = '', $pdf_page_format = '')
	{
		require_once(APPPATH . 'libraries/tcpdf/tcpdf_include.php');
		
		//set format
		$page_orientation = (!empty($page_orientation)) ? $page_orientation : PDF_PAGE_ORIENTATION;
		$pdf_unit = (!empty($pdf_unit)) ? $pdf_unit : PDF_UNIT;
		$pdf_page_format = (!empty($pdf_page_format)) ? $pdf_page_format : PDF_PAGE_FORMAT;

		// create new PDF document
		$pdf = new TCPDF($page_orientation, $pdf_unit, $pdf_page_format, true, 'UTF-8', false);


		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('Nicola Asuni');
		$pdf->SetTitle('TCPDF Example 021');
		$pdf->SetSubject('TCPDF Tutorial');
		$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}

		// ---------------------------------------------------------

		// set font
		$pdf->SetFont('helvetica', '', 9);

		// add a page
		$pdf->AddPage();
		
		$output = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $output);
		$output = preg_replace('#<fieldset(.*?)>(.*?)</fieldset>#is', '', $output);
		$output = preg_replace('#<select(.*?)>(.*?)</select>#is', '', $output);
		#$output = preg_replace('#<button(.*?)>(.*?)</button>#is', '', $output);
		// output the HTML content
		$pdf->writeHTML($output, true, 0, true, 0);

		// reset pointer to the last page
		$pdf->lastPage();

		// ---------------------------------------------------------

		//Close and output PDF document
		$pdf->Output($filename, 'I');
	}
}
