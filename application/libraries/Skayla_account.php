<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Skayla_Account{
  var $CI;
  
  function Skayla_Account(){
    $this->CI =& get_instance();
  }
  
  function add_member($data, $admin=1){
		return $this->register($data, $admin);
	}
	
	function delete_member($user_id){
		return $this->CI->User_model->delete_member($user_id);
	}
	
	function edit_member($data, $user_id){
		
		if(count($data) == 0){
			$this->CI->skayla_message->set_error('front', 'signin_register', 'Must valid user registration data.');
			return FALSE;
		}
		
		extract($data);
		
		$data['password'] = $password1;

		unset($data['username']);
		unset($data['password1']);
		unset($data['password2']);
		unset($data['email']);
		unset($data['meta']);
		
    $user_id = $this->CI->User_model->edit_member($data, $user_id);
    
    return $user_id;
		
	}
  
  function logged_in(){
    if($this->CI->session->userdata('logged_in') === TRUE || $this->CI->session->userdata('logged_in') == 1)
    {
      return TRUE;
    }

    return FALSE;
  }
  
  function is_admin(){
		$user_id = $this->CI->session->userdata('user_id');
		return $this->CI->User_model->is_admin( $user_id );
	}
  
  function get_id(){
		return $this->CI->session->userdata('user_id');
  }
  
  function get_login($user = 0, $use_current = TRUE){
    return $this->CI->User_model->get_login($user, $use_current);
  }
  
	function member_statuses_dropdown($args){
		
		$default_args = array(
			'dd_id' => 'dd_id', 
			'dd_name' => 'dd_name', 
			'dd_default' => '',
			'dd_class' => ''
		);
		
		$args = array_merge($default_args, $args);
		
		extract($args);
		
		$status = array(
					1 => 'Active',
					-1 => 'Suspended'
					
				);
		
		$the_status = '';
		
		$the_status .= '<select name="'.$dd_name.'" id="'.$dd_id.'" class="'.$dd_class.'">';
		
		$the_status .= '<option value="">--Select Member Status--</option>';

		foreach($status as $status_k => $status_v){
			$selected = '';
			if($dd_default == $status_k){
				$selected = 'selected="selected"';	
			}
			$the_status .= '<option '.$selected.' value="'.$status_k.'">'.$status_v.'</option>';
		}
		
		$the_status .= '</select>';
		
		echo $the_status;
	}
  
  function user_status($status){
		switch($status){
			case 1:
			return 'Active';
			break;
			
			case 0:
			return 'Waiting Confirmation';
			break;
			
			case -1:
			return 'Suspended';
			break;
		}
	}
	
  function activate($activation_code){
    $user_id = $this->CI->User_model->activate($activation_code);

    return $user_id;
  }
  
  function status_active($user_id){
		return $this->CI->User_model->status_active($user_id);
	}

  function status_inactive($user_id){
		return $this->CI->User_model->status_inactive($user_id);
	}
  
  function is_exist($args) {
    return $this->CI->User_model->is_exist($args);
  }
  
  function generate_password_code($user_email){
		return $this->CI->User_model->generate_password_code($user_email);
	}
	
  function register_forgot_password($email_address, $code){
		return $this->CI->User_model->register_forgot_password($email_address, $code);
	}
	
	function is_active($user_id){
		return $this->CI->User_model->is_active($user_id);
	}
	
  function login($user, $password){
    $found = TRUE;
    
    if($user == '' || $password == ''){
      return FALSE;
    }

    if(FALSE === ($user_id = $this->CI->User_model->is_exist(array(
				'username' => $user,
				'password' => $password
			)))){
      $found = FALSE;
    }
    
    if(FALSE === $this->is_active($user_id)){
			$found = FALSE;
		}
		
    if($found){
      $this->CI->session->set_userdata('user_id', $user_id);
			$this->CI->session->set_userdata('logged_in', 1);

      return TRUE;
    }
    
    return FALSE;
  }
  
  function gets($args, &$f_users){
    return $this->CI->User_model->gets($args, $f_users);
  }
  
	function get_image($size='full', $profile_id=0){
    if (empty($profile_id) || $profile_id == 0){
      $profile_id = $this->CI->User_model->get_id();
    }
    
		$base_path = $this->CI->config->item('image_base_path');
		$image_thumb_sizes = $this->CI->config->item('image_thumb_sizes');
		
		$user_photo = $this->get_meta('photo', $profile_id);
		
		$profile_image = $user_photo;
		
		if($size != 'full' && array_key_exists($size, $image_thumb_sizes) && !empty($profile_image) && $profile_id != 0){
			
			$rev_size = strrev($size);
			$profile_image_path = str_replace(site_url('uploads') . '/', $base_path, $profile_image);
			$rev_profile_image_path = strrev($profile_image_path);
			
			$parse_ext_profile_image_path = explode('.', $rev_profile_image_path, 2);
			
			$first = strrev($parse_ext_profile_image_path[1]);
			$second = '|'.strrev($rev_size).'.';
			$third = strrev($parse_ext_profile_image_path[0]);
			
			$rebuild_image_name = $first . $second . $third;
			
			$profile_image_url = str_replace($base_path, site_url('uploads') . '/', $rebuild_image_name);
			
			return $profile_image_url;
		}else{
			return (!empty($profile_image))?$profile_image:site_url('uploads/profiles/user-profile.gif');
		}
	}
	
	function get_by_slug($username){
	
		$username = str_replace('%20', ' ', $username);
		
		$args['username'] = $username;
		
		return $this->CI->User_model->is_exist($args);
	}
  
  function get($user_id){
		return $this->CI->User_model->get($user_id);
	}
	
  function register($data=array()) {
		$user_status = 0;
		
		if(count($data) == 0){
			return array('status' => false, 'msg' => 'Terjadi Kesalahan Pada Saat Pendaftaran!');
		}
		
		extract($data);
		
		$username = str_replace(' ', '', $username);
		
		if(!ctype_alnum($username)){
			return array('status' => false, 'msg' => 'Username harus hanya berisi antara huruf dan angka');
		}
		
		if(!isset($username) || empty($username)){
			return array('status' => false, 'msg' => 'Username harus diisi');
		}
		
		if(!isset($password1) || empty($password1)){
			return array('status' => false, 'msg' => 'Password pertama tidak terisi!');
		}
		
		if(!isset($password2) || empty($password2)){
			return array('status' => false, 'msg' => 'Password kedua tidak terisi!');
		}
		
		if($password1 != $password2){
			return array('status' => false, 'msg' => 'Password pertama dan kedua tidak cocok!');
		}
		
    if(strlen($password1) < 8){
			return array('status' => false, 'msg' => 'Password terlalu pendek!');
    }

    if(!preg_match("#[0-9]+#", $password1)){
			return array('status' => false, 'msg' => 'Password harus mengandung karakter huruf');
    }

    if(!preg_match("#[a-zA-Z]+#", $password1)){
			return array('status' => false, 'msg' => 'Password harus mengandung karakter angka');
    }
    
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			return array('status' => false, 'msg' => 'Email tidak valid');
		}
		
		if(!$this->CI->User_model->username_test($username)){
			return array('status' => false, 'msg' => 'Username sudah terdaftar');
		}
		
		if(!$this->CI->User_model->email_test($email)){
			return array('status' => false, 'msg' => 'E-Mail sudah terdaftar');
		}
		
		$data['password'] = $data['password2'];
		
		unset($data['password1']);
		unset($data['password2']);
		
    $user_id = $this->CI->User_model->register($data, 0, '', FALSE);
		
    return array('status' => true, 'msg' => 'Pendaftaran Berhasil, Silahkan Cek Inbox E-Mail Anda dan Aktifasi Pendaftaran Anda.');
  }
  
  function logout(){
    $this->CI->session->sess_destroy();
  }
  
	function validate_forgot_password_code($code){
		return $this->CI->User_model->validate_forgot_password_code($code);
	}
	
  function change_password($change_pass_args){
		$code = '';
		
		extract($change_pass_args);
		
		if(!isset($user_id) || empty($user_id)){
			return array('status' => false, 'msg' => 'User ID tidak terdaftar!');
		}
		
		if(!isset($password1) || empty($password1)){
			return array('status' => false, 'msg' => 'Password pertama tidak terisi!');
		}
		
		if(!isset($password2) || empty($password2)){
			return array('status' => false, 'msg' => 'Password kedua tidak terisi!');
		}
		
		if($password1 != $password2){
			return array('status' => false, 'msg' => 'Password pertama dan kedua tidak cocok!');
		}
		
    if(strlen($password1) < 8){
			return array('status' => false, 'msg' => 'Password terlalu pendek!');
    }

    if(!preg_match("#[0-9]+#", $password1)){
			return array('status' => false, 'msg' => 'Password harus mengandung karakter huruf');
    }

    if(!preg_match("#[a-zA-Z]+#", $password1)){
			return array('status' => false, 'msg' => 'Password harus mengandung karakter angka');
    }
    
    $change_password = $this->CI->User_model->change_password($user_id, $password1, $code);
    if($change_password !== FALSE){
			return array('status' => true, 'msg' => 'Reset password berhasil, silahkan klik link login dibawah.');
		}else{
			return array('status' => false, 'msg' => 'Reset password gagal, silahkan kontak <a href="mailto:support@webinstantpro.com">support@webinstantpro.com</a>');
		}
  }
  
}
