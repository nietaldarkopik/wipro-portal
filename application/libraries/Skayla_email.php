<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Skayla_email{
  var $CI;
  var $email_config;
  var $general_template;
  var $email_sender;

  function Skayla_email(){
    $this->CI =& get_instance();
    
		$default_email_config = array(
			'protocol' => $this->CI->config->item('email_protocol'),
			'smtp_host' => $this->CI->config->item('email_smtp_host'),
			'smtp_user' => $this->CI->config->item('email_smtp_user'),
			'smtp_pass' => $this->CI->config->item('email_smtp_pass'),
			'smtp_port' => $this->CI->config->item('email_smtp_port'),
			'smtp_timeout' => $this->CI->config->item('email_smtp_timeout'),
			'email_sender' => $this->CI->config->item('email_sender'),
			'useragent' => $this->CI->config->item('email_useragent'),
			'mailtype' => $this->CI->config->item('email_mailtype')
		);
		
		$this->email_config = $default_email_config;
		$this->general_template = $this->CI->config->item('email_general_template');
		
		$this->email_sender = 'noreply@webinstantpro.com';
		
  }
  
	function send_email($args=array()){
		
		$default_args = array(
			'to' => $this->email_sender,
			'subject' => 'the-lost-email',
			'content' => 'the-lost-email',
		);
		
		$args = array_merge($default_args, $args);
		
		$template = file_get_contents($this->general_template);
		$template = str_replace('{{content}}', $args['content'], $template);
		
		$this->CI->email->initialize($this->email_config);
		$this->CI->email->from($this->email_config['email_sender'], $this->CI->config->item('site_domain'));
		$this->CI->email->to($args['to']); 
		$this->CI->email->subject($args['subject']);
		$this->CI->email->message($template);

		$this->CI->email->send();
		
		$this->CI->email->clear();

	}
	
	function blogspot($email_args){
		extract($email_args);
		
		$content = file_get_contents('email-templates/blogspot-template.html');
		
		$content = str_replace('{{CONTENT}}', $messege, $content);
		
		$args = array(
			'to' => $email,
			'subject' => $title,
			'content' => $content,
		);	
		
		$this->general_template = 'email-templates/general-blog-template.html';
		
		$this->send_email($args);
	}
	
	function registration_email($registration_data){
		
		extract($registration_data);
		
		$content = file_get_contents('email-templates/registration-template.html');
		
		$activation_key = $user_activation_key;
		$activation_url = '<a href="' . site_url('customer/dashboard/activation/' . $activation_key) . '">Activation Page</a>';
	
		$content = str_replace('{{USER_LOGIN}}', $username, $content);
		$content = str_replace('{{USER_ACTIVATION_URL}}', $activation_url, $content);
		
		$email_args = array(
			'to' => $email,
			'subject' => 'Silahkan Aktifasi Akun Anda',
			'content' => $content,
		);	
		
		$this->send_email($email_args);
		
		$email_args_sys = array(
			'to' => 'support@harganews.com',
			'subject' => 'Silahkan Aktifasi Akun Anda',
			'content' => $content,
		);	
		
		$this->send_email($email_args_sys);
		
	}

	function contact_email($contact_data){
	
		extract($contact_data);
		
		$content = file_get_contents('email-templates/contact-template.html');
		
		$content = str_replace('{{NAME}}', $name, $content);
		$content = str_replace('{{EMAIL}}', $email, $content);
		$content = str_replace('{{PHONE}}', $phone, $content);
		$content = str_replace('{{SUBJECT}}', $subject, $content);
		$content = str_replace('{{DESCRIPTION}}', $description, $content);
		
		$email_admin = $this->CI->skayla_settings->get_option('site_email');
		
		$email_args = array(
			'to' => $email_admin,
			'subject' => 'Contact Email',
			'content' => $content,
		);	
		
		$this->send_email($email_args);
	}
	
	function payment_email($payment_data){
	
		extract($payment_data);
		
		$content = file_get_contents('email-templates/payment-confirm-template.html');
		
		$content = str_replace('{{INVOICE_NUMBER}}', $invoice_number, $content);
		$content = str_replace('{{BANK_NAME}}', $bank_name, $content);
		$content = str_replace('{{BANK_ACCOUNT_NAME}}', $bank_account_name, $content);
		$content = str_replace('{{BANK_ACCOUNT_NUMBER}}', $bank_account_number, $content);
		$content = str_replace('{{PAYMENT_AMOUNT}}', $payment_amount, $content);
		$content = str_replace('{{PAYMENT_DATE_YEAR}}', $payment_date['year'], $content);
		$content = str_replace('{{PAYMENT_DATE_MONTH}}', $payment_date['month'], $content);
		$content = str_replace('{{PAYMENT_DATE_DATE}}', $payment_date['date'], $content);
		$content = str_replace('{{PAYMENT_DESTINATION}}', $payment_destination, $content);		
		
		$email_args = array(
			'to' => $this->email_sender,
			'subject' => 'Payment Confirmation',
			'content' => $content,
		);	
		
		$this->send_email($email_args);
		
	}
	
	function forgot_password_email($forgot_password_data){
		
		extract($forgot_password_data);
		
		$content = file_get_contents('email-templates/forgot-password-template.html');
		
		$content = str_replace('{{RESET_PASS_REQUEST_URL}}', '<a href="' . site_url('customer/dashboard/reset') . '/' . $code . '">Reset Password</a>', $content);
		
		$email_args = array(
			'to' => $email_address,
			'subject' => 'Reset Password Request',
			'content' => $content,
		);	
		
		$this->send_email($email_args);
		
	}

}
