<?php

class Filter_input_autocomplete{
  var $CI;
  var $form_field = "";
  function Filter_input_autocomplete()
  {
		$this->CI =& get_instance();
  }
  
  function config($field = array())
  {
      $the_config = array();
      $output = "";
      $type = "text";
      
      $star_required = (isset($field['star_required']))?$field['star_required']:"";
      $label = (isset($field['label']))?$field['label']:"";
      $name = (isset($field['name']))?$field['name']:"";
      $id = (isset($field['id']))?$field['id']:"";
      $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:"";
      $maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
      $size = (isset($field['size']))?$field['size']:"";
      $style = (isset($field['style']))?$field['style']:"";
      $class = (isset($field['class']))?$field['class']:"";
      $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
      $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
      $rules = (isset($field['rules']))?$field['rules']:"";
      $class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
      $class .= ' form-control';
      $options = (isset($field['options']))?$field['options']:"";
      $query = (isset($field['query']))?$field['query']:"";
      $auto_complete = true;
          
      $param = array();
      $table 	= $field['service_get_json']['table'];
      $where 	= $field['service_get_json']['where'];
      $select	= $field['service_get_json']['select'];
      $primary_key 	= $this->CI->data->primary_key;
      #$value 	= $this->CI->data->primary_key_value;
      $foreign_key 	= $field['service_get_json']['foreign_key'];
      
      $param = array();
      $param['foreign_key'] = $foreign_key;
      $param['table']       = $table;
      $param['where']       = $where;
      $param['select']      = $select;
      $param['value']       = $value;
      
      $object_name = (isset($this->CI->data->object_name) and !empty($this->CI->data->object_name))?$this->CI->data->object_name:$this->CI->uri->segment(2);
      $sess_key = "service_get_json_".$object_name."_".$name;
      
      $where_value_default = ($value != "")?" AND ".$foreign_key."='".$value."' ":"";
      $query = "SELECT ".$select." FROM ".$table." WHERE ".$where;
      
      $value_default = $value;
      if($where_value_default != "")
      {
        $q_value_default = $this->CI->db->query($query . $where_value_default);
        $value_default = $q_value_default->row_array();
        $value_default = (isset($value_default['label']))?$value_default['label']:$value;
      }
      
      $this->CI->session->set_userdata($sess_key,$query);
      $this->CI->session->set_userdata("fk_".$sess_key,$foreign_key);
      $output .= '<script>
                  $(document).ready(function(){
                      $( "#'.$id.'_autocomplete" ).autocomplete({
                        //source: "'.base_url().'services/get_json/key/'.$sess_key.'",
                        source: function( request, respond ) {
                          $.post( "'.base_url().'services/get_json/key/'.$sess_key.'", 
                            { term: request.term },
                            function( data ) 
                            {
                                respond(data);
                            },"json");
                        },
                        change: function( event, ui ) {
                          var curr_val = $( "#'.$id.'_autocomplete" ).val();
                          if(curr_val == "")
                          {
                            $( "#'.$id.'" ).val("");
                          }
                        },
                        minLength: 2,
                        select: function( event, ui ) {
                          $( "#'.$id.'_autocomplete" ).val( ui.item.label );
                          $( "#'.$id.'" ).val( ui.item.value );
                          return false;
                        }
                      });
                  });
                </script>';

      $data = array(
        'name'        => $name.'_autocomplete',
        'id'          => $id.'_autocomplete',
        'value'       => $value_default,
        'maxlength'   => $maxlength,
        'size'        => $size,
        'style'       => $style,
        'type'       => $type,
        'class'		=> $class
      );
      $field_attributes = $data;
      #$field_attributes = array_merge($field_attributes,$data);
      foreach($field_attributes as $i => $o)
      {
        if(is_array($o) and count($o) >  0)
        {
          unset($field_attributes[$i]);
        }
      }
      
      $label = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
      $value = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,$value);
      $form = $this->CI->hook->do_action('hook_create_form_field_value_'.$name,form_input($field_attributes));
      
      $this->form_field = $form;
      
      $output .= '
          <br/>
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="'. $name .'">'. $label .'</label>
              </div>
            </div>
            <div class="col-lg-9">
              <div class="input-group">
                <span class="form-control padding0">
                  '.$form.' 
                  <input type="hidden" name="'.$input_name.'" value="'.$value.'" id="'.$id.'"/>
                </span>
                '.
                (($star_required == 1)?'
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon glyphicon-asterisk"></span>
                </span>':'')
                .
                ((!empty($stardesc_required))?'
                <span class="input-group-addon">
                  <span class="glyphicon glyphicon-warning-sign"></span>
                  '.$stardesc_required.'
                </span>':'')
                .'
                <span class="input-group-addon">
                  <span class="glyphicon">&nbsp;</span>
                </span>
              </div>
              '.form_error($input_name) . '
            </div>
          </div>';
    $this->config_field = array();
    $this->output_field = $output;
  }
  
  function output()
  {
    return $this->output_field;
  }
}

