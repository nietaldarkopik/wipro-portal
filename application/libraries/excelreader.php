<?php

if(!class_exists('PHPExcel'))
{
	require_once(APPPATH."libraries/PHPExcel.php");
	require_once(APPPATH."libraries/PHPExcel/IOFactory.php");
}

#PHPExcel_Reader_IReader

class Excelreader{
	var $CI;
	var $inputfilename;
	var $inputfiletype;
	var $sheetname;
	var $objreader;
	var $objphpexcel;
	
	function __construct(){
		$this->CI =& get_instance();
		$this->objphpexcel = new PHPExcel();
	}
	
	function init($inputfilename = "",$inputFileType = ""){
		
		/**  Identify the type of $inputfilename  **/
		if(empty($inputFileType))
			$inputFileType = PHPExcel_IOFactory::identify($inputfilename);
		
		/**  Create a new Reader of the type that has been identified  **/
		$this->objreader = PHPExcel_IOFactory::createReader($inputFileType);
		$this->inputfilename = $inputfilename;
	}
	
	function alphatonum($letter = "")
	{		
		$upperArr = range('A', strtoupper($letter));

		return (array_search($letter, $upperArr) + 1); 
	}
	
	function numtoalpha($number = "")
	{		
		$upperArr = range('A', 'ZZZZ');

		return $upperArr[$number];
	}
	
	function getRange($row1 = "auto",$row2 = 100,$col1 = "auto",$col2 = "auto", $calculateFormulas = true, $formatData = true, $returnCellRef = false)
	{
		$row1 = ($row1 == "auto")?1:$row1;
		$row2 = ($row2 == "auto")?$this->objphpexcel->setActiveSheetIndex(0)->getHighestRow():$row2;
		$col1 = ($col1 == "auto")?0:$col1;
		$col2 = ($col2 == "auto")?$this->objphpexcel->setActiveSheetIndex(0)->getHighestColumn():$col2;
		
		$col1 = (is_numeric($col1))?$this->numtoalpha($col1):$col1;
		$col2 = (is_numeric($col2))?$this->numtoalpha($col2):$col2;

		$result = array();
		$arrayData = $this->objphpexcel->setActiveSheetIndex(0)->rangeToArray($col1.$row1.':'.$col2.$row2,"", $calculateFormulas, $formatData, $returnCellRef);
		
		if(is_array($arrayData) and count($arrayData) > 0)
		{
			$fields = (isset($arrayData[0]))?$arrayData[0]:array();
			if(isset($arrayData[0]))
				unset($arrayData[0]);
			if(count($fields) > 0)
			{
				foreach($arrayData as $i => $d)
				{
					$the_row = array();
					if(is_array($d) and count($d) > 0)
					{
						foreach($d as $i2 => $v)
						{
							if(isset($fields[$i2]))
							{
								$v = (is_date($v))?date("Y-m-d",strtotime($v)):$v;
								$the_row[$fields[$i2]] = $v;
							}
						}
						$result[] = $the_row;
					}
				}
			}
		}
		return $result;
	}
	
	function getValue($col = 0,$row = 0)
	{
		$value = $this->objphpexcel->getActiveSheet()->getCellByColumnAndRow($col,$row)->getValue();	
		return $value;
	}
	
	function setLoadSheetsOnly($sheetname = false)
	{
		if($sheetname !== false)
		{
			/**  Advise the Reader of which WorkSheets we want to load  **/ 
			$this->objreader->setLoadSheetsOnly($sheetname); 
		}else{
			$this->objreader->setLoadAllSheets(); 
		}
		$this->sheetname = $sheetname;
	}
	
	function load()
	{	
		/**  Load $inputfilename to a PHPExcel Object  **/
		if(empty($this->inputfilename))
			return $this->_error_string(2);
		
		$this->objphpexcel = $this->objreader->load($this->inputfilename);
	}
	
	function _error_string($error_number = 1)
	{
		$errors[0] = "Something wrong";
		$errors[1] = "";
		$errors[2] = "File name empty";
		
		return $errors;
	}
}
