<?php

class Lib_inbox{
  var $list_sent_message;
  var $recieved_message;
  var $unread_message;
  var $total_message;
  var $total_unread_message;
  var $item_perpage = 20;
  var $page = 1;
  var $logged_in_user;
  var $upload_path = './uploads/inbox_attachment/';
  var $upload_url;

  function Lib_inbox(){
    $this->CI =& get_instance();

    $this->upload_url = base_url('uploads/inbox_attachment/');
    $this->logged_in_user = $this->CI->user_access->current_user_id;
    $this->catch_unread_message();
  }

  function get_sent_mail()
  {
    
    
  }

  function send_mail($penerima = array(), $judul_pesan = '', $pesan = '')
  {

    if(empty($penerima) || empty($judul_pesan) || empty($pesan))
      return array('success' => false, 'message' => 'data tidak lengkap');

    $this->CI->db->trans_begin();
    
    //insert data ke table inbox
    $insert_inbox = array(
                      'pengirim' => $this->CI->user_access->current_user_id,
                      'judul_pesan' => $judul_pesan,
                      'pesan' => $pesan,
    );
    if(! $this->CI->db->insert('inbox', $insert_inbox)){
      //rollback, kemudian return error
      $this->CI->db->trans_rollback();
      return array('success' => false, 'message' => 'gagal mengirim pesan');
    }

    $inbox_id = $this->CI->db->insert_id();

    //upload lampiran (if any)
    $files = $_FILES;
    $cpt = count($_FILES['lampiran']['name']);
    $may_upload = false;
    foreach($_FILES['lampiran']['name'] as $nm){
      if(!empty($nm)) $may_upload = true;
    }
    if ( $cpt > 0 && $may_upload === true) {
      $config_upload['upload_path'] = $this->upload_path;
      $config_upload['allowed_types'] = 'gif|jpg|png|csv|xls|xlsx|pdf|txt|doc|docx|xlsx|word';
      $config_upload['max_size'] = '10000'; // 10MB
      $config_upload['max_width'] = '5000';
      $config_upload['max_height'] = '5000';
      $config_upload['encrypt_name'] = true;
      $this->CI->load->library('upload', $config_upload);

      for($i=0; $i<$cpt; $i++)
      {
        $_FILES['userfile']['name'] = $files['lampiran']['name'][$i];
        $_FILES['userfile']['type'] = $files['lampiran']['type'][$i];
        $_FILES['userfile']['tmp_name'] = $files['lampiran']['tmp_name'][$i];
        $_FILES['userfile']['error'] = $files['lampiran']['error'][$i];
        $_FILES['userfile']['size'] = $files['lampiran']['size'][$i];    

        if(empty($_FILES['userfile']['name'])) continue;

        //$this->upload->initialize($config_upload);
        if(! $this->CI->upload->do_upload())
        {
          //jika gagal, rollback, kemudian return error
          $this->CI->db->trans_rollback();
          $error_message = $this->CI->upload->display_errors('', '');
          return array('success' => false, 'message' => 'gagal mengirim lampiran : '. $error_message);
        }
        else
        {

          //masukkan data lampiran ke database
          $uploaded_info = $this->CI->upload->data();
          $insert_inbox_lampiran = array(
              'inbox_id' => $inbox_id,
              'lampiran_file' => $uploaded_info['file_name'],
            );
          if( ! $this->CI->db->insert('inbox_lampiran', $insert_inbox_lampiran)){
            //jika gagal, rollback, kemudian return error
            $this->CI->db->trans_rollback();
            return array('success' => false, 'message' => 'gagal mengirim lampiran');

          }
        }
      }
    }

    //kirim pesan pada penerima
    if(is_array($penerima))
    {
      foreach($penerima as $p)
      {
        $insert_inbox_penerima = array(
          'inbox_id' => $inbox_id,
          'penerima' => $p,
          'diterima_sebagai' => 'penerima'
        );

        if(! $this->CI->db->insert('inbox_penerima', $insert_inbox_penerima)){
          $this->CI->db->trans_rollback();
          return array('success' => false, 'message' => 'gagal mengirim pesan pada penerima');
        }
      }
    }
    else
    {
      $insert_inbox_penerima = array(
        'inbox_id' => $inbox_id,
        'penerima' => $penerima,
        'diterima_sebagai' => 'penerima'
      );

      if(! $this->CI->db->insert('inbox_penerima', $insert_inbox_penerima)){
        $this->CI->db->trans_rollback();
        return array('success' => false, 'message' => 'gagal mengirim pesan pada penerima');
      }
    }

    $this->CI->db->trans_commit();
    return array('success' => true, 'message' => 'pesan berhasil dikirim');
    
  }

  //get list of message in inbox
  function list_message($page = "", $item_perpage = "", $only_unread = false)
  {
    if(empty($page)) $page = $this->page;
    if(empty($item_perpage)) $item_perpage = $this->item_perpage;
    $offset = ($page - 1) * $item_perpage;

    //get data
    $this->CI->db->where('penerima_user_id', $this->CI->user_access->current_user_id);
    if($only_unread) $this->CI->db->where('terbaca', 0);
    
    $this->CI->db->limit($item_perpage, $offset);
    $data = $this->CI->db->get('v_inbox_penerima')->result_array();
    $count_data = count($data);

    //get total data
    $this->CI->db->where('penerima_user_id', $this->CI->user_access->current_user_id);
    if($only_unread) $this->CI->db->where('terbaca', 0);
    $total_data = $this->CI->db->count_all('v_inbox_penerima');
    
    $return = array(
      'data' => $data,
      'count_data' => $count_data,
      'total_data' => $total_data,
      'data_start' => ($offset + 1),
      'data_to' => ($offset + $item_perpage),
    );  

    return $return;

  }

  function catch_unread_message()
  {
    if(!empty($this->logged_in_user))
    {
      $unreads = $this->CI->db->get_where('v_inbox_penerima', array('penerima_user_id' => $this->logged_in_user, 'terbaca' => 0));

      //get total unread message
      $this->total_unread_message = $unreads->num_rows();

      //get unread messages
      $messages = $unreads->result_array();
      if(!empty($messages))
      {
        foreach($messages as $idx => $message)
        {
          $messages[$idx]['lampiran'] = $this->get_lampiran($message['inbox_id']);
        }
      }

      $this->unread_message = $messages;

    }

  }

  function get_detail_message($inboxdet_id)
  {
    $message = $this->CI->db->get_where('v_inbox_penerima', array('inboxdet_id' => $inboxdet_id))->row_array();
    if(!empty($message)){
      $message['lampiran'] = $this->get_lampiran($message['inbox_id']);
    }

    return $message;

  }

  function get_lampiran($inbox_id)
  {
    $data = $this->CI->db->get_where('inbox_lampiran', array('inbox_id' => $inbox_id))->result_array();
    return $data;
  }

  function may_read_message($inboxdet_id)
  {
    if(empty($inboxdet_id)) return false;
    if(empty($this->logged_in_user)) return false;
    
    $check = $this->CI->db->get_where('inbox_penerima', array('inboxdet_id' => $inboxdet_id, 'penerima' => $this->logged_in_user))->num_rows();
    if($check > 0)
      return true;
    else
      return false;

  }

  /*set message as read*/
  function read_message($inboxdet_id)
  {
    //check if user allowed to read it
    $check = $this->may_read_message($inboxdet_id);
    if($check){
      $this->CI->db->where('inboxdet_id', $inboxdet_id);
      $update = $this->CI->db->update('inbox_penerima', array('terbaca' => 1));
      return $update;

    }else{
      return false;
    }

  }

  function may_download_attachment($attachment_id)
  {
    if(empty($attachment_id) || empty($this->logged_in_user)) return false;

    //check apakah yang mendownload adalah penerima atau pengirim email
    $query = "
      SELECT * FROM v_inbox_lampiran WHERE 
      inbox_lampiran_id = {$this->CI->db->escape($attachment_id)} AND 
      (pengirim = {$this->CI->db->escape($this->logged_in_user)} OR 
       penerima = {$this->CI->db->escape($this->logged_in_user)})";
    $check = $this->CI->db->query($query)->num_rows();
    
    if($check > 0)
      return true;
    else
      return false;
  }

   function download_attachment($attachment_id)
  {
    //check if user allowed to read it
    $check = $this->may_download_attachment($attachment_id);
    if($check){
      $get_file = $this->CI->db->get_where('inbox_lampiran', array('inbox_lampiran_id' => $attachment_id))->row_array();
      if(empty($get_file)) return false;

      $check_file_exists = $this->upload_path.$get_file['lampiran_file'];
      if(is_file($check_file_exists)){
        $file_url = $this->upload_url .'/'. $get_file['lampiran_file'];
        header('Content-Type: application/octet-stream');
        header("Content-Transfer-Encoding: Binary"); 
        header("Content-disposition: attachment; filename=\"" . $get_file['lampiran_file'] . "\""); 
        readfile($file_url); // do the double-download-dance (dirty but worky)

      }else{
        return false;
      }


    }else{
      return false;
    }

  }
  
}
