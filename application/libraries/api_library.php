<?php

class Api_library{
	
	var $CI;
	
	function Api_library(){
		$this->CI =& get_instance();
		$this->CI->load->library('skayla_http');
		$this->CI->load->library('user_agent');
	}
	
	function validate_request(){
		if(!isset($_SERVER['HTTP_REFERER'])){
			return FALSE;
		}
		
		$hostname = parse_url($_SERVER['HTTP_REFERER'], PHP_URL_HOST);
		$apikey = $this->CI->input->post('api_key');
		$redirect = $this->CI->input->post('redirect_uri');
		$action = $this->CI->input->post('action');
		
		if(!empty($hostname) && !empty($apikey) && !empty($redirect) && !empty($action)){
			$this->CI->db->select('COUNT(administration_api_access_id) AS is_allowed');
			$this->CI->db->where('hostname', $hostname);
			$this->CI->db->where('apikey', $apikey);
			$g_access = $this->CI->db->get('administration_api_access');
			//var_dump($this->CI->db->last_query());
			$r_access = $g_access->row('is_allowed');
			if($r_access == 1){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
	
	public function redirecting($status=FALSE, $code='unknown_error'){
		$redirect_uri = $this->CI->input->post('redirect_uri');
		$parse_url = parse_url($redirect_uri);
		$query_string = '';
		
		if(isset($parse_url['query'])){
			$redirect_uri = $parse_url['scheme'] . '://' . $parse_url['host'] . $parse_url['path'];
			parse_str($parse_url['query'], $params);
			
			if($status === TRUE){
				$params['status'] = 'success';
				$params['code'] = 'OK';
			}else{
				$params['status'] = 'error';
				$params['code'] = $code;
			}
			
			$query_string = http_build_query($params);
		}else{
			if($status === TRUE){
				$query_string = 'status=success?code=OK';
			}else{
				$query_string = 'status=error?code='.$code;
			}
		}
		redirect($redirect_uri . '?' . $query_string);
	}
	//newsletter =======================================================================================
	public function newsletter_add($email_address){
		$newsletter_is_exists = $this->newsletter_is_exists($email_address);
		if($newsletter_is_exists){
			return FALSE;
		}else{
			$this->CI->db->insert('system_newsletter', array('email' => $email_address));
			return TRUE;
		}
	}
	
	public function newsletter_is_exists($email_address){
		$this->CI->db->select('COUNT(newsletter_id) AS is_exists');
		$this->CI->db->where('email', $email_address);
		$g_exists = $this->CI->db->get('system_newsletter');
		$r_exists = $g_exists->row('is_exists');
		if($r_exists > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	//newsletter =======================================================================================
	//place_order ======================================================================================
	public function place_order_add($args){
		extract($args);
		if(empty($username) || strlen($username) <= 5){
			return array(
				'status' => FALSE,
				'code' => 'place_order_user_bad_username'
			);
		}

		if(empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)){
			return array(
				'status' => FALSE,
				'code' => 'place_order_user_bad_email'
			);
		}
		
		if(empty($password) || strlen($password) < 8 || !preg_match("#[0-9]+#", $password) || !preg_match("#[a-zA-Z]+#", $password)){
			return array(
				'status' => FALSE,
				'code' => 'place_order_user_bad_password'
			);
		}
		
		$place_order_user_is_exists = $this->place_order_user_is_exists($email, $username);
		if($place_order_user_is_exists){
			return array(
				'status' => FALSE,
				'code' => 'place_order_user_exists'
			);
		}else{
			/*register account*/
			$this->CI->db->insert('user_accounts', array(
					'email' => $email,
					'username' => $username,
					'password' => md5($password),
					'user_level_id' => 8,
					'active_status' => 'active'
				)
			);
			$new_user_id = $this->CI->db->insert_id();
			/*add order*/
			$this->CI->db->insert('store_order', array(
					'user_id' => $new_user_id
				)
			);
			$new_order_id = $this->CI->db->insert_id();
			if(isset($products['ids']) && is_array($products['ids']) && count($products['ids']) > 0){
				$product_count = 0;
				$bonus = 0;
				$insert_order_detail = array();
				foreach($products['ids'] as $product_id){
					$detail = $this->get_product($product_id);
					$product_price = $detail->product_price;
					$cycle = $products['cycle'][$product_count];
					$domain = $products['domain'][$product_count]; 
					$insert_order_detail[] = array(
						'product_id' => $product_id,
						'order_id' => $new_order_id,
						'price' => $product_price,
						'domain' => $domain,
						'cycle' => $cycle
					);
					/*calculate bonus*/
					$product_cycle = ($cycle / $detail->cycle);
					$product_bonus_type = $detail->bonus_type;
					$product_bonus_amount = $detail->bonus_amount;
					if($product_bonus_type == 'Percentage'){
						$bonus += ($product_cycle * ($product_price / 100 * $product_bonus_amount));
					}elseif($product_bonus_type == 'Fixed'){
						$bonus += ($product_cycle * $product_bonus_amount);
					}
					$product_count++;
				}
				/*add order detail*/
				$this->CI->db->insert_batch('store_order_detail', $insert_order_detail); 
			}
			/*create affiliate chain & bonus*/
			if(!empty($affiliate)){
				$check_master_chain = $this->place_order_user_is_exists($affiliate);
				if($check_master_chain){
					/*create chain*/
					$parent_user_id = $this->get_user($affiliate);
					$this->CI->db->insert('store_affiliate', array(
							'parent_user_id' => $parent_user_id->user_id,
							'child_user_id' => $new_user_id
						)
					);
					/*create bonus*/
					$this->CI->db->insert('store_bonus', array(
							'user_id' => $parent_user_id->user_id,
							'order_id' => $new_order_id,
							'amount_bonus' => $bonus
						)
					);
				}
			}
			return array(
				'status' => TRUE,
				'code' => 'OK'
			);
		}
	}
	
	public function place_order_user_is_exists($email, $username=''){
		$this->CI->db->select('COUNT(user_id) AS is_exists');
		$this->CI->db->where('email', $email);
		if(!empty($username)){
			$this->CI->db->where('username', $username);
		}
		$g_exists = $this->CI->db->get('user_accounts');
		$r_exists = $g_exists->row('is_exists');
		if($r_exists > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	//place_order ======================================================================================
	//products =========================================================================================
	public function get_products(){
		$g_products = $this->CI->db->get('store_products');
		$r_products = $g_products->result();
		return $r_products;
	}
	//products =========================================================================================
	//product ==========================================================================================
	public function get_product($product_id){
		$this->CI->db->where('product_id', $product_id);
		$g_product = $this->CI->db->get('store_products');
		$r_product = $g_product->row();
		return $r_product;
	}
	//product ==========================================================================================  
	//payment_methods ==================================================================================
	public function get_payment_methods(){
		$g_payment_methods = $this->CI->db->get('store_payment_methods');
		$r_payment_methods = $g_payment_methods->result();
		return $r_payment_methods;
	}
	//payment_methods ==================================================================================
	//payment_method ===================================================================================
	public function get_payment_method($payment_method_id){
		$this->CI->db->where('payment_method_id', $payment_method_id);
		$g_payment_method = $this->CI->db->get('store_payment_methods');
		$r_payment_method = $g_payment_method->row();
		return $r_payment_method;
	}
	//payment_method ===================================================================================
	//product ==========================================================================================
	public function get_user($email){
		$this->CI->db->where('email', $email);
		$g_user = $this->CI->db->get('user_accounts');
		$r_user = $g_user->row();
		return $r_user;
	}
	//product ========================================================================================== 
}
