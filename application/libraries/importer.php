<?php

class Importer{
	
	var $CI;
	var $configs = array();
	
	
	function Importer(){
		$this->CI =& get_instance();
		$this->CI->load->library("hook");
		$this->CI->load->library("excelreader");
	}
	
	function set_config($configs = array())
	{
		$this->configs = $configs;
	}
	
	function upload($input_name = "import_data",$config = array())
	{
		if(empty($input_name))
			return false;
		
		$this->CI->load->library("upload");
		$tconfig = array();
		$tconfig['upload_path'] = './uploads/importer/';
		
		$config = array_merge($tconfig,$config);
		$this->CI->upload->initialize($config);
		$data = "";
		if ( ! $this->CI->upload->do_upload($input_name))
		{
			$data = array('error' => $this->CI->upload->display_errors('<p class="alert alert-danger error">','</p>'));
		}
		else
		{
			$data = array('success' => $this->CI->upload->data());
		}
		return $data;
	}
	
	function upload_file_form($configs = array())
	{
		$path = (isset($config['path']) and !empty($config['path']))?$config['path']:$this->CI->uri->segment(1);
		$controller = (isset($config['controller']) and !empty($config['controller']))?$config['controller']:$this->CI->uri->segment(2);
		$function = (isset($config['function']) and !empty($config['function']))?$config['function']:$this->CI->uri->segment(3);

		$action 		= base_url().$path.'/'.$controller.'/import_add';
		$container 	= '<div class="row"><div class="col-lg-12"><form method="post" enctype="multipart/form-data" action="'.$action.'">[content]</form></div></div>';
		$container 	= $this->CI->hook->do_action('importer_upload_file_form_container',$container);
		
		$upload_text = 'Upload file:';
		$upload_text = $this->CI->hook->do_action('importer_upload_file_upload_text',$upload_text);
		
		$form_uploader = '<div class="row">
												<div class="col-lg-3">
													<div class="input-group">
														<label for="upload_file_import">'.$upload_text.'</label>
													</div>
												</div>
												<div class="col-lg-9">
													<div class="form-group">
														<input name="upload_file_import" type="file" id="upload_file_import">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<button name="import_data" type="submit" id="import_data" class="text-center btn btn-primary btn-block">Upload File</button>
												</div>
											</div>';
		$output = str_replace("[content]",$form_uploader,$container);
		return $output;
	}
	
	function check_type_file()
	{
		
	}
	
	function check_file()
	{
		
	}
	
	function parsing_file($inputfilename = "",$inputFileType = "")
	{
		#$inputfilename = 
		$this->CI->excelreader->init($inputfilename,$inputFileType);
		$this->CI->excelreader->load($inputfilename);
		$getdata = 	$this->CI->excelreader->getRange("1","auto","auto","auto");
		return $getdata;
	}
	
	function import_update()
	{
		
	}
	
  function do_insert($data = array(),$configs = array(),$data_inserted = array(),$foreign_key = array())
  {
    $data_rows = array();
    $field_configs = (isset($configs['fields_insert']))?$configs['fields_insert']:array();
    
    if(is_array($field_configs) and count($field_configs) > 0 and is_array($data) and count($data) > 0)
    {
      $data_insert = array();
      foreach($field_configs as $ic => $rc)
      {
        if(isset($rc['label']) and isset($data[$rc['label']]))
        {
          $data_insert[$rc['name']] = $data[$rc['label']];
        }
      }
      
      $data_insert = $this->CI->hook->do_action('hook_importer_do_insert_data_before_insert_'.$configs['table'],$data_insert);

      $q = $this->CI->db->insert($configs['table'],$data_insert);
      $last_insert_id = $this->CI->db->insert_id();
      
      $this->CI->db->where(array($configs['primary_key'] => $last_insert_id));
      $q_get_inserted = $this->CI->db->get($configs['table']);
      $data_inserted[$configs['table']]  = $q_get_inserted->row_array();
      
      $this->CI->hook->do_action('hook_importer_do_insert_data_after_insert_'.$configs['table'],$data_inserted);

      if(isset($configs['sub_table']))
      {
        $this->do_insert($data,$configs['sub_table'],$data_inserted,$configs['sub_table']['foreign_key']);
      }
    }
  }
  
	function import_add($full_path = "",$configs = array())
	{
		$data = $this->parsing_file($full_path);
    if(is_array($data) and count($data) > 0)
    {
      $config_table = (isset($configs['config_table']))?$configs['config_table']:array();
      foreach($data as $i => $r)
      {
        $this->do_insert($r,$config_table);
      }
    }
	}
	
	function import_check($full_path = "",$configs = array())
	{
		$data = $this->parsing_file($full_path);
		
    $this->CI->load->library("validation");
		$nonce = $this->CI->input->post("nonce");
		
		if(!empty($data) and is_array($data) and count($data) > 0)
		{
			$this->CI->load->library('upload');
			$this->CI->load->library('form_validation');
			$data = (is_array($data) and count($data) > 0)?$data:$this->CI->input->post('data');
			$input_multiple = $this->CI->input->post('input_multiple');
			$fixed_multiple_data = $this->CI->input->post('fixed_multiple_data');
			$the_config = $config;
			$config = array();
			
			$type_files = "";
			$rules_files = "";
			if(is_array($the_config) and count($the_config) > 0)
			{
				foreach($the_config as $index => $con)
				{
          $the_rules = $con['rules'];
          if(strpos($the_rules,"callback_") !== false)
          {
            $the_rules = explode("|",$the_rules);
            foreach($the_rules as $index_rule => $rule)
            {
              if(strpos($rule,"callback_") !== false)
              {
                  $rule = str_replace("callback_","",$rule);
                  $this->CI->validation->add_validate($rule);
              }
            }
          }
          
					$config[] = array(
										'field' => 'data['.$con['name'].']',
										'label' => $con['label'],
										'rules' => $con['rules']
									 );
					if($con['type'] == 'file')
					{
						$type_files[$con['name']] = array("config" => $con['config_upload']);
						$rules_files[$con['name']] = $con['rules'];
					}
				}
			}
			$_POST['data'] = $data;
			$method_name = (!empty($this->method_name))?$this->method_name:'listing';
			$t_nonce = $this->CI->user_access->get_nonce();
			#$action = (isset($config['action']))?$config['action']:base_url().$this->object_name.'/'.$method_name;
			$action = (isset($config['action']))?$config['action']:current_url();
			$nonce = (isset($config['nonce']))?$config['nonce']:$t_nonce;
			
			
			$this->CI->form_validation->set_error_delimiters('<div class="alert alert-danger margin0 padding0">', '</div>');
			$this->CI->form_validation->set_rules($config);
			$this->CI->form_validation->run();
			
			$this->CI->lang->load('upload', $this->CI->config->item('language'));
			
			$errors = validation_errors();
			if(!empty($errors))
      {
				return $errors;
      }
      
      return 1;
			
		}
	}
	
	function form_import($full_path = "",$configs = array())
	{
    if($full_path != "")
    {
      $data = $this->parsing_file($full_path);
      $output = '';
      if(is_array($data) AND count($data) > 0)
      {
        foreach($data as $i => $fields)
        {
          $output .= '<tr>';
          foreach($fields as $field => $value)
          {
            $output .= '<th>'.$field.'</th>';
          }
          $output .= '</tr>';
          break;
        }
        
        foreach($data as $i => $fields)
        {
          $output .= '<tr>';
          foreach($fields as $field => $value)
          {
            $output .= '<td>'.$value.'</td>';
          }
          $output .= '</tr>';
        }
        
      }
      
      $path = (isset($config['path']) and !empty($config['path']))?$config['path']:$this->CI->uri->segment(1);
      $controller = (isset($config['controller']) and !empty($config['controller']))?$config['controller']:$this->CI->uri->segment(2);
      $function = (isset($config['function']) and !empty($config['function']))?$config['function']:$this->CI->uri->segment(3);

      $full_path_encode = my_urlsafe_b64encode($full_path);
      $output = '	
                  <form method="post" action="'.base_url().$path.'/'.$controller.'/do_import_add/'.$full_path_encode.'">
                    <div class="table-responsive">
                      <table width="100%" class="table table-striped table-bordered table-hover">'.
                      $output.
                      '</table>
                    </div>
                    <div class="col-lg-12">
                      <input type="submit" value="Import" name="do_import" class="btn btn-primary btn-block"/>
                    </div>
                  </form>
                ';
      return $output;
    }
    return "";
	}
	
	function link_format_file($config = array())
	{
		$path = (isset($config['path']) and !empty($config['path']))?$config['path']:$this->CI->uri->segment(1);
		$controller = (isset($config['controller']) and !empty($config['controller']))?$config['controller']:$this->CI->uri->segment(2);
		$function = (isset($config['function']) and !empty($config['function']))?$config['function']:$this->CI->uri->segment(3);

		return base_url().$path.'/'.$controller.'/download_format_data';
	}
  
  function get_fields($config_tables = array())
  {
    $configs = (isset($config_tables['fields_insert']))?$config_tables['fields_insert']:"";
    $fields = array();
    if(is_array($configs) and count($configs) > 0)
    {
      foreach($configs as $i => $c)
      {
        $fields[$c['name']] = $c['label'];
      }
      if(isset($configs['sub_table']) and is_array($configs['sub_table']) and count($configs['sub_table']) > 0)
      {
        $tmp_fields = $this->get_fields($configs['sub_table']);
        $fields = array_merge($fields,$tmp_fields);
      }
    }
    
    return $fields;
  }
  
  function get_fields_pertable($config_tables = array())
  {
    $configs = (isset($config_tables['fields_insert']))?$config_tables['fields_insert']:"";
    $fields = array();
    if(is_array($configs) and count($configs) > 0)
    {
      #$tmp_fields = array();
      foreach($configs as $i => $c)
      {
        $fields[$c['label']] = array('name' => $c['name'],'table' => $config_tables['table']);
      }
            
      if(isset($configs['sub_table']) and is_array($configs['sub_table']) and count($configs['sub_table']) > 0)
      {
        $tmp_fields = $this->get_fields_pertable($configs['sub_table']);
        $fields = array_merge($fields,$tmp_fields);
      }
    }
    
    return $fields;
  }
  
  function download_format($configs = array())
  {
    $configs  = $this->configs;
    $fields   = $this->get_fields($configs['config_table']);
    
		$path = (isset($config['path']) and !empty($config['path']))?$config['path']:$this->CI->uri->segment(1);
		$controller = (isset($config['controller']) and !empty($config['controller']))?$config['controller']:$this->CI->uri->segment(2);
		$function = (isset($config['function']) and !empty($config['function']))?$config['function']:$this->CI->uri->segment(3);

    if(is_array($fields) and count($fields) > 0)
    {
      $objPHPExcel = new PHPExcel();
      $objPHPExcel->getActiveSheet()->setTitle('Data');
  
      $rowNumber = 1;
      $col = 'A';
      foreach($fields as $field => $title) {
        $objPHPExcel->getActiveSheet()->setCellValue($col.$rowNumber,$title);
        $col++;
        //$rowNumber++;
      }
      header('Content-Type: application/msexcel');
      header('Content-Disposition: attachment;filename="'.$controller.'.xls"');
      header('Cache-Control: max-age=0');
      $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
      $objWriter->save('php://output');
    }
    exit;
  }
}
