<?php

class Input_date{
  var $CI;
  function Input_date()
  {
		$this->CI =& get_instance();
  }

  function config($field = array())
  {
    $the_config = array();
    $output = "";
    $type = "text";
    
    $star_required = (isset($field['star_required']))?$field['star_required']:"";
    $label = (isset($field['label']))?$field['label']:"";
    $name = (isset($field['name']))?$field['name']:"";
    $id = (isset($field['id']))?$field['id']:"";
    $value = (isset($field['value']) and $field['value'] != "" )?$field['value']:"";
    $maxlength = (isset($field['maxlength']))?$field['maxlength']:"";
    $size = (isset($field['size']))?$field['size']:"";
    $style = (isset($field['style']))?$field['style']:"";
    $class = (isset($field['class']))?$field['class']:"";
    $parent_name = (isset($field['parent_name']))?$field['parent_name']:"";
    $input_name = (!empty($parent_name))?$parent_name.'['.$name.']':$name;
    $rules = (isset($field['rules']))?$field['rules']:"";
    $class .= ' '.((is_array($rules))?implode(' ',$rules):$rules);
    $class .= ' form-control';
    
    $data = array(
      'name'        => $input_name,
      'id'          => $id,
      'value'       => $value,
      'maxlength'   => $maxlength,
      'size'        => $size,
      'style'       => $style,
      'type'       => $type,
      'class'		=> $class
    );
    $field_attributes = $field;
    $field_attributes = array_merge($field_attributes,$data);
    foreach($field_attributes as $i => $o)
    {
      if(is_array($o) and count($o) >  0)
      {
        unset($field_attributes[$i]);
      }
    }

    $error = form_error($input_name);
    $label  = $this->CI->hook->do_action('hook_create_form_field_label_'.$name,$label);
    $output .= '  <div class="row">
                    <br/><div class="col-lg-3">
                    <div class="input-group">
                      <label for="'. $name .'">'. $label .'</label>';
    $form  = $this->CI->hook->do_action('hook_create_form_view_value_'.$name,$value);
    $output .= '    </div>
                  </div>
                  <div class="col-lg-9">
                  <div class="input-group">
                    '.$form.'
                    '.$error.'
                  </div>
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->';      
    $this->config_field = array();
    $this->output_field = $output;
  }
  
  function output()
  {
    return $this->output_field;
  }
}

