<?php

class Required_file{
  var $CI;
  var $form_field = "";
  function __construct($str)
  {
    echo $str;
		$this->CI =& get_instance();
    if (isset($_FILES[$str]) && !empty($_FILES[$str]['name']))
    {
      if ($this->upload->do_upload($str))
      {
        // set a $_POST value for $str that we can use later
        $upload_data    = $this->upload->data();
        $_POST[$str] = $upload_data['file_name'];
        return true;
      }
      else
      {
        // possibly do some clean up ... then throw an error
        $this->form_validation->set_message('handle_upload', $this->upload->display_errors());
        return false;
      }
    }
    else
    {
      // throw an error because nothing was uploaded
      $this->form_validation->set_message('handle_upload', "You must upload an image!");
      return false;
    }
  }
}
