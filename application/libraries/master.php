<?php

class Master{
	
	var $CI;
	
	function Master(){
		$this->CI =& get_instance();
	}
	
	function get_nilai()
	{
		$q = $this->CI->db->get("m_nilai");
		$nilai = $q->result_array();
		return $nilai;
	}
	function get_assesment()
	{
		$q = $this->CI->db->get("m_assesment");
		$nilai = $q->result_array();
		return $nilai;
	}
	
	function get_value($table = "",$field = "",$where = "")
	{
		$this->CI->db->where($where);
		$q = $this->CI->db->get($table);
		
		$output = "";
		if($q->num_rows() == 0)
			return $output;
			
		$data = $q->row_array();
    $output = $data;
    if(!empty($field))
      $output = (isset($data[$field]))?$data[$field]:"";
		
		return $output;
	}
  
}
