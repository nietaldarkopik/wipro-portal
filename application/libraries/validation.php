<?php

class Validation{
	var $CI;
	
	function Validation(){
		$this->CI =& get_instance();
	}
  
	function add_validate($rule = "",$postdata = "", $param = "")
	{
    if(empty($rule))
      return true;

    $rule = (explode("[",$rule));
    $rule = $rule[0];
    if(file_exists(APPPATH.'libraries/form_validation/'.$rule.".php"))
    {
        $this->CI->load->library("form_validation/".$rule);
    }
	}
}
