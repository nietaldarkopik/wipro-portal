<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

  public function __construct(){
    parent::__construct();
    
  }
  
	public function index($action = "",$menu_group = "general"){
    $is_ajax = $this->input->post('is_ajax');
    $this->load->view('layouts/dashboard');
	}
  
	public function fullwidth(){
    if($this->gts_greenwallet->is_registered() == ""){
      $this->load->view('layouts/page-member-unregistered');
    }else{
			$this->load->view('layouts/fullwidth');
		}
	}
  
	public function leftsidebar(){
    if($this->gts_greenwallet->is_registered() == ""){
      $this->load->view('layouts/page-member-unregistered');
    }else{
			$this->load->view('layouts/leftsidebar');
		}
	}
  
	public function rightsidebar(){
    if($this->gts_greenwallet->is_registered() == ""){
      $this->load->view('layouts/page-member-unregistered');
    }else{
			$this->load->view('layouts/rightsidebar');
		}
	}
  
	public function leftrightsidebar(){
    if($this->gts_greenwallet->is_registered() == ""){
      $this->load->view('layouts/page-member-unregistered');
    }else{
			$this->load->view('layouts/leftrightsidebar');
		}
	}
  
	public function nosidebar(){
    if($this->gts_greenwallet->is_registered() == ""){
      $this->load->view('layouts/page-member-unregistered');
    }else{
			$this->load->view('layouts/nosidebar');
		}
	}
  
	public function overlay(){
    if($this->gts_greenwallet->is_registered() == ""){
      $this->load->view('layouts/page-member-unregistered');
    }else{
			$this->load->view('layouts/overlay');
		}
	}
	
	function forgot(){
    $is_login = $this->user_access->is_login();
    if($is_login){
      $error = "";
      $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
      $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
      header("location: ".base_url(strtolower($user_level_name)."dashboard"));
      exit;
    }
		$is_ajax = $this->input->post('is_ajax');
		
		$error = "";
		$success = "";
    
    $this->load->view('layouts/forgot', array(
			'error' => $error,
			'success' => $success
		));
	}
	
	function do_forgot(){
		$error = "";
		$success = "";
		$email = $this->input->post('email');
		$generate_password_code = $this->skayla_account->generate_password_code($email);
		if($generate_password_code !== FALSE){
			$success = "Link perubahan password telah dikirim ke E-Mail Anda";
			$this->skayla_account->register_forgot_password($email, $generate_password_code);
		}else{
			$error = "E-Mail tidak terdaftar!";
		}
    $this->load->view('layouts/forgot',array(
			'error' => $error,
			'success' => $success
		));
	}
	
	function reset($code=0){
		$error = "";
		$success = "";
		$user_id = 0;
		
		$validate_forgot_password_code = $this->skayla_account->validate_forgot_password_code($code);
		if($validate_forgot_password_code !== FALSE){
			$user_id = $validate_forgot_password_code;
			$success = 'Silahkan ganti password lama Anda dengan yang baru.';
		}else{
			$error = 'Kode reset password tidak dapat digunakan lagi, silahkan request reset password lagi.';
		}
		
    $this->load->view('layouts/reset', array(
			'error' => $error,
			'success' => $success,
			'user_id' => $user_id,
			'code' => $code
		));
	}
	
	function do_reset(){
		$error = "";
		$success = "";
		$reset = $this->input->post('reset');
		$change_password = $this->skayla_account->change_password($reset);
		if($change_password['status']){
			$success = $change_password['msg'];
		}else{
			$error = $change_password['msg'];
		}
    $this->load->view('layouts/reset_status', array(
			'error' => $error,
			'success' => $success
		));
	}
	
	function do_register(){
		$error = "";
		$success = "";
		$register = $this->input->post('register');
		$register = $this->skayla_account->register($register);
		if($register['status']){
			$success = $register['msg'];
		}else{
			$error = $register['msg'];
		}
    $this->load->view('layouts/register', array(
			'error' => $error,
			'success' => $success
		));
	}
	
	function register(){
    $is_login = $this->user_access->is_login();
    if($is_login){
      $error = "";
      $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
      $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
      header("location: ".base_url(strtolower($user_level_name)."dashboard"));
      exit;
    }
		$is_ajax = $this->input->post('is_ajax');
		
		$error = "";
		$success = "";
		
    $this->load->view('layouts/register', array(
			'error' => $error,
			'success' => $success
		));
	}
  
	function login(){
    $is_login = $this->user_access->is_login();
    if($is_login){
      $error = "";
      $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
      $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
      header("location: ".base_url(strtolower($user_level_name)."dashboard"));
      exit;
    }
		$is_ajax = $this->input->post('is_ajax');
		
		$error= "";
		$user_name = $this->input->post("user_name");
		$password = $this->input->post("password");
		$is_login = $this->user_access->is_login();
		
		if(!empty($user_name) || !empty($password)){
			$password = md5($password);
			
			$process_login = $this->user_access->do_login($user_name,$password);
			if($process_login){
				$error = "";
        $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
        $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
        header("location: ".base_url(strtolower($user_level_name)."dashboard"));
			}else{
				$error = "Username atau password tidak valid";
			}
		}
		
		$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
	}
	
	function do_login(){
		$user_name = $this->input->post("user_name");
		$password = $this->input->post("password");
		
		if($password == "" and $user_name == ""){
			$is_login = $this->user_access->is_login();
			if($is_login){
        $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
        $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
        header("location: ".base_url($user_level_name."dashboard"));
        exit;
			}else{
				$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
			}
		}else{
			$password = md5($password);
			
			$error= "";
			$process_login = $this->user_access->do_login($user_name,$password);
			if($process_login){
				$error = "";
        $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
        $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
        header("location: ".base_url($user_level_name."dashboard"));
        exit;
			}else{
				$error = "Username atau password tidak valid";
				$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
			}
		}
	}
	
	function activation($registration_key){
		$activate = $this->skayla_account->activate($registration_key);
		$status = '?activation=fail';
		
		if($activate){
			$status = '?activation=success';
		}
		
		redirect('customer/dashboard/activation_result'.$status);
	}
	
	function activation_result(){
		$this->load->view('layouts/activation',array("page_title" => "Login Page"));
	}
	
	function is_login(){
		$is_login = $this->user_access->is_login();
		echo $is_login;
	}
	
	function do_logout(){
		$this->user_access->do_logout();
		header("location: ".base_url()."admin/dashboard/login");
		exit;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
