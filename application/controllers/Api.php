<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->library('api_library');
  }
  
	public function index()
	{
		$validate_request = $this->api_library->validate_request();
		
		if($validate_request){
			$action = $this->input->post('action');
			
			if(!method_exists($this, $action)){
				echo 'Bad Command!';
			}else{
				$this->$action();
			}
			
		}else{
			echo 'Authentication Failed!';
		}
	}
	
	public function newsletter(){
		$email_address = $this->input->post('email_address');
		$newsletter = $this->api_library->newsletter_add($email_address);
		$this->api_library->redirecting($newsletter, 'newsletter_exists');
	}
	
	public function place_order(){
		$username = $this->input->post('username');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$affiliate = $this->input->post('affiliate');
		$products = $this->input->post('products');
		$place_order = $this->api_library->place_order_add(
			array(
				'email' => $email, 
				'username' => $username, 
				'password' => $password,
				'affiliate' => $affiliate,
				'products' => $products
			)
		);
		$this->api_library->redirecting($place_order['status'], $place_order['code']);
	}
	
	public function product($product_id){
		$product = $this->api_library->get_product($product_id);
		echo json_encode($product);
	}
	
	public function products(){
		$products = $this->api_library->get_products();
		echo json_encode($products);
	}
	
	public function payment_method($payment_method_id){
		$payment_method = $this->api_library->get_payment_method($payment_method_id);
		echo json_encode($payment_method);
	}
	
	public function payment_methods(){
		$payment_methods = $this->api_library->get_payment_methods();
		echo json_encode($payment_methods);
	}
	
	public function order($order_id){
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
