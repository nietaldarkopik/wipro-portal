<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

  public function __construct()
  {
    parent::__construct();
    
  }
  
	public function index($action = "",$menu_group = "general")
	{
    $is_ajax = $this->input->post('is_ajax');
    $this->load->view('layouts/dashboard');
	}
  
	public function fullwidth()
	{
    if($this->gts_greenwallet->is_registered() == "")
    {
      $this->load->view('layouts/page-member-unregistered');
    }else
			$this->load->view('layouts/fullwidth');
	}
  
	public function leftsidebar()
	{
    if($this->gts_greenwallet->is_registered() == "")
    {
      $this->load->view('layouts/page-member-unregistered');
    }else
			$this->load->view('layouts/leftsidebar');
	}
  
	public function rightsidebar()
	{
    if($this->gts_greenwallet->is_registered() == "")
    {
      $this->load->view('layouts/page-member-unregistered');
    }else
			$this->load->view('layouts/rightsidebar');
	}
  
	public function leftrightsidebar()
	{
    if($this->gts_greenwallet->is_registered() == "")
    {
      $this->load->view('layouts/page-member-unregistered');
    }else
			$this->load->view('layouts/leftrightsidebar');
	}
  
	public function nosidebar()
	{
    if($this->gts_greenwallet->is_registered() == "")
    {
      $this->load->view('layouts/page-member-unregistered');
    }else
			$this->load->view('layouts/nosidebar');
	}
  
  
	public function overlay()
	{
    if($this->gts_greenwallet->is_registered() == "")
    {
      $this->load->view('layouts/page-member-unregistered');
    }else
			$this->load->view('layouts/overlay');
	}
  
	function login()
	{
    $is_login = $this->user_access->is_login();
    if($is_login)
    {
      $error = "";
      $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
      $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
      header("location: ".base_url($user_level_name."dashboard"));
      exit;
    }
		$is_ajax = $this->input->post('is_ajax');
		
		$error= "";
		$user_name = $this->input->post("user_name");
		$password = $this->input->post("password");
		$is_login = $this->user_access->is_login();
		
		if(!empty($user_name) || !empty($password))
		{
			$password = md5($password);
			
			$process_login = $this->user_access->do_login($user_name,$password);
			if($process_login)
			{
				$error = "";
        $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
        $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
        header("location: ".base_url($user_level_name."dashboard"));
			}else{
				$error = "Username atau password tidak valid";
			}
		}
		
		$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
	}
	
	function do_login()
	{
		$user_name = $this->input->post("user_name");
		$password = $this->input->post("password");
		
		if($password == "" and $user_name == "")
		{
			$is_login = $this->user_access->is_login();
			if($is_login)
			{
        $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
        $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
        header("location: ".base_url(strtolower($user_level_name)."dashboard"));
        exit;
			}else{
				$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
			}
		}else{
			$password = md5($password);
			
			$error= "";
			$process_login = $this->user_access->do_login($user_name,$password);
			if($process_login)
			{
				$error = "";
        $user_level_name = $this->user_access->get_level_detail($this->session->userdata("user_id"));
        $user_level_name = (isset($user_level_name['user_level_name']))?$user_level_name['user_level_name'].'/':"";
        header("location: ".base_url(strtolower($user_level_name)."dashboard"));
        exit;
			}else{
				$error = "Username atau password tidak valid";
				$this->load->view('layouts/login',array('error' => $error,"page_title" => "Login Page"));
			}
		}
	}
	
	function is_login()
	{
			$is_login = $this->user_access->is_login();
			echo $is_login;
	}
	
	function do_logout()
	{
		$this->user_access->do_logout();
		header("location: ".base_url()."admin/dashboard/login");
		exit;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
