<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menus extends Admin_Controller {

	var $init = array();
	var $page_title = "Menus";
		
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'menu/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/menus/edit',array('response' => $response,'page_title' => $this->page_title));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/menus/add',array('response' => $response,'page_title' => 'Tambah menu'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/menus/view',array('response' => '','page_title' => $this->page_title));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		
		$this->hook->add_action('hook_create_form_title',array($this,'_hook_create_form_title_add'));
		$this->hook->add_action('hook_create_form_ajax_target',array($this,'_hook_create_form_ajax_target_add'));
		$this->hook->add_action('hook_create_form_filter_ajax_target',array($this,'_hook_create_form_filter_ajax_target'));
		$this->hook->add_action('hook_create_form_filter_is_ajax',array($this,'_hook_ajax_false'));

		$filter = $this->input->post("data_filter");
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$do_save_menu = $this->input->post("do_save_menu");
		if(!empty($do_save_menu))
		{
			$save_menu = $this->_save_parent_menu();
			echo $save_menu;
			exit;
		}
		
		$get_functions = $this->input->post("get_functions");
		$current_function = $this->input->post("current_function");
		if(!empty($get_functions))
		{
			$this->_get_functions($get_functions,$current_function);
			exit;
		}
		
		
    $the_menus = "";
    $is_login = $this->user_access->is_login();
    if(isset($filter['user_menu_group_id']))
    {		
      $this->db->where($filter);
      $this->db->order_by("sort_order","ASC");
      $menus = $this->db->get(TABLE_USR_MENUS);
      $menus = $menus->result_array();
      $menus = $this->user_access->mapTree($menus,0);
      $the_menus = $this->_display_structure($menus);
    }
		if($is_login)
			$this->load->view('layouts/menus/listing',array('response' => '','page_title' => $this->page_title,'the_menus' => $the_menus));
		else
			$this->load->view('layouts/login');
		
		
	}
	
	function _config($id_object = "")
	{			
		$this->data->_get_controllers();
		$data_methods_arr = $this->data->methods;
		$data_methods = array();
		foreach($data_methods_arr as $i => $v)
		{
			$data_methods[$v] = $v;
		}
        
		$init = array(	'table' => TABLE_USR_MENUS,
						'sort_order' => 'parent_menu ASC,sort_order ASC',
						'fields' => array(
											array(
													'name' => 'user_menu_group_id',
													'label' => 'Menu Group',
													'id' => 'user_menu_group_id',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> TABLE_USR_MENU_GROUPS.' menu2',
													'select' => array('user_menu_group_id AS value','name AS label'),
													'options' => array('' => '---- Choose Menu Group ----'),
													'primary_key' => 'user_menu_group_id',
													'rules' => 'required'
												),
											array(
													'name' => 'parent_menu',
													'label' => 'Parent Menu',
													'id' => 'parent_menu',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'table'	=> TABLE_USR_MENUS.' menu2',
													'select' => array('user_menu_id AS value','menu_title AS label'),
													'options' => array('' => '---- Choose Feature ----','0' => '---- No Parent ----'),
													'primary_key' => 'user_menu_id',
													'rules' => 'required',
                                                    'js_connect_to' => array( 
                                                    'id_field_parent' => '"select[name=\"data[user_menu_group_id]\"]"',
                                                    'table' => TABLE_USR_MENUS,
                                                    'where' => '',
                                                    'select' => 'user_menu_id AS value,menu_title AS label',
                                                    'primary_key' => 'user_menu_id',
                                                    'foreign_key' => 'user_menu_group_id')
												),
											array(
													'name' => 'menu_title',
													'label' => 'Menu Title',
													'id' => 'menu_title',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required',
													'list_style' => 'width="150"'
												),
											array(
													'name' => 'controller',
													'label' => 'Feature',
													'id' => 'controller',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'options' => array_merge(array('' => '---- Choose Feature ----','#' => '-----------void------------'),$this->data->controllers),
													'rules' => 'required'
												),
											array(
													'name' => 'function',
													'label' => 'Action',
													'id' => 'function',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'options' => array_merge(array('' => '---- Choose Action ----'),$data_methods),
													'rules' => 'required'
												),
											array(
													'name' => 'sort_order',
													'label' => 'Nomor Urutan',
													'id' => 'sort_order',
													'value' => '',
													'type' => 'text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'numeric',
													'list_style' => 'width="100" align="center"'
												),
											array(
													'name' => 'type',
													'label' => 'Type',
													'id' => 'type',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'options' => array(	''               => '---- Choose Type ----',
																				'home'    => 'home'
                                                                            ),
													'rules' => ''
												),
											array(
													'name' => 'status',
													'label' => 'Status',
													'id' => 'status',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => false,
													'use_listing' => false,
													'options' => array(	'active' => 'Active','not active' => 'Not Active'),
													'rules' => ''
												)
										),
									'primary_key' => 'user_menu_id',
									'path' => "/admin/",
									'controller' => 'menus',
									'function' => 'index',
									'panel_function' => array(
															  array('title' => 'Edit','name' => 'edit', 'class' => 'glyphicon-share'),
															  array('title' => 'View','name' => 'view', 'class' => 'glyphicon-share'),
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															),
									'bulk_options' => array(
															  array('title' => 'Delete','name' => 'delete', 'class' => 'glyphicon-cog')
															)
					);
		$this->init = $init;
	}
	
	function _display_structure_table($nodes, $indent=0, $max_depth = "") {	
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		$no = 0;
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			foreach ($nodes as $index =>$node) 
			{
				/*
				#$output .= str_repeat('&nbsp;',$indent*4);
				$href = base_url().$node['controller'].'/'.$node['function'];
				if(strpos($href,'#') !== false)
					$href = '#';
					
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<td><h3><a href="'.$href.'"><span class="ui-icon ui-icon-arrowthick-1-e"></span>'.$node['menu_title'].'</a></h3>';
					$output .= $this->_display_structure_table($node['children'],$indent+1, $max_depth);
				}else{
					$output .= '<td><a href="'.$href.'">'.$node['menu_title'].'</a>';
				}
				
				$output .= '</td>'."\n";
				*/
				$panel_function = array("edit","view","delete");
				$action = $this->data->show_panel_allowed("","",$panel_function,$node[$this->init['primary_key']]);
				$no++;
				$output .= '
							<tr class="" style="">
								<td align="center" style="width: 30px;">'. $no .'</td>
								<td style="width: 100px;">'.$node['parent_menu'].'</td>
								<td style="width: 150px;">'.$node['menu_title'].'</td>
								<td style="width: 108px;">'. base_url() . $node['controller'] . '/' . $node['function'] .'</td>
								<td align="center" style="width: 30px;">'.$node['sort_order'].'</td>
								<td>
									' . $action . '
								</td>
							</tr>'."\n";
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<tr style="border-left: none !important;border-right: none !important;">
									<td colspan="6" style="border-left: none !important;border-right: none !important;"> <table width="100%"><tbody>' . $this->_display_structure_table($node['children'],$indent+1, $max_depth) . '</tbody></table></td>
								</tr>';
				}
			}
		}
		
		return $output;
	}
	
	
	function _display_structure($nodes, $indent=0, $max_depth = "") {	
		if(!empty($max_depth) and $indent >= $max_depth)
			return "";	
		
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		$output = "";
		if(count($nodes) > 0 and is_array($nodes) > 0)
		{
			$output = '<ul class="sortables fleft">'."\n";
			foreach ($nodes as $node) {
				#$output .= str_repeat('&nbsp;',$indent*4);
				$href = base_url().$node['controller'].'/'.$node['function'];
				$panel_function = array("edit","view","delete");
				$action = $this->data->show_panel_allowed("",$this->init['path'],"menus",$this->init['panel_function'],$node[$this->init['primary_key']],true);

				if(strpos($href,'#') !== false)
					$href = '#';
					
				if(isset($node['children']) and is_array($node['children']) and count($node['children']) > 0)
				{
					$output .= '<li class="fclear the_menu" id="the_menu-'.$node['user_menu_id'].'">
									<input type="hidden" name="menu['.$node['user_menu_id'].']" alt="'.$node['sort_order'].'" value="'.$node['parent_menu'].'" class="inp_menu"/>
									<span class="ui-icon ui-icon-triangle-1-n fleft showhide"></span><span class="ui-icon '. ((isset($node['attributes']))?$node['attributes']:''). ' fleft"></span><a href="'.$href.'" class="fleft" target="_blank">'.$node['menu_title'].'</a> <span class="sort_action fright">'. $action .'</span> <br class="fleft"/>';
					$output .= $this->_display_structure($node['children'],$indent+1, $max_depth);
				}else{
					$output .= '<li class="fclear the_menu" id="the_menu-'.$node['user_menu_id'].'">
								<input type="hidden" name="menu['.$node['user_menu_id'].']" alt="'.$node['sort_order'].'" value="'.$node['parent_menu'].'" class="inp_menu"/>
								<span class="ui-icon ui-icon-triangle-1-s fleft showhide"></span><span class="ui-icon '. ((isset($node['attributes']))?$node['attributes']:''). ' fleft"></span><a href="'.$href.'" class="fleft" target="_blank">'.$node['menu_title'].'</a> <span class="sort_action fright">'. $action .'</span> <br class="fleft"/>';
					$output .= '<ul class="sortables fleft"></ul>'."\n";
				}
				
				$output .= '</li>'."\n";
			}
			$output .= '</ul>'."\n";
		}
		
		return $output;
	}
		
	function _save_parent_menu()
	{
		$output = 0;
		$structure = $this->input->post('menu');
		if(is_array($structure) and count($structure))
		{
			$sort_order = array();
			foreach($structure as $menu_id => $parent_menu)
			{
				$sort_index = (isset($sort_order[$parent_menu]))?$sort_order[$parent_menu]:0;
				$this->db->where(array("user_menu_id" => $menu_id));
				$output = (int) $this->db->update(TABLE_USR_MENUS,array("parent_menu" => $parent_menu,"sort_order" => $sort_index));
				if(isset($sort_order[$parent_menu]))
				{
					$sort_order[$parent_menu] += 1;
				}else{
					$sort_order[$parent_menu] = 1;
				}
			}
		}
		return $output;
	}
	
	function _get_functions($get_functions = "",$current_function="")
	{
		$get_functions = (empty($get_functions))?$this->input->post("get_functions"):$get_functions;
		$current_function = (empty($current_function))?$this->input->post("current_function"):$current_function;
		$conmeth = $this->data->controller_methods;
		echo '<option value=""> -- Choose Action --</option>';
		print_r($conmeth);
		if(isset($conmeth) and is_array($conmeth))
		{
			foreach($conmeth as $i => $d)
			{
				$class_name = strtolower($d['class_name']);
				if($get_functions == $class_name)
				{
					$methods = $d['methods'];
					foreach($methods as $index => $method)
					{
						if(!is_array($method))
						{
							$selected = ($current_function == $method)?' selected="selected" ':'';
							echo '<option value="'.$method.'" '.$selected.'>'.$method.'</option>';
						}
					}
				}
			}
		}
		echo "";
	}
	
	function _get_functions_($get_functions = "",$current_function="")
	{
		$get_functions = (empty($get_functions))?$this->input->post("get_functions"):$get_functions;
		$current_function = (empty($current_function))?$this->input->post("current_function"):$current_function;
		$conmeth = $this->data->controller_methods;
		echo '<option value=""> -- Choose Action --</option>';
		echo $get_functions;
		print_r($conmeth);
		if(isset($conmeth[$get_functions]) and isset($conmeth[$get_functions]['methods']) and is_array($conmeth[$get_functions]['methods']))
		{
			$methods = $conmeth[$get_functions]['methods'];
			foreach($methods as $index => $method)
			{
				print_r($method);
				if(!is_array($method))
				{
					$selected = ($current_function == $method)?' selected="selected" ':'';
					echo '<option value="'.$method.'" '.$selected.'>'.$method.'</option>';
				}
			}
		}
		echo "";
	}
	
	function _hook_do_add($param = "")
	{
        
		$param['menu_name'] = $param['controller'].'_'.$param['function'].rand(0,999);
		$param['menu_name'] = ($param['controller'] == '#')?$param['function'].rand(0,999):$param['menu_name'];
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		$param['menu_name'] = $param['controller'].'_'.$param['function'].rand(0,999);
		$param['menu_name'] = ($param['controller'] == '#')?$param['function'].rand(0,999):$param['menu_name'];
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}

	function _hook_create_form_title_add($title){
		return "Tambah Menu";
	}

	function _hook_create_form_title_edit($title){
		return "Edit Menu";
	}

	function _hook_create_form_ajax_target_add(){
		return ".tab-content #add";
	}

	function _hook_create_form_filter_ajax_target(){
		return ".tab-content #search";
	}

	function _hook_ajax_false(){
		return "";
	}

	function _hook_ajax_true(){
		return "ajax";
	}

	function _hook_show_panel_allowed($panel = "")
	{
		#$panel = str_replace(".ajax_container",".content-container",$panel);
		return $panel;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
