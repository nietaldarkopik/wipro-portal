<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends Admin_Controller {

	var $init = array();
	var $page_title = "";
	
	function index()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data User'));
		else
			$this->load->view('layouts/login');
			
	}
	
	function delete($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_delete',array($this,'_hook_do_delete'));	
		$response = $this->data->delete("",$this->init['fields']);
		$paging_config = array('base_url' => base_url().'administration_users/listing','uri_segment' => 4);
		$this->data->init_pagination($paging_config);
		$this->listing();
	}	
	
	function edit($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;
		$this->hook->add_action('hook_do_edit',array($this,'_hook_do_edit'));
		
		$init = (isset($this->init['fields']))?$this->init['fields']:array();
		if(is_array($init) and count($init) > 0)
		{
			foreach($init as $index => $i)
			{
				if(isset($i['name']) and $i['name'] == 'password')
				{
					$init[$index]['rules'] = "";
				}
			}
		}
		$this->init['fields'] = $init;
		
		$response = $this->data->edit("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/edit',array('response' => $response,'page_title' => 'Data User'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function add()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/add',array('response' => $response,'page_title' => 'Data User'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function add_csv()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			echo $response;
	}
	
	function check_csv()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();

		$response = $this->data->check_csv("",$this->init['fields']);
    #$response = str_replace("\n","",$response);
    #$response = str_replace("\"","\\\"",$response);
    //$response = strip_tags($response);
    #$response = addslashes($response);
    echo $response;
	}
	
	function import()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/import',array('response' => $response,'page_title' => 'Data User'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function export()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->hook->add_action('hook_do_add',array($this,'_hook_do_add'));
		$response = $this->data->add("",$this->init['fields']);
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/export',array('response' => $response,'page_title' => 'Data User'));
		else
			$this->load->view('layouts/login');
		
	}
	
	
	function view($object_id = "")
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		$this->data->primary_key_value = $object_id;		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/view',array('response' => '','page_title' => 'Data User'));
		else
			$this->load->view('layouts/login');
		
	}
		
	function listing()
	{
		$this->_config();
		$this->data->init($this->init);
		$this->data->set_filter();
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/default/listing',array('response' => '','page_title' => 'Data User'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _config($id_object = "")
	{			
		$init = array(	'table' => 'user_accounts',
						'fields' => array(	
											array(
													'name' => 'username',
													'label' => 'Username',
													'id' => 'username',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required|callback_is_count_match[1]|is_unique[user_accounts.username]'
												),
											array(
													'name' => 'password',
													'label' => 'Password',
													'id' => 'password',
													'value' => '',
													'type' => 'password',
													'class' => 'input_text',
													'use_search' => false,
													'use_listing' => false,
													'rules' => 'required'
												),
											array(
													'name' => 'email',
													'label' => 'Email',
													'id' => 'email',
													'value' => '',
													'type' => 'text',
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required|valid_email'
												),
											array(
													'name' => 'user_level_id',
													'label' => 'User Level',
													'id' => 'user_level_id',
													'value' => '',
													'type' => 'input_selectbox',
													'use_search' => true,
													'use_listing' => true,
													'table'	=> 'user_levels',
													'select' => array('user_level_id AS value','user_level_name AS label'),
													'options' => array('' => '---- Select Option ----'),
													'primary_key' => 'user_level_id',
													'rules' => 'required'
												),
											array(
													'name' => 'active_status',
													'label' => 'Status',
													'id' => 'active_status',
													'value' => '',
													'type' => 'input_selectbox',
													'options' => array('' => '---- Select Option ----','active' => 'Active','not active' => 'Not Active'),
													'use_search' => true,
													'use_listing' => true,
													'rules' => 'required'
												)
										),
									'primary_key' => 'user_id',
                  'csv_fields' => 'username,password,email,user_level_id,status'
					);
		$this->init = $init;
	}
	
	function change_password_admin($object_id = "")
	{
		$response = '';
		$do_change_password = $this->input->post("do_change_password");
		if($do_change_password != "")
		{
			$data_post = $this->input->post("data");
			$old_password = $data_post['old_password'];
			$new_password = $data_post['new_password'];
			$re_new_password = $data_post['re_new_password'];
			$current_user_id = $this->user_access->current_user_id;
			
			if(!empty($current_user_id))
			{
				//check old password
				$this->db->where(array("user_id" => $current_user_id,"password" => md5($old_password)));
				$q = $this->db->get("administration_users");
				if($q->num_rows() != 0)
				{
					//check new password
					$is_same_new_password = ($new_password == $re_new_password)?1:0;
					if($is_same_new_password)
					{
						$this->db->where(array("user_id" => $current_user_id));
						$q = $this->db->update("administration_users",array("password" => md5($new_password)));
						if($q)
						{
							$response .= "<p class='success'>Password berharsil dirubah</p>";
						}
					}else{
						$response .= "<p class='error'>Password baru dan re-enter password baru tidak sama</p>";
					}
				}else{
					$response .= "<p class='error'>Password lama tidak sesuai</p>";
				}
			}
			
		}
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/user_level_role/change_password_admin',array('page_title' => 'Ganti Password Admin','response' => $response));
		else
			$this->load->view('layouts/login');
		
	}
	
	function reset_password($object_id = "")
	{
		$response = '';
		$do_change_password = $this->input->post("do_change_password");
		if($do_change_password != "")
		{
			$data_post = $this->input->post("data");
			$username = $data_post['username'];
			$new_password = $data_post['new_password'];
			$re_new_password = $data_post['re_new_password'];
			
			if(!empty($username))
			{
				//check old password
				$this->db->where(array("username" => $username));
				$q = $this->db->get("administration_users");
				if($q->num_rows() > 0)
				{
					$param = $q->row_array();
					//check new password
					$is_same_new_password = ($new_password == $re_new_password)?1:0;
					if($is_same_new_password)
					{
						$this->db->where(array("username" => $username));
						$q = $this->db->update("administration_users",array("password" => md5($new_password)));
										
						$this->load->helper('string');
						$this->load->library('email');
						$username = $param['email'];
						$password = $new_password;
												
						#$config['protocol'] = 'smtp';
						#$config['smtp_host'] = 'smtp.gmail.com';
						#$config['smtp_user'] = 'nietaldarkopik@gmail.com';
						#$config['smtp_pass'] = 'adaajaada';
						#$config['smtp_port'] = '465';
						#$config['mailpath'] = '/usr/sbin/sendmail';
						$config['charset'] = 'utf-8';
						$config['wordwrap'] = TRUE;
						$config['mailtype'] = 'html';
						$config['useragent'] = 'SIMBINTEK';

						$this->email->initialize($config);
						
						$this->email->from(ADMIN_EMAIL, ADMIN_NAME);
						$this->email->to($param['email']);
						
						$message = $this->load->view('layouts/templates/email_institution_user', array('username' => $username,'password' => $password), true);
						$this->email->subject('User Akses SIMBINTEK');
						$this->email->message($message);
						$this->email->send();
						echo $this->email->print_debugger();
						if($q)
						{
							$response .= "<p class='success'>Password berharsil dirubah</p>";
						}
					}else{
						$response .= "<p class='error'>Password baru dan re-enter password baru tidak sama</p>";
					}
				}else{
					$response .= "<p class='error'>User tidak ditemukan</p>";
				}
			}
			
		}
		
		
		$is_login = $this->user_access->is_login();
		if($is_login)			
			$this->load->view('layouts/user_level_role/reset_password',array('response' => $response,'page_title' => 'Reset dan Kirim Password'));
		else
			$this->load->view('layouts/login');
		
	}
	
	function _hook_do_add($param = "")
	{
		$param['password'] = md5($param['password']);
		return $param;
	}
	
	function _hook_do_edit($param = "")
	{
		if(isset($param['password']) and !empty($param['password']))
			$param['password'] = md5($param['password']);
			
		if(isset($param['password']) and empty($param['password']))
			unset($param['password']);
		return $param;
	}
	
	function _hook_do_delete($param = "")
	{
		return $param;
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
