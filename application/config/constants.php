<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


define('TABLE_SYS_TABLE_CONFIG','system_table_config');
define('TABLE_SYS_SETTINGS','system_settings');
define('TABLE_SYS_LOG','system_log');
define('TABLE_SYS_RECYCLE_BIN_DATA','system_recycle_bin_data');
define('TABLE_SYS_RECYCLE_BIN_FILE','system_recycle_bin_file');
define('TABLE_SYS_PLUGINS','system_plugins');
define('TABLE_SYS_WIDGETS','system_widgets');
define('TABLE_SYS_THEMES','system_themes');
define('TABLE_SYS_BLOCKS','system_blocks');
define('TABLE_SYS_BLOCK_GROUPS','system_block_groups');
define('TABLE_SYS_BLOCK_GROUP_RELS','system_block_group_rels');
define('TABLE_SYS_BLOCK_PLUGINS','system_block_plugins');
define('TABLE_SESSION','system_sessions');
define('TABLE_USERS','user_accounts');
define('TABLE_USR_DATA','user_data');
define('TABLE_USR_LEVELS','user_levels');
define('TABLE_USR_LEVEL_ROLES','user_level_roles');
#define('TABLE_USR_MENUS','user_menus');
define('TABLE_USR_MENUS','user_custom_menus');
define('TABLE_USR_MENU_GROUPS','user_menu_groups');
define('TABLE_USR_PAGES','user_pages');
define('TABLE_USR_ROLES','user_roles');
define('TABLE_USR_THEMES','user_themes');

define('WEB_NAME','Portal Web Administrator (WIPRO)');

define('CURRENT_THEME', 'wipro');
define('CURRENT_ADMIN_THEME', 'bootstrapv33');
