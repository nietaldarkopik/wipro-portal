<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {
    var $table = "";
    var $primary_key = "";
    var $configs = "";
    
	public function __construct()
	{
        parent::__construct();
	}

    function set_configs($config = array())
    {
        $this->configs = $config;
    }
    
    function get_data($fields = "",$where = "",$limit = "",$order_by = "",$group_by = "",$having = "",$get_total_rows = false)
    {
        if($fields != "")
        {
            $this->db->select($fields);
        }
        if($where != "")
        {
            $this->db->where($where);
        }
        if($limit != "")
        {
            $arr_limit = explode(",",$limit);
            if(count($arr_limit) == 2)
            {
                $this->db->limit($arr_limit[1],$arr_limit[0]);
            }elseif(!empty($arr_limit[0])){
                $this->db->limit($arr_limit[0]);
            }
        }
        if($order_by != "")
        {
            $this->db->order_by($order_by);
        }
        if($group_by != "")
        {
            $this->db->group_by($group_by);
        }
        if($having != "")
        {
            $this->db->having($having);
        }
        
        if($get_total_rows)
        {
            $this->db->from($this->table);
            return $this->db->count_all_results();
        }else{
            $query = $this->db->get($this->table);
            $data = $query->result_array();
            return $data;
        }
    }
    
    function get_total_rows($where = "",$group_by = "",$having = "")
    {
        $total_rows = $this->get_data("",$where,"","",$group_by,$having,true);
        return $total_rows;
    }
    
    function get_detail($primary_key_value = "")
    {
        if(!empty($primary_key_value))
        {
            $this->db->where(array($this->primary_key => $primary_key_value));
            $query = $this->db->get($this->table);
            $data = $query->row_array();
            return $data;
        }
        
        return array();
    }
    
    function delete($primary_key_value = "")
    {
        if(!empty($primary_key_value))
        {
            $this->db->where(array($this->primary_key => $primary_key_value));
            $query = $this->db->delete($this->table);
            return $query;
        }
        
        return false;
    }
    
    function insert($data="")
    {
        if(!empty($data))
        {
            $query = $this->db->insert($this->table,$data);
            return $query;
        }
        
        return false;
    }
    
    function update($primary_key_value = "",$data = "")
    {
        if(!empty($primary_key_value) and !empty($data))
        {
            $this->db->where(array($this->primary_key => $primary_key_value));
            $query = $this->db->update($this->table,$data);
            return $query;
        }
        
        return false;
    }
    
}
