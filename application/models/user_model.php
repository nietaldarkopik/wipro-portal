<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

class User_model extends CI_Model{
  var $tbl_usr = '';

  function __construct(){
    parent::__construct();
    $this->tbl_usr = 'user_accounts';
  }
  
  function delete_member($user_id){
		$this->db->where(array('user_id' => $user_id));
		$this->db->delete($this->tbl_usr);
	}
  
  function get_login($user_id = 0, $use_current = TRUE){
    if(empty($user_id) && $use_current){
      $user_id = $this->get_id();
    }

    $query = $this->db->select('username')->where('user_id', $user_id)->get($this->tbl_usr);
    if ($query){
      if ($query->num_rows > 0)
      {
        $row = $query->row();
        return $row->username;
      }
    }

    return '';
  }
  
  function is_admin($user_id){
		$q_active = $this->db->get_where($this->tbl_usr, array('user_id' => $user_id));
		if($q_active->num_rows() > 0){
			$r_active = $q_active->row();
			if($r_active->active_status == 1 && $r_active->user_level_id == 1){
				return 	TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
	}
  
  function is_active($user_id){
		$q_active = $this->db->get_where($this->tbl_usr, array('user_id' => $user_id));
		if($q_active->num_rows() > 0){
			$r_active = $q_active->row();
			if($r_active->active_status == 1){
				return TRUE;
			}else{
				return FALSE;
			}
		}else{
			return FALSE;
		}
  }
  
  function status_active($user_id){
		$user_data = $this->get($user_id);
		
		if(FALSE !== $user_data){
			$active_status = $user_data->active_status;
			if($active_status == 0){
				$this->db->where(array('user_id'=>$user_id));
				return $this->db->update($this->tbl_usr, array('active_status'=>'','active_status'=>1));				
			}else{
				return FALSE;				
			}
		}
	}
  
  function status_inactive($user_id){
		$user_data = $this->get($user_id);
		
		print_r($user_data);
		
		if(FALSE !== $user_data){
			$active_status = $user_data->active_status;
			
			if($user_data->user_level_id == 1){
				return FALSE;
			}
			
			if($active_status == 1){
				$this->db->where(array('user_id'=>$user_id));
				return $this->db->update($this->tbl_usr, array('active_status'=>'','active_status'=>0));				
			}else{
				return FALSE;				
			}
		}
	}
  
  function activate($activation_code){
		$activ_q = $this->db->get_where($this->tbl_usr, array('user_activation_key'=>$activation_code));
		if($activ_q->num_rows() > 0){
			$user_found = $activ_q->row();
			$this->db->where(array('user_id'=>$user_found->user_id));
			$this->db->update($this->tbl_usr, array('user_activation_key'=>'','active_status'=>'active'));
			return TRUE;
		}
		
    return FALSE;
  }
  
  function is_exist($args=array()){
    $default_args = array(
      'user_id' => '',
      'username' => '',
      'password' => '',
      'user_level_id' => '',
      'email' => ''
    );

    if (empty($args)){
      $args = array();
    }
    
    $args = array_merge($default_args, $args);
    
    extract($args);

    $this->db->select('user_id');

    if (!empty($user_id)){
			$this->db->where('user_id', $user_id);
    }

    if (!empty($username)){
      $this->db->where('username', $username);
    }

    if (!empty($email)){
      $this->db->where('email', $email);
    }

    if (!empty($password)){
			$password = md5($password);
      $this->db->where('password', $password);
    }

    if (!empty($user_level_id)){
			$this->db->where('user_level_id', $user_level_id);
    }

    $query = $this->db->get($this->tbl_usr);
    
    //dump($this->db->last_query());
    
    if ($query->num_rows() > 0){
      $row = $query->row_array();
      $user_id = $row['user_id'];
      return $user_id;
    }

    return FALSE;
  }
  
	function generate_password_code($email){
		$pass_code_q = $this->db->get_where($this->tbl_usr, array('email'=>$email));
		if($pass_code_q->num_rows() > 0){
			$user_detail = $pass_code_q->row();
			$user_id = $user_detail->user_id;
			$username = $user_detail->username;
			$password_code = md5($user_id . $username);
			return $password_code;
		}
		return FALSE;
	}
	
	function register_forgot_password($email_address='', $code=''){
		if(!isset($email_address) || empty($email_address)){
			return FALSE;
		}
		
		$email_check = $this->is_exist(array('email'=>$email_address));
		
		if(!$email_check){
			return FALSE;
		}
		
		$this->db->where(array('email'=>$email_address));
		$update_pass_code = $this->db->update($this->tbl_usr, array('active_status'=>'not active','reset_code'=>$code));
		
		if($update_pass_code){
			$email_args = array('email_address' => $email_address, 'code' => $code);
			$this->skayla_email->forgot_password_email($email_args);
		}
		
		return $update_pass_code;
	}
		
  function gets($args = '', &$f_users){
		$default_args = array(
			's' => '',
			'search_by' => '',
			'order_by' => 'user_id',
			'sort' => 'DESC',
			'limit' => 10,
			'paged' => 0,
			'exclude_role' => 0
		);
		
		$args = array_merge($default_args, $args);
		
		extract($args);
		
		$this->db->start_cache();
		
		if(!empty($exclude_role)){
			
			if(!is_array($exclude_role)){
				$exclude_role = explode(',', $exclude_role);
			}
			
			if(count($exclude_role) > 0){
				$this->db->where_not_in('user_level_id' , $exclude_role);
			}
			
		}
		
		if(!empty($s) && empty($search_by)){
			$this->db->where("(username LIKE '%" . $s . "%' OR 
							email LIKE '%" . $s . "%' OR 
							full_name LIKE '%" . $s . "%')");
		}elseif(!empty($s) && !empty($search_by)){
			$this->db->where($search_by." LIKE '%" . $s . "%'");
		}
		
		$this->db->order_by($order_by, $sort);
		
		$this->db->stop_cache();
		
		$query_found_products = $this->db->get($this->tbl_usr);
		
		$cache_query = $this->db->last_query();
		
		$f_users = $query_found_products->num_rows();
		
		if($limit > 0){
		
			$this->db->limit($limit, ($paged * $limit));
			
		}
		
    $query = $this->db->get($this->tbl_usr);
    
    $this->db->flush_cache();
    
    if ($query->num_rows() > 0){
      return $query->result();
    }

    return FALSE;
  }
  
  function get_id(){
    $user_id = $this->session->userdata('user_id');
    return ( ! empty($user_id)) ? $user_id : 0;
  }
  
  function username_test($user_name){
		$q_user_name_test = $this->db->get_where($this->tbl_usr, array('username' => $user_name));
		if($q_user_name_test->num_rows() > 0){
			return FALSE;
		}
		
		return TRUE;
	}
  
  function email_test($email){
		$q_email_test = $this->db->get_where($this->tbl_usr, array('email' => $email));
		if($q_email_test->num_rows() > 0){
			return FALSE;
		}
		
		return TRUE;
	}
	
	function edit_member($data, $user_id){

    if (empty($session)){
      $session = $this->session->userdata('session_id');
    }

    if (is_array($data) && count($data) > 0){
			$default_data = array(
				'username' => '',
				'email' => '',
				'active_status' => 1,
				'full_name' => '',
				'user_level_id' => 3
			);
			
			$data = array_merge($default_data, $data);
      
      if(!empty($data['password'])){
				$data['password'] = md5($data['password']);
			}else{
				unset($data['password']);
			}
			
			if($data['active_status'] == 1){
				$data['user_activation_key'] = '';
			}
			
			$this->db->where(array('user_id' => $user_id));
			$this->db->update($this->tbl_usr, $data);
			
    }

    return FALSE;
	}
	
  function register($args){
		$default_args = array(
			'username' => '',
			'password' => '',
			'email' => '',
			'date_inserted' => date('Y-m-d H:i:s'),
			'active_status' => 'not active',
			'user_level_id' => 8,
		);
		
		$data = array_merge($default_args, $args);
		
		$data['password'] = md5($data['password']);
		$data['user_activation_key'] = $this->create_activation_code();
		
		if ($this->db->insert($this->tbl_usr, $data)){
			$user_id = $this->db->insert_id();
			$this->skayla_email->registration_email(array(
					'email' => $data['email'],
					'username' => $data['username'],
					'user_activation_key' => $data['user_activation_key']
				));
			return $user_id;
		}
      
    return FALSE;
  }
  
  function create_activation_code(){
    return random_string('alnum', 32);
  }
  
	function validate_forgot_password_code($code){
		$q_get_pass_code = $this->db->get_where($this->tbl_usr, array('reset_code'=>$code));
		
		if($q_get_pass_code->num_rows() > 0){
			$user_detail = $q_get_pass_code->row();			
			return $user_detail->user_id;
		}
		return FALSE;
	}
	
	function change_password($user_id, $password, $code=''){
		$change_password_q = $this->db->get_where($this->tbl_usr, array('user_id'=>$user_id));
		if($change_password_q->num_rows() > 0){
			$change_password = $change_password_q->row();
			
			if($change_password->reset_code == $code){
				$this->db->where(array('user_id'=>$user_id));
				$update_user = $this->db->update($this->tbl_usr, array(
					'password'=>md5($password),
					'reset_code'=>'',
					'active_status'=>'active'
				));
				
				if($update_user){
					return $user_id;
				}
			}
			
		}
		
		return FALSE;
	}
  
}
