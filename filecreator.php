<?php

$file_controller = array(
/*'dashboard',*/
'appearence',
'appearence_themes',
'appearence_plugins',
/*'appearence_menu_groups',
'appearence_menus',*/
'appearence_pages',
'administration',
/*'administration_user_levels',
'administration_users',
'administration_user_roles',*/
'administration_api_access',
'catalog',
'catalog_categories',
'catalog_products',
'catalog_cms',
'catalog_themes',
'catalog_plugins',
'catalog_services',
'store',
'store_orders',
'store_customers',
'store_payment_methods',
'store_delivery_methods',
'store_price_manager',
'store_reports',
'client_area',
'client_area_affiliate',
'client_area_bonus',
'client_area_hosting_manager',
'client_area_domain_manager',
'client_area_website_manager',
'configuration');
foreach($file_controller as $i => $file)
{
    $new_file_name = ucfirst($file).".php";
    copy('./application/controllers/admin/data_propinsi.php','./application/controllers/admin/'.$new_file_name);
    
    $new_content = str_replace('Data_propinsi',ucfirst($file),file_get_contents('./application/controllers/admin/'.$new_file_name));
    $new_content = str_replace('data_propinsi',strtolower($file),$new_content);
    $new_content = str_replace('Propinsi',str_replace("_"," ",ucfirst($file)),$new_content);
    
    file_put_contents('./application/controllers/admin/'.$new_file_name,$new_content);
}
?>