<?php

$dir_controller = array(
'dashboard',
//'appearence_menus',
'appearence_pages',
//'administrator_user_roles',
'catalog_categories',
'catalog_products',
'catalog_cms',
'catalog_themes',
'catalog_plugins',
'catalog_services',
'store_orders',
'store_payment_methods',
'store_delivery_methods',
'store_price_manager',
'store_reports',
'client_area_affiliate',
'client_area_bonus'
);

function recurse_copy($src,$dst) { 
    $dir = opendir($src); 
    @mkdir($dst); 
    while(false !== ( $file = readdir($dir)) ) { 
        if (( $file != '.' ) && ( $file != '..' )) { 
            if ( is_dir($src . '/' . $file) ) { 
                recurse_copy($src . '/' . $file,$dst . '/' . $file); 
            } 
            else { 
                copy($src . '/' . $file,$dst . '/' . $file); 
            } 
        } 
    } 
    closedir($dir); 
} 

foreach($dir_controller as $i => $dir)
{
    recurse_copy('./themes/admin/bootstrapv33/templates/layouts/default','./themes/admin/bootstrapv33/templates/layouts/'.$dir);
}
?>