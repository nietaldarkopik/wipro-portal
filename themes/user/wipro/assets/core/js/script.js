$(document).ready(function(){
  function setup_tinymce(selector)
  {
    selector = (typeof selector != "undefined")?selector:".inline_editable";
    $(selector).tinymce({
      //selector: "textarea",
      theme: "simple",
      plugins: [
          "advlist autolink lists link image charmap print preview hr anchor pagebreak",
          "searchreplace wordcount visualblocks visualchars code fullscreen",
          "insertdatetime media nonbreaking save table contextmenu directionality",
          "emoticons template paste textcolor colorpicker textpattern template"
      ],
      toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
      //toolbar2: "print preview media | forecolor backcolor emoticons | template",
      image_advtab: true,
      templates: [
          {title: 'Test template 1', content: 'Test 1'},
          {title: 'Test template 2', content: 'Test 2'}
      ]
    });
  }
  
  function create_panel(config)
  {
    var setting = {
                    modal_type: "modal-sm",
                    class:"",
                    id:"theme-panel",
                    body:"",
                    footer: '<button type="button" class="btn btn-default modal-close" data-dismiss="modal">Close</button>'+
                            '<button type="button" class="btn btn-primary modal-save">Save changes</button>',
                    label:"Setting Widget"
                  };
    var config = (!config)?settng:$.extend(setting,config);
    
    var panel = '<div class="modal fade '+config.class+'" id="' + config.id + '" tabindex="-1" role="modal" aria-labelledby="' + config.id + 'Label" aria-hidden="true">'+
                '  <div class="modal-dialog ' + config.modal_type + '">'+
                '    <div class="modal-content">'+
                '      <div class="modal-header draggable-handle">'+
                '        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
                '        <h4 class="modal-title" id="' + config.id + 'Label">' + config.label + '</h4>'+
                '      </div>'+
                '      <div class="modal-body">'+
                '        <div class="panel-body">'+
                          config.body +
                '        </div>'+
                '      </div>'+
                '      <div class="modal-footer">'+
                         config.footer +
                '      </div>'+
                '    </div>'+
                '  </div>'+
                '</div>';
                
    $("body").append(panel);
  }
  
  create_panel({
    modal_type: "modal-lg block-dragable",
    class:"containment",
    id:"setting-widget",
    body:"Hello World"
  });
  
  create_panel({
    modal_type: "modal-lg block-dragable",
    class:"containment",
    id:"admin-menu-panel",
    body:"&nbsp;",
    footer: ""
  });
  
  $( ".block-sortable > *" ).append("<a href=\"javascript:void(0);\" title=\"Move this block\" class=\"sortable-handle glyphicon glyphicon-move\"></a>");
  //$( ".block-dragable" ).draggable({ scroll: true, scrollSpeed: 100});//,handle: $(this).find(".draggable-handle"),containment: $(this).parent(".containment"),cursor: "move",});
  //$( ".inline_editable" ).after("<a href=\"javascript:void(0);\" title=\"Move this block\" class=\"sortable-handle glyphicon glyphicon-move\"></a>");
  
  $( ".inline_editable" ).each(function(i,k){
    if($(this).find(".editable").length == 0)
    {}else{
      var id = $(this).find(".editable").attr("id");
      $(this).find(".editable").after("<a class=\"toggle_editable glyphicon glyphicon-pencil\" id=\"toogle_"+id+"\" href=\"javascript:void(0);\"></a>");
    }
  });
  
  $( ".block-sortable" ).each(function(i,k){
    var connectwith = $(k).attr("connect-with");
    connectwith = (!connectwith)?false:connectwith;
    var orginalcolorgroup,orginalcolorsingle;
    $(k).sortable({
      scroll: true,
      connectWith: connectwith,
      revert: true,
      items: "> *:not(.configuration)",
      //placeholder: "ui-state-highlight",
      forcePlaceholderSize: false,
      handle: ".sortable-handle",
      tolerance: "pointer",
      start: function( event, ui ) {
        var height = $(".ui-sortable-placeholder").css('height');
        height = height.replace("px","");
        height = parseInt(height);
        height = height - 2;
        $(".ui-sortable-placeholder").css('height',height);
        $(".ui-sortable-placeholder").css('margin',"0");
        orginalcolorgroup = $(ui.item).css('background-color');
        orginalcolorsingle = $(k).css('background-color');
        //$(ui.item).css("padding","-2px");
        $(connectwith).animate({
          backgroundColor: "#ff9"
        });
        $(k).animate({
          backgroundColor: "#ff9"
        });
      },
      stop: function( event, ui ) {
        $(connectwith).animate({
          backgroundColor: orginalcolorgroup
        });
        $(k).animate({
          backgroundColor: orginalcolorsingle
        });
      }
    });
  });

  function submit_inline(form)
  {
    var t_form = $(form).parents("form");
    var page_description = $(t_form).find("input[name*='mce_']").val();
    page_description = $('<textarea/>').html(page_description).html();
    $(t_form).find("input[name='page_description']").val(page_description);
    var data = $(t_form).serializeArray();
    $.ajax({
      url:$(t_form).attr("action"),
      type:"post",
      data:data,
      success:function(msg)
      {
        alert(msg);
      }
    });
  }
  
  tinymce.init({
    selector: ".inline_editable",
    inline: true,
    plugins: [
        "save advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | save | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    save_onsavecallback: function(a)
    {
      submit_inline("#"+a.id);
    }
  });
  /*
  $(".toggle_editable").on("click",function(){
    var id_tinymce = $(this).attr("id");
    id_tinymce = id_tinymce.replace("toogle_","");
    tinymce.execCommand('mceToggleEditor',false,id_tinymce);
  });
  */
  $(".block-widgets-container").each(function(k,v){
    var block_name = $(v).attr("id");
    if(typeof block_name != "undefined")
    {
      $(v).append('<div class="configuration"><a href="javascript:void(0);" title="Click this to configure this widget block" class="btn btn-primary btn-xs setting-widget" id="showblock_'+block_name+'"><span class="glyphicon glyphicon-cog"></span> Settings Widgets</a></div>');
    }
  });
  
  $(".configuration *").on("mouseover",function(){
    $(this).parents(".block-widgets-container").css("background","#ff9").css("opacity","0.9");
  });
  
  $(".configuration *").on("mouseout",function(){
    $(this).parents(".block-widgets-container").css("background","").css("opacity","1");
  });
  
  $(".configuration .setting-widget").on("click",function(){
    var blockid = $(this).attr("id");
    blockid = blockid.replace("showblock_block-","");
    $.ajax({
      url: base_url+"page_block/block/"+blockid,
      type: "post",
      data: "is_ajax=1",
      success: function(result)
      {
        $("#setting-widget .modal-body").html($(result).find("#block-main").html());
        $("#setting-widget").modal('show');
      }
    });
  });
});
