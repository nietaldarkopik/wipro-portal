<?php

class Wipro{
  var $CI;
  var $front_panel = true;
  
  function __construct()
  {
    $this->CI =& get_instance();
  }
  
  function setAssets()
  {
    $css = '
    <!-- CSS -->
    <link href="'.current_theme_url().'assets/bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="'.current_theme_url().'assets/bootstrap/dist/css/bootstrap-theme.css" rel="stylesheet" type="text/css" />
    <link href="'.current_theme_url().'assets/theme/css/flexslider.css" rel="stylesheet" type="text/css" />
    <link href="'.current_theme_url().'assets/theme/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
    <link href="'.current_theme_url().'assets/theme/css/animate.css" rel="stylesheet" type="text/css" media="all" />
    <link href="'.current_theme_url().'assets/theme/css/owl.carousel.css" rel="stylesheet">
    <link href="'.current_theme_url().'assets/theme/css/style.css" rel="stylesheet" type="text/css" />
      
    <!-- FONTS	 -->
    <link href="'.current_theme_url().'assets/theme/css/fonts.css" rel="stylesheet" type="text/css">
    <link href="'.current_theme_url().'assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link type="text/css" rel="stylesheet" media="all" href="'.current_theme_url().'assets/core/css/bootstrap-theme-admin.css">';
    
    $js_head = '
    <!-- SCRIPTS -->
    <!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
      <!--[if IE]><html class="ie" lang="en"> <![endif]-->
    
    <script src="'.current_theme_url().'assets/theme/js/jquery.min.js" type="text/javascript"></script>
    <script src="'.current_theme_url().'assets/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="'.current_theme_url().'assets/theme/js/jquery.prettyPhoto.js" type="text/javascript"></script>
    <script src="'.current_theme_url().'assets/theme/js/jquery.nicescroll.min.js" type="text/javascript"></script>
    <script src="'.current_theme_url().'assets/theme/js/superfish.min.js" type="text/javascript"></script>
    <script src="'.current_theme_url().'assets/theme/js/jquery.flexslider-min.js" type="text/javascript"></script>
    <script src="'.current_theme_url().'assets/theme/js/owl.carousel.js" type="text/javascript"></script>
    <script src="'.current_theme_url().'assets/theme/js/animate.js" type="text/javascript"></script>
    <script src="'.current_theme_url().'assets/theme/js/jquery.BlackAndWhite.js"></script>
    <script src="'.current_theme_url().'assets/theme/js/myscript.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="'.current_theme_url().'assets/core/jquery/tinymce_1.4/tinymce.min.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/core/jquery/tinymce_1.4/jquery.tinymce.min.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/core/js/jquery.form.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/core/js/ajax.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/core/js/data.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/core/js/blocks.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/core/js/script.js"></script>
    <script type="text/javascript" src="'.current_theme_url().'assets/core/js/admin.js"></script>
    <script>
      
      //PrettyPhoto
      jQuery(document).ready(function() {
        $("a[rel^=\'prettyPhoto\']").prettyPhoto();
      });
      
      //BlackAndWhite
      $(window).load(function(){
        $(".client_img").BlackAndWhite({
          hoverEffect : true, // default true
          // set the path to BnWWorker.js for a superfast implementation
          webworkerPath : false,
          // for the images with a fluid width and height 
          responsive:true,
          // to invert the hover effect
          invertHoverEffect: false,
          // this option works only on the modern browsers ( on IE lower than 9 it remains always 1)
          intensity:1,
          speed: { //this property could also be just speed: value for both fadeIn and fadeOut
            fadeIn: 300, // 200ms for fadeIn animations
            fadeOut: 300 // 800ms for fadeOut animations
          },
          onImageReady:function(img) {
            // this callback gets executed anytime an image is converted
          }
        });
      });
      
    </script>
    ';
    
    $this->CI->assets->add_css($css,"head");
    $this->CI->assets->add_js($js_head,"head");
    
    $js = '';
    $this->CI->assets->add_js($js,"body");
    
    if($this->front_panel !== false)
    {
      $this->setAdminAssets();
    }
    
  }
  
  function setAdminAssets()
  {
			$is_login = $this->CI->user_access->is_login();
			if(!$is_login and $this->CI->uri->segment(1) != "admin" and $this->CI->uri->segment(2) != "dashboard" and $this->CI->uri->segment(3) != "login")
			{
        if(file_exists(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/'.CURRENT_ADMIN_THEME . '.php'))
        {
          require_once(FCPATH.'themes/admin/'.CURRENT_ADMIN_THEME.'/'.CURRENT_ADMIN_THEME . '.php');
          $theme_config = CURRENT_ADMIN_THEME;
          $theme_loader = new $theme_config();
          $theme_loader->setAssets();
        }
      }
  }
}
