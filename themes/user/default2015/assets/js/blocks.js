/*
	var ajax_url = "";
	var ajax_form = "";
	var ajax_target = "";
	var theTimeout = "";
	var certificate_id = "";
	var data_json = "";
	var loading_text = '<div class="fclear" style="position: relative;margin: 20% 50%;display: block;width: auto;float: left;text-align: center;"><img src="' + base_url + 'themes/'+ theme_name + '/static/img/loader.gif"/> Loading ... </div>';
	base_url = base_url.replace("index.php/","");
	
	jQuery.ajaxSetup({
		async: false,
		complete:function(){
		}
	});
	
	jQuery.validator.setDefaults({
			errorPlacement: function(error, element){
				var the_error = '<p class="error">' + error.html() + '</p>';
				jQuery(element).parent().find("p.error").remove();
				jQuery(the_error).appendTo(jQuery(element).parent());
				if (jQuery(element).hasClass("input_tinymce") || jQuery(element).hasClass("input_tinymce_simple")) {
					var id_element = jQuery(element).attr("id");
					tinymce.execCommand('mceAddControl', true,id_element);
				}
			}
	});
	
	function myConfirm(dialogText, okFunc, cancelFunc, dialogTitle) {
		jQuery('<div style="padding: 10px; max-width: 500px; word-wrap: break-word;">' + dialogText + '</div>').dialog({
			draggable: false,
			modal: true,
			resizable: false,
			width: 'auto',
			title: dialogTitle || 'Confirm',
			minHeight: 75,
			buttons: {
				OK: function () {
					if (typeof (okFunc) == 'function') {
						setTimeout(okFunc, 50);
					}
					jQuery(this).dialog('destroy');
				},
				Cancel: function () {
					if (typeof (cancelFunc) == 'function') {
						setTimeout(cancelFunc, 50);
					}
					jQuery(this).dialog('destroy');
				}
			}
		});
	}
	
	function load_login_form()
	{
		ajax_target = ".login_form";
		jQuery(ajax_target).html(loading_text);
		jQuery.ajax({
			url : base_url + "dashboard/do_login",
			data : "is_ajax=1",
			type : "post",
			success: function(result){
				jQuery(ajax_target).hide();
				jQuery(ajax_target).html(result);
				jQuery(ajax_target).fadeIn(3000);
				load_all('load_login_form');
			}
		});
	}
	
	function load_rightbar()
	{		
		ajax_target = "#main_content .col2";
		jQuery(ajax_target).html(loading_text);
		jQuery.ajax({
			url : base_url + "dashboard/rightbar/",
			data : "is_ajax=1&current_url="+current_url,
			type : "post",
			success: function(result){
				jQuery(ajax_target).hide();
				jQuery(ajax_target).html(result);
				jQuery(ajax_target).fadeIn(3000);
				if(jQuery(ajax_target).html() == "")
				{
					jQuery(".col1").width("99%");
				}else{
					jQuery(".col1").width("70%");
				}
			}
		});
	}
	
	function load_right_menu()
	{
		ajax_target = ".right_menu";
		//ajax_target = "#main_content .col2";
		jQuery(ajax_target).html(loading_text);
		jQuery.ajax({
			url : base_url + "dashboard/right_menu",
			data : "is_ajax=1&current_url="+current_url,
			type : "post",
			success: function(result){
				jQuery(ajax_target).html(result);
				/*
				jQuery( ajax_target + " > ul" ).menu({ 
						position: { my: "left top", at: "right+2 top-2"} ,
						collision: "none"
					});
				* /
			}
		});
	}
	
	function load_top_menu()
	{
		ajax_target = "#top_menu";
		jQuery(ajax_target).html(loading_text);
		jQuery.ajax({
			url : base_url + "dashboard/top_menu",
			data : "is_ajax=1&current_url="+current_url,
			type : "post",
			success: function(result){
				jQuery(ajax_target).hide();
				jQuery(ajax_target).html(result);
				jQuery(ajax_target).fadeIn(1000);
			}
		});
	}
	
	function load_content()
	{
		ajax_target = "article.col1";
		jQuery(ajax_target).html(loading_text);
		jQuery.ajax({
			url : current_url,
			data : "is_ajax=1",
			type : "post",
			success: function(result){
				var the_result = jQuery('<div/>').html(result).contents();
				the_result = jQuery(the_result).find(ajax_target);
				jQuery(the_result).hide();
				if(the_result.length > 0)
					result = jQuery(the_result).html();
					
				jQuery(ajax_target).hide();
				jQuery(ajax_target).html(result);
				jQuery(ajax_target).fadeIn(1000);
				/*
				if(jQuery(".col2").length == 0)
				{
					jQuery(".col1").width("100%");
				}
				* /
				jQuery("#form").validate();
				init();
			}
		});
	}
		
	function load_all(load_from)
	{
		//if(load_from != 'load_login_form')
		//	load_login_form();
		//if(load_from != 'load_rightbar')
		//load_rightbar();
		//if(load_from != 'load_right_menu')
		//load_right_menu();
		if(load_from != 'load_top_menu')
		load_top_menu();
		if(load_from != 'load_content')
		load_content();
		
		
	}

	function init()
	{
			
		jQuery( ".input_date" ).datepicker({dateFormat :"yy-mm-dd"});
		
		if(jQuery("form#form .input_tinymce_simple,form#form .input_tinymce,.text_editor").length > 0)
		{
			if (typeof(tinyMCE) != "undefined") {
			  if (tinyMCE.activeEditor == null || tinyMCE.activeEditor.isHidden() != false) {
				tinyMCE.editors=[];
			  }
			}
			jQuery("form#form .input_tinymce_simple,form#form .input_tinymce").tinymce({
				plugins: [
					"advlist autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste"
				],
				menubar: false,
				toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
			});
			
			jQuery(".text_editor").tinymce({
				plugins: [
					"save advlist autolink lists link image charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste"
				],
				menubar: false,
				toolbar: "save undo redo",
				save_enablewhendirty: false,
				save_onsavecallback: function(ed){
					//var content = jQuery(ed).html();
					//content =  jQuery('<div/>').html(content).text();
					
					var key = jQuery(ed).attr("id");
					var the_key = jQuery("#"+key).attr("key");
					var content = tinyMCE.get(key).getContent();
					content = content.replace('&','_and_');
					jQuery.ajax({
						url: base_url+"setting_certificate/edit/"+certificate_id,
						type: "post",
						data: "key="+the_key+"&content="+content,
						success: function(msg){
							if(msg == 1)
							{
								alert("Data telah disimpan");
							}else{
								alert("Data gagal disimpan. Silakan coba lagi.");
							}
						}
					});
				},
				inline: true
			});
		}else{
		
			if (typeof(tinyMCE) != "undefined") {
			  if (tinyMCE.activeEditor == null || tinyMCE.activeEditor.isHidden() != false) {
				tinyMCE.editors=[];
			  }
			}
		}
		jQuery("#form").validate();

		jQuery.each(data_json,function(k,v){
			jQuery("[key="+k+"]").html(v);
		});
	}
	
	
var is_hide_menu = 'false';

function create_menuscroller(obj)
{
	if(is_hide_menu == 'false')
	{
		is_hide_menu = 'true';
		jQuery('#main-menu').animate({ marginTop: -10000 }, {
			duration: 500,
			queue: false
		});
		jQuery(jQuery(obj)).find(".ui-icon").removeClass("ui-icon-triangle-1-n");
		jQuery(jQuery(obj)).find(".ui-icon").addClass("ui-icon-triangle-1-s");
		jQuery('#main-menu').hide();
	}else{
		jQuery('#main-menu').show();
		is_hide_menu = 'false';
		jQuery('#main-menu').animate({ marginTop: 0, top: jQuery(document).scrollTop()+50 }, {
			duration: 500,
			queue: false
		});
		jQuery(jQuery(obj)).find(".ui-icon").removeClass("ui-icon-triangle-1-s");
		jQuery(jQuery(obj)).find(".ui-icon").addClass("ui-icon-triangle-1-n");
	}
	jQuery(this).find(".ui-icon").removeClass("ui-icon-triangle-1-n");
	jQuery(this).find(".ui-icon").addClass("ui-icon-triangle-1-s");
	jQuery.cookie('is_hide_menu', is_hide_menu, { expires: 1, path: '/' });
}

var fixedHeader = jQuery('#main-menu').css('position', 'relative');

jQuery(document).on('scroll',function(){
	if(is_hide_menu == 'false')
	{
		jQuery('#main-menu').show();
		jQuery('#main-menu').animate({ top: jQuery(document).scrollTop()+50 }, {
			duration: 500,
			queue: false
		});
	}else{
		jQuery('#main-menu').hide();
		is_hide_menu = 'true';
		jQuery('#main-menu').animate({ marginTop: -10000 }, {
			duration: 500,
			queue: false
		});
	}
});


jQuery(document).ready(function(){
	var tis_hide_menu = jQuery.cookie('is_hide_menu');
	if(typeof(tis_hide_menu) == 'undefined')
	{
		jQuery.cookie('is_hide_menu', is_hide_menu, { expires: 1, path: '/' });
	}
	is_hide_menu = jQuery.cookie('is_hide_menu');
	
	load_all('load_content');
	
	jQuery(".scroll-menu-to-top").click(function(){
		create_menuscroller(jQuery(this));
	});
		
	jQuery("form#form .input_tinymce_simple,form#form .input_tinymce").tinymce({
		plugins: [
			"advlist autolink lists link image charmap print preview anchor",
			"searchreplace visualblocks code fullscreen",
			"insertdatetime media table contextmenu paste"
		],
		menubar: false,
		toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
	});
	
	function updateTips( t ) {
		tips
			.text( t )
			.addClass( "ui-state-highlight" );
		setTimeout(function() {
			tips.removeClass( "ui-state-highlight", 1500 );
		}, 500 );
	}
	
});
*/
