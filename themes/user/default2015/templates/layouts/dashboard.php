<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  echo $this->block->call_block("left");
?>
    <div class="container-fluid" id="maincontainer">
      <div class="row block-top-nav" id="blocktopnav">
        <div class="col-md-12 col-lg-12">
          <div class="row block-widgets">
            <?php echo $this->block->call_block("top");?>
          </div>
        </div>
      </div>
      <div class="row block-top-column" id="topcolumn">
        <div class="col-md-12 col-lg-12">
          <div class="row block-widgets">
            <?php echo $this->block->call_block("topcolumn");?>
          </div>
        </div>
      </div>
      <div class="row block-main-column" id="blockmaincolumn">
        <div class="col-md-12 col-lg-12 block-widgets">
          <div class="row">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="#">Library</a></li>
              <li class="active">Data</li>
            </ol>
          </div>
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div class="container">
                <div class="center wow fadeInDown animated" style="visibility: visible; -webkit-animation: fadeInDown;">
                  <h2>Features</h2>
                  <div class="col-lg-12 inline_editable">
                    <div class="row">
                      <div class="editable" id="page_description">
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                    <div class="features col-lg-12 block-widgets-container block-sortable" id="block-subcontent">
                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-bullhorn"></i>
                                <h2>Fresh and Clean</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div><!--/.col-md-4-->

                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-comments"></i>
                                <h2>Retina ready</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div><!--/.col-md-4-->

                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-cloud-download"></i>
                                <h2>Easy to customize</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div><!--/.col-md-4-->
                    
                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-leaf"></i>
                                <h2>Adipisicing elit</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div><!--/.col-md-4-->

                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-cogs"></i>
                                <h2>Sed do eiusmod</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div><!--/.col-md-4-->

                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-heart"></i>
                                <h2>Labore et dolore</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div><!--/.col-md-4-->
                    </div><!--/.services-->
                </div><!--/.row-->    
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row block-bottom-column" id="blockbottomcolumn">
        <div class="col-md-12 col-lg-12">
          <div class="row block-widgets block-widgets-container block-sortable" id="block-bottom">
            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">We are hiring</a></li>
                        <li><a href="#">Meet the team</a></li>
                        <li><a href="#">Copyright</a></li>
                        <li><a href="#">Terms of use</a></li>
                        <li><a href="#">Privacy policy</a></li>
                        <li><a href="#">Contact us</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Support</h3>
                    <ul>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Forum</a></li>
                        <li><a href="#">Documentation</a></li>
                        <li><a href="#">Refund policy</a></li>
                        <li><a href="#">Ticket system</a></li>
                        <li><a href="#">Billing system</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Developers</h3>
                    <ul>
                        <li><a href="#">Web Development</a></li>
                        <li><a href="#">SEO Marketing</a></li>
                        <li><a href="#">Theme</a></li>
                        <li><a href="#">Development</a></li>
                        <li><a href="#">Email Marketing</a></li>
                        <li><a href="#">Plugin Development</a></li>
                        <li><a href="#">Article Writing</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Our Partners</h3>
                    <ul>
                        <li><a href="#">Adipisicing Elit</a></li>
                        <li><a href="#">Eiusmod</a></li>
                        <li><a href="#">Tempor</a></li>
                        <li><a href="#">Veniam</a></li>
                        <li><a href="#">Exercitation</a></li>
                        <li><a href="#">Ullamco</a></li>
                        <li><a href="#">Laboris</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->
        </div>
        </div>
      </div>
      <div class="row block-bottom-nav" id="blockbottomnav">
        <div class="col-md-12 col-lg-12">
          <div class="row block-widgets">
            <div class="col-sm-6">
                &copy; 2013 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>. All Rights Reserved.
            </div>
            <div class="col-sm-6">
                <ul class="nav navbar-nav pull-right">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Faq</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php
  $this->load->view("footer");
?>
