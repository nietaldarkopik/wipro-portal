<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
?>
    <div class="container-fluid" id="maincontainer">
      <div class="row block-top-nav" id="blocktopnav">
        <div class="col-md-12 col-lg-12">
          <div class="row block-widgets">
            <nav class="navbar navbar-inverse" role="banner">
              <div "/*container*/clearfix">
                <div class="row">
                  <div class="navbar-header">
                      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".user-navbar-collapse">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand" href="index.html"><img src="<?php echo current_theme_url();?>assets/images/logo.png" alt="logo" class="img-responsive"></a>
                  </div>
                  <div class="collapse navbar-collapse user-navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="index.html">Home</a></li>
                        <li class="active"><a href="full-width.html">Full Width</a></li>
                        <li><a href="left-bar.html">Left Bar</a></li>
                        <li><a href="right-bar.html">Right Bar</a></li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <i class="glyphicon glyphicon-angle-down"></i></a>
                          <ul class="dropdown-menu">
                              <li><a href="three-columns.html">Three Columns</a></li>
                              <li><a href="four-columns.html">Four Columns</a></li>
                          </ul>
                        </li>                    
                    </ul>
                  </div>
                </div>
              </div>
            </nav>
          </div>
        </div>
      </div>
      <div class="row block-top-column" id="topcolumn">
        <div class="col-md-12 col-lg-12">
          <div class="row block-widgets">
            <div id="carousel-slider" class="carousel slide" data-ride="carousel">
              <!-- Indicators -->
              <ol class="carousel-indicators visible-xs">
                <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-slider" data-slide-to="1"></li>
                <li data-target="#carousel-slider" data-slide-to="2"></li>
              </ol>

              <div class="carousel-inner">
                <div class="item active">
                  <img src="<?php echo current_theme_url();?>assets/images/sliders/slider_one.jpg" class="img-responsive" alt=""> 
                 </div>
                 <div class="item">
                  <img src="<?php echo current_theme_url();?>assets/images/sliders/slider_one.jpg" class="img-responsive" alt=""> 
                 </div> 
                 <div class="item">
                  <img src="<?php echo current_theme_url();?>assets/images/sliders/slider_one.jpg" class="img-responsive" alt=""> 
                 </div> 
              </div>
              
              <a class="left carousel-control hidden-xs" href="#carousel-slider" data-slide="prev">
                <i class="glyphicon glyphicon-chevron-left"></i> 
              </a>
              
              <a class=" right carousel-control hidden-xs"href="#carousel-slider" data-slide="next">
                <i class="glyphicon glyphicon-chevron-right"></i> 
              </a>
            </div> <!--/#carousel-slider-->
          </div>
        </div>
      </div>
      <div class="row block-main-column" id="blockmaincolumn">
        <div class="col-md-12 col-lg-12 block-widgets block-widgets-container" id="block-main">
          <div class="row">
            <ol class="breadcrumb">
              <li><a href="#">Home</a></li>
              <li><a href="#">Library</a></li>
              <li class="active">Data</li>
            </ol>
          </div>
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <div "/*container*/clearfix">
                <div class="center wow fadeInDown animated" style="visibility: visible; -webkit-animation: fadeInDown;">
                  <h2>Features</h2>
                  <div class="col-lg-12 inline_editable">
                    <div class="row">
                      <div class="editable" id="page_description">
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut <br> et dolore magna aliqua. Ut enim ad minim veniam</p>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-12">
                    <div class="row">
                      <?php echo (isset($content))?$content:"";?>
                    </div>
                  </div>
                </div>

                <div class="row">
                    <div class="features  col-lg-12 block-widgets-container block-sortable" id="">
                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-bullhorn"></i>
                                <h2>Fresh and Clean</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-comments"></i>
                                <h2>Retina ready</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-cloud-download"></i>
                                <h2>Easy to customize</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div>
                    
                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-leaf"></i>
                                <h2>Adipisicing elit</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-cogs"></i>
                                <h2>Sed do eiusmod</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-6 wow fadeInDown animated" data-wow-duration="1000ms" data-wow-delay="600ms" style="visibility: visible; -webkit-animation: fadeInDown 1000ms 600ms;">
                            <div class="feature-wrap">
                                <i class="glyphicon glyphicon-heart"></i>
                                <h2>Labore et dolore</h2>
                                <h3>Lorem ipsum dolor sit amet, consectetur adipisicing elit</h3>
                            </div>
                        </div>
                    </div><!--/.services-->
                </div><!--/.row-->    
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row block-bottom-column" id="blockbottomcolumn">
        <div class="col-md-12 col-lg-12">
          <div class="row block-widgets block-widgets-container block-sortable" id="block-bottom">
            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Company</h3>
                    <ul>
                        <li><a href="#">About us</a></li>
                        <li><a href="#">We are hiring</a></li>
                        <li><a href="#">Meet the team</a></li>
                        <li><a href="#">Copyright</a></li>
                        <li><a href="#">Terms of use</a></li>
                        <li><a href="#">Privacy policy</a></li>
                        <li><a href="#">Contact us</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Support</h3>
                    <ul>
                        <li><a href="#">Faq</a></li>
                        <li><a href="#">Blog</a></li>
                        <li><a href="#">Forum</a></li>
                        <li><a href="#">Documentation</a></li>
                        <li><a href="#">Refund policy</a></li>
                        <li><a href="#">Ticket system</a></li>
                        <li><a href="#">Billing system</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Developers</h3>
                    <ul>
                        <li><a href="#">Web Development</a></li>
                        <li><a href="#">SEO Marketing</a></li>
                        <li><a href="#">Theme</a></li>
                        <li><a href="#">Development</a></li>
                        <li><a href="#">Email Marketing</a></li>
                        <li><a href="#">Plugin Development</a></li>
                        <li><a href="#">Article Writing</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->

            <div class="col-md-3 col-sm-6">
                <div class="widget">
                    <h3>Our Partners</h3>
                    <ul>
                        <li><a href="#">Adipisicing Elit</a></li>
                        <li><a href="#">Eiusmod</a></li>
                        <li><a href="#">Tempor</a></li>
                        <li><a href="#">Veniam</a></li>
                        <li><a href="#">Exercitation</a></li>
                        <li><a href="#">Ullamco</a></li>
                        <li><a href="#">Laboris</a></li>
                    </ul>
                </div>    
            </div><!--/.col-md-3-->
        </div>
        </div>
      </div>
      <div class="row block-bottom-nav" id="blockbottomnav">
        <div class="col-md-12 col-lg-12">
          <div class="row block-widgets">
            <div class="col-sm-6">
                &copy; 2013 <a target="_blank" href="http://shapebootstrap.net/" title="Free Twitter Bootstrap WordPress Themes and HTML templates">ShapeBootstrap</a>. All Rights Reserved.
            </div>
            <div class="col-sm-6">
                <ul class="nav navbar-nav pull-right">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Faq</a></li>
                    <li><a href="#">Contact Us</a></li>
                </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php
  $this->load->view("footer");
?>
