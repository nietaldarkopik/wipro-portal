var loading_text = '<div class="progress margin20">'+
                      '<div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="100" aria-valuemax="100" style="width: 100%;">'+
                        'loading....'+
                      '</div>'+
                    '</div>';
                    
jQuery(document).on('submit',"form.ajax",function(event){
  event.preventDefault();
  var targetmodal = jQuery(this).attr("data-target");
  var target = jQuery(this).attr("data-target-ajax");
  var is_modal = (jQuery(this).hasClass("is_modal") || jQuery(this).parents(".modal").length > 0)?1:0;
  var this_element = this;
  var ajax_container = target;
  if(jQuery(this_element).parents(target).length > 0)
  {
    ajax_container = jQuery(this_element).parents(target);
    jQuery(this_element).parents(target).html(loading_text);
  }else{
    jQuery(target).html(loading_text);
  }
  
  jQuery(this).ajaxSubmit({
    data: {is_modal : ((is_modal)?'-modal':'')},
    target: jQuery(this).attr("data-target"),
    success: function(output, status) {
      var coutput = jQuery('<div/>').append(output).find(target);
      var output = (jQuery(coutput).length > 0)?jQuery(coutput).html():output;
      jQuery(ajax_container).html(output);
    }
  });
});

jQuery(document).on('click',"a.ajax",function(event){
  event.preventDefault();
  var targetmodal = jQuery(this).attr("data-target");
  var target = jQuery(this).attr("data-target-ajax");
  var is_modal = (jQuery(this).hasClass("is_modal") || jQuery(this).parents(".modal").length > 0)?1:0;
  var this_element = this;
  var ajax_container = target;
  
  if(is_modal)
  {
    jQuery(targetmodal).modal('show');
  }
  
  if(jQuery(this_element).parents(target).length > 0)
  {
    ajax_container = jQuery(this_element).parents(target);
    jQuery(this_element).parents(target).html(loading_text);
  }else{
    jQuery(target).html(loading_text);
  }
  
  var href = jQuery(this).attr("href");
  jQuery.ajax({
    url:  href,
    type: "post",
    data: "is_ajax=1&is_modal="+((is_modal)?'-modal':''),
    success:function(output){
      var coutput = jQuery('<div/>').append(output).find(target);
      var output = (jQuery(coutput).length > 0)?jQuery(coutput).html():output;
      if(is_modal)
      {
        if(jQuery(output).find(".panel-body").length > 0)
        {
          output = jQuery(output).find(".panel-body").html();
        }
      }
      jQuery(ajax_container).html(output);
      if(is_modal)
      {
        jQuery(ajax_container).find("a").addClass("is_modal");
      }
    }
  });
});

jQuery(document).on('click',"#pager a",function(event){
  event.preventDefault();
  var target = jQuery(this).attr("data-target-ajax");
  var is_modal = (jQuery(this).hasClass("is_modal") || jQuery(this).parents(".modal").length > 0)?1:0;
  var href = jQuery(this).attr("href");
  var this_element = jQuery(this);
  jQuery(target).html(loading_text);
  jQuery.ajax({
    url:  href,
    type: "post",
    data: "is_ajax=1&is_modal="+((is_modal)?'-modal':''),
    success:function(output){
      var coutput = jQuery('<div/>').append(output).find(target);
      var output = (jQuery(coutput).length > 0)?jQuery(coutput).html():output;
      if(jQuery(this_element).parents(target).length > 0)
      {
        jQuery(this_element).parents(target).html(output);
      }else{
        jQuery(target).html(output);
      }
      if(is_modal)
      {
        jQuery(target).find("a").addClass("is_modal");
      }
    }
  });
});


  function load_notification(container)
  {
    jQuery.ajax({
      url: base_url+'widgets/badges_notification',
      type: "POST",
      data: "",
      success: function(output){
        jQuery(container).html(output);
        setTimeout(function(){load_notification(container);},1000);
      }
    });
  }
  
  function load_messages(container)
  {
    jQuery.ajax({
      url: base_url+'widgets/badges_messages',
      type: "POST",
      data: "",
      success: function(output){
        jQuery(container).html(output);
        setTimeout(function(){load_messages(container);},1000);
      }
    });
  }
  
  function load_widget_greenwallet(container)
  {
    jQuery.ajax({
      url: base_url+'widgets/widget_greenwallet',
      type: "POST",
      data: "",
      success: function(output){
        jQuery(container).replaceWith(output);
        setTimeout(function(){load_widget_greenwallet(container);},10000);
      }
    });
  }
  
  function load_widget_certificates(container)
  {
    jQuery.ajax({
      url: base_url+'widgets/widget_certificates',
      type: "POST",
      data: "",
      success: function(output){
        jQuery(container).replaceWith(output);
        setTimeout(function(){load_widget_certificates(container);},10000);
      }
    });
  }

  jQuery(document).ready(function(){
      setTimeout(function(){load_notification(".badges_notification");},1000);
      setTimeout(function(){load_messages(".badges_messages");},1000);
      setTimeout(function(){load_widget_greenwallet(".widget_greenwallet");},10000);
      setTimeout(function(){load_widget_certificates(".widget_certificates");},10000);
  });
