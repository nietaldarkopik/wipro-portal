<?php
	$this->db->order_by("id","DESC");
	$the_other_facilities = $this->db->get("facilities");
	$the_other_facilities = $the_other_facilities->result_array();
?>
	<div class="container-full">			
        <a class="handle closed" href="javascript:void(0);" onmouseover="tooltipLeft.pop(this, 'Open Content')">Content</a>
        <a class="handle opened" href="javascript:void(0);" onmouseover="tooltipLeft.pop(this, 'Hide Content')">Content</a>
				
		<div class="header-box">
			<h1>Facilities</h1>
			<p>Presenting our latest facilities</p>
		</div>
		<div class="content-box">
			<div class="post-entry">
				<div id="mcts1" class="feature-facilities">
				<?php
				  if(is_array($the_other_facilities) and count($the_other_facilities) > 0)
				  {
					foreach($the_other_facilities as $index => $f)
					{
				?>
				  <div class="slidebox">
					<a href="<?php echo base_url();?>facilities/detail/<?php echo $f['id'];?>">
						<img class="align-left" src="<?php echo base_url();?>uploads/media/facilities/<?php echo $f['thumbnail'];?>" width="200"/>
					</a>
					<h1><?php echo $f['title'];?></h1>
					<p><?php echo character_limiter($f['content'],150);?><br/><a href="<?php echo base_url();?>facilities/detail/<?php echo $f['id'];?>">[ <span>read more</span> ]</a></p>
				  </div>
				<?php
					}
				   }
				?>
				</div>
				<div id="mcts1_control" class="feature-facilities">
					<a class="navPrev"></a>
					<a class="navNext"></a>
				</div>
			</div>
		</div>
		<div class="footer-box">&nbsp;</div>
	</div>
