<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
		<!-- FOOTER START -->
		<div class="site-footer">
			<div class="container">
			
				<div class="row">
					<div class="col-md-12">
						<p class="copyrights">Copyrights &copy; 2014 Green Trading System - All Rights Reserved</p>
					</div>
				</div>
				
			</div>
		</div>
		<!-- FOOTER END -->
