<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$user_id = $this->user_access->current_user_id;
	#echo $this->user_access->get_menus_allowed_structured();
	
	$curr_menu_group = $this->session->userdata("menu_group");
	$curr_menu_group = (empty($curr_menu_group))?"general":$curr_menu_group;
	
	$parent_menu_allowed = $this->user_access->get_custom_menus_allowed(""," AND user_menu_group_id = '69'");
	$the_top_menu = "";
	// Get menu Level 0
	if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
	{
		#$the_top_menu .= '<ul class="nav navbar-nav">';
		foreach($parent_menu_allowed as $index => $mn)
		{
      $is_active = (str_replace('/index.php','',current_url()) == $mn['url'])?' active ':'';
			$sub_menu_allowed2 = $this->user_access->get_menus_allowed($user_id," AND parent_menu = '" . $mn['user_custom_menu_id'] . "'");
      #$the_top_menu .= '<li><a href="' . $mn['url'] . '">'.$mn['menu_title'].'</a></li>';
      $the_top_menu .= '<a href="' . $mn['url'] . '" class="list-group-item '.$is_active.'"><i class="glyphicon '.$mn['attributes'].'">&nbsp;</i>'.$mn['menu_title'].'</a>';
			// Get menu Level 1
			if(is_array($sub_menu_allowed2) and count($sub_menu_allowed2) > 0)
			{
				foreach($sub_menu_allowed2 as $index2 => $mn2)
				{
					$sub_menu_allowed3 = $this->user_access->get_menus_allowed($user_id," AND parent_menu = '" . $mn2['user_custom_menu_id'] . "'");
          #$the_top_menu .= '<li class="dropdown"><a href="'.$mn2['url']. '" class="dropdown-toggle" data-toggle="dropdown">'.$mn2['menu_title'].'</a>';	
          $the_top_menu .= '<a href="'.$mn2['url']. '" class="list-group-item dropdown-toggle '.$is_active.'" data-toggle="dropdown"><i class="glyphicon '.$mn2['attributes'].'">&nbsp;</i>'.$mn2['menu_title'].'</a>';	
					
					if(is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0)
					{
						$the_top_menu .= '<ul class="dropdown-menu">';
						foreach($sub_menu_allowed3 as $index3 => $mn3)
						{
							$the_top_menu .= '<li><a href="'.$mn3['url'] . '">'.$mn3['menu_title'].'</a>';	
						}
						$the_top_menu .= '</ul>';
					}
					
					#$the_top_menu .= '</li>';
				}
			}
		}
		#$the_top_menu .= '</ul>';
	}
	
	echo $the_top_menu;
