<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
          
					<div class="content-container col-sm-12">
						
						<div class="header-page">
							<h2>Sandbox Green Wallet (Virtual Albarkah) <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>Registrasi Green Wallet (Virtual Albarkah) </h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              <div class="row">
                <div class="col-md-12">
                  <?php
                  
                  if($is_registered == "")
                  {
                    if(isset($_SERVER['HTTP_REFERER']))
                    {
                    ?>
                      <div class="alert alert-success"><strong>Sukses!</strong> Registrasi Green Wallet Berhasil!</div>
                      <div class="alert alert-warning"><strong>Warning!</strong> Menunggu proses kembali ke halaman GTS</div>
                    <?php
                    }
                  }else{
                    ?>
                      <div class="alert alert-warning"><strong>Warning!</strong> Anda sudah terdaftar Green Wallet di GTS</div>
                    <?php
                  }
                  ?>
                </div>
              </div>
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
