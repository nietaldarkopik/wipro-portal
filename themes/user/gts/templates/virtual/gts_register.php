<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
					<div class="content-container col-sm-12 col-md-9">
						
						<div class="header-page">
							<h2>Prospective Buyer <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>People who follow your Auction</h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
							<div class="row gts-grid prospectives">
							
								<div class="col-xs-12 col-sm-12 col-md-6">
									<dl class="dl-horizontal">
										<dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img1-small.jpg" /></dt>
										<dd>
											<ul class="clearlist">
												<li><a href="single-buyer.html"><strong>Mrs. Joshua</strong></a></li>
												<li>ID-201400123456789</li>
												<li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li>
												<li class="btn-group">
													<button class="btn btn-cancel btn-xs">No Deal</button>
													<button class="btn btn-bid btn-xs">Deal</button>
												</li>
											</ul>
										</dd>
									</dl>
								</div>
								
								<div class="col-xs-12 col-sm-12 col-md-6"><dl class="dl-horizontal"><dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img2-small.jpg"/></dt><dd><ul class="clearlist"><li><a href="single-buyer.html"><strong>Mr. Joshua</strong></a></li><li>ID-201400123456789</li><li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li><li class="btn-group"><button class="btn btn-cancel btn-xs">No Deal</button><button class="btn btn-bid btn-xs">Deal</button></li></ul></dd></dl></div>
								<div class="col-xs-12 col-sm-12 col-md-6"><dl class="dl-horizontal"><dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img2-small.jpg"/></dt><dd><ul class="clearlist"><li><a href="single-buyer.html"><strong>Mr. Joshua</strong></a></li><li>ID-201400123456789</li><li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li><li class="btn-group"><button class="btn btn-cancel btn-xs">No Deal</button><button class="btn btn-bid btn-xs">Deal</button></li></ul></dd></dl></div>
								<div class="col-xs-12 col-sm-12 col-md-6"><dl class="dl-horizontal"><dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img3-small.jpg"/></dt><dd><ul class="clearlist"><li><a href="single-buyer.html"><strong>Mrs. Joshua II</strong></a></li><li>ID-201400123456789</li><li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li><li class="btn-group"><button class="btn btn-cancel btn-xs">No Deal</button><button class="btn btn-bid btn-xs">Deal</button></li></ul></dd></dl></div>
								<div class="col-xs-12 col-sm-12 col-md-6"><dl class="dl-horizontal"><dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img4-small.jpg"/></dt><dd><ul class="clearlist"><li><a href="single-buyer.html"><strong>Mrs. Joshua II</strong></a></li><li>ID-201400123456789</li><li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li><li class="btn-group"><button class="btn btn-cancel btn-xs">No Deal</button><button class="btn btn-bid btn-xs">Deal</button></li></ul></dd></dl></div>
								<div class="col-xs-12 col-sm-12 col-md-6"><dl class="dl-horizontal"><dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img4-small.jpg"/></dt><dd><ul class="clearlist"><li><a href="single-buyer.html"><strong>Mrs. Joshua II</strong></a></li><li>ID-201400123456789</li><li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li><li class="btn-group"><button class="btn btn-cancel btn-xs">No Deal</button><button class="btn btn-bid btn-xs">Deal</button></li></ul></dd></dl></div>
								<div class="col-xs-12 col-sm-12 col-md-6"><dl class="dl-horizontal"><dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img2-small.jpg"/></dt><dd><ul class="clearlist"><li><a href="single-buyer.html"><strong>Mr. Joshua</strong></a></li><li>ID-201400123456789</li><li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li><li class="btn-group"><button class="btn btn-cancel btn-xs">No Deal</button><button class="btn btn-bid btn-xs">Deal</button></li></ul></dd></dl></div>
								<div class="col-xs-12 col-sm-12 col-md-6"><dl class="dl-horizontal"><dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img2-small.jpg"/></dt><dd><ul class="clearlist"><li><a href="single-buyer.html"><strong>Mr. Joshua</strong></a></li><li>ID-201400123456789</li><li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li><li class="btn-group"><button class="btn btn-cancel btn-xs">No Deal</button><button class="btn btn-bid btn-xs">Deal</button></li></ul></dd></dl></div>
								<div class="col-xs-12 col-sm-12 col-md-6"><dl class="dl-horizontal"><dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img3-small.jpg"/></dt><dd><ul class="clearlist"><li><a href="single-buyer.html"><strong>Mrs. Joshua II</strong></a></li><li>ID-201400123456789</li><li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li><li class="btn-group"><button class="btn btn-cancel btn-xs">No Deal</button><button class="btn btn-bid btn-xs">Deal</button></li></ul></dd></dl></div>
								<div class="col-xs-12 col-sm-12 col-md-6"><dl class="dl-horizontal"><dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img4-small.jpg"/></dt><dd><ul class="clearlist"><li><a href="single-buyer.html"><strong>Mrs. Joshua II</strong></a></li><li>ID-201400123456789</li><li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li><li class="btn-group"><button class="btn btn-cancel btn-xs">No Deal</button><button class="btn btn-bid btn-xs">Deal</button></li></ul></dd></dl></div>
								<div class="col-xs-12 col-sm-12 col-md-6"><dl class="dl-horizontal"><dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img4-small.jpg"/></dt><dd><ul class="clearlist"><li><a href="single-buyer.html"><strong>Mrs. Joshua II</strong></a></li><li>ID-201400123456789</li><li><small class="">Bid Price:</small><p class="margin-0 h4">130.000</p></li><li class="btn-group"><button class="btn btn-cancel btn-xs">No Deal</button><button class="btn btn-bid btn-xs">Deal</button></li></ul></dd></dl></div>
							</div>
							
              <!-- START : Page Navigation -->
              <?php 
                $paging_config = (isset($paging_config) and is_array($paging_config))?$paging_config:array();
                echo $this->m_component->pagination($paging_config);
              ?>
              <!-- END : Page Navigation -->
							
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
