<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8" />
    <title>Single Buyer - Green Trading System</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
		
    <!-- Bootstrap Start -->
    <link href="<?php echo current_theme_url();?>assets/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo current_theme_url();?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--link href="<?php echo current_theme_url();?>assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" /-->
    <link href="<?php echo current_theme_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo current_theme_url();?>assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo current_theme_url();?>assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap End -->
		
    <link rel="shortcut icon" href="<?php echo current_theme_url();?>favicon.ico" />
    <script type="text/javascript">
      var base_url = "<?php echo base_url();?>";
      var current_url = "<?php echo current_url();?>";
      var current_theme_url = "<?php echo current_theme_url();?>";
    </script>
		<?php
      echo $this->assets->print_css("head");
      echo $this->assets->print_css_inline("head");
      echo $this->assets->print_js("head");
      echo $this->assets->print_js_inline("head");
    ?>
  </head>
  <body>
	
