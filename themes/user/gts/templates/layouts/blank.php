<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
				
				  <?php $this->load->view("components/leftbar");?>          


					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Hello World! <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>Welcome Guest</h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              
              <div class="row">
                <div class="col-md-12">
                  
                <!-- Content Start Here-->
                
                </div>
              </div>
            
            </div>
            <!-- END : Primary Content -->
            
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
		
		<!-- FOOTER START -->
		<div class="site-footer">
			<div class="container">
			
				<div class="row">
					<div class="col-md-12">
						<p class="copyrights">Copyrights &copy; 2014 Green Trading System - All Rights Reserved</p>
					</div>
				</div>
				
			</div>
		</div>
		<!-- FOOTER END -->
		
    <!-- JAVASCRIPTS START -->
		
    <!-- CORE PLUGINS START -->
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/hover-dropdown.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.migrate.min.js"></script>
		
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.easing.min.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.mousewheel.js"></script>

		<script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/fancybox/jquery.fancybox.pack.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("a[rel=catalogue]").fancybox({
					'transitionIn'		: 'fade',
					'transitionOut'		: 'fade',
					'padding'		      : '0',
					'titlePosition' 	: 'outside',
					'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
						return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
					}
				});
			});
		</script>
		
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript">
		$('.bxslider').bxSlider({
			minSlides: 1,
			maxSlides: 3,
			slideWidth: 165,
			slideMargin: 15,
			controls: true                     // true, false - previous and next controls
		});
		</script>
    
    <script type="text/javascript">
			$(function () {
				$('#mang_stab').tab('show')
			})
		</script>
		
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/scripts/holder.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/back-to-top.js"></script>
		
    <!--[if lt IE 9]>
    <script src="<?php echo current_theme_url();?>assets/plugins/respond.min.js"></script>  
    <![endif]-->
    <!-- CORE PLUGINS END -->
		
    <!-- JAVASCRIPTS END -->
		
  </body>
</html>
