<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">

				  <?php $this->load->view("components/leftbar");?>          

					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Single Profile <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>User Profiles</h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              
							<!-- START : Single User -->
							<div class="row profile-user">
								<div class="col-md-8 tab-content">
                  
                  <!-- START : Tab Pane 0 -->
									<div class="row tab-pane fade in active" id="tab_0">
										
                    <div class="col-md-3 text-center">
											<p><img width="120" height="120" src="<?php echo current_theme_url();?>assets/upload/img2-small.jpg" class="profile-photo img-circle" alt="Photo" title="Photo"></p>
										</div>
                    
										<dl class="dl-horizontal col-md-9">
											<dt><small>Owner</small></dt><dd>Mrs. Ivanickov</dd>
											<hr class="dashed">
											<dt><small>User ID</small></dt><dd>2014001xxx</dd>
											<hr class="dashed">
											<dt><small>Location</small></dt><dd>Bandung, Indonesia</dd>
											<hr class="dashed">
										</dl>
										
                    <div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Personal Information</div>
												<div class="panel-body">
													<dl class="dl-horizontal with-colon lightext">
														<dt>First Name</dt><dd>Ivan<dd>
														<dt>Last Name</dt><dd>Nickov<dd>
														<dt>Counrty</dt><dd>Spain<dd>
														<dt>Birthday</dt><dd>18 Jan 1982<dd>
														<dt>Occupation</dt><dd>Web Developer<dd>
														<dt>Email</dt><dd><a href="#">ivanmail@mywebsite.com</a><dd>
														<dt>Interests</dt><dd>Design, Web etc.<dd>
														<dt>Website Url</dt><dd><a href="#">http://www.mywebsite.com</a><dd>
														<dt>Mobile Number</dt><dd>+1 646 580 DEMO (6284)dd>
													</dl>
												</div>
											</div>
                    </div>
										
                    <div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Others Information</div>
												<div class="panel-body">
                          <p>List groups are a flexible and powerful component for displaying not only simple lists of elements, but complex ones with custom content.</p>
												</div>
											</div>
                    </div>
										
									</div>
                  <!-- END   : Tab Pane 0 -->
									
                  <!-- START : Tab Pane 1 -->
									<div class="tab-pane fade" id="tab_1">
										<div class="panel panel-default">
											<div class="panel-heading">Personal Info</div>
											<div class="panel-body">
												<form role="form">
													<div class="form-group">
														<label for="exampleInputUsername">Username</label>
														<input type="text" class="form-control" id="exampleInputUsername" placeholder="Your Username" />
													</div>
													
													<div class="form-group">
														<label for="exampleInputFirstName">First Name</label>
														<input type="text" class="form-control" id="exampleInputFirstName" placeholder="First Name" />
													</div>
													
													<div class="form-group">
														<label for="exampleInputLastName">Last Name</label>
														<input type="text" class="form-control" id="exampleInputLastName" placeholder="Last Name" />
													</div>
													
													<div class="form-group">
													
														<p><strong>Gender</strong></p>
														<div class="radio-inline">
															<label for="optionsRadios1">
																<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
																Male
															</label>
														</div>
														
														<div class="radio-inline">
															<label for="optionsRadios2">
																<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
																Female
															</label>
														</div>
														
													</div>
													
													<div class="form-group">
														<label for="exampleInputMobileNumber">Mobile Number</label>
														<input type="text" class="form-control" id="exampleInputMobileNumber" placeholder="+1 646 580 DEMO (6284)">
													</div>
													
													<div class="form-group">
														<label for="exampleInputInterests">Interests</label>
														<input type="text" class="form-control" id="exampleInputInterests" placeholder="Design, Web etc." />
													</div>
													
													<div class="form-group">
														<label for="exampleInputOccupation">Occupation</label>
														<input type="text" class="form-control" id="exampleInputOccupation" placeholder="Rob, Thief and Killer" />
													</div>
													
													<div class="form-group">
														<label for="exampleInputCounrty">Counrty</label>
														<input type="text" class="form-control" id="exampleInputCounrty" placeholder="Your Country" />
													</div>
													
													<div class="form-group">
														<label for="exampleInputWebsite">Website Url</label>
														<input type="text" class="form-control" id="" exampleInputWebsite="">
													</div>
													
													<div class="form-group">
														<label for="exampleInputAbout">About</label>
														<textarea class="form-control" rows="3"></textarea>
													</div>
													
													<div class="form-group">
														<button type="submit" class="btn btn-cancel">Discard</button>
														<button type="submit" class="btn btn-default">Save Changes</button>
													</div>
													
												</form>
											</div>
										</div>
									</div>
                  <!-- END   : Tab Pane 1 -->
									
                  <!-- START : Tab Pane 2 -->
									<div class="tab-pane fade" id="tab_2">
										<div class="panel panel-default">
											<div class="panel-heading">Change Avatar</div>
											<div class="panel-body">
												<form role="form">
												
													<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.</p>
													
													<p class="img-responsive text-center"><img src="<?php echo current_theme_url();?>assets/scripts/holder.js/300x300/photo" title="photo" alt="photo" /></p>
													
													<div class="form-group">
														<label for="exampleInputFile">File input</label>
														<input type="file" id="exampleInputFile" />
														<p class="help-block"><span class="label label-warning">NOTE!</span> You can write some information here..</p>
													</div>
													
													<div class="form-group">
														<button type="submit" class="btn btn-default">Save Changes</button>
													</div>
													
												</form>
											</div>
										</div>
									</div>
                  <!-- END   : Tab Pane 2 -->
									
                  <!-- START : Tab Pane 3 -->
									<div class="tab-pane fade" id="tab_3">
										<div class="panel panel-default">
											<div class="panel-heading">Change Password</div>
											<div class="panel-body">
												<form role="form">
													<div class="form-group">
														<label for="exampleInputUsername">Current Password</label>
														<input type="password" class="form-control" id="exampleInputPassword_0" placeholder="Current Password" />
													</div>
													
													<div class="form-group">
														<label for="exampleInputPassword_1">New Password</label>
														<input type="password" class="form-control" id="exampleInputPassword_1" placeholder="New Password" />
													</div>
													
													<div class="form-group">
														<label for="exampleInputPassword_2">Re-type New Password</label>
														<input type="password" class="form-control" id="exampleInputPassword_2" placeholder="Re-type New Password" />
													</div>
													
													<div class="form-group">
														<button type="submit" class="btn btn-cancel">Password Changes</button>
														<button type="submit" class="btn btn-default">Cancel</button>
													</div>
												</form>
											</div>
										</div>
									</div>
                  <!-- END   : Tab Pane 3 -->
									
                  <!-- START : Tab Pane 4 -->
									<div class="tab-pane fade" id="tab_4">
										<div class="panel panel-default">
											<div class="panel-heading">Privacy Setting</div>
											<div class="panel-body">
												<form role="form">
													<div class="row form-group">
														<div class="col-md-7">
															<p>Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus..</p>
														</div>
														
														<div class="col-md-5">
															<div class="radio-inline">
																<label for="optionsRadios0_1"><input type="radio" name="optionsRadios" id="optionsRadios0_1" value="option0_1" checked />Yes</label>
															</div>
															
															<div class="radio-inline">
																<label for="optionsRadios0_2"><input type="radio" name="optionsRadios" id="optionsRadios0_2" value="option0_2" />No</label>
															</div>
														</div>
													</div><hr/>
													
													<div class="row form-group">
														<div class="col-md-7">
															<p>Pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson</p>
														</div>
														
														<div class="col-md-5">
															<div class="checkbox-inline">
																<label for="optionsCheckbox0_1"><input type="checkbox" name="optionsCheckbox" id="optionsCheckbox0_1" value="option1_1" />Yes</label>
															</div>
														</div>
													</div><hr/>
													
													<div class="row form-group">
														<div class="col-md-7">
															<p>Cliche reprehenderit enim eiusmod high life accusamus terry</p>
														</div>
														
														<div class="col-md-5">
															<div class="checkbox-inline">
																<label for="optionsCheckbox0_2"><input type="checkbox" name="optionsCheckbox" id="optionsCheckbox0_2" value="option2_1" />Yes</label>
															</div>
														</div>
													</div><hr/>
													
													<div class="row form-group">
														<div class="col-md-7">
															<p>Oiusmod high life accusamus terry richardson ad squid wolf fwopo</p>
														</div>
														
														<div class="col-md-5">
															<div class="checkbox-inline">
																<label for="optionsCheckbox0_3"><input type="checkbox" name="optionsCheckbox" id="optionsCheckbox0_3" value="option3_1" />Yes</label>
															</div>
														</div>
													</div><hr/>
													
													<div class="form-group">
														<button type="submit" class="btn btn-cancel">Save Changes</button>
														<button type="submit" class="btn btn-default">Cancel</button>
													</div>
												</form>
											</div>
										</div>
									</div>
                  <!-- END   : Tab Pane 4 -->
									
								</div>
								
								<div class="col-md-4">
                
									<div id="mang_stab" class="list-group">
										<a class="list-group-item active" href="#tab_0" data-toggle="tab">User Panel</a>
										<a class="list-group-item" href="#tab_1" data-toggle="tab">Personal Info</a>
										<a class="list-group-item" href="#tab_2" data-toggle="tab">Change Avatar</a>
										<a class="list-group-item" href="#tab_3" data-toggle="tab">Change Password</a>
										<a class="list-group-item" href="#tab_4" data-toggle="tab">Privacy Setting</a>
									</div>
                  
								</div>
							</div>
							<!-- END : Single User -->
              
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
          
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
		
		<!-- FOOTER START -->
		<div class="site-footer">
			<div class="container">
			
				<div class="row">
					<div class="col-md-12">
						<p class="copyrights">Copyrights &copy; 2014 Green Trading System - All Rights Reserved</p>
					</div>
				</div>
				
			</div>
		</div>
		<!-- FOOTER END -->
		
    <!-- JAVASCRIPTS START -->
		
    <!-- CORE PLUGINS START -->
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/hover-dropdown.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.migrate.min.js"></script>
		
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.easing.min.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.mousewheel.js"></script>

		<script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/fancybox/jquery.fancybox.pack.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("a[rel=catalogue]").fancybox({
					'transitionIn'		: 'fade',
					'transitionOut'		: 'fade',
					'padding'		      : '0',
					'titlePosition' 	: 'outside',
					'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
						return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
					}
				});
			});
		</script>
		
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript">
		$('.bxslider').bxSlider({
			minSlides: 1,
			maxSlides: 3,
			slideWidth: 165,
			slideMargin: 15,
			controls: true                     // true, false - previous and next controls
		});
		</script>
		
    <script type="text/javascript">
			$(function () {
				$('#mang_stab').tab('show')
			})
		</script>
    
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/scripts/holder.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/back-to-top.js"></script>
		
    <!--[if lt IE 9]>
    <script src="<?php echo current_theme_url();?>assets/plugins/respond.min.js"></script>  
    <![endif]-->
    <!-- CORE PLUGINS END -->
		
    <!-- JAVASCRIPTS END -->
		
  </body>
</html>
