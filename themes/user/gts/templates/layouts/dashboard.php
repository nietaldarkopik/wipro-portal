<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
				
				  <?php $this->load->view("components/leftbar");?>          


					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Dashboard <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>Welcome Guest</h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              
							<div id="photo-of-trees" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="item active"><a title="Photo 1" href="#"><img src="<?php echo current_theme_url();?>assets/upload/banner-01.jpg" alt="Photo 1" /></a></div>
                  <div class="item"><a title="Photo 2" href="#"><img src="<?php echo current_theme_url();?>assets/upload/banner-02.jpg" alt="Photo 2" /></a></div>
                  <div class="item"><a title="Photo 3" href="#"><img src="<?php echo current_theme_url();?>assets/upload/banner-03.jpg" alt="Photo 3" /></a></div>
                </div>
                
                <a title="Previous" class="left carousel-control" href="#photo-of-trees" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a title="Next" class="right carousel-control" href="#photo-of-trees" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
              </div><!-- #photo-of-trees -->


              <div class="row">
                <div class="col-md-8">
                  <div class="panel panel-default">
                    <div class="panel-heading">Articles</div>
                    <div class="panel-body no-more-tables">
                      <p>Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Eserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpa</p>
                    </div>
                  </div>
                </div>
                
                <div class="col-md-4">
									<div class="list-group">
										<a class="list-group-item active" href="#tab_01">Sidebar #1</a>
										<a class="list-group-item" href="#">Sidebar #2</a>
										<a class="list-group-item" href="#">Sidebar #3</a>
										<a class="list-group-item" href="#">Sidebar #4</a>
										<a class="list-group-item" href="#">Sidebar #5</a>
									</div>
                </div>
              </div>
            
            </div>
            <!-- END : Primary Content -->
            
            
            
            <!-- START : Primary Content -->
            
						<div class="header-content">
							<h3>Buttons</h3>
						</div>
						
						<div class="main-content">
              
              <div class="row">
                <div class="col-md-12">
                  <div class="alert alert-success"><strong>Well done!</strong> You successfully read this important alert message.</div>
                  <div class="alert alert-info"><strong>Heads up!</strong> This alert needs your attention, but it's not super important.</div>
                  <div class="alert alert-warning"><strong>Warning!</strong> Better check yourself, you're not looking too good.</div>
                  <div class="alert alert-danger"><strong>Oh snap!</strong> Change a few things up and try submitting again.</div>
                </div>
              </div>
							
              <div class="row">
                <div class="col-md-6">
                  <button class="btn btn-default btn-sm">Submit</button>
                  <button class="btn btn-cancel btn-sm">Cancel</button>
                  <button class="btn btn-sale btn-md">Sale)</button>
                  <button class="btn btn-bid btn-md">Bid</button>
                  <button class="btn btn-sale btn-lg">Big Sale</button>
                  <button class="btn btn-bid btn-lg">Bid Now</button>
                </div>
                
                <div class="col-md-6">
									<span class="label label-default">Default</span>
									<span class="label label-primary">Primary</span>
									<span class="label label-success">Success</span>
									<span class="label label-info">Info</span>
									<span class="label label-warning">Warning</span>
									<span class="label label-danger">Danger</span>
                </div>
              </div>
              
              <!-- START : Page Navigation -->
              <?php 
                $paging_config = (isset($paging_config) and is_array($paging_config))?$paging_config:array();
                echo $this->m_component->pagination($paging_config);
              ?>
							
						</div>
            <!-- END : Primary Content -->
						
						
						
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
