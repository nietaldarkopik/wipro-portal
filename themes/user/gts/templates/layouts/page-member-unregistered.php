<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
          
				  <?php $this->load->view("components/leftbar");?>          
					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Green Wallet <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>Register/Login Green Wallet</h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              
              <!--
							<div id="photo-of-trees" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="item active"><a title="Photo 1" href="#"><img src="<?php #echo current_theme_url();?>assets/upload/banner-01.jpg" alt="Photo 1" /></a></div>
                  <div class="item"><a title="Photo 2" href="#"><img src="<?php #echo current_theme_url();?>assets/upload/banner-02.jpg" alt="Photo 2" /></a></div>
                  <div class="item"><a title="Photo 3" href="#"><img src="<?php #echo current_theme_url();?>assets/upload/banner-03.jpg" alt="Photo 3" /></a></div>
                </div>
                
                <a title="Previous" class="left carousel-control" href="#photo-of-trees" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a title="Next" class="right carousel-control" href="#photo-of-trees" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
              </div></!-- #photo-of-trees --/>
              -->

              <!-- START : Login Form -->
              <div class="row">
                <div class="col-md-12">
                  <div class="member-login">
                    
                    <p>Insert your mobile phone number to login</p>
                  
                    <div class="row">
                    
                      <div class="col-sm-6">
                        <form action="<?php echo base_url('virtual/reg_greenwallet/'.$this->gts_members->get_current_member_code());?>" method="post" class="login-form-member">
                          <div class="input-group">
                            <input type="text" placeholder="Phone Number.." class="form-control" name="phone">
                            <div class="input-group-btn">
                              <button class="btn btn-default" type="submit">Login</button>
                            </div><!-- /btn-group -->
                          </div><!-- /input-group -->
                        </form>
                      </div>
                      
                      <div class="col-sm-6"><p class="h4">Don’t Have Account..? Please <a href="#">Sign-Up</a> Now!</p></div>
                      
                    </div>
                  </div>
                </div>
              </div>
              <!-- END   : Login Form -->
              
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
          
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
