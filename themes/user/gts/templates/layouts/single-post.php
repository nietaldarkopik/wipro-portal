<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">

				  <?php $this->load->view("components/leftbar");?>          

					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Messages <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						
            <!-- START : Primary Content -->
						<div class="header-content">
							<h3>Inbox</h3>
						</div>
            
						<div class="main-content">
              
							<!-- START : Single User -->
							<div class="row profile-user">
							
								<div class="col-md-4 col-md-push-8">
                
									<div id="mang_stab" class="list-group">
										<a class="list-group-item btn-bid" href="#tab_00" data-toggle="tab">Compose</a>
										<a class="list-group-item active" href="#tab_01" data-toggle="tab">Inbox</a>
										<a class="list-group-item" href="#tab_02" data-toggle="tab">Sent</a>
										<a class="list-group-item" href="#tab_03" data-toggle="tab">Draft</a>
										<a class="list-group-item" href="#tab_04" data-toggle="tab">Trash</a>
									</div>
                  
								</div>
								
								<div class="col-md-8 col-md-pull-4 tab-content">
                  
                  <!-- START : Tab Pane 0 -->
									<div class="tab-pane fade" id="tab_00">
										<div class="panel panel-default">
											<div class="panel-heading">Compose</div>
											<div class="panel-body">
                      
											</div>
										</div>
									</div>
                  <!-- END   : Tab Pane 0 -->
									
                  <!-- START : Tab Pane 1 -->
									<div class="tab-pane fade in active" id="tab_01">
										<div class="panel panel-default">
											<div class="panel-heading">Inbox</div>
											<div class="panel-body inbox">
											
												<table class="table table-striped table-advance table-hover">
													<thead>
														<tr>
															<th colspan="3">
																<div class="input-group">
																	<div class="checkbox">
																		<input type="checkbox" />
																		<a data-toggle="dropdown" href="#" class="btn btn-default btn-xs dropdown-toggle">
																		More
																		<i class="caret "></i>
																		</a>
																		<ul class="dropdown-menu">
																			<li><a href="#"><i class="glyphicon glyphicon-pencil"></i> Mark as Read</a></li>
																			<li><a href="#"><i class="glyphicon glyphicon-ban-circle"></i> Spam</a></li>
																			<li class="divider"></li>
																			<li><a href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a></li>
																		</ul>
																	</div>
																</div>
															</th>
															<th colspan="3" class="text-right">
																<ul class="list-inline">
																	<li><span>1-30 of 789</span></li>
																	<li><i class="glyphicon glyphicon-chevron-left"></i></li>
																	<li><i class="glyphicon glyphicon-chevron-right"></i></li>
																</ul>
															</th>
														</tr>
													</thead>
													<tbody>
														<tr class="unread">
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td>
															<td class="view-message  hidden-phone">Petronas IT</td>
															<td class="view-message ">New server for datacenter needed</td>
															<td class="view-message  inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td>
															<td class="view-message  text-right">16:30 PM</td>
														</tr>
														<tr class="unread">
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td>
															<td class="view-message hidden-phone">Daniel Wong</td>
															<td class="view-message">Please help us on customization of new secure server</td>
															<td class="view-message inbox-small-cells"></td>
															<td class="view-message text-right">March 15</td>
														</tr>
														<tr>
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td>
															<td class="view-message hidden-phone">John Doe</td>
															<td class="view-message">Lorem ipsum dolor sit amet</td>
															<td class="view-message inbox-small-cells"></td>
															<td class="view-message text-right">March 15</td>
														</tr>
														<tr>
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td>
															<td class="view-message hidden-phone">Facebook</td>
															<td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
															<td class="view-message inbox-small-cells"></td>
															<td class="view-message text-right">March 14</td>
														</tr>
														<tr>
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td>
															<td class="view-message hidden-phone">John Doe</td>
															<td class="view-message">Lorem ipsum dolor sit amet</td>
															<td class="view-message inbox-small-cells"></td>
															<td class="view-message text-right">March 15</td>
														</tr>
														<tr>
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td>
															<td class="view-message hidden-phone">Facebook</td>
															<td class="view-message">Dolor sit amet, consectetuer adipiscing</td>
															<td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td>
															<td class="view-message text-right">March 14</td>
														</tr>
														<tr>
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td>
															<td class="view-message hidden-phone">John Doe</td>
															<td class="view-message">Lorem ipsum dolor sit amet</td>
															<td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td>
															<td class="view-message text-right">March 15</td>
														</tr>
														<tr>
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td>
															<td class="view-message hidden-phone">Facebook</td>
															<td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
															<td class="view-message inbox-small-cells"></td>
															<td class="view-message text-right">March 14</td>
														</tr>
														<tr>
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td>
															<td class="view-message hidden-phone">John Doe</td>
															<td class="view-message view-message">Lorem ipsum dolor sit amet</td>
															<td class="view-message inbox-small-cells"></td>
															<td class="view-message text-right">March 15</td>
														</tr>
														<tr>
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td>
															<td class="view-message hidden-phone">Facebook</td>
															<td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td>
															<td class="view-message inbox-small-cells"></td>
															<td class="view-message text-right">March 14</td>
														</tr>
														<tr>
															<td class="inbox-small-cells">
																<div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div>
															</td>
															<td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td>
															<td class="view-message hidden-phone">John Doe</td>
															<td class="view-message">Lorem ipsum dolor sit amet</td>
															<td class="view-message inbox-small-cells"></td>
															<td class="view-message text-right">March 15</td>
														</tr>
													</tbody>
												</table>											
											</div>
										</div>
									</div>
                  <!-- END   : Tab Pane 1 -->
									
                  <!-- START : Tab Pane 2 -->
									<div class="tab-pane fade" id="tab_02">
										<div class="panel panel-default">
											<div class="panel-heading">Sent</div>
											<div class="panel-body">
											<table class="table table-striped table-advance table-hover"><thead><tr><th colspan="3"><div class="input-group"><div class="checkbox"><input type="checkbox"/><a data-toggle="dropdown" href="#" class="btn btn-default btn-xs dropdown-toggle">More<i class="caret "></i></a><ul class="dropdown-menu"><li><a href="#"><i class="glyphicon glyphicon-pencil"></i> Mark as Read</a></li><li><a href="#"><i class="glyphicon glyphicon-ban-circle"></i> Spam</a></li><li class="divider"></li><li><a href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a></li></ul></div></div></th><th colspan="3" class="text-right"><ul class="list-inline"><li><span>1-30 of 789</span></li><li><i class="glyphicon glyphicon-chevron-left"></i></li><li><i class="glyphicon glyphicon-chevron-right"></i></li></ul></th></tr></thead><tbody><tr class="unread"><td class="inbox-small-cells"><div class="checkbox"><span><input type="checkbox"/></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Petronas IT</td><td class="view-message ">New server for datacenter needed</td><td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td><td class="view-message text-right">16:30 PM</td></tr><tr class="unread"><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Daniel Wong</td><td class="view-message">Please help us on customization of new secure server</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr></tbody></table>
											</div>
										</div>
									</div>
                  <!-- END   : Tab Pane 2 -->
									
                  <!-- START : Tab Pane 3 -->
									<div class="tab-pane fade" id="tab_03">
										<div class="panel panel-default">
											<div class="panel-heading">Draft</div>
											<div class="panel-body">
											<table class="table table-striped table-advance table-hover"><thead><tr><th colspan="3"><div class="input-group"><div class="checkbox"><input type="checkbox"/><a data-toggle="dropdown" href="#" class="btn btn-default btn-xs dropdown-toggle">More<i class="caret "></i></a><ul class="dropdown-menu"><li><a href="#"><i class="glyphicon glyphicon-pencil"></i> Mark as Read</a></li><li><a href="#"><i class="glyphicon glyphicon-ban-circle"></i> Spam</a></li><li class="divider"></li><li><a href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a></li></ul></div></div></th><th colspan="3" class="text-right"><ul class="list-inline"><li><span>1-30 of 789</span></li><li><i class="glyphicon glyphicon-chevron-left"></i></li><li><i class="glyphicon glyphicon-chevron-right"></i></li></ul></th></tr></thead><tbody><tr class="unread"><td class="inbox-small-cells"><div class="checkbox"><span><input type="checkbox"/></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Petronas IT</td><td class="view-message ">New server for datacenter needed</td><td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td><td class="view-message text-right">16:30 PM</td></tr><tr class="unread"><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Daniel Wong</td><td class="view-message">Please help us on customization of new secure server</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr></tbody></table>
											</div>
										</div>
									</div>
                  <!-- END   : Tab Pane 3 -->
									
                  <!-- START : Tab Pane 4 -->
									<div class="tab-pane fade" id="tab_04">
										<div class="panel panel-default">
											<div class="panel-heading">Trash</div>
											<div class="panel-body">
											<table class="table table-striped table-advance table-hover"><thead><tr><th colspan="3"><div class="input-group"><div class="checkbox"><input type="checkbox"/><a data-toggle="dropdown" href="#" class="btn btn-default btn-xs dropdown-toggle">More<i class="caret "></i></a><ul class="dropdown-menu"><li><a href="#"><i class="glyphicon glyphicon-pencil"></i> Mark as Read</a></li><li><a href="#"><i class="glyphicon glyphicon-ban-circle"></i> Spam</a></li><li class="divider"></li><li><a href="#"><i class="glyphicon glyphicon-trash"></i> Delete</a></li></ul></div></div></th><th colspan="3" class="text-right"><ul class="list-inline"><li><span>1-30 of 789</span></li><li><i class="glyphicon glyphicon-chevron-left"></i></li><li><i class="glyphicon glyphicon-chevron-right"></i></li></ul></th></tr></thead><tbody><tr class="unread"><td class="inbox-small-cells"><div class="checkbox"><span><input type="checkbox"/></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Petronas IT</td><td class="view-message ">New server for datacenter needed</td><td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td><td class="view-message text-right">16:30 PM</td></tr><tr class="unread"><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Daniel Wong</td><td class="view-message">Please help us on customization of new secure server</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"><i class="glyphicon glyphicon-paperclip"></i></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star"></i></td><td class="view-message hidden-phone">Facebook</td><td class="view-message view-message">Dolor sit amet, consectetuer adipiscing</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 14</td></tr><tr><td class="inbox-small-cells"><div class="checker"><span><input type="checkbox" class="mail-checkbox"></span></div></td><td class="inbox-small-cells"><i class="glyphicon glyphicon-star inbox-started"></i></td><td class="view-message hidden-phone">John Doe</td><td class="view-message">Lorem ipsum dolor sit amet</td><td class="view-message inbox-small-cells"></td><td class="view-message text-right">March 15</td></tr></tbody></table>
											</div>
										</div>
									</div>
                  <!-- END   : Tab Pane 4 -->
									
								</div>
								
							</div>
							<!-- END : Single User -->
              
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
          
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
		
		<!-- FOOTER START -->
		<div class="site-footer">
			<div class="container">
			
				<div class="row">
					<div class="col-md-12">
						<p class="copyrights">Copyrights &copy; 2014 Green Trading System - All Rights Reserved</p>
					</div>
				</div>
				
			</div>
		</div>
		<!-- FOOTER END -->
		
    <!-- JAVASCRIPTS START -->
		
    <!-- CORE PLUGINS START -->
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/hover-dropdown.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.migrate.min.js"></script>
		
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.easing.min.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.mousewheel.js"></script>

		<script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/fancybox/jquery.fancybox.pack.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("a[rel=catalogue]").fancybox({
					'transitionIn'		: 'fade',
					'transitionOut'		: 'fade',
					'padding'		      : '0',
					'titlePosition' 	: 'outside',
					'titleFormat'		: function(title, currentArray, currentIndex, currentOpts) {
						return '<span id="fancybox-title-over">Image ' + (currentIndex + 1) + ' / ' + currentArray.length + (title.length ? ' &nbsp; ' + title : '') + '</span>';
					}
				});
			});
		</script>
		
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/bxslider/jquery.bxslider.min.js"></script>
    <script type="text/javascript">
		$('.bxslider').bxSlider({
			minSlides: 1,
			maxSlides: 3,
			slideWidth: 165,
			slideMargin: 15,
			controls: true                     // true, false - previous and next controls
		});
		</script>
		
    <script type="text/javascript">
			$(function () {
				$('#mang_stab').tab('show')
			})
		</script>
    
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/scripts/holder.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/back-to-top.js"></script>
		
    <!--[if lt IE 9]>
    <script src="<?php echo current_theme_url();?>assets/plugins/respond.min.js"></script>  
    <![endif]-->
    <!-- CORE PLUGINS END -->
		
    <!-- JAVASCRIPTS END -->
		
  </body>
</html>
