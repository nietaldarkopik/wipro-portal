<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
				
				  <?php $this->load->view("components/leftbar");?>          


          
					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Sales Profiles <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>General Information</h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              
							<!-- START : Single User -->
							<div class="row profile-user">
							
								<div class="col-md-12">
                  <!-- START : Tab Pane 1 -->
									<div class="row tab-pane fade in active">
										
                    <div class="col-md-4 col-md-push-8 text-center">
											<p><img class="img-circle" src="<?php echo current_theme_url();?>assets/upload/img3-small.jpg" class="profile-photo img-circle" alt="Photo" title="Photo"></p>
										</div>
                    
										<dl class="dl-horizontal col-md-8 col-md-pull-4">
											<dt><small>Owner</small></dt><dd>Mrs. Ivanickov</dd>
											<hr class="dashed">
											<dt><small>User ID</small></dt><dd>2014001xxx</dd>
											<hr class="dashed">
											<dt><small>Location</small></dt><dd>Bandung, Indonesia</dd>
											<hr class="dashed">
											<dt><small>Occupation</small></dt><dd>Web Developer</dd>
											<hr class="dashed">
											<dt><small>Email</small></dt><dd><a href="#">ivanmail@mywebsite.com</a></dd>
											<hr class="dashed">
											<dt><small>Interests</small></dt><dd>Design, Web etc.</dd>
											<hr class="dashed">
											<dt><small>Website Url</small></dt><dd><a href="#">http://www.mywebsite.com</a></dd>
											<hr class="dashed">
										</dl>
										
									</div>
                  <!-- END   : Tab Pane 1 -->
								</div>
								
								
								<div class="col-md-8">
									<div class="panel panel-default">
										<div class="panel-heading">Certificate on Demand</div>
										
										<div class="panel-body">
											<p>While not always necessary, sometimes you need to put your DOM in a box. For those situations, try the panel component.</p>
										</div>
										
										<hr class="dashed" />
										<div class="panel-body row">
											<div class="col-md-3"><img class="img-responsive" title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img1.jpg" width="120" /></div>
											<ul class="list-unstyled col-md-6">
												<li>Certificate No.:</li>
												<li><a href="single-certificate.html"><strong>I-GIST.2014.001.0000.1</strong></a></li>
												<li>
													<small class="">Base Price:</small>
													<p class="margin-0 h4"><strong class="text-red">130.000</strong> IDR</p>
												</li>
											</ul>
											<div class="col-md-3 text-right"><button class="btn btn-bid">Bid Now</button></div>
										</div>
										
										<hr class="dashed" />
										<div class="panel-body row">
											<div class="col-md-3"><img class="img-responsive" title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img2.jpg" width="120" /></div>
											<ul class="list-unstyled col-md-6">
												<li>Certificate No.:</li>
												<li><a href="single-certificate.html"><strong>I-GIST.2014.001.0800.2</strong></a></li>
												<li>
													<small class="">Base Price:</small>
													<p class="margin-0 h4"><strong class="text-red">200.000</strong> IDR</p>
												</li>
											</ul>
											<div class="col-md-3 text-right"><button class="btn btn-bid">Bid Now</button></div>
										</div>
										
										<hr class="dashed" />
										<div class="panel-body row">
											<div class="col-md-3"><img class="img-responsive" title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img3.jpg" width="120" /></div>
											<ul class="list-unstyled col-md-6">
												<li>Certificate No.:</li>
												<li><a href="single-certificate.html"><strong>I-GIST.2014.001.0004.2</strong></a></li>
												<li>
													<small class="">Base Price:</small>
													<p class="margin-0 h4"><strong class="text-red">50.000</strong> IDR</p>
												</li>
											</ul>
											<div class="col-md-3 text-right"><button class="btn btn-bid">Bid Now</button></div>
										</div>
										
									</div>
								</div>
										
										
								<div class="col-md-4">
									
									<div class="panel panel-default">
										<div class="panel-heading">Latest Activities</div>
										<div class="panel-body">
										
											<p>While not always necessary, sometimes you need to put your DOM in a box. For those...</p>
											
											<dl>
												<hr class="dashed" />
												<dt>Maret 12 <span class="label label-primary">Deal</span></dt>
												<dd>Bid Certificate for 120.000 IDR</dd>
												
												<hr class="dashed" />
												<dt>Maret 9 <span class="label label-primary">Deal</span></dt>
												<dd>Bid Certificate for 17.500 IDR</dd>
												
												<hr class="dashed" />
												<dt>Maret 7 <span class="label label-danger">Abort</span></dt>
												<dd>Sale Certificate for 17.500 IDR</dd>
												
												<hr class="dashed" />
												<dt>Maret 6 <span class="label label-warning">Pending</span></dt>
												<dd>Bid Certificate for 214.500 IDR</dd>
												
												<hr class="dashed" />
												<dt>Maret 6 <span class="label label-warning">Pending</span></dt>
												<dd>Bid Certificate for 90.990 IDR</dd>
												
												<hr class="dashed" />
												<dt>Maret 5 <span class="label label-success">Sold</span></dt>
												<dd>Sale Certificate for 90.990 IDR</dd>
											</dl>
											
										</div>
									</div>
									
								</div>
                
							</div>
							<!-- END : Single User -->
              
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
