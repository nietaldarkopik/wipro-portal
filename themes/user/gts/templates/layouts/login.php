<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8" />
    <title>Guest - Green Trading System</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
		
    <!-- Bootstrap Start -->
    <link href="<?php echo current_theme_url();?>assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <!--link href="<?php echo current_theme_url();?>assets/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" /-->
    <link href="<?php echo current_theme_url();?>assets/fonts/stylesheet.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo current_theme_url();?>assets/css/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo current_theme_url();?>assets/plugins/bxslider/jquery.bxslider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo current_theme_url();?>assets/plugins/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" />
    <!-- Bootstrap End -->
		
    <link rel="shortcut icon" href="<?php echo current_theme_url();?>favicon.ico" />
  </head>
  <body class="login-page">
	
		<!-- HEADER START -->
		<div class="site-header">&nbsp;</div>
		<!-- HEADER END -->
		
		<!-- CONTENT START -->
		<div class="site-content">
			<div class="container">
        
        <!-- START : Login Page -->
        <div class="row">
        
          <div class="col-md-4 col-md-push-4 member-login-page">
            <p class="text-center"><img class="img-responsive" title="GTSystem" alt="GTSystem" src="<?php echo current_theme_url();?>assets/images/dummy_logo.png" /></p>
            <div class="panel panel-default">
            
              <div class="panel-heading"><h3>Login</h3></div>
              
              <div class="panel-body">
                <form role="form" action="<?php echo site_url('dashboard/do_login');?>" method="post">
                  <div class="form-group">
                    <label for="InputEmail1">User ID</label>
                    <input type="text" name="user_name" class="form-control" id="InputEmail1" placeholder="User ID"  required autofocus />
                  </div>
                  <div class="form-group">
                    <label for="InputPassword">Password</label>
                    <input type="password" name="password" class="form-control" id="InputPassword" placeholder="Password"  required />
                    <!-- <p class="forgot-pass"><a href="#">Forgot Password?</a></p> -->
                  </div>
                  <!--
                  <div class="checkbox pull-left">
                    <input type="checkbox" /> Remember Me
                  </div>
                  -->
                  <button type="submit" class="btn btn-default btn-green pull-right">Login <i class="glyphicon glyphicon-circle-arrow-right"></i></button>
                </form>				
              </div>
              
            </div>
          
            <?php if(!empty($error)){ ?>
              <div class="alert alert-danger text-center">
                  <strong>Warning!</strong> <?php echo $error;?>
              </div>
            <?php } ?>
            <p class="text-center"><a href="#">Terms of Use</a>  |  <a href="#">Privacy Policy</a></p>
            
          </div>
        </div>
        <!-- END   : Login Page -->
            
			</div>
		</div>
		<!-- CONTENT END -->
		
		<!-- FOOTER START -->
		<div class="site-footer">
			<div class="container">
			
				<div class="row">
					<div class="col-md-12">
						<p class="copyrights">Copyrights &copy; 2014 Green Trading System - All Rights Reserved</p>
					</div>
				</div>
				
			</div>
		</div>
		<!-- FOOTER END -->
		
    <!-- JAVASCRIPTS START -->
		
    <!-- CORE PLUGINS START -->
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/hover-dropdown.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.migrate.min.js"></script>
		
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.easing.min.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/jquery.mousewheel.js"></script>
		
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/scripts/holder.js"></script>
    <script type="text/javascript" src="<?php echo current_theme_url();?>assets/plugins/back-to-top.js"></script>
		
    <!--[if lt IE 9]>
    <script src="<?php echo current_theme_url();?>assets/plugins/respond.min.js"></script>  
    <![endif]-->
    <!-- CORE PLUGINS END -->
		
    <!-- JAVASCRIPTS END -->
		
  </body>
</html>
