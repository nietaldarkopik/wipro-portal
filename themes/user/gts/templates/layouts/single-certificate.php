<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
				
				  <?php $this->load->view("components/leftbar");?>          


          
					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Certificate <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>Certificate Details</h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
							<div class="row single-certificate">
							
								<div class="col-md-8">
									<div class="panel panel-default">
										<h4 class="panel-heading accdn" data-toggle="collapse" data-target="#certificate1">Tree Detail <span class="caret"></span></h4>
										<ul id="certificate1" class="list-group collapse in">
											<li class="list-group-item">
												<p><small class="sub">Certificate No.:</small>
													<?php echo $certificate['nomor_sertifikat'];?>
                        </p>
											</li>
											<li class="list-group-item">
												<p>
                          <small class="sub">Registration No.:</small>
													<?php echo $certificate['no_registrasi'];?>
                        </p>
											</li>
											<li class="list-group-item">
												<p>
                          <small class="sub">Effective Date:</small>
                          <?php echo $certificate['tanggal_berlaku'];?>
                        </p>
											</li>
											<li class="list-group-item">
												<p>
                          <small class="sub">Quantity:</small>
													<?php echo $certificate['jumlah_pohon'];?> pohon
                        </p>
											</li>
											<li class="list-group-item">
												<p>
                          <small class="sub">Tree Index No.:</small>
													<?php echo $certificate['nomor_index_pohon1'];?> &amp; <?php echo $certificate['nomor_index_pohon2'];?> 
                        </p>
											</li>
											<li class="list-group-item">
												<p>
                          <small class="sub">Cluster Address:</small>
													<?php echo $certificate['alamat_cluster'];?>
                        </p>
											</li>
											<li class="list-group-item">
												<p>
                          <small class="sub">Cluster City:</small>
													<?php echo $certificate['kota_cluster'];?>
                        </p>
											</li>
										</ul>

                    <h4 class="panel-heading accdn collapsed" data-toggle="collapse" data-target="#certificate2">Owner Info <span class="caret"></span></h4>
                    <ul id="certificate2" class="list-group collapse">
                      <li class="list-group-item">
                        <p>
                          <small class="sub">
                            Owner Name:
                          </small>
                          <?php echo $certificate['nama'];?>
                        </p>
                      </li>
                      <li class="list-group-item">
                        <p>
                          <small class="sub">
                            Gender:
                          </small>
                          <?php echo $certificate['jenis_kelamin'];?>
                        </p>
                      </li>
                      <li class="list-group-item">
                        <p>
                          <small class="sub">
                            Birth Date:
                          </small>
                          <?php echo $certificate['tempat_lahir'];?>,<?php echo $certificate['tanggal_lahir'];?>
                        </p>
                      </li>
                      <li class="list-group-item">
                      <p>
                        <small class="sub">
                          ID Number:
                        </small>
                        <?php echo $certificate['member_code'];?>
                        </p>
                      </li>
                      <li class="list-group-item">
                        <p>
                          <small class="sub">
                            Address:
                          </small>
                          <?php echo $certificate['alamat_rumah'];?>
                        </p>
                      </li>
                      <li class="list-group-item">
                        <p>
                          <small class="sub">
                            Mother's Maiden Name:
                          </small>
                          <?php echo $certificate['ibu'];?>
                        </p>
                      </li>
                    </ul>

                    <h4 class="panel-heading accdn collapsed" data-toggle="collapse" data-target="#certificate3">Bank Account <span class="caret"></span></h4>
                    <ul id="certificate3" class="list-group collapse">
                      <li class="list-group-item">
                        <p>
                          <small class="sub">
                            Bank Name:
                          </small>
                          <?php echo $certificate['bank'];?>
                        </p>
                      </li>
                      <li class="list-group-item">
                        <p>
                          <small class="sub">
                            Account Holder:
                          </small>
                          <?php echo $certificate['pemilik_rekening'];?>
                        </p>
                      </li>
                      <li class="list-group-item">
                      <p>
                        <small class="sub">
                          Account No:
                        </small>
                        <?php echo $certificate['nomor_rekening'];?>
                        </p>
                      </li>
                    </ul>
                    
                    <!--
                    <h4 class="panel-heading accdn collapsed" data-toggle="collapse" data-target="#certificate4">Inheritance <span class="caret"></span>
                    </h4>
                    <ul id="certificate4" class="list-group collapse">
                      <li class="list-group-item">
                        <p>
                          <small class="sub">
                          Heir 1:
                          </small>
                          <?php #echo $certificate['jumlah_pohon'];?>
                        </p>
                      </li>
                      <li class="list-group-item">
                        <p>
                          <small class="sub">
                          Heir 2:
                          </small>
                          <?php #echo $certificate['jumlah_pohon'];?>
                        </p>
                      </li>
                    </ul>
                    -->
                    
                    <h4 class="panel-heading accdn collapsed" data-toggle="collapse" data-target="#certificate4">History Ceritificate <span class="caret"></span>
                    </h4>
                    <ul id="certificate4" class="list-group collapse">
                      <li class="list-group-item">
                        <table class="table table-striped table-advance table-hover">
                          <tr>
                            <td>
                              <small>Date:</small>
                              <p>&nbsp;</p>
                            </td>
                            <td>
                              <small>Member Code:</small>
                              <p>&nbsp;</p>
                            </td>
                            <td>
                              <small>No. Certificate:</small>
                              <p>&nbsp;</p>
                            </td>
                          </tr>
                        <?php
                        $history = $this->gts_certificates->get_history_by_id($certificate['certificate_id']);
                        if(is_array($history) and count($history) > 0)
                        {
                          foreach($history as $index => $h)
                          {
                        ?>
                        <tr>
                          <td>
                            <p><?php echo $h['owning_date'];?></p>
                          </td>
                          <td>
                            <p><?php echo $h['member_code'];?></p>
                          </td>
                          <td>
                            <p><?php echo $h['nomor_sertifikat'];?></p>
                          </td>
                        </tr>
                      <?php
                        }
                      }
                      ?>    
                        </table>
                      </li>
                    </ul>
                  </div>
									
								</div>
								
								<div class="col-md-4">
								
										<!-- Button For Sell / Tombol utk User yang mau jual sertifikat -->
										<!--h4 class="text-center"><a href="#" class="btn btn-default btn-lg fluid">Sale Certificate Now!</a></h4-->
										
										<!-- Button For Buy / Tombol utk User yang mau menawar(BID) sertifikat -->
                    <form method="post" action="" id="bidform">
                      <input class="form-control margin-top-10 required number" type="text" placeholder="Your Price(IDR)" />
                      <h4 class="text-center"><button class="btn btn-bid btn-lg fluid" type="submit">Bid Certificate Now!</button></h4>
                      <div class="alert alert-danger" id="error_bidding" style="display:none;"></div>
                    </form>
										<br/>
										
									<div class="panel panel-default" style="display:none;">
										<h4 class="panel-heading">Inheritances</h4>
										<div class="panel-body text-center">
											<p><img class="img-circle" title="Image" alt="Image" src="<?php echo current_theme_url();?>assets/upload/img1-small.jpg" width="100" height="100" /></p>
											<strong><a href="single-sales.html">Pasmina</a></strong>
										</div>
										
										<div class="panel-body text-center">
											<p><img class="img-circle" title="Image" alt="Image" src="<?php echo current_theme_url();?>assets/upload/img2-small.jpg" width="100" height="100" /></p>
											<strong><a href="single-sales.html">Juna</a></strong>
										</div>
										
										<div class="panel-body text-center">
											<p><img class="img-circle" title="Image" alt="Image" src="<?php echo current_theme_url();?>assets/upload/img3-small.jpg" width="100" height="100" /></p>
											<strong><a href="single-sales.html">Aisha</a></strong>
										</div>
									</div>
									
								</div>
								
							</div>
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
          
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
