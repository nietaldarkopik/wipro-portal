<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
				
				  <?php $this->load->view("components/leftbar");?>          


          
					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Auction <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>Auction by <?php echo (isset($owner['bank_account_name']))?$owner['bank_account_name']:"-";?>
                <div class="btn-group pull-right">
                  <button type="button" class="btn btn-filter" data-toggle="modal" data-target="#private_message">
                    Private Messages <i class="glyphicon glyphicon-comment"></i></span>
                  </button>
                </div></h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              
							<!-- START : Single Sales -->
							<div class="row profile-member">
              
                <!-- START : Profile Summary -->
								<div class="col-md-4 col-md-push-8 text-center">
									<p><a href="single-sales.html"><img width="120" height="120" src="<?php echo current_theme_url();?>assets/upload/img3-small.jpg" class="profile-photo img-circle" alt="Photo" title="Photo"></a></p>
									<dl>
                    <!--
										<dt><small>Owner</small></dt><dd><a href="single-sales.html">Mrs. Ivanickov <i class="glyphicon glyphicon-circle-arrow-right"></i></a></dd>
                    -->
										<hr class="dashed">
										
										<dt><small>Nama</small></dt><dd><?php echo (isset($owner['bank_account_name']))?$owner['bank_account_name']:"-";?></dd>
										<hr class="dashed">
										
										<dt><small>User ID</small></dt><dd><?php echo (isset($owner['user_id']))?$this->gts_members->str_user_code($owner['user_id']):"-";?></dd>
										<hr class="dashed">
										
										<!--
										<dt><small>Location</small></dt><dd>Bandung, Indonesia</dd>
										<hr class="dashed">
                    
										<dt><small>Price</small></dt><dd>10.000.000</dd>
										<hr class="dashed">
										-->
                    <?php
                    if(isset($certificate['certificate_id']))
                    {
                    ?>
										<dt><small>&nbsp;</small></dt><dd><a href="<?php echo base_url('certificates/detail_certificate/'.$certificate['certificate_id']);?>" class="btn btn-sale btn-show-certificate">View Certificate</a></dd>
                    <?php
                    }
                    ?>
									</dl>
								</div>
                <!-- END   : Profile Summary -->								
                
                
								<!-- START : Certificate Block -->
                <div class="col-md-8 col-md-pull-4">
									
                  <!-- START Carousel --/>
                  <div data-ride="carousel" class="carousel slide margin-bottom-10" id="photo-of-trees">
										<div class="carousel-inner">
											<div class="item active"><a rel="catalogue" href="<?php echo current_theme_url();?>assets/upload/photo-1.jpg" title="Photo 1"><img alt="Photo 1" src="<?php echo current_theme_url();?>assets/upload/photo-1.jpg"></a></div>
											<div class="item"><a rel="catalogue" href="<?php echo current_theme_url();?>assets/upload/photo-2.jpg" title="Photo 2"><img alt="Photo 2" src="<?php echo current_theme_url();?>assets/upload/photo-2.jpg"></a></div>
											<div class="item"><a rel="catalogue" href="<?php echo current_theme_url();?>assets/upload/photo-3.jpg" title="Photo 3"><img alt="Photo 3" src="<?php echo current_theme_url();?>assets/upload/photo-3.jpg"></a></div>
										</div>
										
										<a data-slide="prev" href="#photo-of-trees" class="left carousel-control" title="Previous"><span class="glyphicon glyphicon-chevron-left"></span></a>
										<a data-slide="next" href="#photo-of-trees" class="right carousel-control" title="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
									</div>
									</!-- END Carousel -->
									
									<!-- START Certificate Summary Info -->
                  <div class="panel panel-default">
                    <div class="panel-body">
                    
                      <dl class="dl-horizontal with-colon">
                        <dt>Certificate No.</dt>
                        <dd ><?php echo (isset($certificate['nomor_sertifikat']))?$certificate['nomor_sertifikat']:'-';?></dd>
                        
                        <dt>Quantity</dt>
                        <dd><?php echo (isset($certificate['jumlah_pohon']))?$certificate['jumlah_pohon']:'-';?> (Unit)</dd>
                        
                        <dt>Location</dt>
                        <dd><?php echo (isset($certificate['alamat_cluster']))?$certificate['alamat_cluster']:'-';?></dd>
                        
                        <!--
                        <dt>Price</dt>
                        <dd class="h4"><strong class="text-red">10.000.000</strong></dd>
                        -->
                      </dl>
                      
                      <?php
                      if(isset($certificate['certificate_id']) and !$this->gts_certificates->is_owned($certificate['certificate_id']))
                      {
                        if(isset($selling['selling_id']))
                        {
                          if($this->gts_certificates->is_bid($selling['selling_id'],$this->gts_members->get_current_member_code()) === false)
                          {
                        ?>
                        <form method="post" action="<?php echo base_url('certificates/do_bidding/');?>" id="bidform">
                          <div class="input-group">
                              <input type="text" name="amount_bid" class="form-control number required" placeholder="Your Price (IDR)">
                              <input type="hidden" name="selling_id" class="form-control" value="<?php echo $selling['selling_id'];?>">
                              <input type="hidden" name="action" class="form-control" value="do_bidding">
                              <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">Bid Now</button>
                              </span>
                          </div>
                        </form>
                        <?php
                          }else{
                            $bidding = $this->gts_certificates->is_bid($selling['selling_id'],$this->gts_members->get_current_member_code());
                        ?>
                        <form method="post" action="<?php echo base_url('certificates/do_bidding/');?>" id="bidform">
                          <div class="alert alert-info">Anda sudah melakukan penawaran pada sertifikat ini.</div>
                          <div class="input-group">
                              <input type="hidden" name="bidding_id" class="form-control" value="<?php echo $bidding['bidding_id'];?>">
                              <input type="hidden" name="action" class="form-control" value="do_cancel_bidding">
                              <button class="btn btn-bid btn-default" type="submit">Cancel Bid</button>
                          </div>
                        </form>
                        <?php
                          }
                        }
                      }
                      ?>
                      <div class="alert alert-danger" id="error_bidding" style="display:none;"></div>
                      
                    </div>
                  </div>
                  
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Followers on this Auction
                    </div>
                    <div class="panel-body">
                      <table class="table">
                        <?php
                          if(isset($bidder) and is_array($bidder) and count($bidder) > 0)
                          {
                        ?>
                        <tr>
                          <td>Kode Penawaran</td>
                          <td>Kode Member</td>
                          <td>Tanggal</td>
                          <td>Status</td>
                          <td class="text-right">Jumlah Penawaran</td>
                        </tr>
                        <?php
                          foreach($bidder as $i => $b)
                          {
                        ?>
                          <tr>
                            <td><?php echo $b['bid_code'];?></td>
                            <td><?php echo $this->gts_members->str_user_code($b['member_id']);?></td>
                            <td><?php echo $b['date_bidding'];?></td>
                            <td>
                              <?php 
                                $status = "";
                                if(empty($b['status_owner']))
                                {
                                  
                                  if(
                                    isset($certificate['certificate_id']) and 
                                    isset($b['selling_id']) and 
                                    $this->gts_certificates->is_owned($certificate['certificate_id']) and
                                    !$this->gts_certificates->get_approved_bidder($b['selling_id']))
                                  {
                                    $status = '<a href="'.base_url('transaction/approve_bid/'.$b['bidding_id']).'" class="label label-warning">Calon (Setujui penawaran ini?)</a>';
                                  }else{
                                    $status = '<span class="label label-warning">Calon</span>';
                                  }
                                }elseif($b['status_owner'] == 'cancel')
                                {
                                  $status = '<span class="label label-danger">Batal</span>';
                                }
                                elseif($b['status_owner'] == 'approve')
                                {
                                  $status = '<span class="label label-info">Terpilih</span>';
                                }
                                echo $status;
                              ?>
                            </td>
                            <td class="text-right"><?php echo number_format($b['amount'],0);?> IDR</td>
                          </tr>
                        <?php
                          }
                        }else{
                        ?>
                        <tr>
                          <td colspan="3">Belum ada penawaran</td>
                        </tr>
                        <?php
                        }
                        ?>
                      </table>
                    </div>
                  </div>
                  
                  <!-- END Certificate Summary Info -->
                  
								</div>
                <!-- END   : Certificate Block -->
                
              </div>
							<!-- END   : Single Sales -->
              
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
    <div class="modal fade" id="private_message">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Private Messsage</h4>
          </div>
          <div class="modal-body">
            <p>Kirim pesan kepada member GTS2342342</p>
            <div class="row">
              <div class="col-sm-12 col-md-12">
                <form method="post" action="<?php echo base_url('notification/send_message');?>">
                  <div class="form-group">
                    <label for="validate-text">Pesan</label>
                    <div class="input-group">
                      <textarea cols="30" rows="10" class="form-control" name="validate-text" id="validate-text" placeholder="Validate Text" required></textarea>
                      <span class="input-group-addon danger">&nbsp;</span>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" name="selling_id" value="<?php echo (isset($selling['selling_id']))?$selling['selling_id']:0;?>"/>
            <input type="hidden" name="member_id" value="<?php echo $this->gts_members->get_current_member_id();?>"/>
            <input type="hidden" name="member_member_code" value="<?php echo $this->gts_members->get_current_member_code();?>"/>
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Send Message</button>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
