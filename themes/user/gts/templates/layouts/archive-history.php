<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
				
				  <?php $this->load->view("components/leftbar");?>          


					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Archives Page <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              
						<div class="header-content">
							<h3>Histories &amp; Transactions</h3>
						</div>
						
						<div class="main-content">
            
              <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-body no-more-tables">
                      
                    <table class="table table-striped table-group-buyer panel-default">                                        
                      <tbody>
                        <tr>
                          <td class="">
                            <dl class="dl-horizontal with-colon">
                              <dt>Auction No.</dt>
                              <dd>B1398350326<span class="label label-default bg-green" style="display:none;"><i class="glyphicon glyphicon-ok"></i> Available</span></dd>
     
                              <dt>Starting Date</dt>
                              <dd>2014-04-24 22:38:46</dd>
     
                              <dt>Bid Price</dt>
                              <dd><strong class="text-green">5000.00</strong> IDR</dd>
                            </dl>
                          </td>
                        </tr>
                        </tbody>
                      </table>
                      <table class="table table-striped table-advance table-hover">
                        <thead class="panel-default">
                          <tr>
                            <td rowspan="2" class="text-center" width="15%">
                              <p>Date</p>
                            </td>
                            <td rowspan="2" class="text-center" width="40%">
                              <p>Desc</p>
                            </td>
                            <td colspan="2" class="text-center">
                              <p>Mutasi</p>
                            </td>
                            <td rowspan="2" class="text-right">
                              <p>Saldo</p>
                            </td>
                            <!--
                            <td class="text-right">
                              <p class="clearfix">Status</p>
                            </td>
                            -->
                          </tr>
                          <tr>
                            <td class="text-center">
                              <p>Jumlah</p>
                            </td>
                            <td class="text-center">
                              <p class="clearfix">Status</p>
                            </td>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>
                              <p>2012-12-12</p>
                            </td>
                            <td>
                              <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p>
                            </td>
                            <td class="text-right">
                              <p>Joshua</p>
                            </td>
                            <td>
                              <p><span class="label label-success">CR</span></p>
                              <p><span class="label label-warning">DB</span></p>
                            </td>
                            <td class="text-right">
                              <p>10000 IDR</p>
                            </td>
                            <!--
                            <td class="text-right">
                              <span class="label label-default">SOLD</span>
                            </td>
                            -->
                          </tr>
                        </tbody>                        
                        <!--
                        <tr><td><small>Date:</small><p>2012-12-12</p></td><td><small>Username:</small><p>Joshua</p></td><td><small>Certificate:</small><p>2014.001.001.001-IGIST</p></td><td><small>Desc:</small><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p></td><td class="text-right"><small class="clearfix">Status:</small><span class="label label-primary">Primary</span></td></tr>
                        
                        <tr><td><small>Date:</small><p>2012-12-12</p></td><td><small>Username:</small><p>Joshua</p></td><td><small>Certificate:</small><p>2014.001.001.001-IGIST</p></td><td><small>Desc:</small><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p></td><td class="text-right"><small class="clearfix">Status:</small><span class="label label-danger">Danger</span></td></tr>
                        
                        <tr><td><small>Date:</small><p>2012-12-12</p></td><td><small>Username:</small><p>Joshua</p></td><td><small>Certificate:</small><p>2014.001.001.001-IGIST</p></td><td><small>Desc:</small><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p></td><td class="text-right"><small class="clearfix">Status:</small><span class="label label-success">Success</span></td></tr>
                        
                        <tr><td><small>Date:</small><p>2012-12-12</p></td><td><small>Username:</small><p>Joshua</p></td><td><small>Certificate:</small><p>2014.001.001.001-IGIST</p></td><td><small>Desc:</small><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p></td><td class="text-right"><small class="clearfix">Status:</small><span class="label label-info">Info</span></td></tr>
                        
                        <tr><td><small>Date:</small><p>2012-12-12</p></td><td><small>Username:</small><p>Joshua</p></td><td><small>Certificate:</small><p>2014.001.001.001-IGIST</p></td><td><small>Desc:</small><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p></td><td class="text-right"><small class="clearfix">Status:</small><span class="label label-warning">Warning</span></td></tr>
                        -->
                      </table>
                    
                    </div>
                    <table class="table">
                      <tbody>
                        <tr>
                          <td width="20%">Saldo awal</td>
                          <td width="5%">:</td>
                          <td>10000</td>
                        </tr>
                        <tr>
                          <td>Mutasi Credit</td>
                          <td>:</td>
                          <td>10000</td>
                        </tr>
                        <tr>
                          <td>Mutasi Debit</td>
                          <td>:</td>
                          <td>10000</td>
                        </tr>
                        <tr>
                          <td>Saldo Akhir</td>
                          <td>:</td>
                          <td>10000</td>
                        </tr>
                        </tbody>
                      </table>
                  </div>
                </div>
              </div>
              
              
              <!-- START : Page Navigation -->
              <?php 
                $paging_config = (isset($paging_config) and is_array($paging_config))?$paging_config:array();
                echo $this->m_component->pagination($paging_config);
              ?>
							
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
