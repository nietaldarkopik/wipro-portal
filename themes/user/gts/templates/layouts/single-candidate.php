<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">

				  <?php $this->load->view("components/leftbar");?>          

					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Candidate Profiles <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>General Information
                <div class="btn-group pull-right">
                  <button type="button" class="btn btn-filter">
                    Private Messages <i class="glyphicon glyphicon-comment"></i></span>
                  </button>
                </div></h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              
							<!-- START : Single User -->
							<div class="row profile-user">
								<div class="col-md-8 tab-content">
                  
                  <!-- START : Tab Pane 1 -->
									<div class="row tab-pane fade in active">
										
                    <div class="col-md-3 text-center">
											<p><img width="120" height="120" src="<?php echo current_theme_url();?>assets/upload/img2-small.jpg" class="profile-photo img-circle" alt="Photo" title="Photo"></p>
										</div>
                    
										<dl class="dl-horizontal col-md-9">
											<dt><small>Owner</small></dt><dd>Mrs. Ivanickov</dd>
											<hr class="dashed">
											<dt><small>User ID</small></dt><dd>2014001xxx</dd>
											<hr class="dashed">
											<dt><small>Location</small></dt><dd>Bandung, Indonesia</dd>
											<hr class="dashed">
										</dl>
										
                    <div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Personal Information</div>
												<div class="panel-body">
													<dl class="dl-horizontal with-colon lightext">
														<dt>First Name</dt><dd>Ivan<dd>
														<dt>Last Name</dt><dd>Nickov<dd>
														<dt>Counrty</dt><dd>Spain<dd>
														<dt>Birthday</dt><dd>18 Jan 1982<dd>
														<dt>Occupation</dt><dd>Web Developer<dd>
														<dt>Email</dt><dd><a href="#">ivanmail@mywebsite.com</a><dd>
														<dt>Interests</dt><dd>Design, Web etc.<dd>
														<dt>Website Url</dt><dd><a href="#">http://www.mywebsite.com</a><dd>
														<dt>Mobile Number</dt><dd>+1 646 580 DEMO (6284)dd>
													</dl>
												</div>
											</div>
                    </div>
										
                    <div class="col-md-12">
											<div class="panel panel-default">
												<div class="panel-heading">Others Information</div>
												<div class="panel-body">
                          <p>List groups are a flexible and powerful component for displaying not only simple lists of elements, but complex ones with custom content.</p>
												</div>
											</div>
                    </div>
										
									</div>
                  <!-- END   : Tab Pane 1 -->
									
								</div>
								
								<div class="col-md-4">
                
                  <p>This candidate waiting for your confirmation about price that offered to you</p>
                  
                  <dl class="offered-price">
                    <dt>Price Offered:</dt>
                    <dd><p class="h3 margin-0">24.500.000 IDR</p></dd>
                  </dl>
                  
                  <p><a class="btn btn-bid btn-lg fluid" href="#"><strong><i class="glyphicon glyphicon-thumbs-up"></i><br> Deal</strong></a></p>
                  
                  <p><a class="btn btn-cancel btn-lg fluid" href="#"><strong><i class="glyphicon glyphicon-thumbs-down"></i><br> No Deal</strong></a></p>
                  
								</div>
							</div>
							<!-- END : Single User -->
              
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
