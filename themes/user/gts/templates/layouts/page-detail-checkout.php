<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
				
				  <?php $this->load->view("components/leftbar");?>          
         
					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Auction <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>Auction by <?php echo (isset($owner['bank_account_name']))?$owner['bank_account_name']:"-";?>
                <div class="btn-group pull-right">
                  <button type="button" class="btn btn-filter">
                    Private Messages <i class="glyphicon glyphicon-comment"></i></span>
                  </button>
                </div></h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              
							<!-- START : Single Sales -->
							<div class="row profile-member">
              
                <!-- START : Profile Summary -->
								<div class="col-md-4 col-md-push-8 text-center">
									<p><a href="single-sales.html"><img width="120" height="120" src="<?php echo current_theme_url();?>assets/upload/img3-small.jpg" class="profile-photo img-circle" alt="Photo" title="Photo"></a></p>
									<dl>
                    <!--
										<dt><small>Owner</small></dt>
                    <dd><a href="single-sales.html">Mrs. Ivanickov <i class="glyphicon glyphicon-circle-arrow-right"></i></a></dd>
                    -->
										<hr class="dashed">
                    
										<dt><small>Nama</small></dt>
                    <dd><?php echo (isset($owner['bank_account_name']))?$owner['bank_account_name']:"-";?></dd>
										<hr class="dashed">
										
										<dt><small>User ID</small></dt>
                    <dd><?php echo (isset($owner['user_id']))?$this->gts_members->str_user_code($owner['user_id']):"-";?></dd>
										<hr class="dashed">
										
										<!--
										<dt><small>Location</small></dt>
                    <dd>Bandung, Indonesia</dd>
										<hr class="dashed">
                    
										<dt><small>Price</small></dt>
                    <dd>10.000.000</dd>
										<hr class="dashed">
										-->
                    <?php
                    if(isset($certificate['certificate_id']))
                    {
                    ?>
										<dt><small>&nbsp;</small></dt>
                    <dd><a href="<?php echo base_url('certificates/detail_certificate/'.$certificate['certificate_id']);?>" class="btn btn-sale btn-show-certificate">View Certificate</a></dd>
                    <?php
                    }
                    ?>
									</dl>
								</div>
                <!-- END   : Profile Summary -->								
                
                
								<!-- START : Certificate Block -->
                <div class="col-md-8 col-md-pull-4">
									
                  <!-- START Carousel --/>
                  <div data-ride="carousel" class="carousel slide margin-bottom-10" id="photo-of-trees">
										<div class="carousel-inner">
											<div class="item active"><a rel="catalogue" href="<?php echo current_theme_url();?>assets/upload/photo-1.jpg" title="Photo 1"><img alt="Photo 1" src="<?php echo current_theme_url();?>assets/upload/photo-1.jpg"></a></div>
											<div class="item"><a rel="catalogue" href="<?php echo current_theme_url();?>assets/upload/photo-2.jpg" title="Photo 2"><img alt="Photo 2" src="<?php echo current_theme_url();?>assets/upload/photo-2.jpg"></a></div>
											<div class="item"><a rel="catalogue" href="<?php echo current_theme_url();?>assets/upload/photo-3.jpg" title="Photo 3"><img alt="Photo 3" src="<?php echo current_theme_url();?>assets/upload/photo-3.jpg"></a></div>
										</div>
										
										<a data-slide="prev" href="#photo-of-trees" class="left carousel-control" title="Previous"><span class="glyphicon glyphicon-chevron-left"></span></a>
										<a data-slide="next" href="#photo-of-trees" class="right carousel-control" title="Next"><span class="glyphicon glyphicon-chevron-right"></span></a>
									</div>
									</!-- END Carousel -->
									
									<!-- START Certificate Summary Info -->
                  <div class="panel panel-default">
                    <div class="panel-body">
                    
                      <dl class="dl-horizontal with-colon">                        
                        <dt>Certificate No.</dt>
                        <dd><?php echo (isset($certificate['nomor_sertifikat']))?$certificate['nomor_sertifikat']:'-'; ?></dd>
                        
                        <dt>No Index Jabon</dt>
                        <dd><?php echo (isset($certificate['nomor_index_pohon1']))?$certificate['nomor_index_pohon1']:'-';?></dd>
                        
                        <dt>Location</dt>
                        <dd><?php echo (isset($certificate['alamat_cluster']))?$certificate['alamat_cluster']:'-'; ?></dd>
                        
                        <dt>Quantity</dt>
                        <dd><?php echo (isset($certificate['jumlah_pohon']))?$certificate['jumlah_pohon']:'-'; ?> (Unit)</dd>
    
                        <!--
                        <dt>Price</dt>
                        <dd class="h4"><strong class="text-red">10.000.000</strong></dd>
                        -->
                      </dl>
                      
                    </div>
                  </div>

                  <!--
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Followers on this Auction
                    </div>
                    <div class="panel-body">
                      <table class="table">
                        <?php
                          if(isset($bidder) and is_array($bidder) and count($bidder) > 0)
                          {
                        ?>
                        <tr>
                          <td>Kode Penawaran</td>
                          <td>Kode Member</td>
                          <td>Tanggal</td>
                          <td>Status</td>
                          <td class="text-right">Jumlah Penawaran</td>
                        </tr>
                        <?php
                          foreach($bidder as $i => $b)
                          {
                        ?>
                          <tr>
                            <td><?php echo $b['bid_code'];?></td>
                            <td><?php echo $this->gts_members->str_user_code($b['member_id']);?></td>
                            <td><?php echo $b['date_bidding'];?></td>
                            <td>
                              <?php 
                                $status = "";
                                if(empty($b['status_owner']))
                                {
                                  $status = '<span class="label label-warning">Calon</span>';
                                }elseif($b['status_owner'] == 'cancel')
                                {
                                  $status = '<span class="label label-danger">Batal</span>';
                                }
                                elseif($b['status_owner'] == 'approve')
                                {
                                  $status = '<span class="label label-info">Terpilih</span>';
                                }
                                echo $status;
                              ?>
                            </td>
                            <td class="text-right"><?php echo number_format($b['amount'],0);?> IDR</td>
                          </tr>
                        <?php
                          }
                        }else{
                        ?>
                        <tr>
                          <td colspan="3">Belum ada penawaran</td>
                        </tr>
                        <?php
                        }
                        ?>
                      </table>
                    </div>
                  </div>
                  -->
                  
                  <div class="panel panel-default">
                    <div class="panel-heading">
                      Data Pembayaran
                    </div>
                    <div class="panel-body">
                      <?php
                        $approval = 0;
                        $bidding_id = "";
                        $bidder = array();
                        if(isset($selling['selling_id']))
                        {
                          $bidder = $this->gts_certificates->get_approved_bidder($selling['selling_id']);
                          if(isset($bidder) and is_array($bidder) and count($bidder) > 0)
                          {
                      ?>
                      <table class="table">
                        <tr>
                          <td>Kode Penawaran</td>
                          <td>Tanggal</td>
                          <td class="text-right">Jumlah Pembayaran</td>
                        </tr>
                        <?php
                            if($b['status_owner'] == 'approve')
                            {
                              $approval += 1;
                              $bidding_id = $b['bidding_id'];
                        ?>
                          <tr>
                            <td><?php echo $b['bid_code'];?></td>
                            <td><?php echo $b['date_bidding'];?></td>
                            <td class="text-right"><?php echo number_format($b['amount'],0);?> IDR</td>
                          </tr>
                      </table>
                        <?php
                            }
                        }else{
                        ?>
                        <div class="alert alert-info"><strong>Belum ada persetujuan pembeli</strong></div>
                        <?php
                        }
                        }
                        ?>
                    </div>
                  </div>
                  <?php
                  if($approval == 1)
                  {
                  ?>
                  <form method="post" action="<?php echo base_url('transaction/payment');?>">
                    <input type="hidden" name="data[phone_from]" value="<?php echo $this->gts_greenwallet->get_phone_account("current");?>"/>
                    <input type="hidden" name="data[phone_to]" value="<?php echo $this->gts_greenwallet->get_phone_account($bidder['certificate_owner_code']);?>"/>
                    <input type="hidden" name="data[selling_id]" value="<?php echo $bidder['selling_id'];?>"/>
                    <input type="hidden" name="data[bidding_id]" value="<?php echo $bidder['bidding_id'];?>"/>
                    <input type="hidden" name="data[selling_code]" value="<?php echo $bidder['sell_code'];?>"/>
                    <input type="hidden" name="data[bidding_code]" value="<?php echo $bidder['bid_code'];?>"/>
                    <input type="hidden" name="data[certificate_id]" value="<?php echo $bidder['certificate_id'];?>"/>
                    <input type="hidden" name="data[no_certificate]" value="<?php echo $bidder['no_certificate'];?>"/>
                    <input type="hidden" name="data[owner_member_id]" value="<?php echo $bidder['certificate_owner_id'];?>"/>
                    <input type="hidden" name="data[bidder_member_id]" value="<?php echo $bidder['member_id'];?>"/>
                    <input type="hidden" name="data[owner_member_code]" value="<?php echo $bidder['certificate_owner_code'];?>"/>
                    <input type="hidden" name="data[bidder_member_code]" value="<?php echo $bidder['member_code'];?>"/>
                    <input type="hidden" name="data[amount]" value="<?php echo $bidder['amount'];?>"/>
                    <input type="hidden" name="data[url_success]" value="<?php echo base_url('transaction/payment_success/'.$bidder['selling_id'].'/'.$bidder['bidding_id']);?>"/>
                    <input type="hidden" name="data[url_error]" value="<?php echo base_url('transaction/payment_error/'.$bidder['selling_id'].'/'.$bidder['bidding_id']);?>"/>
                    <input type="hidden" name="data[url_cancel]" value="<?php echo base_url('transaction/payment_cancel/'.$bidder['selling_id'].'/'.$bidder['bidding_id']);?>"/>
                    <button type="submit" name="do_transaction" value="do_transaction" class="btn btn-default btn-do-transaction pull-right">Lanjutkan Pembayaran &raquo;</button>
                  </form>
                  <?php
                  }else{
                  ?>
                  
                  <?php
                  }
                  ?>
                  <!-- END Certificate Summary Info -->
                  
								</div>
                <!-- END   : Certificate Block -->
                
              </div>
							<!-- END   : Single Sales -->
              
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
