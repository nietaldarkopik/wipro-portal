<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">

				  <?php $this->load->view("components/leftbar");?>                    
          
					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Archives Page <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> 15-03-2014</small></h2>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              
						<div class="header-content">
							<h3>Histories &amp; Transactions</h3>
						</div>
						
						<div class="main-content">
            
              <div class="row">
                <div class="col-md-12">
                  <div class="panel panel-default">
                    <div class="panel-body no-more-tables">
                      <table class="table table-striped table-advance table-hover">                        
                      <?php 
                        $notifications = $this->gts_notification->get_all_notification(); 
                        if(is_array($notifications) and count($notifications) > 0)
                        {
                          foreach($notifications as $i => $n)
                          {                              
                              $status = $n['status_owner'];
                              $do_status = $status;
                              $container_status = "";
                              switch($status)
                              {
                                case "cancel":
                                $do_status = "<b>membatalkan penawaran</b>";
                                #$is_read = ($this->gts_notification->is_read($n['id'],'bidding'))?'':' alert-danger';
                                $is_read = ' alert-danger';
                                $container_status = "class='margin-0 alert".$is_read."'";
                                break;
                                case "approve" :
                                $do_status = "<b>menyetujui penawaran</b>";
                                #$is_read = ($this->gts_notification->is_read($n['id'],'bidding'))?'':' alert-success';
                                $is_read = ' alert-success';
                                $container_status = "class='margin-0 alert".$is_read."'";
                                break;
                                case "" :
                                $do_status = "<b>melakukan penawaran</b>";
                                #$is_read = ($this->gts_notification->is_read($n['id'],'bidding'))?'':' alert-info';
                                $is_read = ' alert-info';
                                $container_status = "class='margin-0 alert".$is_read."'";
                                break;
                              }
                              $messages = 'Member '.$this->gts_members->str_user_code($n['member_id']).' '.$do_status.' pada sertifikat '.$n['no_certificate'];
                      ?>
                          <tr>
                            <td>
                              <small>Date:</small>
                              <p><?php echo $n['date_updated'];?></p>
                            </td>
                            <td>
                              <small>Member ID:</small>
                              <p><?php echo $this->gts_members->str_user_code($n['member_id']);?></p>
                            </td>
                            <td>
                              <small>Certificate:</small>
                              <p><?php echo $n['no_certificate'];?></p>
                            </td>
                            <td>
                              <small>Desc:</small>
                              <p <?php echo $container_status;?>><?php echo $messages;?></p>
                            </td>
                            <td class="text-right">
                              <small class="clearfix">Status:</small>
                              <span class="label label-default"><?php echo $do_status;?></span>
                              <!--
                              <a  href="<?php echo base_url('certificates/bidding_detail/'.$n['selling_id'].'/show/'.$n['id']);?>">
                                  <?php echo $do_status;?>
                              </a>
                              -->
                            </td>
                          </tr>
                        <?php
                          }
                        }
                        ?>
                        <!--
                        <tr><td><small>Date:</small><p>2012-12-12</p></td><td><small>Username:</small><p>Joshua</p></td><td><small>Certificate:</small><p>2014.001.001.001-IGIST</p></td><td><small>Desc:</small><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p></td><td class="text-right"><small class="clearfix">Status:</small><span class="label label-primary">Primary</span></td></tr>
                        
                        <tr><td><small>Date:</small><p>2012-12-12</p></td><td><small>Username:</small><p>Joshua</p></td><td><small>Certificate:</small><p>2014.001.001.001-IGIST</p></td><td><small>Desc:</small><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p></td><td class="text-right"><small class="clearfix">Status:</small><span class="label label-danger">Danger</span></td></tr>
                        
                        <tr><td><small>Date:</small><p>2012-12-12</p></td><td><small>Username:</small><p>Joshua</p></td><td><small>Certificate:</small><p>2014.001.001.001-IGIST</p></td><td><small>Desc:</small><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p></td><td class="text-right"><small class="clearfix">Status:</small><span class="label label-success">Success</span></td></tr>
                        
                        <tr><td><small>Date:</small><p>2012-12-12</p></td><td><small>Username:</small><p>Joshua</p></td><td><small>Certificate:</small><p>2014.001.001.001-IGIST</p></td><td><small>Desc:</small><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p></td><td class="text-right"><small class="clearfix">Status:</small><span class="label label-info">Info</span></td></tr>
                        
                        <tr><td><small>Date:</small><p>2012-12-12</p></td><td><small>Username:</small><p>Joshua</p></td><td><small>Certificate:</small><p>2014.001.001.001-IGIST</p></td><td><small>Desc:</small><p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti</p></td><td class="text-right"><small class="clearfix">Status:</small><span class="label label-warning">Warning</span></td></tr>
                        -->
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              
              
              <div class="bottom-page-navi">
                <div class="row">
                  <div class="col-md-12">
                  
                    <ul class="pagination custom">
                      <li class="disable"><a href="#">&laquo;</a></li>
                      <li class="active"><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">5</a></li>
                      <li><a href="#">&raquo;</a></li>
                    </ul>
                    
                  </div>
                </div>
              </div><!-- .bottom-page-navi -->
							
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
