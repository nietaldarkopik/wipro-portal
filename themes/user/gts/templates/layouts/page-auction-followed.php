<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">

				  <?php $this->load->view("components/leftbar");?>          

					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Followed Auction <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
            <!-- START : Primary Content -->
						<div class="header-content">
							<h3>Status of Followed Auctions</h3>
						</div>
						
						<div class="main-content">
							<div class="table-responsive">
              
                <?php
                $member_id = $this->gts_members->get_current_member_id();
                $following = $this->gts_certificates->get_following($member_id);
                if(is_array($following) and count($following) > 0)
                {
                  foreach($following as $i => $f)
                  {
                ?>
                  <table class="table table-group-buyer">
                  
                    <tr>
                      <td class="text-center td-middle">
                          <img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img1.jpg" width="120"  style="display:none;"/>
                      </td>
                      <td class="">
                        <dl class="dl-horizontal with-colon">
                          <dt>Auction No.</dt>
                          <dd><?php echo $f['bid_code'];?><span class="label label-default bg-green" style="display:none;"><i class="glyphicon glyphicon-ok"></i> Available</span></dd>

                          <dt>Starting Date</dt>
                          <dd><?php echo $f['date_bidding'];?></dd>

                          <dt>Bid Price</dt>
                          <dd><strong class="text-green"><?php echo $f['amount'];?></strong> IDR</dd>
                        </dl>
                      </td>
                      <td class="text-center td-middle bg-green-dark" rowspan="2" width="25%">
                        <small class="clearfix">Your Latest Bid:</small>
                        <p class="text-center h4"><strong><?php echo $f['amount'];?> IDR</strong></p>
                        <a href="javascript:void();" style="display:none;" class="btn btn-default btn-lg">Re-Bid</a>
                      </td>
                    </tr>
                    
                    <tr>
                      <td colspan="2">
                        <div class="col-table">
                          <p class="status-ok"><i class="glyphicon glyphicon-ok-circle"> </i> You has been following this auction</p>
                          <div class="col-td"><p class="text-right"><a href="<?php echo base_url('certificates/bidding_detail/'.$f['selling_id']);?>" class="btn btn-bid btn-sm">See Auction</a></p></div>
                        </div>
                      </td>
                    </tr>
                    
                  </table>
              <?php
                }
              }
              ?>
              </div><!-- .table-responsive -->
            
            
              <!-- START : Page Navigation -->
              <?php 
                $paging_config = (isset($paging_config) and is_array($paging_config))?$paging_config:array();
                echo $this->m_component->pagination($paging_config);
              ?>
              <!-- END : Page Navigation -->
							
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
