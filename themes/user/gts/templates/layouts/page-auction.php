<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
				
				  <?php $this->load->view("components/leftbar");?>
          
					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Auction <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>Today's Auctions
                <div class="btn-group pull-right">
                  <button type="button" class="btn btn-filter dropdown-toggle" data-toggle="dropdown">
                    Filter By <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </div></h3>
						</div>
						

            <!-- START : Primary Content -->
						<div class="main-content">
              
              <!-- START : Category Seller -->
              
							<div class="table-responsive">
								<table width="100%" class="table table-group-sale">
                  <?php
                    $certificates = $this->gts_certificates->get_on_sales();
                    if(is_array($certificates) and count($certificates) > 0)
                    {
                      foreach($certificates as $i => $c)
                      {
                  ?>
                  <tr>
                    <!-- 
										<td class="text-center" rowspan="2">
                      <p><a href="single-sales.html"><img title="Photo" alt="Photo" class="img-circle" width="100" height="100" src="<?php echo current_theme_url();?>assets/upload/img1-small.jpg" /></a></p>
                      <p><a href="single-sales.html">Mrs.Joshua <i class="glyphicon glyphicon-circle-arrow-right"></i></a></p>
                    </td>
                     -->
										<td>
                      <dl class="dl-horizontal with-colon">
                        <dt>Sell Code</dt>
                        <dd><?php echo $c['sell_code'];?></dd>
                        
                        <dt>Date</dt>
                        <dd><?php echo $c['date_selling'];?></dd>
                        
                        <dt>Member ID</dt>
                        <dd><?php echo $this->gts_members->str_user_code($c['member_id']);?></dd>
                        
                        <dt>Certificate No.</dt>
                        <dd><?php echo $c['nomor_sertifikat'];?></dd>
                        
                        <dt>No Index Jabon</dt>
                        <dd><?php echo $c['nomor_index_pohon1'];?></dd>
                        
                        <dt>Location</dt>
                        <dd><?php echo $c['alamat_cluster'];?></dd>
                        
                        <dt>Quantity</dt>
                        <dd><?php echo $c['jumlah_pohon'];?> (Unit)</dd>
                        <!--
                        <dt>Price:</dt>
                        <dd class="h4 margin-0 text-green"><strong>120.000 IDR</strong></dd>
                        -->
                      </dl>
                    </td>
										<!-- <td class="text-center td-middle"><a href="<?php echo current_theme_url();?>assets/upload/img1.jpg" rel="catalogue"><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img1.jpg" width="120" /></a></td> -->
									</tr>
									<tr><td colspan="2" class="text-right"><a href="<?php echo base_url('certificates/bidding_detail/'.$c['selling_id']);?>" class="btn btn-bid btn-md">Bid (<?php echo $this->gts_certificates->get_total_bid($c['selling_id']);?> bidder)</a></tr>
                  <?php
                    }
                  }
                  ?>
								</table>
                <?php
                if(isset($message))
                  echo $message;
                ?>
							</div><!-- .table-responsive -->
              
              <!-- START : Category Seller -->
              
              <!-- START : Page Navigation -->
              <?php 
                $paging_config = (isset($paging_config) and is_array($paging_config))?$paging_config:array();
                echo $this->m_component->pagination($paging_config);
              ?>
							
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
    <div class="modal fade" id="showdetailcertificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Konfirmasi Penjualan</h4>
          </div>
          <div class="modal-body">
            Apakah anda yakin ingin menjualnya?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="button" class="btn btn-primary" id="confirm_sure">Yakin</button>
            <form method="post" action="<?php echo base_url("certificates/do_bid");?>" id="do_sell_form">
              <input type="hidden" class="certificate_id" name="certificate_id" value=""/>
              <input type="hidden" class="action" name="action" value="do_sell"/>
            </form>
          </div>
        </div>
      </div>
    </div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
