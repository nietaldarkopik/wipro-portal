<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">

				  <?php $this->load->view("components/leftbar");?>          

					<div class="content-container col-sm-8 col-md-9">
						
						<div class="header-page">
							<h2>Archive Certificates <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
            <!-- START : Primary Content -->
						<div class="header-content">
							<h3>Current Existing Certificates</h3>
						</div>
						
						<div class="main-content">
              <!-- START : Certificate on Sale -->
							<div class="row gts-grid certificates">
                
                <?php
                $certificates = $this->gts_certificates->get_my_certificates();
                if(is_array($certificates) and count($certificates) > 0)
                {
                  foreach($certificates as $i => $certificate)
                  {
                    $is_sale = $this->gts_certificates->is_sale($certificate['certificate_id']);
                ?>
								<div class="col-xs-12 col-sm-6 col-md-6 sell-items <?php echo ($is_sale !== false)?'on-sale':'';?>" id="certificate_<?php echo $certificate['certificate_id'];?>">
									<dl class="dl-horizontal">
										<dt><img title="Photo" alt="Photo" src="<?php echo current_theme_url();?>assets/upload/img3.jpg" />
											<span class="label bg-green"><i class="glyphicon glyphicon-ok"></i> On Sale</span></dt>
										<dd>
											<ul class="clearlist">
												<li>Certificate No.:</li>
												<li class="no_certificate"><a href="<?php echo base_url('certificates/detail_certificate/'.$certificate['certificate_id']);?>"><?php echo $certificate['nomor_sertifikat'];?></a></li>
												<li><small>Jumlah:</small><p class="margin-0 h4"><?php echo $certificate['jumlah_pohon'];?> pohon</p></li>
												<li class="clearfix">
													<?php 
                            if(isset($is_sale['selling_id']) and !empty($is_sale['selling_id']))
                            {
                          ?>
                              <button class="btn btn-cancel btn-xs pull-left btn-cancel-sell" id="confcancelsell-<?php echo (isset($is_sale['selling_id']))?$is_sale['selling_id']:"";?>">Cancel Sale</button>
                          <?php
                            }
                          ?>
                          <button class="btn btn-sale btn-xs pull-left btn-sale-sell" id="confsell-<?php echo $certificate['certificate_id'];?>">Sale Now</button>
                          <a href="<?php echo base_url('certificates/detail_certificate/'.$certificate['certificate_id']);?>" class="btn btn-default btn-xs pull-right">Detail</a>
												</li>
											</ul>
										</dd>
									</dl>
								</div>
                <?php
                }
              }
              ?>
							</div>
              <!-- END   : Available Certificate -->
						
              <!-- START : Page Navigation -->
              <?php 
                $paging_config = (isset($paging_config) and is_array($paging_config))?$paging_config:array();
                echo $this->m_component->pagination($paging_config);
              ?>
              <!-- END : Page Navigation -->
							
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
    
		</div>
    <div class="modal fade" id="ConfirmSell" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Konfirmasi Penjualan</h4>
          </div>
          <div class="modal-body">
            Apakah anda yakin ingin menjualnya?
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
            <button type="button" class="btn btn-primary" id="confirm_sure">Yakin</button>
            <form method="post" action="<?php echo base_url("certificates/do_sell");?>" id="do_sell_form">
              <input type="hidden" class="certificate_id" name="certificate_id" value=""/>
              <input type="hidden" class="action" name="action" value="do_sell"/>
            </form>
          </div>
        </div>
      </div>
    </div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
