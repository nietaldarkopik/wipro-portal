<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
		<?php
      echo $this->assets->print_css("body");
      echo $this->assets->print_css_inline("body");
      echo $this->assets->print_js("body");
      echo $this->assets->print_js_inline("body");
    ?>
  </body>
</html>
