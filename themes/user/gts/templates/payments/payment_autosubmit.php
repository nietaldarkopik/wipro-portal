<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
  $this->load->view("components/top");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
					<div class="content-container col-sm-12 col-md-12">
            <!-- START : Primary Content -->
						<div class="main-content">
							<div class="row single-certificate">
							
								<div class="col-md-12">
									<div class="panel panel-default">
										<h4 class="panel-heading accdn" data-toggle="collapse" data-target="#certificate1">Proses Pembayaran</h4>
																			
										<div class="panel-body text-center">
                      <div class="alert alert-warning">
                        Anda sedang dialihkan menuju halaman pembayaran.
                      </div>
                      <?php
                        if(isset($params) and is_array($params) and count($params) > 0 and isset($selling_id))
                        {
                      ?>
                      <form method="post" action="<?php echo base_url('virtual/virtual_payment/'.$selling_id);?>" id="autosubmit">
                        <?php
                          foreach($params as $i => $p)
                          {
                        ?>
                          <input type="hidden" name="<?php echo $i;?>" value="<?php echo $p;?>"/>
                        <?php
                          }
                        ?>
                      </form>
                      <?php                      
                      }
                      ?>
                      <br/>
                    </div>
                  </div>
								</div>

							</div>
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
          
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
