<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $this->load->view("header");
?>
		<!-- CONTENT START -->
		<div class="site-content">
			
			<div class="container">
				<div class="row">
          
					<div class="content-container col-sm-12">
						
						<div class="header-page">
							<h2>Sandbox Green Wallet (Virtual Albarkah) <small class="pull-right margin-top-10"><i class="glyphicon glyphicon-calendar"></i> <?php echo date("Y-m-d");?></small></h2>
						</div>
						
						<div class="header-content">
							<h3>Payment Green Wallet (Virtual Albarkah) </h3>
						</div>
						
            <!-- START : Primary Content -->
						<div class="main-content">
              <div class="row">
                <div class="col-md-12">
                  Pilih Sandbox Respon :<br/><br/>
                  <a href="<?php echo $this->input->post("url_success");?>" class="alert alert-success"><strong>Respon Sukses Pembayaran</strong></a>
                  <a href="<?php echo $this->input->post("url_error");?>" class="alert alert-danger"><strong>Respon Gagal Pembayaran</a>
                  <a href="<?php echo $this->input->post("url_cancel");?>" class="alert alert-warning"><strong>Respon Batal Pembayaran</a>
                </div>
              </div>
						</div>
            <!-- END : Primary Content -->
						
					</div><!-- .content-container -->
					
				</div><!-- .row -->
			</div><!-- .container -->
			
		</div>
		<!-- CONTENT END -->
<?php
  $this->load->view("components/bottom");
  $this->load->view("footer");
?>
