<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php

  $user_id = $this->user_access->current_user_id;
  $current_menu_allowed = $this->user_access->get_custom_menus_allowed($user_id," AND (`path` = '".$this->uri->segment(1)."' AND `controller` = '".$this->uri->segment(2)."' AND `function` = 'index' AND `type` = 'home')");
  $current_menu_allowed = (isset($current_menu_allowed[0]))?$current_menu_allowed[0]:array();
  $parent_menu_allowed = array();
  if(isset($current_menu_allowed['user_menu_id']))
  {
    $parent_menu_allowed = $this->user_access->get_custom_menus_allowed($user_id," AND (parent_menu = '".$current_menu_allowed['user_menu_id']."') ");
    if(is_array($parent_menu_allowed) and count($parent_menu_allowed) == 0)
    {
      $parent_menu_allowed = $this->user_access->get_custom_menus_allowed($user_id," AND (parent_menu = '".$current_menu_allowed['parent_menu']."') ");
    }
  }
?>
<?php
if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
{
?>
  <div class="col-sm-4 col-md-3 sidebar-container hidden-print">
    <form action="#" method="get" class="form form-search clearfix">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" />
        <span class="input-group-btn">
          <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i></button>
        </span>
      </div><!-- /input-group -->
    </form>
            
    <div class="panel-group list-group control-panel marginbottom4px">
      <a href="#" class="list-group-item"><i class="glyphicon glyphicon-home">&nbsp;</i>Home</a>
    </div><!-- .panel-user -->
    
    <div class="panel-group marginbottom4px" id="accordion">
      
      <div class="panel-body padding0">
        <div class="list-group control-panel marginbottom4px">
          
          <?php
            
            if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
            {
              foreach($parent_menu_allowed as $i => $m)
              {
            ?>
                <a href="<?php echo base_url($m['path']."/".$m['controller'].'/'.$m['function']);?>" class="list-group-item"><i class="<?php echo $m['attributes'];?>">&nbsp;</i><?php echo $m['menu_title'];?></a>
            <?php                        
              }
            }
          ?>

        </div><!-- .panel-user -->
      </div>
      
    </div>

    <div class="list-group control-panel">
      <a href="<?php echo base_url('admin/dashboard/do_logout');?>" class="list-group-item"><i class="glyphicon glyphicon-cog">&nbsp;</i>Logout</a>
    </div><!-- .panel-user -->
    
  </div><!-- .sidebar-container -->
  <div class="col-sm-12 col-md-9">
    <div class="row">
  <?php
}else
{
?>
  <div class="col-sm-12 col-md-12">
    <div class="row">
<?php
}
?>
