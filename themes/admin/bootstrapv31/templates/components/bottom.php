<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  if($is_ajax == 1){}else{
?>
      </div>
    </div>
		<!-- FOOTER START -->
		<div class="site-footer">
			<div class="container-fluid">
			
				<div class="row">
					<div class="col-md-12">
						<p class="copyrights">Copyrights &copy; <?php echo date("Y");?> <?php echo WEB_NAME;?> - All Rights Reserved</p>
					</div>
				</div>
				
			</div>
		</div>
		<!-- FOOTER END -->
<?php } ?>
