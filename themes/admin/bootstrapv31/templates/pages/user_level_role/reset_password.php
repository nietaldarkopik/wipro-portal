<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

				<div class="pad_left1"><h2 class="pad_bot1"><?php echo $page_title;?></h2></div>
				<br class="fclear"/><br/>
				<form id="form" action="<?php echo base_url();?>users/reset_password" method="post" class="form" enctype="multipart/form-data">
					<fieldset id="edit_form_fieldset">
						<legend>Form Data</legend>
						<label for="username">Username</label>
						<span class="value_view"> : 
							<input type="text" name="data[username]" value="" id="username" maxlength="" size="" style="" class="input_text required">
						</span>
						<br class="fclear">
						<label for="new_password">New Password</label>
						<span class="value_view"> : 
							<input type="password" name="data[new_password]" value="" id="new_password" maxlength="" size="" style="" class="input_text required">
						</span>
						<br class="fclear">
						<label for="re_new_password">Re-enter New Password</label>
						<span class="value_view"> : 
							<input type="password" name="data[re_new_password]" value="" id="re_new_password" maxlength="" size="" style="" class="input_text required">
						</span>
						<br class="fclear">
						<span class="value_view">
							<input type="hidden" name="nonce" value="1385199458152907762950be1"> 
							<input type="hidden" name="ajax_target" value="article.col1"> 
							<input type="hidden" name="is_ajax" value="1"> 
							<input type="hidden" name="do_change_password" value="1"> 
							<input type="submit" value="Simpan">
						</span>
					</fieldset>
				</form>
				<?php
					echo $response;
				?>

