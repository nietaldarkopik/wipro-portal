<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="body1">
  <div class="main">
    <!-- header -->
    <header>
      <nav>
      	<div id="menu" class="ddsmoothmenu">
			  <?php
				$this->load->view("topmenu");
			  ?>	      
        </div>
      </nav>
      <div class="wrapper">
        <h1><a href="index.html" id="logo">SIM BINTEK</a></h1>
      </div>
      <!-- <div id="slogan">Balai Teknik Air Minum Dan Sanitasi<span>Wilayah II Surabaya</span></div> -->
      <!-- div id="slogan">
				<marquee direction="left" scrollamount="8">
					<?php
					/*
						$q_news = $this->db->get("berita");
						$f_news = $q_news->result_array();
						foreach($f_news as $index => $n)
						{
							echo $n['isiberita'] . ' | ';
						}
					*/
					?>
				</marquee>
      </div -->
		<?php
		$pages = array(	
						"bintek" => 'banner1.jpg',
						"uji_kompetensi" => 'banner2.jpg',
						"sertifikasi" => 'banner3.jpg',
						"pnbp" => 'banner4.jpg',
						"administrasi" => 'banner5.jpg',
						"utility" => 'banner6.jpg'
						);
		
		$user_id = $this->user_access->current_user_id;
		$parent_menu_allowed = $this->user_access->get_menus_allowed($user_id," AND parent_menu = 0");
		$accessable = array();
		if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
		{
			foreach($parent_menu_allowed as $i => $p)
			{
				$c_accessable = $this->user_access->is_menu_allowed($user_id,$p['controller']);
				if($c_accessable and isset($pages[$p['type']]))
				{
					$accessable[] = array('page' => $p['type'],'img' => $pages[$p['type']]);
				}
			}
		}
		?>
		<ul class="bannerleft">
			<?php
			if(is_array($accessable) and count($accessable) > 0)
			{
				for($i = 0; $i < 3; $i++)
				{
					$li = (isset($accessable[$i]) and is_array($accessable[$i]) and count($accessable[$i]) > 0)?$accessable[$i]:"";
					if(empty($li))
						continue;
			?>
					<li><a href="<?php echo base_url();?>dashboard/index/set_menu_group/<?php echo $li['page'];?>"><img src="<?php echo current_theme_url() . "static/images/";?><?php echo $li['img'];?>" alt=""></a></li>
			<?php
				}
			}
			?>
		</ul>	
		<ul class="bannerright">
			<?php
			if(is_array($accessable) and count($accessable) > 0)
			{
				for($i = 3; $i < 6; $i++)
				{
					$li = (isset($accessable[$i]) and is_array($accessable[$i]) and count($accessable[$i]) > 0)?$accessable[$i]:"";
					if(empty($li))
						continue;
			?>
					<li><a href="<?php echo base_url();?>dashboard/index/set_menu_group/<?php echo $li['page'];?>"><img src="<?php echo current_theme_url() . "static/images/";?><?php echo $li['img'];?>" alt=""></a></li>
			<?php
				}
			}
			?>
		</ul>	
      <div class="clear"></div>
    </header>
    <!-- / header -->
  </div>
</div>

