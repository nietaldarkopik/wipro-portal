/*
	function BindAllEvent()
	{
		jQuery(document).on('click', "#pager a, a.a_ajax", function (event) {
			event.preventDefault();
			var the_this = jQuery(this);
			ajax_url = jQuery(this).attr("href");
			current_url = ajax_url;
			ajax_target = jQuery(this).attr("_target");
			jQuery(ajax_target).html(loading_text);
			
			if(typeof(ajax_target) == "undefined" || ajax_target == "")
			{
				ajax_target = "article.col1";
			}
			
			if(jQuery(this).hasClass("delete"))
			{
				myConfirm("Apakah anda yakin akan melakukannya?", function(){load_content()}, function(){}, "Delete Confirmation")
				return false;
			}
			
			jQuery(ajax_target).html(loading_text);
			jQuery.ajax({
				url: ajax_url,
				data: "is_ajax=1",
				type: "post",
				success: function(result){
					var the_result = jQuery('<div/>').html(result).contents();
					the_result = jQuery(the_result).find(ajax_target);
					jQuery(the_result).hide();
					if(the_result.length > 0)
						result = jQuery(the_result).html();
						
					jQuery(ajax_target).hide();
					jQuery(ajax_target).html(result);
					jQuery(ajax_target).fadeIn(1000);
					
					if(jQuery(the_this).parents(".list1").find("a").length > 0)
					{
						jQuery(the_this).parents(".list1").find("a").removeClass("active");
						jQuery(the_this).addClass("active");
					}
					
					//load_rightbar();
					init();
				}
			});
			return false;
		});

		jQuery(document).on("click",".do_change_background",function(e){
			e.preventDefault();
			jQuery(".change_background").click();
		});
		
		jQuery(document).on("change",".change_background",function(){
			myConfirm("Apakah anda yakin akan mengganti background?", function(){
				jQuery("form#change_background_form").ajaxSubmit({
						target: ".dumper",
						success: function(result, status) {
							if(result != "")
							{
								jQuery("#certificate_container").css("background","url('" + base_url + "uploads/media/sertifikat/"+result+"') no-repeat");
								jQuery("#certificate_container").css("background-size","100% 100%");
								jQuery("#certificate_container").css("background-position","center center");
							}else{
								jQuery("#certificate_container").css("background","none");
							}
							//load_rightbar();
							init();
						}
				});
			}, function(){}, "Update Confirmation")
			return false;
		});
		
		jQuery(document).on("submit","form:not(.no_ajax)",function(event){
			event.preventDefault();
			tinyMCE.triggerSave();
			var the_this = jQuery(this);
			ajax_url = jQuery(this).attr("action");
			current_url = ajax_url;
			ajax_form = jQuery(this);
			ajax_target = jQuery(this).find("input[name='ajax_target']").val();
			if(typeof(ajax_target) == "undefined" || ajax_target == "")
			{
				ajax_target = "article.col1";
			}
			jQuery(ajax_target).html(loading_text);
			jQuery(this).ajaxSubmit({
					target: ajax_target,
					success: function(result, status) {
						var the_result = jQuery('<div/>').html(result).contents();
						the_result = jQuery(the_result).find(ajax_target);
						jQuery(the_result).hide();
						if(the_result.length > 0)
							result = jQuery(the_result).html();
							
						jQuery(ajax_target).hide();
						jQuery(ajax_target).html(result);
						jQuery(ajax_target).fadeIn(1000);
						
						if(jQuery(the_this).parents(".list1").find("a").length > 0)
						{
							jQuery(the_this).parents(".list1").find("a").removeClass("active");
							jQuery(the_this).addClass("active");
						}
						
						//load_rightbar();
						init();
					}
			});
			return false;
		});

		jQuery(document).on("click", ".remove_row_input_multiple",function(){
			
			var table = jQuery(this).parents("table");
			jQuery(this).parents("tr").fadeOut();
			jQuery(this).parents("tr").remove();
			
			for(var i = 0; i < jQuery(table).find("tr.master2").length;i++)
			{
				jQuery(table).find("tr.master2").eq(i).find("td:eq(0)").html(i+1);
			}
		});
		
		jQuery(document).on("click", ".add_row_input_multiple",function(){
			var table = jQuery(this).parents("table");
			var tr = jQuery(table).find("tr").eq(1).clone();
			jQuery(tr).removeClass("master").addClass("master2");
			jQuery(tr).insertBefore(jQuery(table).find("tr").last());
			jQuery(table).find("tr").last().prev().fadeIn();
			
			
			for(var i = 0; i < jQuery(table).find("tr.master2").length;i++)
			{
				jQuery(table).find("tr.master2").eq(i).find("td:eq(0)").html(i+1);
			}
		});
		
		init();

	}

function check_is_login()
{
	jQuery.ajax({
		url: base_url + "dashboard/is_login",
		data: "is_ajax=1",
		type: "post",
		success: function(result){
			is_login = result;
			if(result == 0)
			{
				window.location.href = base_url + "dashboard/login";
			}
		}
	});
}

jQuery(document).on("change","form#form_user_level_role #user_level_id",function(){		
		ajax_form = jQuery(this).parents("form#form_user_level_role");
		ajax_url = jQuery(ajax_form).attr("action");
		ajax_target = jQuery(ajax_form).find("input[name='ajax_target']").val();
		jQuery(ajax_target).html(loading_text);
		jQuery.ajax({
			url: ajax_url,
			data: jQuery(ajax_form).serialize(),
			type: "post",
			success: function(result){
				jQuery(ajax_target).html(result);
			}
		});
});

var is_login = false;

jQuery(document).ready(function(){
	check_is_login();
	
	if(is_login)
	{
		BindAllEvent();
	}
	

});
*/



jQuery(document).on("change","select[name='bulk_action']",function(){
	jQuery(this).parents("form").attr("action",base_url + jQuery(this).val());
});


function get_bulk(object)
{
	var bulk_data = jQuery("input.bulk_data:checked").toArray();
  if(!object)
  {}else{
    bulk_data = jQuery(object).toArray();
  }
	var a = [];
	for ( var i = 0; i < bulk_data.length; i++ ) 
	{
		a.push( jQuery(bulk_data[ i ]).val() );
	}
	return a.join(",");
}

jQuery(document).on("change","input.bulk_data_all",function(){
	var bulk_data = jQuery(this).parents("table").find("input.bulk_data");
	for ( var i = 0; i < bulk_data.length; i++ ) 
	{
		if(jQuery(this).is(":checked"))
		{
			jQuery(bulk_data[ i ]).prop("checked",true);
		}else{
			jQuery(bulk_data[ i ]).prop("checked",false);
		}
	}
  var obj = jQuery(this).parents(".ajax_container").find("input.bulk_data:checked");
	jQuery(this).parents(".ajax_container").find("input.bulk_data_tmp").val(get_bulk(obj));
});

jQuery(document).on("change","input.bulk_data",function(){
  var obj = jQuery(this).parents(".ajax_container").find("input.bulk_data:checked");
	jQuery(this).parents(".ajax_container").find("input.bulk_data_tmp").val(get_bulk(obj));
});


jQuery(document).on("change","form#form_user_level_role .checkall_method",function(){		
		var id_checkall = jQuery(this).attr("id");
		var is_checked = false;
		if (jQuery("#"+id_checkall).is(':checked')) {
				is_checked = true;
		} else {
				is_checked = false;
		}
		
		
		var i = jQuery("form#form_user_level_role ." + id_checkall).length;
		for(var o = 0;o < i;o++)
		{
				jQuery("form#form_user_level_role").find("." + id_checkall).eq(o).prop("checked",is_checked);
		}
});

jQuery(document).on("change","form#form_user_level_role .checkall_methods",function(){		
		var is_checked = false;
		if (jQuery(this).is(':checked')) {
				is_checked = true;
		} else {
				is_checked = false;
		}
		
		
		var i = jQuery("form#form_user_level_role .checkalls").length;
		for(var o = 0;o < i;o++)
		{
				jQuery("form#form_user_level_role").find(".checkalls").eq(o).prop("checked",is_checked);
		}
});


function sort_menu()
{
  var inp_menu = jQuery("#sortable li");
  if(jQuery(inp_menu).length > 0)
  {
    for(var i = 0; i < jQuery(inp_menu).length; i++)
    {
      var parent_id = jQuery(inp_menu).eq(i).parents("li");
      if(parent_id.length > 0)
      {
        parent_id = jQuery(inp_menu).eq(i).parents("li").attr("id");
        parent_id = parent_id.replace("the_menu-","");
      }else{
        parent_id = 0;
      }
      jQuery(inp_menu).eq(i).find("input").val(parent_id);
    }
    
    jQuery.ajax({
      type: "post",
      url: jQuery("form#sort_menu").attr("action"),
      data: jQuery("form#sort_menu").serialize(),
      success: function(msg){
        //jQuery("#output").html(msg);
      }
    });
  }
}


jQuery("span.showhide").on("click",function(){
  if(jQuery(this).hasClass("ui-icon-triangle-1-n"))
  {
    jQuery(this).removeClass("ui-icon-triangle-1-n");
    jQuery(this).addClass("ui-icon-triangle-1-s");
    jQuery(this).parent().children("ul").show();
  }else if(jQuery(this).hasClass("ui-icon-triangle-1-s")){
    jQuery(this).removeClass("ui-icon-triangle-1-s");
    jQuery(this).addClass("ui-icon-triangle-1-n");
    if(jQuery(this).parent().children("ul").find("li").length > 0)
    {
      //jQuery(this).parent().children("ul").hide();
    }
  }
});

jQuery(document).on("do_sortable","#sortable ul",function(){
  jQuery(this).sortable({
    connectWith: "#sortable ul",
    placeholder: "ui-state-highlight",
    tolerance: "intersect",
    items:  '> li',
    axis: 'y',
    revert: true,
    opacity: 0.4,
    update: function(event, ui) 
    {
      //var index = ui.placeholder.index();
      //alert(ui.item.html());
      sort_menu();
    }
  }).disableSelection();
});



jQuery(document).ready(function(){
  jQuery("#sortable ul").trigger("do_sortable");
  /*
  if(jQuery(".col2").length == 0 || !jQuery(".col2").html())
  {
    jQuery(".col1").width("100%");
  }
  */
  jQuery("span.showhide").each(function(k,v){
    if(jQuery(v).parent().children("ul").find("li").length > 1)
    {
      //jQuery(v).removeClass("ui-icon-triangle-1-n");
      //jQuery(v).removeClass("ui-icon-triangle-1-s");
      jQuery(v).addClass("ui-icon-triangle-1-s");
      //jQuery(v).parent().children("ul").hide();
    }else{
      jQuery(v).removeClass("ui-icon-triangle-1-n");
      jQuery(v).removeClass("ui-icon-triangle-1-s");
      jQuery(v).addClass("ui-icon-blank");
    }
  });
});
