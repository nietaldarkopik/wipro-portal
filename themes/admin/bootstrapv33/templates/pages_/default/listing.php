<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/topmenu');?>
<?php $this->load->view('components/container-top');?>
<?php $this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
  
  if(!empty($is_modal)){
?>
      <?php
      if(isset($this->page_title) and !empty($this->page_title))
      {
      ?>
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $this->page_title;?></h4>
      </div>
      <?php
      }
      ?>
      <div class="modal-body paddingbottom0 paddingtop0">
<?php  
  }else{
?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
<?php
}
?>
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
            echo $response;
            ?>
            <div class="col-lg-12">
              <ul class="nav nav-tabs margintop-1 marginleft-16">
                <li class="active"><a href="#listing<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-list"></span></a></li>
                <li><a href="#add<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-plus"></span></a></li>
                <li><a href="#download<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-download-alt"></span></a></li>
                <li><a href="#print<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-print"></span></a></li>
                <li><a href="#config<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon-wrench"></span></a></li>
                <li><a href="#search<?php echo $is_modal;?>" data-toggle="tab" class="box"><span class="glyphicon glyphicon glyphicon-search"></span></a></li>
              </ul>
              <?php
                #echo $this->data->show_panel_allowed("","admin","",array("add"));
              ?>
              <div id='content' class="tab-content">
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
                  <br/>
                  <div class="row">
                    <div class="col-lg-8">
                      <?php
                        $paging_config = (isset($paging_config))?$paging_config:array('uri_segment' => 4);
                        $this->data->init_pagination($paging_config);
                        $pagination =  $this->data->create_pagination($paging_config);
                        $pagination =  str_replace('<a ','<a data-target-ajax=".data_target'.$is_modal.'" ',$pagination);
                        if(!empty($is_modal))
                          $pagination =  str_replace('<a ','<a class="is_modal" ',$pagination);
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array("delete")));
                      ?>
                    </div>
                  </div>
                  <div class="table-responsive">
                    <?php
                      echo $this->data->create_listing();
                    ?>
                  </div>
                  <div class="row">
                    <div class="col-lg-8">
                      <?php
                        echo $pagination;
                      ?>
                    </div>
                    <div class="col-lg-4">
                      <?php
                        echo $this->data->show_bulk_allowed("","admin","",array_merge(((isset($this->init['bulk_options']) and is_array($this->init['bulk_options']))?$this->init['bulk_options']:array()),array("delete")));
                      ?>
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="add<?php echo $is_modal;?>">
                  <?php 
                  echo $this->data->create_form($this->init);
                  ?>
                </div>
                <div class="tab-pane" id="download<?php echo $is_modal;?>">download</div>
                <div class="tab-pane" id="print<?php echo $is_modal;?>">print</div>
                <div class="tab-pane" id="config<?php echo $is_modal;?>">                  
                  <?php
                    echo $this->data->create_form_filter();
                  ?>
                </div>
                <div class="tab-pane" id="search<?php echo $is_modal;?>">                  
                  <?php
                    $init = $this->init;
                    echo $this->data->create_form_filter($init);
                  ?>
                </div>
              </div>
            </div>
        </div>
      
<?php
  if(!empty($is_modal)){
?>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
<?php  
  }else{
?>
          </div>
        </div>
      </div>
<?php
}
?>
<!--
			<?php
				#echo $response;
				#echo $this->data->create_form_filter();
			?>
-->


<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/container-bottom');?>
<?php $this->load->view('footer');?>

