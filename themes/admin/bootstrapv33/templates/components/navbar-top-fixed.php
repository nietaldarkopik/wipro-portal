<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
  $is_ajax = $this->input->post('is_ajax');
  if($is_ajax == 1){}else{
		$is_login = $this->user_access->is_login();
    if($is_login)
    {
?>
					<!-- Header Navbar -->
					<nav class="navbar navbar-static-top" role="navigation">
						<a href="#" class="logo">
							<!-- Add the class icon to your logo image or logo icon to add the margining
							<img class="icon" src="img/logo.png" /> -->
							<h1 class="pull-left">Portal Administrator</h1>
							<h2 class="pull-left">WIPRO</h2>
						</a>
						<!-- Sidebar toggle button-->
						<button class="navbar-btn btn btn-lg navbar-toggle visible-lg visible-md visible-sm visible-xs sidebar-toggle" data-toggle="offcanvas" role="button">
							<span class="glyphicon glyphicon-transfer"></span>
						</button>
						<button type="button" class="navbar-toggle sidebar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
                    </nav>
					<!-- Content Header (Page header) -->
					<nav class="navbar navbar-static-top navbar-pages">
						<div class="container-fluid navbar-pages-container">
							<div class="row">
								<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
								<?php
								$this->load->view('components/topmenu');
								?>
								</div>
							</div>
						</div>
					</nav>
				</div>
			</div>
		</header>
        
        <div class="wrapper row-offcanvas row-offcanvas-left">
			<?php $this->load->view("components/leftbar");?>
				<!--
                <section class="content-header">
                    <h1>Dashboard <small>Control panel</small></h1>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Dashboard</li>
                    </ol>
                </section>
				-->
				<!-- Modal -->
				<div class="modal fade" id="popupoverlay" tabindex="-1" role="dialog" aria-labelledby="overlaycontainer" aria-hidden="true">
					<div class="modal-dialog modal-lg">
					  <div class="modal-content ajax_container" id="data_target-modal">
					  </div>
					</div>
				</div>
				
                <section class="content">

                    <!-- Small boxes (Stat box) -->
                    <div class="row">
<?php 
  }
} ?>
