<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$user_id = $this->user_access->current_user_id;
	#echo $this->user_access->get_custom_menus_allowed_structured();
	
	$curr_menu_group = $this->session->userdata("menu_group");
	$curr_menu_group = (empty($curr_menu_group))?"general":$curr_menu_group;
	
	$parent_menu_allowed = $this->user_access->get_custom_menus_allowed($user_id," AND (`type` = 'home') AND parent_menu = 0 AND user_custom_menus.status = 'active'");

	$the_top_menu = "";
	// Get menu Level 0
	if(is_array($parent_menu_allowed) and count($parent_menu_allowed) > 0)
	{
		$the_top_menu .= '<ul class="nav navbar-nav">
							<li>
								<a href="'.base_url().'">
									<span class="glyphicon glyphicon-home"></span>
								</a>
							</li>';
		foreach($parent_menu_allowed as $index => $mn2)
		{
          $path = $this->uri->segment(1);
          $controller = $this->uri->segment(2);
          $function = $this->uri->segment(3);
          $active = ($path == $mn2['path'] and $controller == $mn2['controller'] and $function == $mn2['function'])?' topmenu-list active ':'';
		  $sub_menu_allowed3 = $this->user_access->get_custom_menus_allowed($user_id," AND parent_menu = '" . $mn2['user_menu_id'] . "' AND user_custom_menus.status = 'active'");
		  
		  if($mn2['type'] != $curr_menu_group or (!is_array($sub_menu_allowed3) or count($sub_menu_allowed3) == 0))
		  {
            if($mn2['controller'] == "#" and (is_array($sub_menu_allowed3) and count($sub_menu_allowed3) == 0))
            {}else{
              $the_top_menu .= '<li class="'.((is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0)?' dropdown ':'') . $active . '">
                                  <a href="'.base_url() . $mn2['path'] . '/'  . $mn2['controller'] . '/' . $mn2['function'] . '" '.((is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0)?' class="dropdown-toggle" data-toggle="dropdown"':'').'>
                                    '.((!empty($mn2['attributes']))?'<span class="'.$mn2['attributes'].'"></span> ':'').$mn2['menu_title'].'
                                  </a>';	
            }
		  }
		  
		  if(is_array($sub_menu_allowed3) and count($sub_menu_allowed3) > 0)
		  {
        $the_top_menu .= '<ul class="dropdown-menu">';
        foreach($sub_menu_allowed3 as $index3 => $mn3)
        {
          $active2 = ($path == $mn3['path'] and $controller == $mn3['controller'] and $function == $mn3['function'])?'topmenu-list active ':'';
          $the_top_menu .= '<li class="'.$active2.'"><a href="'.base_url() . $mn3['path'] . '/' . $mn3['controller'] . '/' . $mn3['function'] . '">'.((!empty($mn3['attributes']))?'<span class="'.$mn3['attributes'].'"></span> ':'').$mn3['menu_title'].'</a>';	
        }
        $the_top_menu .= '</ul>';
		  }
		}
		$the_top_menu .= '
						</ul>';
	}
	
	echo $the_top_menu;
