<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?php echo current_admin_theme_url();?>dist/ico/favicon.ico">

    <title>WIPRO Portal Administrator</title>
    <?php echo $this->assets->print_css("head");?>
    <script type="text/javascript">
       var base_url = "<?php echo base_url();?>";
       var current_url = "<?php echo current_url();?>";
       var current_theme_url = "<?php echo current_admin_theme_url();?>";
       var theme_name = "<?php echo CURRENT_THEME;?>";
    </script>
    <?php echo $this->assets->print_js("head");?>
    <style type="text/css">
      <?php echo $this->assets->print_css_inline("head");?>
    </style>
    <script type="text/javascript">
      <?php echo $this->assets->print_js_inline("head");?>
    </script>
    
		<link rel="shortcut icon" href="<?php echo base_url('icons');?>/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('icons');?>/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('icons');?>/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('icons');?>/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('icons');?>/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('icons');?>/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('icons');?>/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('icons');?>/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('icons');?>/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('icons');?>/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo base_url('icons');?>/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?php echo base_url('icons');?>/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo base_url('icons');?>/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo base_url('icons');?>/android-chrome-192x192.png" sizes="192x192">
		<meta name="msapplication-square70x70logo" content="<?php echo base_url('icons');?>/smalltile.png" />
		<meta name="msapplication-square150x150logo" content="<?php echo base_url('icons');?>/mediumtile.png" />
		<meta name="msapplication-wide310x150logo" content="<?php echo base_url('icons');?>/widetile.png" />
		<meta name="msapplication-square310x310logo" content="<?php echo base_url('icons');?>/largetile.png" />
    
  </head>

  <body class="login-page">
	
		<!-- HEADER START -->
		<div class="site-header">&nbsp;</div>
		<!-- HEADER END -->
		
		<!-- CONTENT START -->
		<div class="site-content">
			<div class="container">
        
        <!-- START : Login Page -->
        <div class="row">
        
          <div class="col-md-4 col-md-push-4 member-login-page">
            <p class="text-center"><img class="img-responsive" title="Web Instant Pro Logo" alt="WIPRO Logo" src="<?php echo current_admin_theme_url();?>assets/images/webinstantpro-logo.gif" /></p>
            <div class="box box-solid bg-navy">
            
              <div class="box-header">
								<div class="box-title" style="text-align:center;width:100%;"><h3>Login Web Instant Pro Portal</h3></div>
								</div>
											
									<div class="box-body">
										<form role="form" action="<?php echo base_url('admin/dashboard/do_login');?>" method="post">
										
											<div class="form-group">
												<label for="InputEmail1">Username</label>
												<input type="text" name="user_name" class="form-control" id="InputEmail1" placeholder="Username"  required autofocus />
											</div>
											
											<div class="form-group">
												<label for="InputPassword">Password</label>
												<input type="password" name="password" class="form-control" id="InputPassword" placeholder="Password"  required />
											</div>
											
											<div class="form-group">
												<button type="submit" class="form-control btn btn-primary">Login <i class="glyphicon glyphicon-circle-arrow-right"></i></button>
											</div>
										</form>				
									</div>
              
							</div>
          
							<?php if(!empty($error)){ ?>
								<div class="alert alert-danger text-center">
										<strong>Warning!</strong> <?php echo $error;?>
								</div>
							<?php } ?>
							
            <p class="text-center"><a href="<?php echo site_url('customer/dashboard/forgot'); ?>">Lupa Password</a>  |  <a href="<?php echo site_url('customer/dashboard/register'); ?>">Daftar Baru</a></p>
            
          </div>
        </div>
        <!-- END   : Login Page -->
            
			</div>
		</div>
		<!-- CONTENT END -->
		
		<!-- FOOTER START -->
		<div class="site-footer">
			<div class="container">
			
				<div class="row">
					<div class="col-md-12">
						<p class="copyrights">Copyrights &copy; 2015 Portal Web Administrator (WIPRO) - All Rights Reserved</p>
					</div>
				</div>
				
			</div>
		</div>
		<!-- FOOTER END -->
		
    <!-- JAVASCRIPTS START -->
		
    <!-- CORE PLUGINS START -->
    <script type="text/javascript" src="<?php echo current_admin_theme_url();?>assets/plugins/jquery-1.10.1.min.js"></script>
    <script type="text/javascript" src="<?php echo current_admin_theme_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo current_admin_theme_url();?>assets/plugins/hover-dropdown.js"></script>
    <script type="text/javascript" src="<?php echo current_admin_theme_url();?>assets/plugins/jquery.migrate.min.js"></script>
		
    <script type="text/javascript" src="<?php echo current_admin_theme_url();?>assets/plugins/modernizr.js"></script>
    <script type="text/javascript" src="<?php echo current_admin_theme_url();?>assets/plugins/jquery.easing.min.js"></script>
    <script type="text/javascript" src="<?php echo current_admin_theme_url();?>assets/plugins/jquery.mousewheel.js"></script>
		
    <script type="text/javascript" src="<?php echo current_admin_theme_url();?>assets/scripts/holder.js"></script>
    <script type="text/javascript" src="<?php echo current_admin_theme_url();?>assets/plugins/back-to-top.js"></script>
		
    <!--[if lt IE 9]>
    <script src="<?php echo current_admin_theme_url();?>assets/plugins/respond.min.js"></script>  
    <![endif]-->
    <!-- CORE PLUGINS END -->
		
    <!-- JAVASCRIPTS END -->
		
  </body>
</html>
