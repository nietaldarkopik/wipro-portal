<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;
?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><?php echo $page_title;?></h3>
      </div>
      <div class="panel-body paddingtop0">
          <div class="row data_target<?php echo $is_modal;?>">
            <?php 
              echo (isset($response))?$response:'';
            ?>
            <div class="col-lg-12">

              <?php
                #echo $this->data->show_panel_allowed("","admin","",array("add"));
              ?>
              <div id='content' class="tab-content">
                <div class="tab-pane active" id="listing<?php echo $is_modal;?>">
				  <br/>
                  <div class="table-responsive">
					<?php 
					  echo $this->data->create_form_filter($this->init);
					?>
                    <div id="sortable" class="fclear">
                      <form method="post" action="<?php echo current_url();?>" form="sort_menu" id="sort_menu" class="ajax" data-target-ajax=".ajax_container">
                        <?php
                          echo $the_menus;
                        ?>
                        <input type="hidden" name="do_save_menu" value="do_save_menu"/>
                        <input type="hidden" name="is_ajax" value="1"/>
                      </form>
                    </div>
                    <div id="output">
                    
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="add<?php echo $is_modal;?>">
                </div>
              </div>
            
            </div>
            <script type="text/javascript">
              jQuery(document).ready(function(){
                jQuery("#sortable ul").trigger("do_sortable");
              });
            </script>
		</div>
          </div>
        </div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>
