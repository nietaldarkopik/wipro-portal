<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php $this->load->view('header');?>
<?php $this->load->view('components/top');?>
<?php $this->load->view('components/navbar-top-fixed');?>
<?php #$this->load->view('components/container-top');?>
<?php #$this->load->view('components/container-main-open');?>

<?php
  $is_ajax = $this->input->post('is_ajax');
  $is_modal = $this->input->post('is_modal');
  $pagination_data_target = "#data_target-".$is_ajax;

?>
  <div class="ajax_container content-container col-sm-12 col-md-12">
	<div class="panel panel-default">
	  <div class="panel-heading">
		<h3 class="panel-title"><?php echo $page_title;?></h3>
	  </div>
	  <div class="panel-body">
		<?php echo $response; ?>
		<form method="post" action="<?php echo current_admin_url();?>" id="form_user_level_role">
			<label for="user_level_id">User Level : </label>
			<select id="user_level_id" name="user_level_id">
				<option value="">
					--- option ---
				</option>
				<?php
				if(isset($user_levels) and is_array($user_levels) and count($user_levels) > 0)
				{
					foreach($user_levels as $index => $user_level)
					{
				?>
					<option value="<?php echo $user_level['user_level_id'];?>" <?php echo ($user_level_id == $user_level['user_level_id'])?'selected="selected"':"";?>><?php echo $user_level['user_level_name'];?></option>
				<?php
					}
				}
				?>
			</select>						
			<?php				
			if(is_array($controllers) and count($controllers) > 0)
			{
				?>
			<table width="100%">
				<thead>
					<tr>
						<th>No</th>
						<th>Halaman</th>
						<th><input type="checkbox" name="checkalls" value="1" class="checkall_methods"/> Pilih Semua</th>
						<th>Akses</th>
					</tr>
				</thead>				
				<tbody>
				<?php
					foreach($controllers as $index => $controller)
					{
				?>
						<tr>
							<td><?php echo $index+1;?></td>
							<td><?php echo trim(implode("/",array(trim(strtolower($controller['path']),"/"),strtolower($controller['class_name']))),"/");?></td>
              <?php 
                  $controller['path'] = ($controller['path'] == '/')?'user':$controller['path'];
              ?>
							<td align="center">
									<input type="checkbox" name="checkall[<?php echo trim(strtolower($controller['path']),"/");?>_<?php echo strtolower($controller['class_name']);?>]" value="1" class="checkall_method checkalls" id="checkall_<?php echo trim(strtolower($controller['path']),"/").'_'.strtolower($controller['class_name']);?>"/>
								</td>
							<td>
								<?php
									$methods = (isset($controller['methods']))?$controller['methods']:array();
									if(is_array($methods) and count($methods) > 0)
									{
										foreach($methods as $index => $method)
										{
											$checked = ((isset($user_access[trim(strtolower($controller['path']),"/")][strtolower($controller['class_name'])][$method]) and $user_access[trim(strtolower($controller['path']),"/")][strtolower($controller['class_name'])][$method] == $method)?'checked="checked"':'');
											echo '<div class="fleft" style="margin-right:10px;">
															<input type="checkbox" value="'.$method.'" name="method['.trim(strtolower($controller['path']),"/").']['.strtolower($controller['class_name']).']['.$method.']"  class="fleft checkalls checkall_' . trim(strtolower($controller['path']),"/") . '_' . trim(strtolower($controller['class_name']),"/") . '" style="margin:3px;" ' . $checked . '/>'. 
															'<span class="fleft">' . $method . '</span>
														</div>';
										}
									}
								?>
							</td>
						</tr>
				<?php
					}
				?>
				</tbody>
			</table>
			<div align="center">
				<input type="hidden" value="<?php echo $user_level_id;?>" name="user_level_id_save"> 
				<input type="hidden" value="do_save" name="do_process"> 
				<input type="submit" value="Save">
			</div>
			<?php
			}
			?>
			<input type="hidden" value="<?php echo $this->user_access->get_nonce();?>" name="nonce"> 
			<input type="hidden" value="#main_content .col1" name="ajax_target"> 
			<!-- <input type="hidden" value="1" name="is_ajax">  -->
		</form>	
		</div>
	  </div>
  </div>
<?php $this->load->view('components/container-main-close');?>
<?php $this->load->view('components/bottom');?>
<?php $this->load->view('footer');?>

