<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<?php
	$this->load->view('header');
	$this->load->view('components/navbar-top-fixed');
?>
  	<div class="container-fluid padding0">
      <div class="row">
        <div class="col-sm-12">
        <ul class="nav nav-pills">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="#">Profile</a></li>
          <li><a href="#">Messages</a></li>
          <li><a href="#">Messages</a></li>
          <li><a href="#">Messages</a></li>
          <li><a href="#">Messages</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Messages <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
            </ul>
          </li>
          <li><a href="#">Messages</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Messages <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li class="active"><a href="#">Home</a></li>
              <li><a href="#">Profile</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
              <li><a href="#">Messages</a></li>
            </ul>
          </li>
        </ul>
      </div>	
    </div>	
  	
	  <div class="row main">
      <div class="col-sm-12 padding0">
        <div class="panel panel-default">
          <div class="panel-heading">
          <h3 class="panel-title">Panel title</h3>
          </div>
          <div class="panel-body">
            
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>Head 1</th>
                <th>Head 1</th>
                <th>Head 1</th>
                <th>Head 1</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>Head 1</td>
                <td>Head 1</td>
                <td>Head 1</td>
                <td>Head 1</td>
              </tr>
              <tr>
                <td>Head 1</td>
                <td>Head 1</td>
                <td>Head 1</td>
                <td>Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1</td>
              </tr>
              </tbody>
            </table>
          </div>
        
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
          <h3 class="panel-title">Panel title</h3>
          </div>
          <div class="panel-body">
            
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
              <tr>
                <th>Head 1</th>
                <th>Head 1</th>
                <th>Head 1</th>
                <th>Head 1</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>Head 1</td>
                <td>Head 1</td>
                <td>Head 1</td>
                <td>Head 1</td>
              </tr>
              <tr>
                <td>Head 1</td>
                <td>Head 1</td>
                <td>Head 1</td>
                <td>Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1Head 1</td>
              </tr>
              </tbody>
            </table>
          </div>
        
          </div>
        </div>
        
        <div class="panel panel-default">
          <div class="panel-heading">
          <h3 class="panel-title">Panel title</h3>
          </div>
          <div class="panel-body">
          <div class="input-group">
            <span class="input-group-addon">@</span>
            <input type="text" class="form-control" placeholder="Username">
          </div>

          <div class="input-group">
            <input type="text" class="form-control">
            <span class="input-group-addon">.00</span>
          </div>

          <div class="input-group">
            <span class="input-group-addon">$</span>
            <input type="text" class="form-control">
            <span class="input-group-addon">.00</span>
          </div>
          <div class="input-group input-group-lg">
            <span class="input-group-addon">@</span>
            <input type="text" class="form-control" placeholder="Username">
          </div>

          <div class="input-group">
            <span class="input-group-addon">@</span>
            <input type="text" class="form-control" placeholder="Username">
          </div>

          <div class="input-group input-group-sm">
            <span class="input-group-addon">@</span>
            <input type="text" class="form-control" placeholder="Username">
          </div>
          <div class="row">
            <div class="col-lg-6">
            <div class="input-group">
              <span class="input-group-addon">
              <input type="checkbox">
              </span>
              <input type="text" class="form-control">
            </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
            <div class="input-group">
              <span class="input-group-addon">
              <input type="radio">
              </span>
              <input type="text" class="form-control">
            </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <div class="row">
            <div class="col-lg-6">
            <div class="input-group">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
              </span>
              <input type="text" class="form-control">
            </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
            <div class="input-group">
              <input type="text" class="form-control">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
              </span>
            </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <div class="row">
            <div class="col-lg-6">
            <div class="input-group">
              <div class="input-group-btn">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
              </div><!-- /btn-group -->
              <input type="text" class="form-control">
            </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
            <div class="input-group">
              <input type="text" class="form-control">
              <div class="input-group-btn">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu pull-right">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
              </div><!-- /btn-group -->
            </div><!-- /input-group -->
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <div class="input-group">
            <div class="input-group-btn">
            <!-- Button and dropdown menu -->
            </div>
            <input type="text" class="form-control">
          </div>

          <div class="input-group">
            <input type="text" class="form-control">
            <div class="input-group-btn">
            <!-- Button and dropdown menu -->
            </div>
          </div>
          </div>
        </div>
        
        
        <div class="panel panel-default">
          <div class="panel-heading">
          <h3 class="panel-title">Panel title</h3>
          </div>
          <div class="panel-body">
            
          <div class="input-group input-group-sm">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span>
            <div class="input-group-btn">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
            </ul>
            </div><!-- /btn-group -->
            <span class="input-group-addon">
            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
            </span>
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-search"></span>
            </span>
            <input type="text" class="form-control" placeholder="Username">
            <span class="input-group-addon">
            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
            </span>
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-search"></span>
            </span>				  
            <div class="input-group-btn">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
            </ul>
            </div><!-- /btn-group -->
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
          <br/>
          <div class="row">
            <div class="col-lg-6">
            <div class="input-group input-group-sm">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
              </span>
              <div class="input-group-btn">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
              </div><!-- /btn-group -->
              <span class="input-group-addon">
              <span class="glyphicon glyphicon glyphicon-asterisk"></span>
              </span>
              <span class="input-group-addon">
              <span class="glyphicon glyphicon-search"></span>
              </span>
              <input type="text" class="form-control" placeholder="Username">
            </div>
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
            <div class="input-group input-group-sm">
              <input type="text" class="form-control" placeholder="Username">
              <span class="input-group-addon">
              <span class="glyphicon glyphicon glyphicon-asterisk"></span>
              </span>
              <span class="input-group-addon">
              <span class="glyphicon glyphicon-search"></span>
              </span>				  
              <div class="input-group-btn">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
              </div><!-- /btn-group -->
              <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
              </span>
            </div>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <br/>
          
          
          <div class="input-group">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span>
            <div class="input-group-btn">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
            </ul>
            </div><!-- /btn-group -->
            <span class="input-group-addon">
            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
            </span>
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-search"></span>
            </span>
            <input type="text" class="form-control" placeholder="Username">
            <span class="input-group-addon">
            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
            </span>
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-search"></span>
            </span>				  
            <div class="input-group-btn">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
            </ul>
            </div><!-- /btn-group -->
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
          <br/>
          <div class="row">
            <div class="col-lg-6">
            <div class="input-group">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
              </span>
              <div class="input-group-btn">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
              </div><!-- /btn-group -->
              <span class="input-group-addon">
              <span class="glyphicon glyphicon glyphicon-asterisk"></span>
              </span>
              <span class="input-group-addon">
              <span class="glyphicon glyphicon-search"></span>
              </span>
              <input type="text" class="form-control" placeholder="Username">
            </div>
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Username">
              <span class="input-group-addon">
              <span class="glyphicon glyphicon glyphicon-asterisk"></span>
              </span>
              <span class="input-group-addon">
              <span class="glyphicon glyphicon-search"></span>
              </span>				  
              <div class="input-group-btn">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
              </div><!-- /btn-group -->
              <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
              </span>
            </div>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <br/>
          
          
          
          <div class="input-group input-group-lg">
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span>
            <div class="input-group-btn">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
            </ul>
            </div><!-- /btn-group -->
            <span class="input-group-addon">
            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
            </span>
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-search"></span>
            </span>
            <input type="text" class="form-control" placeholder="Username">
            <span class="input-group-addon">
            <span class="glyphicon glyphicon glyphicon-asterisk"></span>
            </span>
            <span class="input-group-addon">
            <span class="glyphicon glyphicon-search"></span>
            </span>				  
            <div class="input-group-btn">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
            </ul>
            </div><!-- /btn-group -->
            <span class="input-group-btn">
            <button class="btn btn-default" type="button">Go!</button>
            </span>
          </div>
          <br/>
          <div class="row">
            <div class="col-lg-6">
            <div class="input-group input-group-lg">
              <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
              </span>
              <div class="input-group-btn">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
              </div><!-- /btn-group -->
              <span class="input-group-addon">
              <span class="glyphicon glyphicon glyphicon-asterisk"></span>
              </span>
              <span class="input-group-addon">
              <span class="glyphicon glyphicon-search"></span>
              </span>
              <input type="text" class="form-control" placeholder="Username">
            </div>
            </div><!-- /.col-lg-6 -->
            <div class="col-lg-6">
            <div class="input-group input-group-lg">
              <input type="text" class="form-control" placeholder="Username">
              <span class="input-group-addon">
              <span class="glyphicon glyphicon glyphicon-asterisk"></span>
              </span>
              <span class="input-group-addon">
              <span class="glyphicon glyphicon-search"></span>
              </span>				  
              <div class="input-group-btn">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
              </div><!-- /btn-group -->
              <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
              </span>
            </div>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <br/>
          
          
          </div>
        </div>
        
        
        <div class="panel panel-default">
          <div class="panel-heading">
          <h3 class="panel-title">Panel title</h3>
          </div>
          <div class="panel-body">
            
          
          <div class="row">
            <div class="col-lg-3">
            <div class="input-group">
              <label for="user_name">Isi Textbox Isi Textbox</label>
            </div>
            </div>
            <div class="col-lg-9">
            <div class="input-group">
              <input type="text" class="form-control" placeholder="Username">
              <span class="input-group-addon">
              <span class="glyphicon glyphicon glyphicon-asterisk"></span>
              </span>
              <span class="input-group-addon">
              <span class="glyphicon glyphicon-search"></span>
              </span>				  
              <div class="input-group-btn">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Action <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li><a href="#">Separated link</a></li>
              </ul>
              </div><!-- /btn-group -->
              <span class="input-group-btn">
              <button class="btn btn-default" type="button">Go!</button>
              </span>
            </div>
            </div><!-- /.col-lg-6 -->
          </div><!-- /.row -->
          <br/>
          <div class="row">
            <div class="col-lg-3">
              <div class="input-group">
                <label for="user_name">Pilih Checkbox</label>
              </div>
            </div>
            <div class="col-lg-9">
              <div class="input-group">
                <span class="input-group-addon">
                <input type="checkbox">
                </span>
                <span class="form-control">Jawsd fsdf sdf saf sadfsdaf ds fsadf aban 1</span>
              </div>
              <br/>
              <div class="input-group">
                <span class="input-group-addon">
                <input type="checkbox">
                </span>
                <span class="form-control">Jawasdf sd fdsf asd fasdfs aban 2</span>
              </div>
              <br/>
              <div class="input-group">
                <span class="input-group-addon">
                <input type="checkbox">
                </span>
                <span class="form-control">Jawaban 1</span>
              </div>
              <br/>
              <div class="input-group">
                <span class="input-group-addon">
                <input type="checkbox">
                </span>
                <span class="form-control">Jawaban 2</span>
              </div>
              <br/>
            </div>
            </div>
          
          <div class="row">
            <div class="col-lg-3">
            <div class="input-group">
              <label for="user_name">Pilih Radiobutton</label>
            </div>
            </div>
            <div class="col-lg-9">
              <div class="input-group">
                <span class="input-group-addon">
                <input type="radio">
                </span>
                <span class="form-control">Jawaban 1</span>
              </div>
              <br/>
              <div class="input-group">
                <span class="input-group-addon">
                <input type="radio">
                </span>
                <span class="form-control">Jawaban 2</span>
              </div>
              <br/>
            </div>
          </div>
          
          
          <div class="row">
            <div class="col-lg-3">
            <div class="input-group">
              <label for="user_name">Selectbox</label>
            </div>
            </div>
            <div class="col-lg-9">
              <div class="input-group">
                <span class="input-group-addon">
                &nbsp;
                </span>
                <span class="form-control padding0">
                <select name="">
                  <option value="">Options</option>
                  <option value="">Options</option>
                  <option value="">Options</option>
                </select>
                </span>
              </div>
              <br/>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-3">
            <div class="input-group">
              <label for="user_name">Textarea</label>
            </div>
            </div>
            <div class="col-lg-9">
              <div class="input-group input-group-auto">
                <span class="input-group-addon">
                &nbsp;
                </span>
                <span class="form-control padding0">
                <textarea></textarea>
                </span>
              </div>
              <br/>
            </div>
          </div>
          
          <div class="row">
            <div class="col-lg-3">
            </div>
            <div class="col-lg-3">
            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
            </div>
            <div class="col-lg-3">
            </div>
            </div>
          </div>
        </div>
        
        <ul class="pagination pull-right">
          <li><a href="#">&laquo;</a></li>
          <li><a href="#">1</a></li>
          <li><a href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
          <li><a href="#">&raquo;</a></li>
        </ul>
      </div>
      
    </div>
    
	<div class="container">
		<div class="row">
		  <div class="col-md-8"> <p class="text-muted">Place sticky footer content here.</p></div>
		  <div class="col-md-4">
			<ul class="nav nav-pills small pull-right">
			  <li><a href="#">Home</a></li>
			  <li><a href="#">Profile</a></li>
			  <li><a href="#">Messages</a></li>
			</ul>
		  </div>
		</div>
	</div>	
<?php
	$this->load->view('footer');
?>
