<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>WIPRO Portal Administrator</title>
    <?php echo $this->assets->print_css("head");?>
    <script type="text/javascript">
       var base_url = "<?php echo base_url();?>";
       var current_url = "<?php echo current_url();?>";
       var theme_name = "<?php echo CURRENT_THEME;?>";
    </script>
    <?php echo $this->assets->print_js("head");?>
    <?php echo $this->assets->print_css_inline("head");?>
    <?php echo $this->assets->print_js_inline("head");?>
    
		<link rel="shortcut icon" href="<?php echo base_url('icons');?>/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url('icons');?>/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url('icons');?>/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url('icons');?>/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('icons');?>/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url('icons');?>/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('icons');?>/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url('icons');?>/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('icons');?>/apple-touch-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url('icons');?>/apple-touch-icon-180x180.png">
		<link rel="icon" type="image/png" href="<?php echo base_url('icons');?>/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="<?php echo base_url('icons');?>/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="<?php echo base_url('icons');?>/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="<?php echo base_url('icons');?>/android-chrome-192x192.png" sizes="192x192">
		<meta name="msapplication-square70x70logo" content="<?php echo base_url('icons');?>/smalltile.png" />
		<meta name="msapplication-square150x150logo" content="<?php echo base_url('icons');?>/mediumtile.png" />
		<meta name="msapplication-wide310x150logo" content="<?php echo base_url('icons');?>/widetile.png" />
		<meta name="msapplication-square310x310logo" content="<?php echo base_url('icons');?>/largetile.png" />
    
  </head>

  <body>

    <div class="container">

      <form class="form-signin" role="form" action="<?php echo base_url();?>dashboard/do_login/" method="post">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="text" class="form-control" placeholder="Username" name="user_name" required autofocus>
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Remember me
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
